//****************************************************************************//
// author: @yusufakdas													      //
// student id: 151044043													  //
// last modified date: 08.10.2017 - 11:25 p.m.		 					      //
// site: github.com/yusufakdas						 						  //
//****************************************************************************//

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

// Oyun tahtasında yer alacak unsurlar
enum Cell { X, O, WINX, WINO, EMPTY };

// Oyunda olabilecek durumlar
enum State { ONE, TWO, COMPUTER, DEUCE, NONE, LOADCMP };

// Oyun tahtasının maksimum boyutu
const int MAX_SIZE = 20;

// Oyunu kaydetmek ve yüklemek için sabit değişkenler 
const string SAVE = "SAVE";
const string LOAD = "LOAD";

// Oyun tahtası
struct GameBoard {
	Cell grid[MAX_SIZE][MAX_SIZE];
	int rowCount[MAX_SIZE];
	int size;
	char gameType;
};

// Koordinatlar
struct Coord {
	// Coord structını ilklendirir.
	Coord() : row(0), col(0) { } 
	int row;
	int col;
};

// Oyun tahtasını ekrana basar.
void printGameBoard(const GameBoard& board);

// Oyun tahtasını tüm hücrelerini EMPTY yapar.
void initGameBoard(GameBoard& board);

// Player vs. Player modu
void playAgainstUser(GameBoard& board, State& end);

// Beraberlik durumunu kontrol eder.
bool checkDeuce(const GameBoard& board);

// Bufferı temizler.
void emptyBuffer(void);

// Oyun tahtasının boyutunu ve oyun tipini kullanıcıdan alır.
char getSizeAndGameType(GameBoard& board);

// Program başlangıcında oyunla ilgili bilgiyi ekrana basar.
void instruct(void);

// Yatay kontrol
bool checkHorizontal(const GameBoard& board, int row, Cell checkerType,
					 Coord win[]);

// Dikey kontrol
bool checkVertical(const GameBoard& board, int col, Cell checkerType,
				   Coord win[]);

// Sağ çapraz kontrol
bool checkRightDiagonal(const GameBoard& board, const Coord& coordinate,
						Cell checkerType, Coord win[]);

// Sol çapraz kontrol
bool checkLeftDiagonal(const GameBoard& board, const Coord& coordinate,
					   Cell checkerType, Coord win[]);

// Oyun sonucunu ekrana basar.
void showResult(State end);

// Checkerları küçük yapar
void changeCheckers(GameBoard& board, Coord win[], Cell checkerType);

// Computer vs. Player modu 
//(***** Default parameterler burada kullanıldı! *****)
void playAgainstComputer(GameBoard& board, State& end, 
/* Default parameterler */ Coord curr = Coord(), Coord comp = Coord());

// Kullanıcının hamlelerini kontrol eder
bool controlUserMoves(const GameBoard& board, const Coord& userMove,
					  const Coord& computer, Coord& coordinate);

// Bilgisayarın hamlelerini belirler.
void determineComputerMoves(const GameBoard& board, Coord& computer, bool& isFirst);

// Kullanıcıdan geçerli sütun harfi alır.
bool getRequest(const GameBoard& board, string& request, char& letter);

// Kullanıcıdan alınan komut parcalara ayrılarak dosya ismi alınır.
void getFileName(const string& input, string& output);

// Oyununu şu anki durumunu dosyaya kaydeder.
bool saveTheGame(const GameBoard& board, const string& filename, int turn,
				 const Coord& curr, const Coord& comp);
				 
// Oyunu dosyadan yükler.
bool loadTheGame(GameBoard& board, const string& filename, int& turn, 
				 Coord& curr, Coord& comp);

// String karşılaştırması yapar.
bool compareSaveLoad(GameBoard& board, const string& request, int& turn, 
					 Coord& curr, Coord& comp);

int main(int argc, char *argv[]) {
	State endOfGame = NONE;
	GameBoard board;

	instruct();
	board.gameType = getSizeAndGameType(board);
	initGameBoard(board);
	if (board.gameType == 'C' || board.gameType == 'c') {
		cout << "Player vs Computer mode has been selected!" << endl;
		playAgainstComputer(board, endOfGame);
	} else {
		cout << "Player vs Player mode has been selected!" << endl;
		playAgainstUser(board, endOfGame);
	}
	showResult(endOfGame);
	printGameBoard(board);

	return (0);
}

/*
 * Input: GameBoard (struct)
 * Output: Grid'in board.size kadar tüm elemanları EMPTY yapılır.
 * 		   board.rowCount arrayi sıfırlanır.
 */
void initGameBoard(GameBoard& board) {
	for (int row = 0; row < MAX_SIZE; ++row) {
		board.rowCount[row] = 0;
		for (decltype(row) col = 0; col < MAX_SIZE; ++col)
			board.grid[row][col] = EMPTY;
	}
}

/*
 * Input: GameBoard (struct)
 * Output: Grid elemanları matris şeklinde ekrana basılır.
 */
void printGameBoard(const GameBoard& board) {
	cout << endl;
	for (int i = 0; i < board.size; ++i)
		cout << static_cast<char> ('A' + i) << ' ';
	cout << endl;

	for (int row = 0; row < board.size; ++row) {
		for (int col = 0; col < board.size; ++col) {
			switch (board.grid[row][col]) {
				case EMPTY: cout << ". "; break;
				case WINX : cout << "x "; break;
				case WINO : cout << "o "; break;
				case X    : cout << "X "; break;
				case O 	  : cout << "O "; break;
			}
		}
		cout << endl;
	}
}

/*
 * Input: const GameBoard (struct), char&
 * Output: Kullanıcıdan geçerli sütun harfi alınır ve call-by-reference ile
 *		   return edilir. Eğer karakter geçerli ise foksiyon true return eder aksi
 *		   taktirde false return eder.
 */
bool getRequest(const GameBoard& board, string& request, char& letter) {
	const char MIN = 'A';
	const char MAX = static_cast<char> ('A' + board.size - 1);

	cout << "Enter a column [" << MIN << ", " << MAX << "]> ";
	getline(cin, request);

	if (request.length() == 1) {
		char tmp = static_cast<char> (request[0]);
		letter = tmp;
		
		// Eğer harf küçük girildi ise büyültülür...
		if (letter >= 'a' && letter <= 'z')
			letter -= ('a' - 'A');

		//Harfin gecerliliği kontrol edilir.
		if (letter >= MIN && letter <= MAX) {
			return (true);
		} else { // Geçerli değilse hata mesajı
			cerr << endl << "#### You should enter a column between '" << MIN
				<< "' and '" << MAX << "'! ####" << endl;
			return (false);
		}
	} else if (request.compare(0, 4, SAVE) != 0 && request.compare(0, 4, LOAD) != 0) {
		cerr << endl << "#### Enter a column letter or to save the game "
			 << "'SAVE filename' and to load a game 'LOAD filename' ####" 
			 << endl;
		return (false);
	}	
	return (false);
}

/*
 * Input: GameBoard& (struct), State& (enum)
 * Output: Player vs. Player modunu oynatır. Oyun tahtasındaki değisikler yapar.
 * 		   Oyunun sonucunu kontrol eder.
 */
void playAgainstUser(GameBoard& board, State& end) {
	int selectUser = 0;
	Cell checker;
	State control;
	Coord winner[4];
	char letter;

	while (end == NONE) {
		string request;
		printGameBoard(board);
		cout << endl;
		Coord curr, cmp;
		// Kullanıcıdan girdi alınır.
		bool getReq = getRequest(board, request, letter);

		// Eğer girdi LOAD/SAVE FILENAME formatında ise gerekli işlem yapılır.
		bool slc = compareSaveLoad(board, request, selectUser, curr, cmp);

		// LOAD yapılmış ve oyun tipi CvP ise...
		if (slc && (board.gameType == 'C' || board.gameType == 'c')) {
			end = LOADCMP; // Oyunun LOAD olduğunu belirtir.
			playAgainstComputer(board, end, curr, cmp);
			return;
		}	
		// Eğer girdi doğru ve LOAD/SAVE komutları girilmedi ise...
		if (getReq && !slc) {
			Coord coordinate; 
			coordinate.col = static_cast <int> (letter - 'A');
			coordinate.row = board.size - board.rowCount[coordinate.col] - 1;
			if (board.rowCount[coordinate.col] < board.size) { // Overflow önlenir.
				if (selectUser == 0) { // Player X
					cout << endl << "Player ONE has just made a move." << endl;
					checker = X;
					control = ONE;
					selectUser += 1;
				} else { // Player O
					cout << endl << "Player TWO has just made a move." << endl;
					checker = O;
					control = TWO;
					selectUser = 0;
				}
				// Girdi oyun tahtasını yazılır ve rowCount 1 arttırılır.
				board.rowCount[coordinate.col] += 1;
				board.grid[coordinate.row][coordinate.col] = checker;
				// Oyun sonu kontrol edilir.
				if (checkDeuce(board)) 
					end = DEUCE;   // Beraberlik kontrol
				else if (checkHorizontal(board, coordinate.row, checker, winner)) 
					end = control; // Yatay kontrol
				else if (checkVertical(board, coordinate.col, checker, winner))
					end = control; // Dikey kontrol 
				else if (checkLeftDiagonal(board, coordinate, checker, winner))
					end = control; // Sol çapraz kontrol
				else if (checkRightDiagonal(board, coordinate, checker, winner))
					end = control; // Sağ çapraz kontrol
			} else {
				cerr << endl << "#### You cannot move on this column. "
					 << "Choose another one! ####" << endl;
			}
		}
	}
	// Son durum beraberlik değil ise checkerlar küçültülür.
	if (end != DEUCE) 
		changeCheckers(board, winner, checker);
}

/*
 * Input: -
 * Output: Eğer kullanıcı birden fazla harf girdiği durumda bufferı boşaltır.
 */
void emptyBuffer(void) {
	while (cin.get() != '\n');
}

/*
 * Input: -
 * Output: Program başlangıcında oyunu tanıtan yazıyı ekrana basar.
 */
void instruct(void) {
	cout << "Hello, Welcome to Connect Four!"   << endl << endl;
	cout << "Connect Four is a simple game. You need to collect four of your "
	     << "checker " << endl << "in a row to win the game. This row can be "
		 << "horizontal, vertical or diagonal." << endl;
}

/*
 * Input: const GameBoard& (struct)
 * Output: Oyunun beraberlik durumunu kontrol eder.
 */
bool checkDeuce(const GameBoard& board) {
	auto sum = 0; // *****auto burada kullanıldı.
 
	for (int i = 0; i < board.size; ++i)
		sum += board.rowCount[i];
	return (sum == board.size * board.size);
}

/*
 * Input: const GameBoard& (struct), int, Cell (enum), Coord (struct array)
 * Output: Oyunda yatay aynı tipte dört checker oluşup oluşmadığını kontrol eder.
 *		   Eğer oluşmuş ise koordinatları win arrayine yazar ve true return eder.
 */
bool checkHorizontal(const GameBoard& board, int row, Cell checkerType,
					 Coord win[]) {
	// *****auto burada kullanıldı.
	auto checkerCount = 0, j = 0;  
	auto found = false; 
	
	for (int i = 0; i < board.size && !found; ++i) {
		if (board.grid[row][i] == checkerType) {
			checkerCount += 1;
			win[j].row = row;
			win[j].col = i;
			j += 1;
		} else {
			j = 0;
			checkerCount = 0;
		}
		found = (checkerCount == 4) ? true : false;
	}
	return (found);
}

/*
 * Input: const GameBoard& (struct), int, Cell (enum), Coord (struct array)
 * Output: Oyunda dikey aynı tipte dört checkerın oluşup oluşmadığını kontrol eder.
 *		   Eğer oluşmuş ise koordinatları win arrayine yazar ve true return eder.
 */
bool checkVertical(const GameBoard& board, int col, Cell checkerType,
				   Coord win[]) {
	// *****auto burada kullanıldı.
	auto checkerCount = 0, j = 0; 
	auto found = false;

	for (int i = 0; i < board.size && !found; ++i) {
		if (board.grid[i][col] == checkerType) {
			checkerCount += 1;
			win[j].row = i;
			win[j].col = col;
			j += 1;
		} else {
			j = 0;
			checkerCount = 0;
		}
		found = (checkerCount == 4) ? true : false;
	}
	return (found);
}

/*
 * Input: const GameBoard& (struct), const Coord&, Cell (enum), Coord (struct array)
 * Output: Oyunda sol çapraz aynı tipte dört checkerın oluşup oluşmadığını kontrol eder.
 *		   Eğer oluşmuş ise koordinatları win arrayine yazar ve true return eder.
 */
bool checkLeftDiagonal(const GameBoard& board, const Coord& coordinate,
					   Cell checkerType, Coord win[]) {
	// *****auto burada kullanıldı.
	auto checkerCount = 0, j = 0;
	auto found = false, one = false, two = false; 

	for (int i = 0; i < 4 && !found; ++i) {
		if (!one && coordinate.row-i >= 0 && coordinate.col-i < board.size) {
			if (board.grid[coordinate.row-i][coordinate.col-i] == checkerType) {
				checkerCount += 1;
				win[j].row = coordinate.row-i;
				win[j].col = coordinate.col-i;
				j += 1;
			} else {
				one = true;
			}
		}
		if (i != 0 && !two && coordinate.row+i < board.size && coordinate.col+i >= 0) {
			if (board.grid[coordinate.row+i][coordinate.col+i] == checkerType) {
				checkerCount += 1;
				win[j].row = coordinate.row+i;
				win[j].col = coordinate.col+i;
				j += 1;
			} else {
				two = true;
			}
		}
		found = (checkerCount == 4) ? true : false;
	}
	return (found);
}

/*
 * Input: const GameBoard& (struct), const Coord&, Cell (enum), Coord (struct array)
 * Output: Oyunda sağ çapraz aynı tipte dört checkerın oluşup oluşmadığını kontrol eder.
 *		   Eğer oluşmuş ise koordinatları win arrayine yazar ve true return eder.
 */
bool checkRightDiagonal(const GameBoard& board, const Coord& coordinate,
					    Cell checkerType, Coord win[]) {
	// *****auto burada kullanıldı.
	auto checkerCount = 0, j = 0;
	auto found = false, one = false, two = false; 

	for (int i = 0; i < 4 && !found; ++i) {
		if (!one && coordinate.row+i < board.size && coordinate.col-i < board.size) {
			if (board.grid[coordinate.row+i][coordinate.col-i] == checkerType) {
				checkerCount += 1;
				win[j].row = coordinate.row+i;
				win[j].col = coordinate.col-i;
				j += 1;
			} else {
				one = true;
			}
		}
		if (i != 0 && !two && coordinate.row-i >= 0 && coordinate.col+i >= 0) {
			if (board.grid[coordinate.row-i][coordinate.col+i] == checkerType) {
				checkerCount += 1;
				win[j].row = coordinate.row-i;
				win[j].col = coordinate.col+i;
				j += 1;
			} else {
				two = true;
			}
		}
		found = (checkerCount == 4) ? true : false;
	}
	return (found);
}

/*
 * Input: State (enum)
 * Output: Oyunun sonunda sonucunu ekrana basar.
 */
void showResult(State end) {
	cout << endl;
	switch (end) {
		case ONE     : cout << "Player ONE has just won!" << endl; break;
		case TWO	 : cout << "Player TWO has just won!" << endl; break;
		case COMPUTER: cout << "Computer has just won!"   << endl; break;
		case DEUCE   : cout << "Nobody won this game!"    << endl; break;
		default		 : ;
	}
}

/*
 * Input: GameBoard& (struct), Coord (struct array), Cell (enum)
 * Output: Eğer dörtlü aynı tip row oluşmuş ise, rowun harflerini küçük yapar.
 */
void changeCheckers(GameBoard& board, Coord win[], Cell checkerType) {
	for (int i = 0; i < 4; ++i) {
		if (checkerType == X)
			board.grid[win[i].row][win[i].col] = WINX; // X --> x
		else if (checkerType == O)
			board.grid[win[i].row][win[i].col] = WINO; // O --> o
	}
}

/*
 * Input: GameBoard& (struct)
 * Output: Kullanıcıdan oyun tahtasının boyutunu ve oyun tipini alır. Boyutu
 * 		   call-by-reference ile return eder. Oyun tipini karakter ile return eder.
 */
char getSizeAndGameType(GameBoard& board) {
	auto err = false, type = false;  // ****auto burada kullanıldı!
	char get;

	do {
		if (!err) { // Oyun tahtasının boyutunu alır.
			cout << endl << "Enter the size of Connect Four game board [4, 20]> ";
			cin >> board.size;
			cin.clear();
			cin.ignore(256, '\n');
			err = board.size % 2 == 0 && board.size >= 4 && board.size <= 20 ? true : false;
			if (!err) // Gecersiz girdi kontrolü...
				cerr << endl << "#### Please enter an even number in the range of 4 "
					 << "and 20! ####" << endl;
		} else if (!type) { // Oyun tipi alınır.
			cout << endl << "Enter 'P' for Player vs Player or 'C' for Player "
				 << "vs Computer> ";
			cin >> get;
			emptyBuffer();
			type = get == 'C' || get == 'c' || get == 'P' || get == 'p' ? true : false;
			if (!type) // Gecersiz girdi kontrolü
				cerr << endl << "#### Please enter a valid game type! ####" << endl;
		}
	} while (!err || !type);

	return (get);
}

/*
 * Input: GameBoard& (struct), State& (enum), Coord& (struct), Coord& (struct)
 * Output: Computer vs. Player modunu oynatır. Oyun tahtasındaki değisikler yapar.
 * 		   Oyunun sonucunu kontrol eder.
 */
void playAgainstComputer(GameBoard& board, State& end, Coord curr, Coord comp) {
	Coord user, computer, coordinate;
	int selection = 0;
	Cell checker;
	State control;
	Coord winner[4];
	bool first = false;
	
	// ****decltype burada kullanıldı.
	decltype(first) slc = false;
	decltype(first) req;
	char letter;
	string request;
	
	// LOAD yapıldığı durumda dosyadan alınan bilgisayar ve kullanıcı 
	// koordinatları atanır. 
	if (end == LOADCMP) {
		first = true;
		coordinate = curr;
		computer = comp;
		end = NONE;
	}
	while (end == NONE) {
		// Oyun Tahtası ekrana basılır.
		printGameBoard(board);
		cout << endl;
		if (selection == 0) { //Kullanıcı
			// Kullanıcıdan girdi alınır.
			req = getRequest(board, request, letter);

			// Eğer girdi LOAD/SAVE FILENAME formatında ise gerekli işlem yapılır.
			slc = compareSaveLoad(board, request, selection, computer, coordinate);

			// LOAD yapılmış ve oyun tipi PvP ise...
			if (slc && (board.gameType == 'P' || board.gameType == 'p')) {
				playAgainstUser(board, end); 
				return;
			}
			// Eğer girdi doğru ve LOAD/SAVE komutları girilmedi ise...
			if (req && !slc) {
				cout << endl << "Player has just made a move." << endl;
				checker = X;
				control = ONE;
				user.col = static_cast <int> (letter - 'A'); 
				user.row = board.size - board.rowCount[user.col] - 1;
				selection = 1; // Bilgisayara geçilir
				coordinate = user;
			}
		} else if (selection == 1) { //Bilgisayar
			cout << "---- Computer has just made a move! ----" << endl;
			checker = O;
			control = COMPUTER;
			// Kullanıcın gelecek seferki hamlesi engellenir.
			// Eğer üçlememişse if bloğuna girilir ve bilgisayarın gelecek seferki
			// hamlesine karar verilir.
			if (!controlUserMoves(board, user, computer, coordinate)) {
				determineComputerMoves(board, computer, first);
				coordinate = computer;
			}
			selection = 0;
		}
		if (req && (selection == 0 || selection == 1)) {
			// Girdi oyun tahtasını yazılır ve rowCount 1 arttırılır.
			board.rowCount[coordinate.col] += 1;
			board.grid[coordinate.row][coordinate.col] = checker;

			// Oyun sonu kontrol edilir.
			if (checkDeuce(board)) 
				end = DEUCE;   // Beraberlik kontrol
			else if (checkHorizontal(board, coordinate.row, checker, winner)) 
				end = control; // Yatay kontrol
			else if (checkVertical(board, coordinate.col, checker, winner))
				end = control; // Dikey kontrol 
			else if (checkLeftDiagonal(board, coordinate, checker, winner))
				end = control; // Sol çapraz kontrol
			else if (checkRightDiagonal(board, coordinate, checker, winner))
				end = control; // Sağ çapraz kontrol
		}
	}
	// Son durum beraberlik değil ise checkerlar küçültülür.
	if (end != DEUCE) 
		changeCheckers(board, winner, checker);
}

/*
 * Input: const GameBoard& (struct), Coord& (struct), bool&
 * Output: Bilgisayarın hamlelerini belirler. Bilgisayarın ilk hamlesi
 * 		   yapıldıktan sonra isFirst true yapılır.
 */
void determineComputerMoves(const GameBoard& board, Coord& computer, bool& isFirst) {
	Coord tmp;
	int row;

	tmp.col = board.size / 2 - 1;
	tmp.row = board.size - 1;

	if (board.grid[tmp.row][tmp.col] == EMPTY && !isFirst) {
		isFirst = true;
		computer = tmp;
	} else if (board.grid[tmp.row][tmp.col+1] == EMPTY && !isFirst) {
		isFirst = true;
		computer.row = tmp.row;
		computer.col = tmp.col+1;
	} else {
		if (board.grid[computer.row-1][computer.col] == EMPTY) {
			if (board.grid[computer.row][computer.col+1] == EMPTY &&
				board.grid[computer.row][computer.col] == O &&
				board.grid[computer.row][computer.col-1] == O &&
				board.grid[computer.row][computer.col-2] == O)
			{
				computer.row = computer.row;
				computer.col += 1;
			} else if (board.grid[computer.row][computer.col-1] == EMPTY &&
					   board.grid[computer.row][computer.col] == O &&
					   board.grid[computer.row][computer.col+1] == O &&
					   board.grid[computer.row][computer.col+2] == O)
			{
				computer.row = computer.row;
				computer.col -= 1;
			} else {
				computer.row -= 1;
			}
		} else if (board.grid[computer.row-1][computer.col] == X) {
			row = board.size-board.rowCount[computer.col+1]-1;
			if (board.grid[row][computer.col+1] == EMPTY && board.rowCount[computer.col+1] == 0) {
				computer.row = row;
				computer.col = computer.col+1;
				return;
			}
			row = board.size-board.rowCount[computer.col-1]-1;
			if (board.grid[row][computer.col-1] == EMPTY && computer.col >= 0) {
				computer.row = row;
				computer.col = computer.col-1;
				if (computer.col == 0) {
					computer.col = tmp.col+1;
					computer.row = board.size-board.rowCount[computer.col]-1;
				}
				return;
			}
		}
	}
}

/*
 * Input: const GameBoard& (struct), const Coord& (userMove), const Coord& (computer)
 *		  Coord& (coordinate)
 * Output: Kullanıcının kazanamasını engellemeye çalışır. Eğer kullanıcı dörtlü row
 *		   oluşturmak üzereyse true return eder.
 */
bool controlUserMoves(const GameBoard& board, const Coord& userMove,
					  const Coord& computer, Coord& coordinate) {
	if (board.size - userMove.row >= 3) {
		if (board.grid[userMove.row][userMove.col]   == X &&
			board.grid[userMove.row+1][userMove.col] == X &&
			board.grid[userMove.row+2][userMove.col] == X)
		{
			if (board.grid[computer.row][computer.col]   == O &&
				board.grid[computer.row+1][computer.col] == O &&
			 	board.grid[computer.row+2][computer.col] == O)
			{
				return (false);
			}
			coordinate.row = userMove.row-1;
			coordinate.col = userMove.col;
			return (true);
		}
	}
	if (userMove.col+3 < board.size) {
		// X X * . durumu
		if (board.grid[userMove.row][userMove.col-2] == X &&
			board.grid[userMove.row][userMove.col-1] == X &&
			board.grid[userMove.row][userMove.col]   == X &&
			board.rowCount[userMove.col+1]+1 == board.rowCount[userMove.col])
		{
			coordinate.row = userMove.row;
			coordinate.col = userMove.col+1;
			return (true);
		}
	}
	if (userMove.col >= 0) {
		// . * X X durumu
		if (board.grid[userMove.row][userMove.col+2] == X &&
			board.grid[userMove.row][userMove.col+1] == X &&
			board.grid[userMove.row][userMove.col]   == X &&
			board.rowCount[userMove.col-1]+1 == board.rowCount[userMove.col])
		{
			coordinate.row = userMove.row;
			coordinate.col = userMove.col-1;
			return (true);
		}
		// * X . X durumu
		if (board.grid[userMove.row][userMove.col+1] == X &&
			board.grid[userMove.row][userMove.col+3] == X &&
			board.grid[userMove.row][userMove.col]   == X &&
			board.rowCount[userMove.col+2]+1 == board.rowCount[userMove.col])
		{
			coordinate.row = userMove.row;
			coordinate.col = userMove.col+2;
			return (true);
		}
		//  * . X X durumu
		if (board.grid[userMove.row][userMove.col+2] == X &&
			board.grid[userMove.row][userMove.col+3] == X &&
			board.grid[userMove.row][userMove.col]   == X &&
			board.rowCount[userMove.col+1]+1 == board.rowCount[userMove.col])
		{
			coordinate.row = userMove.row;
			coordinate.col = userMove.col+1;
			return (true);
		}
	}
	if (userMove.col - 3 >= 0) {
		// X . X * durumu
		if (board.grid[userMove.row][userMove.col-1] == X &&
			board.grid[userMove.row][userMove.col-3] == X &&
			board.grid[userMove.row][userMove.col]   == X &&
			board.rowCount[userMove.col-2]+1 == board.rowCount[userMove.col])
		{
			coordinate.row = userMove.row;
			coordinate.col = userMove.col-2;
			return (true);
		}
		// X X . * durumu
		if (board.grid[userMove.row][userMove.col-2] == X &&
			board.grid[userMove.row][userMove.col-3] == X &&
			board.grid[userMove.row][userMove.col]   == X &&
			board.rowCount[userMove.col-1]+1 == board.rowCount[userMove.col])
		{
			coordinate.row = userMove.row;
			coordinate.col = userMove.col-1;
			return (true);
		}
	}
	if (userMove.col - 1 >= 0) {
		// 	. X * X durumu
		if (board.grid[userMove.row][userMove.col+1] == X &&
			board.grid[userMove.row][userMove.col-1] == X &&
			board.grid[userMove.row][userMove.col]   == X &&
			board.rowCount[userMove.col-2]+1 == board.rowCount[userMove.col])
		{
			coordinate.row = userMove.row;
			coordinate.col = userMove.col-2;
			return (true);
		}

		// X X * . durumu
		if (board.grid[userMove.row][userMove.col-1] == X &&
			board.grid[userMove.row][userMove.col+2] == X &&
			board.grid[userMove.row][userMove.col]   == X &&
			board.rowCount[userMove.col+1]+1 == board.rowCount[userMove.col])
		{
			coordinate.row = userMove.row;
			coordinate.col = userMove.col+1;
			return (true);
		}
		if (board.grid[userMove.row][userMove.col-1] == X &&
			board.grid[userMove.row][userMove.col]   == X &&
			board.rowCount[userMove.col+1]+1 == board.rowCount[userMove.col])
		{
			coordinate.row = userMove.row;
			coordinate.col = userMove.col+1;
			return (true);
		}
	}
	if (userMove.col - 2 >= 0) {
		// X . * X durumu
		if (board.grid[userMove.row][userMove.col-2] == X &&
			board.grid[userMove.row][userMove.col+1] == X &&
			board.grid[userMove.row][userMove.col]   == X &&
			board.rowCount[userMove.col-1]+1 == board.rowCount[userMove.col])
		{
			coordinate.row = userMove.row;
			coordinate.col = userMove.col-1;
			return (true);
		}
	}
	return (false);
}

/*
 * Input: const GameBoard& (struct), const string&, int, const Coord& (struct), 
 * 		  const Coord& (struct) 
 * Output: Oyunun anlık durumunu verilen dosyaya kaydeder. Dosya başarılı ile
 * 		   açılırsa true, aksi takdirde false return edilir.
 */
bool saveTheGame(const GameBoard& board, const string& filename, int turn, 
				 const Coord& curr, const Coord& comp) {
	// Dosya açılır.
	ofstream outputStream(filename.c_str());
	if (outputStream.fail())
		return false;
	
	// Oyun tahtasının boyutu
	outputStream << board.size << endl << endl;

	// Oyun tipi
	outputStream << board.gameType << endl << endl;

	// Sıra
	outputStream << turn << endl << endl;

	// rowCount
	for (int i = 0; i < board.size; ++i)
		outputStream << board.rowCount[i] << ' ';
	outputStream << endl << endl;

	// Oyun tahtası 
	for (int row = 0; row < board.size; ++row) {
		for (int col = 0; col < board.size; ++col) {
			switch (board.grid[row][col]) {
				case EMPTY: outputStream << 0 << ' '; break;
				case X    : outputStream << 1 << ' '; break;
				case O 	  : outputStream << 2 << ' '; break;
				case WINX : outputStream << 3 << ' '; break;
				case WINO : outputStream << 4 << ' '; break;
			}
		}
		outputStream << endl;
	}
	cout << endl;

	// Kullanıcının son hamlesi
	outputStream << endl;
	outputStream << curr.row << ' ' << curr.col << endl;

	// Bilgisayarın son hamlesi
	outputStream << endl;
	outputStream << comp.row << ' ' << comp.col << endl;

	// Dosya kapatılır.
	outputStream.close();
	return true;
}

/*
 * Input: const GameBoard& (struct), const string&, int&, Coord& (struct), Coord& (struct)
 * Output: Oyunun anlık durumunu verilen dosyayadan alır. Dosya başarılı ile
 * 		   açılırsa true, aksi takdirde false return edilir.
 */
bool loadTheGame(GameBoard& board, const string& filename, int& turn, 
				 Coord& curr, Coord& comp) {
	// Dosya açılır.
	ifstream inputStream(filename.c_str());
	int size;
	if (inputStream.fail()) 
		return false;
		
	inputStream >> size;
	// Dosyanın boş olup olmadığını kontrol eder.
	if (inputStream.eof())
		return false;

	// Oyun tahtasının boyutunu alır.
	board.size = size;
	initGameBoard(board);

	// Oyun tipini alır.
	inputStream >> board.gameType;

	// Oyuncu sırasını alır.	
	inputStream >> turn;
	
	// rowCount'u alır.
	for (int i = 0; i < board.size; ++i)
		inputStream >> board.rowCount[i];

	// Oyun tahtasını dosyadan alır
	for (int row = 0; row < board.size; ++row) {
		for (int col = 0; col < board.size; ++col) {
			int get;
			inputStream >> get;
			switch (get) {
				case 0 : board.grid[row][col] = EMPTY; break;
				case 1 : board.grid[row][col] = X	 ; break;
				case 2 : board.grid[row][col] = O	 ; break;
				case 3 : board.grid[row][col] = WINX ; break;
				case 4 : board.grid[row][col] = WINO ; break;
			}
		}
	}

	// Kullanıcın son hamlesini alır.
	inputStream >> curr.row >> curr.col;

	// Bilgisayarın son hamlesini alır.
	inputStream >> comp.row >> comp.col;

	// Dosya kapatılır.
	inputStream.close();
	return true;
}

/*
 * Input: GameBoard& (struct), const string&, int&, Coord& (struct), Coord& (struct)
 * Output: Kullanıcıdan gelen istek analiz edilir ve LOAD/SAVE işlemleri yapılır.
 */
bool compareSaveLoad(GameBoard& board, const string& request, int& turn,
					 Coord& curr, Coord& comp) {
	string filename;
	bool saveOrLoad;

	// Eğer girdi SAVE ile başlıyor ve dosya ismi varsa...
	if (request.compare(0, 4, SAVE) == 0 && request.length() > 5) {
		getFileName(request, filename); // Dosya adı alınır.
		saveOrLoad = saveTheGame(board, filename, turn, curr, comp);
		if (saveOrLoad) { // Dosya kaydedildi.
			cout << "--------------------------------------------------";
			cout << endl << "#### Current game state has been saved to " 
				 << filename << "! ####" << endl;			
			return true;
		} else { // Dosya açılamadı.
			cerr << endl << "**** " << filename << " could not be opened! ****\n";
			return false;
		}
	}
	// Eğer girdi LOAD ile başlıyor ve dosya ismi varsa...
	if (request.compare(0, 4, LOAD) == 0 && request.length() > 5) {
		getFileName(request, filename); // Dosya adı alınır.
		saveOrLoad = loadTheGame(board, filename, turn, curr, comp);
		if (saveOrLoad) { // Dosya load edildi.
			cout << "--------------------------------------------------";
			cout << endl << "#### Loading the game from " << filename
				 << "... ####" << endl;
			if (board.gameType == 'P' || board.gameType == 'p') {
				if (turn == 0)
					cout << endl << "Player ONE will make a move!" << endl;
				else 
					cout << endl << "Player TWO will make a move!" << endl;
			} else {
				if (turn == 0)
					cout << endl << "Player will make a move!" << endl;
			}
			return true;
		} else { // Dosya açılamadı.
			cerr << endl << "**** " << filename << " could not be opened! ****\n";
			return false;
		}
	}
	return false;
}

/*
 * Input: const string&, string& 
 * Output: Kullanıcının girdisinden dosya adı alınarak output stringi ile 
 * 		   return edilir.
 */
void getFileName(const string& input, string& output) {
	output = ""; 
	for (unsigned int i = 5; i < input.length(); ++i) 
		output += input[i];
}
//****************************************************************************//
//							~ END OF main.cpp ~								  //
//****************************************************************************//
