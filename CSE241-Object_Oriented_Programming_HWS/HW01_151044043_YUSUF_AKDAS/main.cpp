//****************************************************************************//
// author: @yusufakdas													      //
// student id: 151044043 													  //
//****************************************************************************//
#include <iostream>
using namespace std;

// Oyun tahtasında yer alacak unsurlar
enum Cell { X, O, WINX, WINO, EMPTY };

// Oyunda olabilecek durumlar
enum State { ONE, TWO, COMPUTER, DEUCE, NONE };

// Oyun tahtasının maksimum boyutu
const int MAX_SIZE = 20;

// Oyun tahtası
struct GameBoard {
	Cell grid[MAX_SIZE][MAX_SIZE];
	int rowCount[MAX_SIZE];
	int size;
};

// Koordinatlar
struct Coord {
	int row;
	int col;
};

// Oyun tahtasını oluşturur.
bool createGameBoard(GameBoard& board);

// Oyun tahtasını ekrana basar.
void printGameBoard(const GameBoard& board);

// Oyun tahtasını tüm hücrelerini EMPTY yapar.
void initGameBoard(GameBoard& board);

// Player vs. Player modu
void playAgainstUser(GameBoard& board, State& end);

// Beraberlik durumunu kontrol eder.
bool checkDeuce(const GameBoard& board);

// Bufferı temizler.
void emptyBuffer(void);

// Oyun tahtasının boyutunu ve oyun tipini kullanıcıdan alır.
char getSizeAndGameType(GameBoard& board);

// Program başlangıcında oyunla ilgili bilgiyi ekrana basar.
void instruct(void);

// Yatay kontrol
bool checkHorizontal(const GameBoard& board, int row, Cell checkerType,
					 Coord win[]);
// Dikey kontrol
bool checkVertical(const GameBoard& board, int col, Cell checkerType,
				   Coord win[]);
// Sağ çapraz kontrol
bool checkRightDiagonal(const GameBoard& board, const Coord& coordinate,
						Cell checkerType, Coord win[]);
// Sol çapraz kontrol
bool checkLeftDiagonal(const GameBoard& board, const Coord& coordinate,
					   Cell checkerType, Coord win[]);
// Oyun sonucunu ekrana basar.
void showResult(State end);

// Checkerları küçük yapar
void changeCheckers(GameBoard& board, Coord win[], Cell checkerType);

// Computer vs. Player modu
void playAgainstComputer(GameBoard& board, State& end);

// Kullanıcının hamlelerini kontrol eder
bool controlUserMoves(const GameBoard& board, const Coord& userMove,
					  const Coord& computer, Coord& coordinate);

// Bilgisayarın hamlelerini belirler.
void determineComputerMoves(const GameBoard& board, Coord& computer, bool& isFirst);

// Kullanıcıdan geçerli sütun harfi alır.
bool getRequest(const GameBoard& board, char& request);

int main(int argc, char *argv[]) {
	State endOfGame = NONE;
	GameBoard board;
	int gameType;

	instruct();
	gameType = getSizeAndGameType(board);
	initGameBoard(board);
	if (gameType == 'C' || gameType == 'c') {
		cout << "Player vs Computer mode has been selected!" << endl;
		playAgainstComputer(board, endOfGame);
	} else {
		cout << "Player vs Player mode has been selected!" << endl;
		playAgainstUser(board, endOfGame);
	}
	showResult(endOfGame);
	printGameBoard(board);

	return (0);
}

/*
 * Input: GameBoard (struct)
 * Output: Grid'in board.size kadar tüm elemanları EMPTY yapılır.
 * 		   board.rowCount arrayi sıfırlanır.
 */
void initGameBoard(GameBoard& board) {
	for (int row = 0; row < board.size; ++row) {
		board.rowCount[row] = 0;
		for (int col = 0; col < board.size; ++col)
			board.grid[row][col] = EMPTY;
	}
}

/*
 * Input: GameBoard (struct)
 * Output: Grid elemanları matris şeklinde ekrana basılır.
 */
void printGameBoard(const GameBoard& board) {
	cout << endl;
	for (int i = 0; i < board.size; ++i)
		cout << static_cast<char> ('A' + i) << ' ';
	cout << endl;

	for (int row = 0; row < board.size; ++row) {
		for (int col = 0; col < board.size; ++col) {
			switch (board.grid[row][col]) {
				case EMPTY: cout << ". "; break;
				case WINX : cout << "x "; break;
				case WINO : cout << "o "; break;
				case X    : cout << "X "; break;
				case O 	  : cout << "O "; break;
			}
		}
		cout << endl;
	}
}

/*
 * Input: const GameBoard (struct), char&
 * Output: Kullanıcıdan geçerli sütun harfi alınır ve call-by-reference ile
 *		   return edilir. Eğer karakter geçerli ise foksiyon true return eder aksi
 *		   taktirde false return eder.
 */
bool getRequest(const GameBoard& board, char& request) {
	const char MIN = 'A';
	const char MAX = static_cast<char> ('A' + board.size - 1);

	cout << "Enter a column [" << MIN << ", " << MAX << "]> ";
	cin >> request;
	emptyBuffer();

	if (request >= 'a' && request <= 'z')
		request -= ('a' - 'A');

	if (request >= MIN && request <= MAX) {
		return (true);
	} else {
   		cout << endl << "#### You should enter a column between '" << MIN
   			 << "' and '" << MAX << "'! ####" << endl;
		return (false);
	}
}

/*
 * Input: GameBoard& (struct), State& (enum)
 * Output: Player vs. Player modunu oynatır. Oyun tahtasındaki değisikler yapar.
 * 		   Oyunun sonucunu kontrol eder.
 */
void playAgainstUser(GameBoard& board, State& end) {
	int selectUser = 0;
	char request;
	Cell checker;
	State control;
	Coord winner[4];

	printGameBoard(board);
	while (end == NONE) {
		cout << endl;
		if (getRequest(board, request)) {
			Coord coordinate;
			coordinate.col = static_cast <int> (request - 'A');
			coordinate.row = board.size - board.rowCount[coordinate.col] - 1;
			if (board.rowCount[coordinate.col] < board.size) {
				if (selectUser == 0) {
					checker = X;
					control = ONE;
					selectUser += 1;
				} else {
					checker = O;
					control = TWO;
					selectUser = 0;
				}
				board.grid[coordinate.row][coordinate.col] = checker;
				board.rowCount[coordinate.col] += 1;
				printGameBoard(board);
				// Durum Kontrolü
				if (checkDeuce(board))
					end = DEUCE;
				else if (checkHorizontal(board, coordinate.row, checker, winner))
					end = control;
				else if (checkVertical(board, coordinate.col, checker, winner))
					end = control;
				else if (checkLeftDiagonal(board, coordinate, checker, winner))
					end = control;
				else if (checkRightDiagonal(board, coordinate, checker, winner))
					end = control;
			} else {
				cout << endl << "#### You cannot move on this column. "
					 << "Choose another one! ####" << endl;
			}
		}
	}
	if (end != DEUCE)
		changeCheckers(board, winner, checker);
}

/*
 * Input: -
 * Output: Eğer kullanıcı birden fazla harf girdiği durumda bufferı boşaltır.
 */
void emptyBuffer(void) {
	while (cin.get() != '\n');
}

/*
 * Input: -
 * Output: Program başlangıcında oyunu tanıtan yazıyı ekrana basar.
 */
void instruct(void) {
	cout << "Hello, Welcome to Connect Four!"   << endl << endl;
	cout << "Connect Four is a simple game. You need to collect four of your "
	     << "checker " << endl << "in a row to win the game. This row can be "
		 << "horizontal, vertical or diagonal." << endl;
}

/*
 * Input: const GameBoard& (struct)
 * Output: Oyunun beraberlik durumunu kontrol eder.
 */
bool checkDeuce(const GameBoard& board) {
	int sum = 0;

	for (int i = 0; i < board.size; ++i)
		sum += board.rowCount[i];
	return (sum == board.size * board.size);
}

/*
 * Input: const GameBoard& (struct), int, Cell (enum), Coord (struct array)
 * Output: Oyunda yatay aynı tipte dört checker oluşup oluşmadığını kontrol eder.
 *		   Eğer oluşmuş ise koordinatları win arrayine yazar ve true return eder.
 */
bool checkHorizontal(const GameBoard& board, int row, Cell checkerType,
					 Coord win[]) {
	int checkerCount = 0, j = 0;
	bool found = false;

	for (int i = 0; i < board.size && !found; ++i) {
		if (board.grid[row][i] == checkerType) {
			checkerCount += 1;
			win[j].row = row;
			win[j].col = i;
			j += 1;
		} else {
			j = 0;
			checkerCount = 0;
		}
		found = (checkerCount == 4) ? true : false;
	}
	return (found);
}

/*
 * Input: const GameBoard& (struct), int, Cell (enum), Coord (struct array)
 * Output: Oyunda dikey aynı tipte dört checkerın oluşup oluşmadığını kontrol eder.
 *		   Eğer oluşmuş ise koordinatları win arrayine yazar ve true return eder.
 */
bool checkVertical(const GameBoard& board, int col, Cell checkerType,
				   Coord win[]) {
	int checkerCount = 0, j = 0;
	bool found = false;

	for (int i = 0; i < board.size && !found; ++i) {
		if (board.grid[i][col] == checkerType) {
			checkerCount += 1;
			win[j].row = i;
			win[j].col = col;
			j += 1;
		} else {
			j = 0;
			checkerCount = 0;
		}
		found = (checkerCount == 4) ? true : false;
	}
	return (found);
}

/*
 * Input: const GameBoard& (struct), const Coord&, Cell (enum), Coord (struct array)
 * Output: Oyunda sol çapraz aynı tipte dört checkerın oluşup oluşmadığını kontrol eder.
 *		   Eğer oluşmuş ise koordinatları win arrayine yazar ve true return eder.
 */
bool checkLeftDiagonal(const GameBoard& board, const Coord& coordinate,
					   Cell checkerType, Coord win[]) {
	int checkerCount = 0, j = 0;
	bool found = false, one = false, two = false;

	for (int i = 0; i < 4 && !found; ++i) {
		// Caprazın sağ tarafı
		if (!one && coordinate.row-i >= 0 && coordinate.col+i < board.size) {
			if (board.grid[coordinate.row-i][coordinate.col+i] == checkerType) {
				checkerCount += 1;
				win[j].row = coordinate.row-i;
				win[j].col = coordinate.col+i;
				j += 1;
			} else {
				one = true;
			}
		}
		// Caprazın sol tarafı
		if (i != 0 && !two && coordinate.row+i < board.size && coordinate.col-i >= 0) {
			if (board.grid[coordinate.row+i][coordinate.col-i] == checkerType) {
				checkerCount += 1;
				win[j].row = coordinate.row+i;
				win[j].col = coordinate.col-i;
				j += 1;
			} else {
				two = true;
			}
		}
		found = (checkerCount == 4) ? true : false;
	}
	return (found);
}

/*
 * Input: const GameBoard& (struct), const Coord&, Cell (enum), Coord (struct array)
 * Output: Oyunda sağ çapraz aynı tipte dört checkerın oluşup oluşmadığını kontrol eder.
 *		   Eğer oluşmuş ise koordinatları win arrayine yazar ve true return eder.
 */
bool checkRightDiagonal(const GameBoard& board, const Coord& coordinate,
					    Cell checkerType, Coord win[]) {
	int checkerCount = 0, j = 0;
	bool found = false, one = false, two = false;

	for (int i = 0; i < 4 && !found; ++i) {
		// Caprazın sağ tarafı
		if (!one && coordinate.row+i < board.size && coordinate.col+i < board.size) {
			if (board.grid[coordinate.row+i][coordinate.col+i] == checkerType) {
				checkerCount += 1;
				win[j].row = coordinate.row+i;
				win[j].col = coordinate.col+i;
				j += 1;
			} else {
				one = true;
			}
		}
		// Caprazın sol tarafı
		if (i != 0 && !two && coordinate.row-i >= 0 && coordinate.col-i >= 0) {
			if (board.grid[coordinate.row-i][coordinate.col-i] == checkerType) {
				checkerCount += 1;
				win[j].row = coordinate.row-i;
				win[j].col = coordinate.col+i;
				j += 1;
			} else {
				two = true;
			}
		}
		found = (checkerCount == 4) ? true : false;
	}
	return (found);
}

/*
 * Input: State (enum)
 * Output: Oyunun sonunda sonucunu ekrana basar.
 */
void showResult(State end) {
	cout << endl;
	switch (end) {
		case ONE     : cout << "Player1 has just won!"  << endl; break;
		case TWO	 : cout << "Player2 has just won!"  << endl; break;
		case COMPUTER: cout << "Computer has just won!" << endl; break;
		case DEUCE   : cout << "Nobody won this game!"  << endl; break;
		default		 : ;
	}
}

/*
 * Input: GameBoard& (struct), Coord (struct array), Cell (enum)
 * Output: Eğer dörtlü aynı tip row oluşmuş ise, rowun harflerini küçük yapar.
 */
void changeCheckers(GameBoard& board, Coord win[], Cell checkerType) {
	for (int i = 0; i < 4; ++i) {
		if (checkerType == X)
			board.grid[win[i].row][win[i].col] = WINX;
		else if (checkerType == O)
			board.grid[win[i].row][win[i].col] = WINO;
	}
}

/*
 * Input: GameBoard& (struct)
 * Output: Kullanıcıdan oyun tahtasının boyutunu ve oyun tipini alır. Boyutu
 * 		   call-by-reference ile return eder. Oyun tipini karakter ile return eder.
 */
char getSizeAndGameType(GameBoard& board) {
	bool err = false, type = false;
	char get;

	do {
		if (!err) {
			cout << endl << "Enter the size of Connect Four game board [4, 20]> ";
			cin >> board.size;
			cin.clear();
			cin.ignore(256, '\n');
			err = board.size % 2 == 0 && board.size >= 4 && board.size <= 20 ? true : false;
			if (!err)
				cout << endl << "#### Please enter an even number in the range of 4 "
					 << "and 20! ####" << endl;
		} else if (!type) {
			cout << endl << "Enter 'P' for Player vs Player or 'C' for Player "
				 << "vs Computer> ";
			cin >> get;
			emptyBuffer();
			type = get == 'C' || get == 'c' || get == 'P' || get == 'p' ? true : false;
			if (!type)
				cout << endl << "#### Please enter a valid game type! ####" << endl;
		}
	} while (!err || !type);

	return (get);
}

/*
 * Input: GameBoard& (struct), State& (enum)
 * Output: Computer vs. Player modunu oynatır. Oyun tahtasındaki değisikler yapar.
 * 		   Oyunun sonucunu kontrol eder.
 */
void playAgainstComputer(GameBoard& board, State& end) {
	Coord user, computer, coordinate;
	int selection = 0;
	char request;
	Cell checker;
	State control;
	Coord winner[4];
	bool first = false, req;

	printGameBoard(board);
	while (end == NONE) {
		cout << endl;
		if (selection == 0) {
			req = getRequest(board, request);
			if (req) {
				checker = X;
				control = ONE;
				user.col = static_cast <int> (request - 'A');
				user.row = board.size - board.rowCount[user.col] - 1;
				selection = 1;
				coordinate = user;
			}
		} else if (selection == 1) {
			cout << "---- Computer has just made a move! ----" << endl;
			checker = O;
			control = COMPUTER;
			if (!controlUserMoves(board, user, computer, coordinate)) {
				determineComputerMoves(board, computer, first);
				coordinate = computer;
			}
			selection = 0;
		}
		if ((req && selection == 0) || (selection == 1)) {
			board.rowCount[coordinate.col] += 1;
			board.grid[coordinate.row][coordinate.col] = checker;
			printGameBoard(board);
			// Durum Kontrolü
			if (checkDeuce(board))
				end = DEUCE;
			else if (checkHorizontal(board, coordinate.row, checker, winner))
				end = control;
			else if (checkVertical(board, coordinate.col, checker, winner))
				end = control;
			else if (checkLeftDiagonal(board, coordinate, checker, winner))
				end = control;
			else if (checkRightDiagonal(board, coordinate, checker, winner))
				end = control;
		}
	}
	if (end != DEUCE)
		changeCheckers(board, winner, checker);
}

/*
 * Input: const GameBoard& (struct), Coord& (struct), bool&
 * Output: Bilgisayarın hamlelerini belirler. Bilgisayarın ilk hamlesi
 * 		   yapıldıktan sonra isFirst true yapılır.
 */
void determineComputerMoves(const GameBoard& board, Coord& computer, bool& isFirst) {
	Coord tmp;
	int row;

	tmp.col = board.size / 2 - 1;
	tmp.row = board.size - 1;

	if (board.grid[tmp.row][tmp.col] == EMPTY && !isFirst) {
		isFirst = true;
		computer = tmp;
	} else if (board.grid[tmp.row][tmp.col+1] == EMPTY && !isFirst) {
		isFirst = true;
		computer.row = tmp.row;
		computer.col = tmp.col+1;
	} else {
		if (board.grid[computer.row-1][computer.col] == EMPTY) {
			if (board.grid[computer.row][computer.col+1] == EMPTY &&
				board.grid[computer.row][computer.col] == O &&
				board.grid[computer.row][computer.col-1] == O &&
				board.grid[computer.row][computer.col-2] == O)
			{
				computer.row = computer.row;
				computer.col += 1;
			} else if (board.grid[computer.row][computer.col-1] == EMPTY &&
					   board.grid[computer.row][computer.col] == O &&
					   board.grid[computer.row][computer.col+1] == O &&
					   board.grid[computer.row][computer.col+2] == O)
			{
				computer.row = computer.row;
				computer.col -= 1;
			} else {
				computer.row -= 1;
			}
		} else if (board.grid[computer.row-1][computer.col] == X) {
			row = board.size-board.rowCount[computer.col+1]-1;
			if (board.grid[row][computer.col+1] == EMPTY && board.rowCount[computer.col+1] == 0) {
				computer.row = row;
				computer.col = computer.col+1;
				return;
			}
			row = board.size-board.rowCount[computer.col-1]-1;
			if (board.grid[row][computer.col-1] == EMPTY && computer.col >= 0) {
				computer.row = row;
				computer.col = computer.col-1;
				if (computer.col == 0) {
					computer.col = tmp.col+1;
					computer.row = board.size-board.rowCount[computer.col]-1;
				}
				return;
			}
		}
	}
}

/*
 * Input: const GameBoard& (struct), const Coord& (userMove), const Coord& (computer)
 *		  Coord& (coordinate)
 * Output: Kullanıcının kazanamasını engellemeye çalışır. Eğer kullanıcı dörtlü row
 *		   oluşturmak üzereyse true return eder.
 */
bool controlUserMoves(const GameBoard& board, const Coord& userMove,
					  const Coord& computer, Coord& coordinate) {
	if (board.size - userMove.row >= 3) {
		if (board.grid[userMove.row][userMove.col]   == X &&
			board.grid[userMove.row+1][userMove.col] == X &&
			board.grid[userMove.row+2][userMove.col] == X)
		{
			if (board.grid[computer.row][computer.col]   == O &&
				board.grid[computer.row+1][computer.col] == O &&
			 	board.grid[computer.row+2][computer.col] == O)
			{
				return (false);
			}
			coordinate.row = userMove.row-1;
			coordinate.col = userMove.col;
			return (true);
		}
	}
	if (userMove.col+3 < board.size) {
		// X X * . durumu
		if (board.grid[userMove.row][userMove.col-2] == X &&
			board.grid[userMove.row][userMove.col-1] == X &&
			board.grid[userMove.row][userMove.col]   == X &&
			board.rowCount[userMove.col+1]+1 == board.rowCount[userMove.col])
		{
			coordinate.row = userMove.row;
			coordinate.col = userMove.col+1;
			return (true);
		}
	}
	if (userMove.col >= 0) {
		// . * X X durumu
		if (board.grid[userMove.row][userMove.col+2] == X &&
			board.grid[userMove.row][userMove.col+1] == X &&
			board.grid[userMove.row][userMove.col]   == X &&
			board.rowCount[userMove.col-1]+1 == board.rowCount[userMove.col])
		{
			coordinate.row = userMove.row;
			coordinate.col = userMove.col-1;
			return (true);
		}
		// * X . X durumu
		if (board.grid[userMove.row][userMove.col+1] == X &&
			board.grid[userMove.row][userMove.col+3] == X &&
			board.grid[userMove.row][userMove.col]   == X &&
			board.rowCount[userMove.col+2]+1 == board.rowCount[userMove.col])
		{
			coordinate.row = userMove.row;
			coordinate.col = userMove.col+2;
			return (true);
		}
		//  * . X X durumu
		if (board.grid[userMove.row][userMove.col+2] == X &&
			board.grid[userMove.row][userMove.col+3] == X &&
			board.grid[userMove.row][userMove.col]   == X &&
			board.rowCount[userMove.col+1]+1 == board.rowCount[userMove.col])
		{
			coordinate.row = userMove.row;
			coordinate.col = userMove.col+1;
			return (true);
		}
	}
	if (userMove.col - 3 >= 0) {
		// X . X * durumu
		if (board.grid[userMove.row][userMove.col-1] == X &&
			board.grid[userMove.row][userMove.col-3] == X &&
			board.grid[userMove.row][userMove.col]   == X &&
			board.rowCount[userMove.col-2]+1 == board.rowCount[userMove.col])
		{
			coordinate.row = userMove.row;
			coordinate.col = userMove.col-2;
			return (true);
		}
		// X X . * durumu
		if (board.grid[userMove.row][userMove.col-2] == X &&
			board.grid[userMove.row][userMove.col-3] == X &&
			board.grid[userMove.row][userMove.col]   == X &&
			board.rowCount[userMove.col-1]+1 == board.rowCount[userMove.col])
		{
			coordinate.row = userMove.row;
			coordinate.col = userMove.col-1;
			return (true);
		}
	}
	if (userMove.col - 1 >= 0) {
		// 	. X * X durumu
		if (board.grid[userMove.row][userMove.col+1] == X &&
			board.grid[userMove.row][userMove.col-1] == X &&
			board.grid[userMove.row][userMove.col]   == X &&
			board.rowCount[userMove.col-2]+1 == board.rowCount[userMove.col])
		{
			coordinate.row = userMove.row;
			coordinate.col = userMove.col-2;
			return (true);
		}

		// X X * . durumu
		if (board.grid[userMove.row][userMove.col-1] == X &&
			board.grid[userMove.row][userMove.col+2] == X &&
			board.grid[userMove.row][userMove.col]   == X &&
			board.rowCount[userMove.col+1]+1 == board.rowCount[userMove.col])
		{
			coordinate.row = userMove.row;
			coordinate.col = userMove.col+1;
			return (true);
		}
		if (board.grid[userMove.row][userMove.col-1] == X &&
			board.grid[userMove.row][userMove.col]   == X &&
			board.rowCount[userMove.col+1]+1 == board.rowCount[userMove.col])
		{
			coordinate.row = userMove.row;
			coordinate.col = userMove.col+1;
			return (true);
		}
	}
	if (userMove.col - 2 >= 0) {
		// X . * X durumu
		if (board.grid[userMove.row][userMove.col-2] == X &&
			board.grid[userMove.row][userMove.col+1] == X &&
			board.grid[userMove.row][userMove.col]   == X &&
			board.rowCount[userMove.col-1]+1 == board.rowCount[userMove.col])
		{
			coordinate.row = userMove.row;
			coordinate.col = userMove.col-1;
			return (true);
		}
	}
	return (false);
}
