package connectfour;

/**
 * Test class of Connect Four.
 * 
 * @author yusufakdas
 */
public class ConnectFourTest {  
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ConnectFour.createGame();
    }
}
