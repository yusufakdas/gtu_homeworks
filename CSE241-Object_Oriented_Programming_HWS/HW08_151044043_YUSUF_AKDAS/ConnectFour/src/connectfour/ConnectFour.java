package connectfour;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import java.awt.Color;
import java.awt.Container;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Random;

/**
 * <h1>Connect Four</h1>
 *
 * <p>This is the implementation of Connect Four game in Java using GUI.</p>
 *
 * @since 2018-01-04
 * @version 1.5
 * @author yusufakdas
 */
public class ConnectFour extends JFrame {
    /**
     * Game board size.e.g.4 X 4.
     */
    private int boardSize;

    /**
     * Pvp or Pvc.
     */
    private char gameType;

    /**
     * Playing order.0 - Player1, 1 - Player2, 3 - Computer.
     */
    private int order;

    /**
     * The column number of button choosen.
     */
    private int eventPos;

    /**
     * User's last move.
     */
    private Cell userMove = new Cell();

    /**
     * Keeps track of how the last checker's row number in cells.
     */
    private final int[] rowCount;

    /**
     * Connect Four game board.
     */
    private final Cell[][] gameCells;

    /**
     * Winner's four checkers' coordinates on the board.
     */
    private final Cell[] winnerCheckers;

    /**
     * Computer's last move.
     */
    private final Cell computerMove = new Cell();

    /**
     * User1's section.
     */
    private final JLabel user1;

    /**
     * User2's and computer's section.
     */
    private final JLabel user2;

    /**
     * Icon for User1.
     */
    private final ImageIcon[] icon1;

    /**
     * Icon for User2.
     */
    private final ImageIcon[] icon2;

    /**
     * No parameter constructor of ConnectFour class.The constructor does
     * so many tasks such as creating game board and panels, adding them to
     * container...
     */
    public ConnectFour() {
        super("ConnectFour");
        getSizeAndGameType();

        // Initializes the rowCount array.
        rowCount = new int[boardSize];
        for (int i = 0; i < boardSize; ++i)
            rowCount[i] = boardSize;

        // Initializes the winner checkers array.
        winnerCheckers = new Cell[4];
        for (int i = 0; i < 4; ++i)
            winnerCheckers[i] = new Cell();

        // Initilaizes the left and right status images.
        icon1 = new ImageIcon[2];
        icon2 = new ImageIcon[2];
        String[] pathIcon1 = new String[2];
        String[] pathIcon2 = new String[2];
        if (gameType == 'P') {
            pathIcon1[0] = "images/1.png";
            pathIcon1[1] = "images/1not.png";
            pathIcon2[0] = "images/2.png";
            pathIcon2[1] = "images/2not.png";
         } else {
            pathIcon1[0] = "images/cu.png";
            pathIcon1[1] = "images/cunot.png";
            pathIcon2[0] = "images/c.png";
            pathIcon2[1] = "images/cnot.png";
        }
        setIconForUser(icon1, pathIcon1);
        setIconForUser(icon1, pathIcon1);
        setIconForUser(icon2, pathIcon2);
        setIconForUser(icon2, pathIcon2);

        user1 = new JLabel();
        user2 = new JLabel();
        if (gameType == 'C')
            user1.setIcon(icon1[1]);
        else
            user1.setIcon(icon1[0]);
        user2.setIcon(icon2[1]);

        // Initializes the game board.
        gameCells = new Cell[boardSize][boardSize];
        for (int row = 0; row < boardSize; ++row) {
            for (int col = 0; col < boardSize; ++col) {
                gameCells[row][col] = new Cell();
                gameCells[row][col].getChecker().putClientProperty("c", col);
                gameCells[row][col].getChecker().putClientProperty("r", row);
                gameCells[row][col].setRowNumber(row);
                gameCells[row][col].setPosition(col);
            }
        }

        // Adds the buttons to a grid panel.
        JPanel table = new JPanel(new GridLayout(boardSize, boardSize, 2, 2));
        for (int row = 0; row < boardSize; ++row) {
            for (int col = 0; col < boardSize; ++col)
                table.add(gameCells[row][col].getChecker());
        }

        // Creates new panels.
        JPanel right = new JPanel(new BorderLayout());
        JPanel left = new JPanel(new BorderLayout());
        JPanel up = new JPanel(new BorderLayout());
        JPanel down = new JPanel(new BorderLayout());

        // Adds right and left status labels to panel.
        left.add(user2, BorderLayout.NORTH);
        right.add(user1, BorderLayout.NORTH);

        // Changes backgrounds
        right.setBackground(new Color(57, 53, 50));
        left.setBackground(new Color(57, 53, 50));
        up.setBackground(new Color(57, 53, 50));
        down.setBackground(new Color(57, 53, 50));
        table.setBackground(new Color(81, 53, 50));

        // Creates empty bordes for up and down panels
        down.setBorder(BorderFactory.createEmptyBorder(40, 40, 40, 40));
        up.setBorder(BorderFactory.createEmptyBorder(40, 40, 40, 40));

        // The container that holds all panels
        Container window = this.getContentPane();
        window.setLayout(new BorderLayout());
        window.add(left, BorderLayout.LINE_START);
        window.add(table, BorderLayout.CENTER);
        window.add(right, BorderLayout.LINE_END);
        window.add(up, BorderLayout.PAGE_START);
        window.add(down, BorderLayout.PAGE_END);
        window.setBackground((new Color(57, 40, 33)));

    }

    /**
     * Sets icons for JLabels.
     *
     * @param icon Icon array.
     * @param path String array for icon path
     */
    private void setIconForUser(ImageIcon[] icon, String[] path) {
        icon[0] = new ImageIcon(getClass().getResource(path[0]));
        Image image = icon[0].getImage();
        Image newImg = image.getScaledInstance(125, 200, Image.SCALE_SMOOTH);
        icon[0] = new ImageIcon(newImg);

        icon[1] = new ImageIcon(getClass().getResource(path[1]));
        image = icon[1].getImage();
        newImg = image.getScaledInstance(125, 200, Image.SCALE_SMOOTH);
        icon[1] = new ImageIcon(newImg);
    }

    /**
     * Plays the game for user for single time step.
     *
     * @param pos Position played by user.
     */
    private void play(int pos) {
        int r = rowCount[pos] - 1;

        if (isLegal(r, pos, 'e')) {
            if (order == 0) {
                gameCells[r][pos].setType('1');
                markButton(r, pos, Color.BLUE);
            } else {
                gameCells[r][pos].setType('2');
                markButton(r, pos, Color.RED);
            }
            if (gameType == 'P') { // Fixes order
                if (order == 0) {
                    order = 1;
                    user1.setIcon(icon1[1]);
                    user2.setIcon(icon2[0]);
                } else {
                    order = 0;
                    user1.setIcon(icon1[0]);
                    user2.setIcon(icon2[1]);
                }
            } else {
                order = 2;
            }
            userMove = gameCells[r][pos];
            --rowCount[pos];
        }
    }

    /**
     * Plays the game for computer for single time step.
     */
    private void play() {
        if (preventUserMoves())
            ;
        else
            determineComputerMoves();

        int r = computerMove.getRowNumber();  // ROW
        int c = computerMove.getPosition();   // COLUMN

        gameCells[r][c].setType('3');
        markButton(r, c, Color.RED);
        --rowCount[c];
        order = 0;
    }

    /**
     * Plays a whole game in pvp or pvc mode.
     *
     * @return If game is ended, returns true, otherwise false.
     */
    private boolean playGame() {
        boolean end = false;
        if (gameType == 'P') {
            play(eventPos);
            end = controlEnd();
        } else if (gameType == 'C') {
            play(eventPos);
            end = controlEnd();
            if (!end) {
                play();
                end = controlEnd();
            }
        }

        return (end);
    }

    /**
     * Marks button as used given color.
     *
     * @param r Row number of button.
     * @param c Column number of button
     * @param color Color that is used to mark button
     */
    private void markButton(int r, int c, Color color) {
        gameCells[r][c].getChecker().setBackground(color);
        gameCells[r][c].getChecker().setContentAreaFilled(true);
        gameCells[r][c].getChecker().setOpaque(true);
    }

    /**
     * Controls whether game ends or not.
     *
     * @return If game ends, returns true, otherwise false.
     */
    private boolean controlEnd() {
        Cell move = userMove;

    	// Checker control
    	if (gameType == 'C' && order == 0) // Fixes move cell
                move = computerMove;

    	if (checkDraw()) {
                order = -1;
                return (true);
    	}
    	if (checkVertical(move) || checkHorizontal(move) ||
                checkRightDiagonal(move) || checkLeftDiagonal(move)) {
                changeCheckers();
                if (gameType == 'P')  // Fixes order
                    order = (order == 0) ? 1 : 0; // from p1 to p2, vice versa.
                else
                    order = (order == 0) ? 2 : 0;
                return (true);
    	}
    	return (false);
    }

    /**
     * Change the color of winners' button colors to LIGHT_GRAY.
     */
    private void changeCheckers() {
        for (int i = 0; i < 4; ++i) {
            int r = winnerCheckers[i].getRowNumber();
            int c = winnerCheckers[i].getPosition();
            gameCells[r][c].getChecker().setBackground(Color.LIGHT_GRAY);
        }
    }

    /**
     * Checks game board vertically.
     *
     * @param move Move made.
     * @return If 4 row is detected as same color, then return true,
     *         otherwise false
     */
    private boolean checkVertical(Cell move) {
        boolean found = false;
        int checkerCount = 0;
        int c = move.getPosition();

        int j = 0;
        for (int i = 0; i < boardSize && !found; ++i) {
            if (isLegal(i, c, move.getType())) {
                winnerCheckers[j] = gameCells[i][c];
                ++checkerCount;
                ++j;
            } else {
                j = 0;
                checkerCount = 0;
            }
            if (checkerCount == 4)
                found = true;
        }
        return (found);
    }

    /**
     * Checks game board horizontally.
     *
     * @param move Move made.
     * @return If 4 row is detected as same color, then return true,
     *         otherwise false
     */
    private boolean checkHorizontal(Cell move) {
        boolean found = false;
        int checkerCount = 0;
        int r = move.getRowNumber();

        int j = 0;
        for (int i = 0; i < boardSize && !found; ++i) {
            if (isLegal(r, i, move.getType())) {
                winnerCheckers[j] = gameCells[r][i];
                ++checkerCount;
                ++j;
            } else {
                j = 0;
                checkerCount = 0;
            }
            if (checkerCount == 4)
                found = true;
        }
        return (found);
    }

    /**
     * Checks game board left diagonally.
     *
     * @param move Move made.
     * @return If 4 row is detected as same color, then return true,
     *         otherwise false
     */
    private boolean checkLeftDiagonal(Cell move) {
        boolean found = false, one = false, two = false;
        int checkerCount = 0;
        int c = move.getPosition();
        int r = move.getRowNumber();

        int j = 0;
        for (int i = 0; i < 4 && !found; ++i) {
            if (!one && r-i >= 0 && c-i >= 0) {
                if (isLegal(r-i, c-i, move.getType())) {
                    winnerCheckers[j] = gameCells[r-i][c-i];
                    ++checkerCount;
                    ++j;
                } else {
                    one = true;
                }
            }
            if (i != 0 && !two && r+i < boardSize && c+i >= 0) {
                if (isLegal(r+i, c+i, move.getType())) {
                    winnerCheckers[j] = gameCells[r+i][c+i];
                    ++checkerCount;
                    ++j;
                } else {
                    two = true;
                }
            }
            if (checkerCount == 4)
                found = true;
        }
        return (found);
   }

    /**
     * Checks game board right diagonally.
     *
     * @param move Move made.
     * @return If 4 row is detected as same color, then return true,
     *         otherwise false
     */
    private boolean checkRightDiagonal(Cell move) {
        boolean found = false, one = false, two = false;
        int checkerCount = 0;
        int c = move.getPosition();
        int r = move.getRowNumber();

        int j = 0;
        for (int i = 0; i < 4 && !found; ++i) {
            if (!one && r+i < boardSize && c-i >= 0) {
                if (isLegal(r+i, c-i, move.getType())) {
                    winnerCheckers[j] = gameCells[r+i][c-i];
                    ++checkerCount;
                    ++j;
                } else {
                    one = true;
                }
            }
            if (i != 0 && !two && r-i >= 0 && c+i < boardSize) {
                if (isLegal(r-i, c+i, move.getType())) {
                    winnerCheckers[j] = gameCells[r-i][c+i];
                    ++checkerCount;
                    ++j;
                } else {
                    two = true;
                }
            }
            if (checkerCount == 4)
                found = true;
        }
        return (found);
    }

    /**
     * Checks draw situation.
     *
     * @return If game ends in draw situation, then returns true,
     *         otherwise false.
     */
    private boolean checkDraw() {
        for (int i = 0; i < boardSize; ++i)
            if (isLegal(rowCount[i] - 1 , i, 'e'))
                return (false);
        return (true);
    }

    /**
     *
     * @param r Row number
     * @param c Column number
     * @param type Checker type
     * @return If it is legal, then return true, otherwise false.
     */
    private boolean isLegal(int r, int c, char type) {
        if (r >= 0 && c >= 0 && c < boardSize && r < boardSize)
            return (gameCells[r][c].getType() == type);
        return (false);
    }

    /**
     * Shows a dialog to inform user in end of game.
     */
    private String showResult() {
        String message;
        switch (order) {
            case 0:  message = "Player ONE has just won!"; break;
            case 1:  message = "Player TWO has just won!"; break;
            case 2:  message = "Computer has just won!"  ; break;
            default: message = "Nobody won this game!"   ; break;
        }
    }

    /**
     * Determines what computer's move will be.
     */
    private void determineComputerMoves() {
        Random rand = new Random();

        if (computerMove.getPosition() == -1) {
            int col = rand.nextInt(boardSize);
            computerMove.setRowNumber(rowCount[col]-1);
            computerMove.setPosition(col);
            computerMove.setType('3');
        }

        boolean found = findSpacePlus(computerMove.getType());
        if (!found)
            found = findSpaceDiagonal(computerMove.getType());

        boolean ok = false;
        for (int i = 0; i < boardSize && !found && !ok; ++i) {
            if (isLegal(rowCount[i], i, computerMove.getType())) {
                if (isLegal(rowCount[i]-1, i, 'e')) {
                    computerMove.setRowNumber(rowCount[i]-1);
                    computerMove.setPosition(i);
                    ok = true;
                }
            }
        }

        if (!ok && !found) {
            for (;;) {
                int col = rand.nextInt(boardSize);
                if (isLegal(rowCount[col]-1, col, 'e')) {
                    computerMove.setRowNumber(rowCount[col]-1);
                    computerMove.setPosition(col);
                    computerMove.setType('3');
                    break;
                }
            }
        }
    }

    /**
     * Search for horizontal or vertical place.
     *
     * @param check Checker type
     * @return If suitable place is found return true, otherwise false.
     */
    private boolean findSpacePlus(char check) {
        boolean found = false;

        // Counter for counting the same checker around
        int checkerCount = 0;

        // Checking around
        int newRow = -1, newCol = -1;
        int emptyCount = 0 ;
        for (int i = 0; i < boardSize && !found; ++i) {
            if (isLegal(rowCount[i], i, check)) {
                for (int j = 0; j < 4; ++j) {
                    if (isLegal(rowCount[i], i + j, check)) {
                        ++checkerCount;
                    } else if (isLegal(rowCount[i], i + j, 'e')) {
                        newRow = rowCount[i];
                        newCol = i + j;
                        ++emptyCount;
                    }
                }
                if (checkerCount == 3 && emptyCount == 1 &&
                    !isLegal(newRow + 1, newCol, 'e'))
                    found = true;
            }
            checkerCount = emptyCount = 0;
        }

        for (int i = 0; i < boardSize && !found; ++i) {
            if (isLegal(rowCount[i], i, check)) {
                for (int j = 0; j < 4; ++j) {
                    if (isLegal(rowCount[i] + j, i, check))
                        ++checkerCount;
                    else
                        break;
                    if (checkerCount == 3 && isLegal(rowCount[i] - 1, i, 'e')) {
                        newRow = rowCount[i] - 1;
                        newCol = i;
                        found = true;
                    }
                }
            }
            checkerCount = 0;
        }

        if (found) {
            computerMove.setRowNumber(newRow);
            computerMove.setPosition(newCol);
        }

        return (found);
    }

    /**
     * Search for diagonal place.
     *
     * @param check Checker type
     * @return If suitable place is found return true, otherwise false.
     */
    private boolean findSpaceDiagonal(char check) {
        boolean found = false;

        // Counter for counting the same checker around
        int checkerCount = 0;

        // Checking around
        int newRow = -1, newCol = -1;
        int emptyCount = 0 ;
        for (int i = 0; i < boardSize && !found; ++i) {
            if (isLegal(rowCount[i], i, check)) {
                for (int j = 0; j < 4; ++j) {
                    if (isLegal(rowCount[i] - j, i + j, check)) {
                        ++checkerCount;
                    } else if (isLegal(rowCount[i] - j, i + j, 'e')) {
                        newRow = rowCount[i] - j;
                        newCol = i + j;
                        ++emptyCount;
                    }
                    if (checkerCount == 3 && emptyCount == 1 &&
                        !isLegal(newRow + 1, newCol, 'e'))
                        found = true;
                }
            }
            checkerCount = emptyCount = 0;
        }

        for (int i = 0; i < boardSize && !found; ++i) {
            if (isLegal(rowCount[i], i, check)) {
                for (int j = 0; j < 4; ++j) {
                    if (isLegal(rowCount[i] - j, i - j, check)) {
                        ++checkerCount;
                    } else if (isLegal(rowCount[i] - j, i - j, 'e')) {
                        newRow = rowCount[i] - j;
                        newCol = i - j;
                        ++emptyCount;
                    }
                    if (checkerCount == 3 && emptyCount == 1 &&
                        !isLegal(newRow + 1, newCol, 'e'))
                        found = true;
                }
            }
            checkerCount = emptyCount = 0;
        }

        for (int i = 0; i < boardSize && !found; ++i) {
            if (isLegal(rowCount[i], i, check)) {
                for (int j = 0; j < 4; ++j) {
                    if (isLegal(rowCount[i] + j, i - j, check)) {
                        ++checkerCount;
                    } else if (isLegal(rowCount[i] + j, i - j, 'e')) {
                        newRow = rowCount[i] + j;
                        newCol = i - j;
                        ++emptyCount;
                    }
                    if (checkerCount == 3 && emptyCount == 1 &&
                        !isLegal(newRow + 1, newCol, 'e'))
                        found = true;
                }
            }
            checkerCount = emptyCount = 0;
        }

        for (int i = 0; i < boardSize && !found; ++i) {
            if (isLegal(rowCount[i], i, check)) {
                for (int j = 0; j < 4 && !found; ++j) {
                    if (isLegal(rowCount[i] + j, i + j, check)) {
                        ++checkerCount;
                    } else if (isLegal(rowCount[i] + j, i + j, 'e')) {
                        newRow = rowCount[i] + j;
                        newCol = i + j;
                        ++emptyCount;
                    }
                    if (checkerCount == 3 && emptyCount == 1 &&
                        !isLegal(newRow + 1, newCol, 'e'))
                        found = true;
                }
            }
            checkerCount = emptyCount = 0;
        }

        if (found) {
            computerMove.setRowNumber(newRow);
            computerMove.setPosition(newCol);
        }

        return (found);
    }

    /**
     * Prevents user to win the game for computer vs player mode.
     *
     * @return if user is about to win, return true, otherwise false.
     */
    private boolean preventUserMoves() {
        boolean found = findSpacePlus(userMove.getType());
        if (!found)
            found = findSpaceDiagonal(userMove.getType());
        return (found);
    }

    /**
     * Creates the game
     */
    public static void createGame() {
        ConnectFour game = new ConnectFour();
        game.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        game.setSize(new Dimension(825, 825));
        game.setLocationRelativeTo(null);
        game.setTitle("ConnectFour");
        game.setResizable(false);
        game.setVisible(true);
    }

    /**
     * The cell class that holds a cell for game board.
     */
    private class Cell {
        /**
         * Position of cell.
         */
        private int position = -1;

        /**
         * Row number of cell.
         */
        private int rowNumber = -1;

        /**
         * Button for a checker.
         */
        private final JButton checker;

        /**
         * Initializes the checker type as empty.
         */
        char type = 'e';

        /**
         * No parameter constructor of Cell class that initializes
         * checker(button).
         */
        public Cell() {
            // Creates the JButton.
            checker = new JButton();

            // Add action listeners the buttons
            checker.addActionListener(new CellHandler());

            checker.setOpaque(true);
            checker.setContentAreaFilled(true);
            checker.setBackground(new Color(57, 40, 50));
            checker.setBorderPainted(true);
        }

        /**
         * Accessor method for cell position.
         *
         * @return Position.
         */
        public int getPosition() {
            return (position);
        }

        /**
         * Accessor method for row number.
         *
         * @return Row number.
         */
        public int getRowNumber() {
            return (rowNumber);
        }

        /**
         * Accessor method for checker type.
         *
         * @return Checker type.
         */
        public char getType() {
            return (type);
        }

        /**
         * Accessor method for checker.
         *
         * @return Reference to button on the game board.
         */
        public JButton getChecker() {
            return (checker);
        }

        /**
         * Mutator method for position.
         *
         * @param pos New position.
         */
        void setPosition(int pos) {
            position = pos;
        }

        /**
         * Mutator method for row number.
         *
         * @param row New row number.
         */
        void setRowNumber(int row) {
            rowNumber = row;
        }

        /**
         * Mutator method for checker type.
         *
         * @param t New checker type.
         */
        void setType(char t) {
            type = t;
        }

    }

    /**
     * Handler class for button events.
     */
    private class CellHandler implements ActionListener {
        /**
         * If button is clicked, this method is called.In addition this method
         * plays the game by calling playGame method actually.Eventually if game
         * ends, creates new game
         *
         * @param event Button event.
         */
        @Override
        public void actionPerformed(ActionEvent event) {
            JButton button = (JButton) event.getSource();
            eventPos = Integer.parseInt(button.getClientProperty("c").toString());
            if (playGame()) {
                ConnectFour.this.showResult();
                ConnectFour.this.dispose();
                ConnectFour.createGame();
            }
        }
    }
}
