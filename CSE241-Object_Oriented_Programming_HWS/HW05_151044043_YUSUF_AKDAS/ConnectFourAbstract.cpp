/* ConnectFourAbstract.cpp */ 

#include "ConnectFourAbstract.h"

namespace TheConnectFourGame {
	
	/*
	 * Constructor that creates a 5x5 game board as default.
	 */
	ConnectFourAbstract::ConnectFourAbstract() : row(5), column(5), order(0),
							     				 gameType('P'), filename("")
	{
		userMove = Cell(EMPTY);
		computerMove = Cell(EMPTY);

		// Allocates memory using allocate function.
		allocate();

		// Initializes
		init();
	}
	
	/*
	 * Constructor that takes row and column.
	 */
	ConnectFourAbstract::ConnectFourAbstract(int r, int c) : row(r), column(c), 
										  order(0), gameType('P'), filename("")
	{
		userMove = Cell(EMPTY);
		computerMove = Cell(EMPTY);

		// Allocates memory using allocate function.
		allocate();

		// Initializes
		init();
	}
	
	/*
	 * Copy constructor for ConnectFour class
	 */
	ConnectFourAbstract::ConnectFourAbstract(const ConnectFourAbstract& c)
	{
		row = c.row;
		column = c.column;
		gameType = c.gameType;
		order = c.order;
		filename = c.filename;
		computerMove = c.computerMove;
		userMove = c.userMove;

		allocate();

		// Copies gameBoard from copy to this
		for (int i = 0; i < row; ++i)
			for (int j = 0; j < column; ++j)
				gameBoard[i][j] = c.gameBoard[i][j];

		// Copies lastRow from copy to this object
		for (int i = 0; i < column; ++i)
			lastRow[i] = c.lastRow[i];

		// Copies winnerCheckers array from copy to this object
		for (int i = 0; i < 4; ++i)
			winnerCheckers[i] = c.winnerCheckers[i];
	}
	
	/*
	 * Destruct the object when end of scope is reached
	 */
	ConnectFourAbstract::~ConnectFourAbstract()
	{
		deallocate();
	}


	/*
	 * Assignment operator for ConnectFour class
	 */
	ConnectFourAbstract& ConnectFourAbstract::operator =
										  (const ConnectFourAbstract& rightSide)
	{
		// Controls that the incoming object does not equal to this object
		if (this != &rightSide) {
			// Does deep copy
			deallocate();             // Deallocates this object's game board

			row = rightSide.row;
			column = rightSide.column;
			gameType = rightSide.gameType;
			order = rightSide.order;
			filename = rightSide.filename;
			computerMove = rightSide.computerMove;
			userMove = rightSide.userMove;

			allocate();				  // Allocates the game board array

			// Copies gameBoard from rightSide to this object
			for (int i = 0; i < row; ++i)
				for (int j = 0; j < column; ++j)
					gameBoard[i][j] = rightSide.gameBoard[i][j];

			// Copies lastRow array from rightSide to this object
			for (int i = 0; i < column; ++i)
				lastRow[i] = rightSide.lastRow[i];

			// Copies winnerCheckers array from rightSide to this object
			for (int i = 0; i < 4; ++i)
				winnerCheckers[i] = rightSide.winnerCheckers[i];
		}
		return (*this);
	}
	
	/*
	 * Prints whole game board on the screen
	 */
	void ConnectFourAbstract::printGameBoard() const
	{
		// Letter column
		for (int i = 0; i < column; ++i)
			cout << static_cast<char> ('A' + i) << ' ';
		cout << endl;

		// Game board
		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < column; ++j)
				cout << gameBoard[i][j] << ' ';
			cout << endl;
		}
		cout << endl;
	}
	
	/*
	 * Allocates memory row x column
	 */
	void ConnectFourAbstract::allocate()
	{
		// Allocates memory spaces for array that keeps last row numbers
		lastRow = new int[column];

		// Allocates memory space for rows
		gameBoard = new Cell*[row];

		// Allocates memory space for column and
		for (int i = 0; i < row; ++i)
			gameBoard[i] = new Cell[column];

		// Allocates memory space for winner's checkers.
		winnerCheckers = new Cell[4];
	}
	
	/*
	 * Deallocates memory row x column
	 */
	void ConnectFourAbstract::deallocate()
	{
		// Deallocate arrays inside
		for (int i = 0; i < row; ++i)
			delete[] gameBoard[i];

		// Deallocate array of pointer arrays
		delete[] gameBoard;

		// Deallocate array of winners' checkers
		delete[] winnerCheckers;

		// Deallocate the lastRow array
		delete[] lastRow;

		// Prevents from dangling pointer
		gameBoard = nullptr;
		winnerCheckers = nullptr;
		lastRow = nullptr;
	}
	
	/*
	 * Initializes the connect four game board
	 */
	void ConnectFourAbstract::init() 
	{
		// Game board.
		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < column; ++j) {
				gameBoard[i][j].setType(EMPTY);
				gameBoard[i][j].setRowNumber(i);
				gameBoard[i][j].setPosition(static_cast<char> (j + 'A'));
			}
		}

		// Last Row
		for (int i = 0; i < column; ++i)
			lastRow[i] = row;
	}

	/*
	 * Resizes the game according to given row and column number
	 */
	void ConnectFourAbstract::resize()
	{
		allocate();
		init();
	}
	
	/*
	 * Returns the row number
	 */
	int ConnectFourAbstract::getRowNumber() const
	{
		return (row);
	}

	/*
	 * Returns the column number
	 */
	int ConnectFourAbstract::getColumnNumber() const
	{
		return (column);
	}

	/*
	 * Returns the game type (P, C)
	 */
	char ConnectFourAbstract::getGameType() const
	{
		return (gameType);
	}

	/*
	 * Returns player order
	 * 	  "0 -> PLAYER1",  " 1 -> PLAYER2",
	 *	  "2 -> COMPUTER", "-1 -> INVALID"
	 */
	int ConnectFourAbstract::getPlayerOrder() const
	{
		return (order);
	}
	
	/*
	 * Control whether a column is legal or not;
	 */
	bool ConnectFourAbstract::isLegal(int r, int c, Checker check) const
	{
		if (r >= 0 && c >= 0 && c < column && r < row)
			return (gameBoard[r][c].getType() == check);
		return (false);
	}
	
	/*
	 * Plays the game for user for single time step
	 */
	bool ConnectFourAbstract::play(char pos)
	{
		int c = static_cast <int> (pos - 'A');
		int r = lastRow[c] - 1;
		bool value = false;
	
		if (isLegal(r, c, EMPTY)) {
			if (order == 0)  // Player 1
				gameBoard[r][c].setType(PLAYER1);
			else			 // Player 2
				gameBoard[r][c].setType(PLAYER2);
			userMove = gameBoard[r][c];
			--lastRow[c];
			if (gameType == 'P')  // Fixes order
				order = (order == 0) ? 1 : 0; // from p1 to p2, vice versa.
			else
				order = 2;
			value = true;
		} else {
			cerr << "#### You cannot move on this column."
				 << " Choose another one! ####" << endl;
			value = false;
		}
		return (value);
	}

	/*
	 * Plays the game for computer for single time step
	 */
	bool ConnectFourAbstract::play()
	{
		int c, r;

		if (preventUserMoves())
			;
		else
			determineComputerMoves();

		r = computerMove.getRowNumber();						 // ROW
		c = static_cast<int> (computerMove.getPosition() - 'A'); // COLUMN

		cout << "Computer has made a move." << endl;
		gameBoard[r][c].setType(COMP);
		--lastRow[c];
		order = 0;
		return (true);
	}

	/*
	 * Plays a whole game in pvp or pvc mode by taking a filename
	 */
	void ConnectFourAbstract::playGame()
	{

		// Resize function (for filename) is called in this function.
		getGameTypeAndSizes();

		bool end = false;
		while (!end) {
			int get;
			string input;
			char position;
			printGameBoard();
			if (order != 2)
				get = getInput(input, position);
			if (get == 1)		// SAVE mode
				saveTheGame();
			else if (get == 2)  // LOAD mode
				loadTheGame();	
			if (gameType == 'P' && get == 0) {
				play(position);
				preventUserMoves();
				end = controlEnd();
			} else if (gameType == 'C' && get == 0) {
				if (order == 0)
					play(position);
				else
					play();
				end = controlEnd();
			}
		}
		printGameBoard();
		showResult();
	}
	
	/*
	 * Displays who wins the game at end of the game
	 */
	void ConnectFourAbstract::showResult() const
	{
		if (order == 0)
			cout << "Player ONE has just won!" << endl;
		else if (order == 1)
			cout << "Player TWO has just won!" << endl;
		else if (order == 2)
			cout << "Computer has just won!" << endl;
		else
			cout << "Nobody won this game!" << endl;
	}
	
	/*
	 * Checks whether game ends in draw
	 */
	bool ConnectFourAbstract::checkDraw() const
	{
		for (int i = 0; i < column; ++i)
			if (isLegal(lastRow[i] - 1 , i, EMPTY))
				return (false);
		return (true);
	}
	
	/*
	 * Make winner's checker's small at the end of the game
	 */
	void ConnectFourAbstract::changeCheckers()
	{
		Checker check = winnerCheckers[0].getType();

		for (int i = 0; i < 4; ++i) {
			int r = winnerCheckers[i].getRowNumber();
			int c = static_cast<int> (winnerCheckers[i].getPosition() - 'A');
			if (check == PLAYER1) {
				gameBoard[r][c].setType(WINX);
			} else if (check == PLAYER2 || check == COMP) {
				gameBoard[r][c].setType(WINO);
			}
		}
	}
	
	/*
	 * Gets user moves and commands.
	 * Returns values: "0 --> MOVE", "1 --> SAVE", "2 --> LOAD", "-1 --> INVALID"
	 */
	int ConnectFourAbstract::getInput(string& request, char& letter)
	{
		const char MIN = 'A';
		const char MAX = static_cast <char> ('A' + column - 1);
		int retValue = -1; // Default return value is -1 (invalid).
		if (order == 0)
			cout << "P1: ";
		else
			cout << "P2: ";

		if (!getline(cin, request)) {
			cerr << endl << "#### END OF FILE REACHED ####" << endl;
			exit(1);
		}
		if (request.length() == 1) {
			letter = request[0];
			if (letter >= MIN && letter <= MAX)
				retValue = 0; // MOVE return value
			else
				cerr << "#### You should enter a column between '" << MIN
					 << "' and '" << MAX << "'! ####" << endl;
		} else if (request.compare(0, 4, "SAVE") == 0 && request.length() > 5) {
			getFileName(request);
			retValue = 1;  // SAVE return value
		} else if (request.compare(0, 4, "LOAD") == 0 && request.length() > 5) {
			getFileName(request);
			retValue = 2;  // LOAD return value
		} else {
			cerr << "#### Enter a column letter or to save the game "
				 << "'SAVE filename' and to load a game 'LOAD filename' ####"
				 << endl;
		}
		return (retValue);
	}

	/*
	 * Splits input and get file name from it
	 */
	void ConnectFourAbstract::getFileName(const string& request)
	{
		filename = "";
		for (unsigned int i = 5; i < request.length(); ++i)
			if (request[i] != ' ')
				filename += request[i];
	}


	/*
	 * Gets game type and file name from user
	 */ 
	void ConnectFourAbstract::getGameTypeAndSizes()
	{
		bool valid = false;

		// Deallocate the previous game board.
		deallocate();

		// Gets width and height until it is valid.
		do {
			cout << "Enter width and height for game board" << endl;
			cout << "Enter width> ";
			cin >> column;
			cout << "Enter height> ";
			cin >> row;

			if (!cin || column < 4 || row < 4) {
				cerr << endl << "#### Width and height must be greater "
					 << "than 4. ####" << endl;
				cin.clear();
				cin.ignore(256, '\n');
			} else {
				valid = true;
			}
		} while (!valid);

		// Resizes the gameboard
		resize();

		// Gets game type until it is valid.
		valid = false;
		do {
			cout << "Player | Computer> ";
			cin >> gameType;
			emptyBuffer();
			if (gameType == 'P' || gameType == 'C')
				valid = true;
			else
				cerr << "#### Invalid game type. Enter 'P' or 'C'. ####" << endl;
		} while (!valid);
	}

	/*
	 * Loads game from given file
	 */
	bool ConnectFourAbstract::loadTheGame()
	{
		ifstream inputStream(filename.c_str());

		// Controls whether the file is opened.
		if (inputStream.fail()) {
			cerr << "#### " << filename << " could not be opened! ####" << endl;
			return (false);
		}

		// Reads row and column number values to tmp variables.
		int tmpRow, tmpCol;
		inputStream >> tmpRow >> tmpCol;

		// Controls whether file is empty or not.
		if (inputStream.eof()) {
			cerr << "#### " << filename << " might be empty! ####" << endl;
			return (false);
		}

		// Cleans gameBoard, lastRow and winnerCheckers arrays.
		deallocate();

		// Assigns temporary variables to permanent variables.
		row = tmpRow;
		column = tmpCol;

		// Creates clean gameBoard, lastRow and winnerCheckers arrays.
		allocate();

		// Reads game type.
		inputStream >> gameType;

		// Reads user order.
		inputStream >> order;

		// Reads lastRow array.
		for (int i = 0; i < column; ++i)
			inputStream >> lastRow[i];

		// Reads gameBoard
		char pos;
		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < column; ++j) {
				int get;
				pos = static_cast<char> ('A' + j);
				inputStream >> get;
				gameBoard[i][j].setRowNumber(i);
				gameBoard[i][j].setPosition(pos);
				switch (get) {
					case 0: gameBoard[i][j].setType(EMPTY);   break;
					case 1: gameBoard[i][j].setType(PLAYER1); break;
					case 2: gameBoard[i][j].setType(PLAYER2); break;
					case 3: gameBoard[i][j].setType(COMP);	  break;
				}
			}
		}

		// Reads user moves.
		inputStream >> tmpRow >> pos;
		userMove.setRowNumber(tmpRow);
		userMove.setPosition(pos);

		// Determines user checker type.
		if (order == 0 || order == 2)
			userMove.setType(PLAYER1);
		else if (order == 1)
			userMove.setType(PLAYER2);

		// If game type is PvC, then reads computer moves.
		if (gameType == 'C') {
			inputStream >> tmpRow >> pos;
			computerMove.setRowNumber(tmpRow);
			computerMove.setPosition(pos);
			computerMove.setType(COMP);
		}

		// Closes the file
		inputStream.close();

		// Prints informative message on the screen
		cout << "--------------------------------------------------" << endl;
		cout << "**** Loading the game from " << filename << "... ****" << endl;
	
		return (true);
	}

	/*
	 * Saves game to given file
	 */
	bool ConnectFourAbstract::saveTheGame() const
	{
		ofstream outputStream(filename.c_str());

		// Controls whether file is opened
		if (outputStream.fail()) {
			cerr << "#### " << filename << " could not be opened! ####" << endl;
			return (false);
		}

		// Board row and column are written
		outputStream << row << ' ' << column << endl << endl;

		// Gametype is written
		outputStream << gameType << endl << endl;

		// User order is written
		outputStream << order << endl << endl;

		// Last row counter is written
		for (int i = 0; i < column; ++i)
			outputStream << lastRow[i] << ' ';
		outputStream << endl;

		// Game board is written
		outputStream << endl;
		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < column; ++j) {
				if (gameBoard[i][j].getType() == EMPTY)
					outputStream << 0 << ' ';
				else if (gameBoard[i][j].getType() == PLAYER1)
					outputStream << 1 << ' ';
				else if (gameBoard[i][j].getType() == PLAYER2)
					outputStream << 2 << ' ';
				else if (gameBoard[i][j].getType() == COMP)
					outputStream << 3 << ' ';
			}
			outputStream << endl;
		}
		outputStream << endl;

		// User move is written
		outputStream << userMove.getRowNumber() << ' ' << userMove.getPosition()
	 				 << endl;

		// If game type is computer, computer move is written too
		if (gameType == 'C') {
			outputStream << endl;
			outputStream << computerMove.getRowNumber() << ' '
	 					 << computerMove.getPosition();
			outputStream << endl;
		}

		// Closes the file
		outputStream.close();

		// informative message to screen
		cout << "--------------------------------------------------" << endl;
		cout << "**** Current game state has been saved to " << filename 
			 << "! ****" << endl;
			 
		return (true);
	}

	/*
	 * Empties buffer
	 */
	void ConnectFourAbstract::emptyBuffer() const {
		while (cin.get() != '\n')
			;
	}

	// Prevent user from win the game
	bool ConnectFourAbstract::preventUserMoves() 
	{
		bool found = false;
		
		found = findSpacePlus(userMove.getType());
		if (!found)
			found = findSpaceDiagonal(userMove.getType());

		return (found);
	}

	// Determine the move of computer
	void ConnectFourAbstract::determineComputerMoves()
	{	
		srand(time(NULL));
		bool found = false;
		
		if (computerMove.getPosition() == '\0') {
			int col = rand() % column;
			computerMove.setRowNumber(lastRow[col]-1);
			computerMove.setPosition(col + 'A');
			computerMove.setType(COMP);
		}

		found = findSpacePlus(computerMove.getType());
		if (!found) 
			found = findSpaceDiagonal(computerMove.getType());
		
		bool ok = false;	
		for (int i = 0; i < column && !found && !ok; ++i) {
			if (isLegal(lastRow[i], i, computerMove.getType())) {
				if (isLegal(lastRow[i]-1, i, EMPTY)) {
					computerMove.setRowNumber(lastRow[i]-1);
					computerMove.setPosition(i + 'A');
					ok = true;
				}
			}
		}

		if (!ok && !found) {
			for (;;) {
				int col = rand() % column;
				if (isLegal(lastRow[col]-1, col, EMPTY)) {
					computerMove.setRowNumber(lastRow[col]-1);
					computerMove.setPosition(col + 'A');
					computerMove.setType(COMP);
					break;
				}
			}
		}
	}
	
	bool ConnectFourAbstract::findSpacePlus(Checker check) 
	{
		bool found = false;

		// Counter for counting the same checker around
		int checkerCount = 0;
		
		// Checking around
		int newRow = -1, newCol = -1;
		int emptyCount = 0 ;
		for (int i = 0; i < column && !found; ++i) {
			if (isLegal(lastRow[i], i, check)) {
				for (int j = 0; j < 4; ++j) {
					if (isLegal(lastRow[i], i + j, check)) {
						++checkerCount;
					} else if (isLegal(lastRow[i], i + j, EMPTY)) {
						newRow = lastRow[i];
						newCol = i + j;
						++emptyCount;
					}
				}
				if (checkerCount == 3 && emptyCount == 1 && 
					!isLegal(newRow + 1, newCol, EMPTY))
					found = true;
			}
			checkerCount = emptyCount = 0;
		}

		for (int i = 0; i < column && !found; ++i) {
			if (isLegal(lastRow[i], i, check)) {
				for (int j = 0; j < 4; ++j) {
					if (isLegal(lastRow[i] + j, i, check))
						++checkerCount;
					else 
						break;
					if (checkerCount == 3 && isLegal(lastRow[i] - 1, i, EMPTY)) {
						newRow = lastRow[i] - 1;
						newCol = i;
						found = true;	
					}
				}
			}
			checkerCount = 0;
		}

		if (found) {
			computerMove.setRowNumber(newRow);
			computerMove.setPosition(static_cast <char> ('A' + newCol));
		}

		return (found);
	}
	
	bool ConnectFourAbstract::findSpaceDiagonal(Checker check) 
	{
		bool found = false;
		
		// Counter for counting the same checker around
		int checkerCount = 0;
		
		// Checking around
		int newRow = -1, newCol = -1;
		int emptyCount = 0 ;
		for (int i = 0; i < column && !found; ++i) {
			if (isLegal(lastRow[i], i, check)) {
				for (int j = 0; j < 4; ++j) {
					if (isLegal(lastRow[i] - j, i + j, check)) { 
						++checkerCount;
					} else if (isLegal(lastRow[i] - j, i + j, EMPTY)) {
						newRow = lastRow[i] - j;
						newCol = i + j;
						++emptyCount;
					}
					if (checkerCount == 3 && emptyCount == 1 && 
						!isLegal(newRow + 1, newCol, EMPTY))
						found = true;
				}
			}
			checkerCount = emptyCount = 0;
		}

		for (int i = 0; i < column && !found; ++i) {
			if (isLegal(lastRow[i], i, check)) {
				for (int j = 0; j < 4; ++j) {
					if (isLegal(lastRow[i] - j, i - j, check)) { 
						++checkerCount;
					} else if (isLegal(lastRow[i] - j, i - j, EMPTY)) {
						newRow = lastRow[i] - j;
						newCol = i - j;
						++emptyCount;
					}
					if (checkerCount == 3 && emptyCount == 1 && 
						!isLegal(newRow + 1, newCol, EMPTY))
						found = true;
				}
			}
			checkerCount = emptyCount = 0;
		}

		for (int i = 0; i < column && !found; ++i) {
			if (isLegal(lastRow[i], i, check)) {
				for (int j = 0; j < 4; ++j) {
					if (isLegal(lastRow[i] + j, i - j, check)) { 
						++checkerCount;
					} else if (isLegal(lastRow[i] + j, i - j, EMPTY)) {
						newRow = lastRow[i] + j;
						newCol = i - j;
						++emptyCount;
					}
					if (checkerCount == 3 && emptyCount == 1 && 
						!isLegal(newRow + 1, newCol, EMPTY))
						found = true;
				}
			}
			checkerCount = emptyCount = 0;
		}

		for (int i = 0; i < column && !found; ++i) {
			if (isLegal(lastRow[i], i, check)) {
				for (int j = 0; j < 4 && !found; ++j) {
					if (isLegal(lastRow[i] + j, i + j, check)) { 
						++checkerCount;
					} else if (isLegal(lastRow[i] + j, i + j, EMPTY)) {
						newRow = lastRow[i] + j;
						newCol = i + j;
						++emptyCount;
					}
					if (checkerCount == 3 && emptyCount == 1 && 
						!isLegal(newRow + 1, newCol, EMPTY))
						found = true;
				}
			}
			checkerCount = emptyCount = 0;
		}

		if (found) {
			computerMove.setRowNumber(newRow);
			computerMove.setPosition(static_cast <char> ('A' + newCol));
		}

		return (found);
	}
}
