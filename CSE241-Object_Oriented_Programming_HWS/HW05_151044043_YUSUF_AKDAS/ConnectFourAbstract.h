/* ConnectFourAbstract.h */

#ifndef __CONNECT_FOUR_ABSTRACT_H__
#define __CONNECT_FOUR_ABSTRACT_H__

#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include "Cell.h"

using std::cout;
using std::cin;
using std::cerr;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::getline;
using std::string;

namespace TheConnectFourGame {
	
	class ConnectFourAbstract {
	public:

	    // No parameter constructor. Default size 5X5.
  		ConnectFourAbstract();
	
	 	// Constructor that takes one parameter.
		ConnectFourAbstract(int size);
	
		// Constructor that takes row and column number
    	ConnectFourAbstract(int r, int c);

		// Copy Constructor for ConnectFourAbstract class
    	ConnectFourAbstract(const ConnectFourAbstract& c);

		// Assingment operator for ConnectFourAbstract class
		ConnectFourAbstract& operator =(const ConnectFourAbstract& rightSide);
		
		// Desructor that deallocates memory
		~ConnectFourAbstract();
	
		// Accessor Functions
		int getRowNumber() const;
		int getColumnNumber() const;
		char getGameType() const;
		int getPlayerOrder() const;

		// Plays a whole game in pvp or pvc mode
		void playGame();

	protected:  
		
		// Controls end of the game (PURE VIRTUAL) (NEW)
		virtual bool controlEnd() = 0;
		
		// Initializes game board
		void init();
		
		// Prevent user to win the game
		bool preventUserMoves();

		// Determine the move of computer
		void determineComputerMoves();

		// Finds an empty space for specified checker (PLUS)
		bool findSpacePlus(Checker check);

		// Finds an empty space for specified checker (DIAGONAL)
		bool findSpaceDiagonal(Checker check);
		
		// For memory allocation operations in the class
		void allocate();

		// For memory deallocation operations in the class
		void deallocate();
		
		// Overloads resize function according to row and column number
		void resize();

		// Plays the game for user for single time step.
		bool play(char pos);

		// Plays the game for computer for single time step
		bool play();

		// The function to print game board on the screen
		void printGameBoard() const;

		// Controls draw
		bool checkDraw() const;		
				
		// Make checkers small in end of the game
		void changeCheckers();
		
		// Prints informative message on the screen at end of the game
		void showResult() const;

		// Gets game type and and filename from user
		void getGameTypeAndSizes();
		
		// Empties the buffer
		void emptyBuffer() const;
		
		// Loads game from file
		bool loadTheGame();

		// Saves game to file
		bool saveTheGame() const;
		
		// Split and get the file name 
		void getFileName(const string& request);
	
		// Gets input and commands (LOAD or SAVE) from user
		int getInput(string& request, char& letter);

		// Determines whether given cell is valid or not.
		bool isLegal(int r, int c, Checker check) const;
	
		// Keeps track of how the last checker's row number in cells
		int *lastRow;

		// Winner's four checkers' coordinates on the board
		Cell *winnerCheckers;
		
		// ****Connect Four game board (2d dynamic array)****
		Cell **gameBoard;

		// Last move made by user
		Cell userMove;

		// Last move made by computer
		Cell computerMove;

		int row; 		 		// Row number of the board.
		int column; 	 		// Column number of the board.
		int order;				// "0 --> P1(X)", "1 --> P2(O)", "2 --> C(O)"
		char gameType;			// "'P' --> PvP", "'C' --> PvC"
		string filename; 		// Filename
	};
}

#endif
