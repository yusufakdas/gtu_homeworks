/* Cell.h */

#ifndef __CELL_H__
#define __CELL_H__

#include <iostream>

using std::ostream;

namespace TheConnectFourGame {
		
	// Checker types
	enum Checker { EMPTY, PLAYER1, PLAYER2, COMP, WINX, WINO };	
	
	class Cell {
	public:

		// No parameter constructor
		Cell();

		// Constructor that takes type as parameter
		Cell(Checker t);

		// Accessor Functions
		char getPosition() const;
		int getRowNumber() const;
		Checker getType() const;

		// Mutator Functions
		void setPosition(char p);
		void setRowNumber(int r);
		void setType(Checker t);

		// Compare two cells each other
		bool operator ==(const Cell& other) const;
		bool operator !=(const Cell& other) const;

		// Extraction operator for Cell class
		friend ostream& operator <<(ostream& outStream, const Cell& cell);

	private:

		char position; 		// Position of a cell
		int rowNumber;      // Row number of a cell
		Checker type;		// X or O
	};
	
}
#endif
