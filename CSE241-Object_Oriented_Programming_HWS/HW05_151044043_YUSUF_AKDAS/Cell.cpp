/* Cell.cpp */

#include "Cell.h"

namespace TheConnectFourGame {
 	
 	/*
 	 * Cell's no parameter constructor.
 	 */
	Cell::Cell() : position('\0'), rowNumber(0),
				   type(EMPTY)
	{/* Intentionally left blank */}
	
	/*
	 * Cell constructor that takes checker
	 */
	Cell::Cell(Checker t) : position('\0'), rowNumber(0), type(t)
	{/* Intentionally left blank */}

	/*
	 * Compare two cells each other
	 */
	bool Cell::operator ==(const Cell& other) const
	{
		return (position == other.position && rowNumber == other.rowNumber &&
				type == other.type);
	}
	
	/*
	 * Compare two cells each other
	 */
	bool Cell::operator !=(const Cell& other) const
	{
		return (!(*this == other));
	}
	
	/*
	 * Returns the current position of the cell
	 */
	char Cell::getPosition() const
	{
		return (position);
	}

	/*
	 * Returns the current row number of the cell
	 */
	int Cell::getRowNumber() const
	{
		return (rowNumber);
	}

	/*
	 * Returns the type of this cell
	 */
	Checker Cell::getType() const
	{
		return (type);
	}

	/*
	 * Sets position according to p
	 */
	void Cell::setPosition(char p)
	{
		position = p;
	}

	/*
	 * Sets row number according to r
	 */
	void Cell::setRowNumber(int r)
	{
		rowNumber = r;
	}

	/*
	 * Sets checker type according to t
	 */
	void Cell::setType(Checker t)
	{
		type = t;
	}

	/*
	 * Prints one cell on the screen
	 */
	ostream& operator <<(ostream& outStream, const Cell& cell)
	{
		switch (cell.getType()) {
			case EMPTY   : outStream << '.'; break;
			case PLAYER1 : outStream << 'X'; break;
			case PLAYER2 : outStream << 'O'; break;
			case COMP    : outStream << 'O'; break;
			case WINX    : outStream << 'x'; break;
			case WINO    : outStream << 'o'; break;
		}
		return (outStream);
	}
}
