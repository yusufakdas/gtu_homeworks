/* ConnectFourDiag.h */

#ifndef __CONNECT_FOUR_DIAG_H__
#define __CONNECT_FOUR_DIAG_H__

#include "ConnectFourAbstract.h"

namespace TheConnectFourGame {
	
	class ConnectFourDiag : public ConnectFourAbstract {
	public:

		// No parameter function 
		ConnectFourDiag();
		
		// Constructor that takes width and height as parameters	
		ConnectFourDiag(int r, int c);
		
		// Caution: Default assignment operator, copy constructor, destructor
		// 			work for this class
		
	private:

		// Controls the end of game
		virtual bool controlEnd();
		
		// Controller functions
		bool checkLeftDiagonal(const Cell& move);
		bool checkRightDiagonal(const Cell& move);
	};

}
#endif
