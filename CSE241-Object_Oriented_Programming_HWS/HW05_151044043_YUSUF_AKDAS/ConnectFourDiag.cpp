/* ConnectFoutDiag.cpp */

#include "ConnectFourDiag.h"

namespace TheConnectFourGame {
	
	/*
	 * No parameter constructor of ConnectFourDiag class
	 */
	ConnectFourDiag::ConnectFourDiag() : ConnectFourAbstract() 
	{/* Intentionally left blank */}

	/*
	 * Constructor that takes height and width as parameters
	 */
	ConnectFourDiag::ConnectFourDiag(int r, int c) : ConnectFourAbstract(r, c) 
	{/* Intentionally left blank */}
	
	
	bool ConnectFourDiag::controlEnd()  
	{
		Cell move = userMove;

		// Checker control
		if (gameType == 'C' && order == 0) // Fixes move cell
			move = computerMove;

		if (checkDraw()) {
			order = -1;
			return (true);
		}
		if (checkRightDiagonal(move) || checkLeftDiagonal(move)) {
			changeCheckers();
			if (gameType == 'P')  // Fixes order
				order = (order == 0) ? 1 : 0; // from p1 to p2, vice versa.
			else
				order = (order == 0) ? 2 : 0;
			return (true);
		}
		return (false);
	}
		
	/*
	 * Checks game board left dioganally.
	 */
	bool ConnectFourDiag::checkLeftDiagonal(const Cell& move)
	{
		bool found = false, one = false, two = false;
		int checkerCount = 0;
		int c = static_cast<int> (move.getPosition() - 'A');
		int r = move.getRowNumber();

		int j = 0;
		for (int i = 0; i < 4 && !found; ++i) {
			if (!one && r-i >= 0 && c-i >= 0) {
				if (isLegal(r-i, c-i, move.getType())) {
					winnerCheckers[j] = gameBoard[r-i][c-i];
					++checkerCount;
					++j;
				} else {
					one = true;
				}
			}
			if (i != 0 && !two && r+i < row && c+i >= 0) {
				if (isLegal(r+i, c+i, move.getType())) {
					winnerCheckers[j] = gameBoard[r+i][c+i];
					++checkerCount;
					++j;
				} else {
					two = true;
				}
			}
			found = (checkerCount == 4) ? true : false;
		}
		return (found);
	}

	/*
	 * Checks game board right dioganally.
	 */
	bool ConnectFourDiag::checkRightDiagonal(const Cell& move)
	{
		bool found = false, one = false, two = false;
		int checkerCount = 0;
		int c = static_cast<int> (move.getPosition() - 'A');
		int r = move.getRowNumber();

		int j = 0;
		for (int i = 0; i < 4 && !found; ++i) {
			if (!one && r+i < row && c-i >= 0) {
				if (isLegal(r+i, c-i, move.getType())) {
					winnerCheckers[j] = gameBoard[r+i][c-i];
					++checkerCount;
					++j;
				} else {
					one = true;
				}
			}
			if (i != 0 && !two && r-i >= 0 && c+i < column) {
				if (isLegal(r-i, c+i, move.getType())) {
					winnerCheckers[j] = gameBoard[r-i][c+i];
					++checkerCount;
					++j;
				} else {
					two = true;
				}
			}
			found = (checkerCount == 4) ? true : false;
		}
		return (found);
	}
}
