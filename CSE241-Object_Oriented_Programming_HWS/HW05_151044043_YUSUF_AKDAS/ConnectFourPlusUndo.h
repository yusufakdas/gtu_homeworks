/* ConnectFoutPlusUndo.h */

#ifndef __CONNECT_FOUR_PLUS_UNDO_H__
#define __CONNECT_FOUR_PLUS_UNDO_H__

#include "ConnectFourPlus.h"
#include "Cell.h"

namespace TheConnectFourGame {

	class ConnectFourPlusUndo : public ConnectFourPlus {
	public:

		// No parameter constructor
		ConnectFourPlusUndo();
		
		// Constructor that takes height and width as parameters
		ConnectFourPlusUndo(int r, int c);
		
		// Copy Constructor
		ConnectFourPlusUndo(const ConnectFourPlusUndo& copy);
		
		// Assignment operator
		ConnectFourPlusUndo& operator =(const ConnectFourPlusUndo& rightSide);
		
		// Destructor
		~ConnectFourPlusUndo();
	
		// Returns how many moves made
		int getMoveCount() const;
		
		// Redefines playGame function to initialize moveCoordinates array and
		// use UNDO command
		void playGame();

	private:	

		// Undos the last move
		void undo();

		// Redefines getInput function for adding UNDO command
		int getInput(string& request, char& letter);

		// Redefines load function.
		bool loadTheGame();

		// Redefines save function to save all Cell coordinates
		bool saveTheGame() const;

		Cell *moveCoordinates; // Keeps all moves. 

		int moveCount;		   // For keeping the last index.
	};

}

#endif
