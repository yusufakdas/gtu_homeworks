/* ConnectFoutPlusUndo.cpp */ 

#include "ConnectFourPlusUndo.h"

namespace TheConnectFourGame {
	/*
	 * No parameter Constructor.
	 */
	ConnectFourPlusUndo::ConnectFourPlusUndo() 
					   : ConnectFourPlus(), moveCount(0)
	{ 
		moveCoordinates = new Cell[row * column];
	}

	/*
	 * Constructor that takes width and height 
	 */
	ConnectFourPlusUndo::ConnectFourPlusUndo(int r, int c) 
					   : ConnectFourPlus(r, c), moveCount(0)

	{ 
		moveCoordinates = new Cell[row * column];
	}

	/*
	 * Copy constructor 
	 */
	ConnectFourPlusUndo::ConnectFourPlusUndo(const ConnectFourPlusUndo& copy) 
					   : ConnectFourPlus(copy), moveCount(copy.moveCount)
	{
		moveCoordinates = new Cell[row * column];
		
		for (int i = 0; i < row * column; ++i)
			moveCoordinates[i] = copy.moveCoordinates[i];
	}
	
	/*
	 * Assignment operator
	 */
	ConnectFourPlusUndo& ConnectFourPlusUndo::operator =
				         (const ConnectFourPlusUndo& rightSide)
	{
		ConnectFourAbstract::operator =(rightSide);
		
		if (this != &rightSide) {
			delete[] moveCoordinates;
			moveCount = rightSide.moveCount;
			moveCoordinates = new Cell[row * column];

			for (int i = 0; i < row * column; ++i)
				moveCoordinates[i] = rightSide.moveCoordinates[i];	
		}
		return (*this);
	}

	/*
	 * Destructor
	 */
	ConnectFourPlusUndo::~ConnectFourPlusUndo() 
	{
		delete[] moveCoordinates;
		moveCoordinates = nullptr;
	}

	/*
	 * Returns how many move made
	 */
	int ConnectFourPlusUndo::getMoveCount() const
	{
		return (moveCount);
	}
	
	/*
	 * Undos moves made
	 */
	void ConnectFourPlusUndo::undo() 
	{
		if (moveCount > 0) {	
			char pos = moveCoordinates[moveCount-1].getPosition() - 'A';
			int r = moveCoordinates[moveCount-1].getRowNumber();
			int c = static_cast<int> (pos);
			gameBoard[r][c].setType(EMPTY);
			
			// from p1 to p2, vice versa
			if (gameType == 'P') {
				order = (order == 0) ? 1 : 0; 
			} 
				
			++lastRow[c];
			--moveCount;
			
			cout << "UNDO!!!" << endl;
			if (gameType == 'C') {
				play();
				moveCoordinates[moveCount] = computerMove;
				++moveCount;
			}
		} else {
			cerr << "#### Cannot undo anymore! ####" << endl;
		}	
	}
	
	/*
	 * Plays a whole game in pvp or pvc mode by taking a width and height
	 */
	void ConnectFourPlusUndo::playGame()
	{

		// Resize function (for filename) is called in this function.
		getGameTypeAndSizes();

		delete[] moveCoordinates;
		moveCoordinates = new Cell[row * column];

		bool end = false;
		while (!end) {
			int get;
			string input;
			char position;
			printGameBoard();
			if (order != 2)
				get = getInput(input, position);
			if (get == 1)		// SAVE mode
				saveTheGame();  
			else if (get == 2)	// LOAD mode
				loadTheGame();	
			else if (get == 3)  // UNDO mode
				undo();		
			if (gameType == 'P' && get == 0) {
				bool valid = play(position);
				if (valid) {
					moveCoordinates[moveCount] = userMove;
					++moveCount;
					end = controlEnd();
				}
			} else if (gameType == 'C' && get == 0) {
				if (order == 0) {
					play(position);
				} else {
					play();
					moveCoordinates[moveCount] = computerMove;
					++moveCount;		
				}
				end = controlEnd();
			}
		}
		printGameBoard();
		showResult();
	}
	/*
	 * Gets user moves and commands.
	 * Returns values: "0 --> MOVE", "1 --> SAVE", "2 --> LOAD", "3 --> UNDO"
	 * 				   "-1 --> INVALID"
	 */
	int ConnectFourPlusUndo::getInput(string& request, char& letter)
	{
		const char MIN = 'A';
		const char MAX = static_cast <char> ('A' + column - 1);
		int retValue = -1; // Default return value is -1 (invalid).
		
		if (order == 0)
			cout << "P1: ";
		else
			cout << "P2: ";
		
		// Controls EOF for redirection state
		if (!getline(cin, request)) {
			cerr << endl << "#### END OF FILE REACHED ####" << endl;
			exit(1);
		}
		
		if (request.length() == 1) {
			letter = request[0];
			if (letter >= MIN && letter <= MAX)
				retValue = 0; // MOVE return value
			else
				cerr << "#### You should enter a column between '" << MIN
					 << "' and '" << MAX << "'! ####" << endl;
		} else if (request.compare(0, 4, "SAVE") == 0 && request.length() > 5) {
			getFileName(request);
			retValue = 1;  // SAVE return value
		} else if (request.compare(0, 4, "LOAD") == 0 && request.length() > 5) {
			getFileName(request);
			retValue = 2;  // LOAD return value
		} else if (request == "UNDO") {
			retValue = 3;  // UNDO return value
		} else {
			cerr << "#### Enter a column letter or to save the game "
				 << "'SAVE filename', to load a game 'LOAD filename'"
				 << " and to undo a move 'UNDO' ####"
				 << endl;
		}
		return (retValue);
	}
	
	/*
	 * Loads game from given file
	 */
	bool ConnectFourPlusUndo::loadTheGame()
	{
		ifstream inputStream(filename.c_str());

		// Controls whether the file is opened.
		if (inputStream.fail()) {
			cerr << "#### " << filename << " could not be opened! ####" << endl;
			return (false);
		}

		// Reads row and column number values to tmp variables.
		int tmpRow, tmpCol;
		inputStream >> tmpRow >> tmpCol;

		// Controls whether file is empty or not.
		if (inputStream.eof()) {
			cerr << "#### " << filename << " might be empty! ####" << endl;
			return (false);
		}

		// Cleans gameBoard, lastRow and winnerCheckers arrays.
		deallocate();

		// Assigns temporary variables to permanent variables.
		row = tmpRow;
		column = tmpCol;

		// Creates clean gameBoard, lastRow and winnerCheckers arrays.
		allocate();

		// Reads game type.
		inputStream >> gameType;

		// Reads user order.
		inputStream >> order;

		// Reads lastRow array.
		for (int i = 0; i < column; ++i)
			inputStream >> lastRow[i];

		// Reads gameBoard
		char pos;
		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < column; ++j) {
				int get;
				pos = static_cast<char> ('A' + j);
				inputStream >> get;
				gameBoard[i][j].setRowNumber(i);
				gameBoard[i][j].setPosition(pos);
				switch (get) {
					case 0: gameBoard[i][j].setType(EMPTY);   break;
					case 1: gameBoard[i][j].setType(PLAYER1); break;
					case 2: gameBoard[i][j].setType(PLAYER2); break;
					case 3: gameBoard[i][j].setType(COMP);	  break;
				}
			}
		}

		// Reads user moves.
		inputStream >> tmpRow >> pos;
		userMove.setRowNumber(tmpRow);
		userMove.setPosition(pos);

		// Determines user checker type.
		if (order == 0 || order == 2)
			userMove.setType(PLAYER1);
		else if (order == 1)
			userMove.setType(PLAYER2);

		// If game type is PvC, then reads computer moves.
		if (gameType == 'C') {
			inputStream >> tmpRow >> pos;
			computerMove.setRowNumber(tmpRow);
			computerMove.setPosition(pos);
			computerMove.setType(COMP);
		}
		
		// Loading move counts.
		inputStream >> moveCount;

		// Deletes and creates
		delete[] moveCoordinates;
		moveCoordinates = new Cell[row * column];

		// Loading moves 
		for (int i = 0; i < moveCount; ++i) {
			int get;
			inputStream >> tmpRow >> pos;
			inputStream >> get;
			moveCoordinates[i].setRowNumber(tmpRow);
			moveCoordinates[i].setPosition(pos);
			switch (get) {
				case 0: moveCoordinates[i].setType(EMPTY);   break;
				case 1: moveCoordinates[i].setType(PLAYER1); break;
				case 2: moveCoordinates[i].setType(PLAYER2); break;
				case 3: moveCoordinates[i].setType(COMP);	 break;
			}
		}
		
		// Closes the file
		inputStream.close();

		// Prints informative message on the screen
		cout << "--------------------------------------------------" << endl;
		cout << "**** Loading the game from " << filename << "... ****" << endl;
	
		return (true);
	}

	/*
	 * Saves game to given file
	 */
	bool ConnectFourPlusUndo::saveTheGame() const
	{
		ofstream outputStream(filename.c_str());

		// Controls whether file is opened
		if (outputStream.fail()) {
			cerr << "#### " << filename << " could not be opened! ####" << endl;
			return (false);
		}

		// Board row and column are written
		outputStream << row << ' ' << column << endl << endl;

		// Gametype is written
		outputStream << gameType << endl << endl;

		// User order is written
		outputStream << order << endl << endl;

		// Last row counter is written
		for (int i = 0; i < column; ++i)
			outputStream << lastRow[i] << ' ';
		outputStream << endl;

		// Game board is written
		outputStream << endl;
		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < column; ++j) {
				if (gameBoard[i][j].getType() == EMPTY)
					outputStream << 0 << ' ';
				else if (gameBoard[i][j].getType() == PLAYER1)
					outputStream << 1 << ' ';
				else if (gameBoard[i][j].getType() == PLAYER2)
					outputStream << 2 << ' ';
				else if (gameBoard[i][j].getType() == COMP)
					outputStream << 3 << ' ';
			}
			outputStream << endl;
		}
		outputStream << endl;

		// User move is written
		outputStream << userMove.getRowNumber() << ' ' 
					 << userMove.getPosition() << endl;

		// If game type is computer, computer move is written too
		if (gameType == 'C') {
			outputStream << endl;
			outputStream << computerMove.getRowNumber() << ' '
	 					 << computerMove.getPosition();
			outputStream << endl;
		}

		// Saving move counts.
		outputStream << endl << moveCount << endl;

		// Saving moves 
		outputStream << endl;
		for (int i = 0; i < moveCount; ++i) {
			outputStream << moveCoordinates[i].getRowNumber() << ' '
					 	 << moveCoordinates[i].getPosition()  << ' ';
			if (moveCoordinates[i].getType() == EMPTY)
				outputStream << 0 << ' ';
			else if (moveCoordinates[i].getType() == PLAYER1)
				outputStream << 1 << ' ';
			else if (moveCoordinates[i].getType() == PLAYER2)
				outputStream << 2 << ' ';
			else if (moveCoordinates[i].getType() == COMP)
				outputStream << 3 << ' ';
			outputStream << endl;
		}
		
		// Closes the file
		outputStream.close();

		// informative message to screen
		cout << "--------------------------------------------------" << endl;
		cout << "**** Current game state has been saved to " << filename 
			 << "! ****" << endl;
			 
		return (true);
	}

}
