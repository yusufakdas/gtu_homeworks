/******************************************************************************
 * >> Connect Four v1.5  << 												  *
 *																			  *
 * filename: 'main.cpp'				    						  	 	      *
 * author: yusufakdas						      							  *
 * last modified date: 10:08 p.m. - 26 Nov 2017	          					  *
 * site: github.com/yusufakdas												  *
 ******************************************************************************/

#include "ConnectFourPlus.h"
#include "ConnectFourDiag.h"
#include "ConnectFourPlusUndo.h"
using namespace TheConnectFourGame;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;

int main(int argc, char *argv[]) 
{
	// Prints introductory message on the screen
	cout << "Hello, Welcome to Connect Four v1.5!" << endl << endl;
	cout << "Connect Four is a simple game. You need to collect four of your "
	     << "checker " << endl << "in a row to win the game. This row can be "
		 << "horizontal, vertical or diagonal." << endl << endl;
 
	ConnectFourPlus plus;
	ConnectFourDiag diag;
	ConnectFourPlusUndo undo;

	bool control = false;
	char request;

	// Takes the game type from user
	do {
		cout << "Enter 'P' for plus, 'D' for diagonal and 'U' for undo> ";
		cin >> request;
		if (!cin || (request != 'P' && request != 'D' && request != 'U'))
			cerr << "#### Invalid request! ####" << endl;		
		else
			control = true;
	} while(!control);

	// Selection for game type
	if (request == 'P')
		plus.playGame();
	else if (request == 'D')
		diag.playGame();
	else
		undo.playGame();	

	return (0);
}
