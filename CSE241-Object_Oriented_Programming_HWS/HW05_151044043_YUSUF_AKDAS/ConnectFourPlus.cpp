/* ConnectFoutPlus.cpp */

#include "ConnectFourPlus.h"

namespace TheConnectFourGame {
	
	/*
	 * No parameter constructor of ConnectFourPlus class
	 */
	ConnectFourPlus::ConnectFourPlus() : ConnectFourAbstract() 
	{/* Intentionally left blank */}

	/*
	 * Constructor that takes height and width as parameters
 	 */
	ConnectFourPlus::ConnectFourPlus(int r, int c) : ConnectFourAbstract(r, c) 
	{/* Intentionally left blank */}
	
	bool ConnectFourPlus::controlEnd()
	{
		Cell move = userMove;

		// Checker control
		if (gameType == 'C' && order == 0) // Fixes move cell
			move = computerMove;

		if (checkDraw()) {
			order = -1;
			return (true);
		}
		if (checkVertical(move) || checkHorizontal(move)) {
			changeCheckers();
			if (gameType == 'P')  // Fixes order
				order = (order == 0) ? 1 : 0; // from p1 to p2, vice versa.
			else
				order = (order == 0) ? 2 : 0;
			return (true);
		}
		return (false);
	}

	/*
	 * Checks game board vertically.
	 */
	bool ConnectFourPlus::checkVertical(const Cell& move)
	{
		bool found = false;
		int checkerCount = 0;
		int c = static_cast <int> (move.getPosition() - 'A');

		int j = 0;
		for (int i = 0; i < row && !found; ++i) {
			if (isLegal(i, c, move.getType())) {
				winnerCheckers[j] = gameBoard[i][c];
				++checkerCount;
				++j;
			} else {
				j = 0;
				checkerCount = 0;
			}
			found = (checkerCount == 4) ? true : false;
		}
		return (found);
	}

	/*
	 * Checks game board horizontally.
	 */
	bool ConnectFourPlus::checkHorizontal(const Cell& move)
	{
		bool found = false;
		int checkerCount = 0;
		int r = move.getRowNumber();

		int j = 0;
		for (int i = 0; i < column && !found; ++i) {
			if (isLegal(r, i, move.getType())) {
				winnerCheckers[j] = gameBoard[r][i];
				++checkerCount;
				++j;
			} else {
				j = 0;
				checkerCount = 0;
			}
			found = (checkerCount == 4) ? true : false;
		}
		return (found);
	}
}

