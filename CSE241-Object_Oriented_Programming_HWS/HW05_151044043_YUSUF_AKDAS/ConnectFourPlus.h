/* ConnectFourPlus.h */

#ifndef __CONNECT_FOUR_PLUS_H__
#define __CONNECT_FOUR_PLUS_H__

#include "ConnectFourAbstract.h"

namespace TheConnectFourGame {

	class ConnectFourPlus : public ConnectFourAbstract {
	public:

		// No parameter constructor
		ConnectFourPlus();
		
		// Constructor that take width and height values as parameters
		ConnectFourPlus(int r, int c);
		
		// Caution: Default assignment operator, copy constructor and destructor
		// 			work for this class
	
	protected:

		// Controls end of game
		virtual bool controlEnd();
		
		// Controller functions
		bool checkVertical(const Cell& move);
		bool checkHorizontal(const Cell& move);
	};

}
#endif
