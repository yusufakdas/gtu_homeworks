#ifndef CONNECT_FOUR_H_
#define CONNECT_FOUR_H_

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <cstdlib>
using namespace std;

class ConnectFour {
private:
	class Cell;  // Forward declaration for Cell class
	enum class Checker { X, O, WINX, WINO, EMPTY };
	enum class State { ONE, TWO, COMPUTER, DEUCE, NONE, LOADCMP };
public:
	// No parameter 
	ConnectFour();
	
	// Width, height, gametype 
	ConnectFour(int w, int h, char g);
	
	// size * size game board
	ConnectFour(int size);

	// Accessor Functions
	int getWidth() const;
	int getHeight() const;
	char getGameType() const;
	int getOrder() const;

	// Mutator Functions
	/* Kullanıcının oyunun boyutuna ve tipine doğrudan değiştirmemesi için
	   setter fonksiyonlar yazılmadı */

	// Functions making some tasks
	void printGameBoard() const;

	// Computer one time play
	void play();
	
	// User one time play
	void play(char position);
	
	// Controlling end of the game
	bool controlEnd();
	
	// Play single mode
	void playGame();
	
	// Compare two games
	bool compareGames(const ConnectFour& other) const;
	
	// Save and load the game
	bool compareSaveLoad(const string& request);
	
	// Print who is the winner of game
	void showResult() const;
	
	// Get the size and game type from user 
	void getSizeAndGameType();
	
	// Control column letter, LOAD and SAVE commands 
	bool getRequest(string& request, char& letter);

private:
	class Cell {
	public:
		Cell();

		// Accessor Functions
		char getPosition() const;
		int getRowNumber() const;
		Checker getType() const;

		// Mutator Functions
		void setPosition(char pos);
		void setRowNumber(int row);
		void setType(Checker t);
	private:
		char position;
		int rowNumber;
		Checker type;
	};

	// Resize the essential vector for the game
	void resize();
	
	// Check the board
	bool checkVertical();
	bool checkHorizontal();
	bool checkLeftDiagonal();
	bool checkRightDiagonal();
	bool checkDeuce() const;
	
	// Make small checkers 
	void changeCheckers();
	
	// Parse the file name from string typed by the user
	void getFileName(const string& request);
	
	// LOAD the game from file
	bool loadTheGame();
	
	// SAVE the current state of game to file
	bool saveTheGame();
	
	// Empty junk values in buffer
	void emptyBuffer() const;

	// The game board
	vector< vector<Cell> > gameCells;
	
	// Winner's four checkers' coordinates on the board 
	vector<Cell> winnerCheckers;

	// Move made by user
	Cell userMove;
	
	// Move made by computer
	Cell computerMove;
		
	// Prevent user to win the game
	bool preventUserMoves();
	
	// Determine the move of computer
	bool determineComputerMoves();
	
	// Return empty row number according to position
	int getValidRowNumber(char pos) const;
	
	// Return what checker is 
	Checker returnTheChecker(int row, int col) const;
	
	// Return how many living cell are on the board
	static int getLivingCells();
	
	static int livingCells; // Sum of the printable cells(X, O) on the game 

	int width;		 // Width of the board
	int height;		 // Height of the board
	char gameType;   // Whether pvp or pvc
	int selectUser;  // 0 --> (Player1), 1 --> (Computer or Player2)
	State end;		 // Result of the game
	string filename; // Filename got from user for LOAD and SAVE
	int cellSize;    // The cell size for one cell. It is for MULTIPLAYER mode
};

#endif
