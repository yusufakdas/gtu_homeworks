#include "ConnectFour.h"

int ConnectFour::livingCells = 0;

ConnectFour::ConnectFour(int w, int h, char g) : width(w), height(h), gameType(g),
								selectUser(0), end(ConnectFour::State::NONE),
								cellSize(0)
{
	resize();
}

ConnectFour::ConnectFour(int size) : width(size), height(size), selectUser(0),
									 end(ConnectFour::State::NONE), cellSize(0)
{
	resize();
}

ConnectFour::ConnectFour() : width(4), height(4), selectUser(0),
							 end(ConnectFour::State::NONE), cellSize(0)
{/* Intentionally left blank */}


ConnectFour::Cell::Cell() : position('-'), rowNumber(0),
						    type(ConnectFour::Checker::EMPTY)
{/* Intentionally left blank */}

int ConnectFour::getHeight() const {
	return (height);
}

int ConnectFour::getWidth() const {
	return (width);
}

inline ConnectFour::Checker ConnectFour::Cell::getType() const {
	return (type);
}

inline void ConnectFour::Cell::setPosition(char pos) {
	position = pos;
}

inline char ConnectFour::Cell::getPosition() const {
	return (position);
}

inline int ConnectFour::Cell::getRowNumber() const {
	return (rowNumber);
}

inline void ConnectFour::Cell::setRowNumber(int row) {
	rowNumber = row;
}

inline void ConnectFour::Cell::setType(Checker t) {
	type = t;
}

int ConnectFour::getLivingCells() {
	return (livingCells);
}

char ConnectFour::getGameType() const {
	return (gameType);
}

inline ConnectFour::Checker ConnectFour::returnTheChecker(int row, int col) const {
	return (gameCells[row][col].getType());
}

int ConnectFour::getOrder() const {
	return (selectUser);
}

void ConnectFour::printGameBoard() const {
	for (int i = 0; i < width; ++i)
		cout << static_cast<char> ('A' + i) << ' ';
	cout << endl;

	for (unsigned int row = 0; row < gameCells.size(); ++row) {
		for (unsigned int col = 0; col < gameCells.at(0).size(); ++col) {
			switch (gameCells.at(row).at(col).getType()) {
			case Checker::X: 	 cout << "X "; break;
			case Checker::O: 	 cout << "O "; break;
			case Checker::WINX:  cout << "x "; break;
			case Checker::WINO:	 cout << "o "; break;
			case Checker::EMPTY: cout << ". "; break;
			}
		}
		cout << endl;
	}
	cout << endl;
	cout << "LivingCell: " << ConnectFour::getLivingCells() << endl;
	cout << endl;
}

void ConnectFour::emptyBuffer() const {
	while (cin.get() != '\n')
		;
}
bool ConnectFour::getRequest(string& request, char& letter) {
	const char MIN = 'A';
	const char MAX = static_cast<char> ('A' + width - 1);
	bool retValue;

	if (selectUser == 0)
		cout << "P1: ";
	else
		cout << "P2: ";
	getline(cin, request);

	if (request.length() == 1) {
		char tmp = static_cast<char> (request[0]);
		letter = tmp;

		if (letter >= MIN && letter <= MAX) {
			retValue = true;
		} else {
			cerr << endl << "#### You should enter a column between '" << MIN
				 << "' and '" << MAX << "'! ####" << endl << endl;
			retValue = false;
		}
	} else if (request.compare(0, 4, "SAVE") != 0 && request.compare(0, 4, "LOAD") != 0) {
		cerr << endl << "#### Enter a column letter or to save the game "
			 << "'SAVE filename' and to load a game 'LOAD filename' ####"
			 << endl << endl;
		retValue = false;
	} else {
		//cerr << endl << "You have to provide a filename!" << endl;
		retValue = false;
	}
	return (retValue);
}

void ConnectFour::getSizeAndGameType() {
	auto err = false, type = false;  // ****auto burada kullanıldı!

	do {
		if (!err) { // Oyun tahtasının boyutunu alır.
			cout << "En giriniz: ";
			cin >> width;
			cout << "Boy giriniz: ";
			cin >> height;

			if (height < 4 || width < 4) {
				cerr << endl << "#### Boy ve en 4 ve uzeri olmali! ####" << endl;
				cin.clear();
				cin.ignore(256, '\n');
			} else {
				err = true;
			}
		} else if (!type) { // Oyun tipi alınır.
			cout << "Player | Computer: ";
			cin >> gameType;
			emptyBuffer();
			type = (gameType == 'C' || gameType == 'c' ||
					gameType == 'P' || gameType == 'p') ? true : false;
			if (!type) // Gecersiz girdi kontrolü
				cerr << endl << "#### Gecersiz oyun tipi! ####" << endl;
		}
	} while (!err || !type);
}

void ConnectFour::playGame() {
	getSizeAndGameType();
	resize();

	string request;
	char position;
	if (gameType == 'P' || gameType == 'p') {
		bool endOfGame = false;

		while (!endOfGame) {
			printGameBoard();
			bool get = getRequest(request, position);
			bool slc = compareSaveLoad(request);
			cout << endl;
			if (get && !slc) {
				play(position);
				endOfGame = controlEnd();
			}
		}
		printGameBoard();
		showResult();
	}
	if (gameType == 'c' || gameType == 'C') {
		bool endOfGame = false;

		while (!endOfGame) {
			printGameBoard();
			if (selectUser == 0) {
				bool get = getRequest(request, position);
				bool slc = compareSaveLoad(request);
				cout << endl;
				if (get && !slc) {
					play(position);
					endOfGame = controlEnd();
				}
			} else {
				play();
				endOfGame = controlEnd();
			}
		}
		printGameBoard();
		showResult();
	}
}

bool ConnectFour::controlEnd() {
	bool retValue = false;

	if (selectUser == 1) {
		end = ConnectFour::State::ONE;
	} else {
		if (gameType == 'P' || gameType == 'p')
			end = ConnectFour::State::TWO;
		else
			end = ConnectFour::State::COMPUTER;
	}
	if (checkDeuce()) {
		end = ConnectFour::State::DEUCE;
		retValue = true;
	} else if (checkVertical()) {
		retValue = true;
	} else if (checkHorizontal()) {
		retValue = true;
	} else if (checkLeftDiagonal()) {
		retValue = true;
	} else if (checkRightDiagonal()) {
		retValue = true;
	}
	if (retValue && end != ConnectFour::State::DEUCE) {
		changeCheckers();
	}
	return (retValue);
}

int ConnectFour::getValidRowNumber(char pos) const {
	bool found = false;
	int col = static_cast<int> (pos - 'A');
	int row = -1;

	for (int i = getHeight() - 1; i >= 0 && !found; --i) {
		if (gameCells[i][col].getType() == ConnectFour::Checker::EMPTY) {
			row = i;
			found = true;
		}
	}
	return (row);
}

// Play user for one time 
void ConnectFour::play(char position) {
	int row = getValidRowNumber(position);
	int col = static_cast<int> (position - 'A');

	if (row != -1) {
		gameCells[row][col].setPosition(position);
		gameCells[row][col].setRowNumber(row);
		if (selectUser == 0) {
			gameCells[row][col].setType(ConnectFour::Checker::X);
		} else {
			gameCells[row][col].setType(ConnectFour::Checker::O);
		}
		userMove = gameCells[row][col];
		selectUser = selectUser == 0 ? 1 : 0;
		cellSize += 1;
		ConnectFour::livingCells += 1;
	} else {
		cerr << endl << "#### You cannot move on this column. "
			 << "Choose another one! ####" << endl;
	}
}

// Create the vector given according to its size
void ConnectFour::resize() {
	vector<Cell> tmp;
	Cell object;

	for (int row = 0; row < height; ++row) {
		gameCells.push_back(tmp);
		for (int col = 0; col < width; ++col) {
			gameCells.at(row).push_back(object);
		}
	}
}

bool ConnectFour::checkDeuce() const {
	auto deuce = true;

	for (int i = 0; i < width && deuce; ++i)
		if (gameCells[0][i].getType() == ConnectFour::Checker::EMPTY)
			deuce = false;
	return (deuce);
}

bool ConnectFour::checkVertical() {
	auto checkerCount = 0;
	auto found = false;
	int colNumber;

	ConnectFour::Checker checkMe;
	if (selectUser == 1) {
		checkMe = ConnectFour::Checker::X;
		colNumber = static_cast<int> (userMove.getPosition() - 'A');
	} else {
		checkMe = ConnectFour::Checker::O;
		if (gameType == 'C' || gameType == 'c')
			colNumber = static_cast<int> (computerMove.getPosition() - 'A');
		else
			colNumber = static_cast<int> (userMove.getPosition() - 'A');
	}

	for (int i = 0; i < height && !found; ++i) {
		if (returnTheChecker(i, colNumber) == checkMe) {
			checkerCount += 1;
			winnerCheckers.push_back(gameCells[i][colNumber]);
		} else {
			winnerCheckers.clear();
			checkerCount = 0;
		}
		found = (checkerCount == 4) ? true : false;
	}
	return (found);
}

bool ConnectFour::checkHorizontal() {
	auto checkerCount = 0;
	auto found = false;
	int rowNumber;

	ConnectFour::Checker checkMe;
	if (selectUser == 1) {
		checkMe = ConnectFour::Checker::X;
		rowNumber = userMove.getRowNumber();
	} else {
		checkMe = ConnectFour::Checker::O;
		if (gameType == 'C' || gameType == 'c')
			rowNumber = computerMove.getRowNumber();
		else
			rowNumber = userMove.getRowNumber();
	}

	for (int i = 0; i < width && !found; ++i) {
		if (returnTheChecker(rowNumber, i) == checkMe) {
			checkerCount += 1;
			winnerCheckers.push_back(gameCells[rowNumber][i]);
		} else {
			winnerCheckers.clear();
			checkerCount = 0;
		}
		found = (checkerCount == 4) ? true : false;
	}
	return (found);
}

bool ConnectFour::checkLeftDiagonal()  {
	auto checkerCount = 0;
	auto found = false, one = false, two = false;
	int colNumber, rowNumber;

	ConnectFour::Checker checkMe;
	if (selectUser == 1) {
		checkMe = ConnectFour::Checker::X;
		colNumber = static_cast<int> (userMove.getPosition() - 'A');
		rowNumber = userMove.getRowNumber();
	} else {
		checkMe = ConnectFour::Checker::O;
		if (gameType == 'C' || gameType == 'c') {
			colNumber = static_cast<int> (computerMove.getPosition() - 'A');
			rowNumber = computerMove.getRowNumber();
		} else {
			colNumber = static_cast<int> (userMove.getPosition() - 'A');
			rowNumber = userMove.getRowNumber();
		}
	}

	for (int i = 0; i < 4 && !found; ++i) {
		if (!one && rowNumber-i >= 0 && colNumber-i >= 0) {
			if (gameCells[rowNumber-i][colNumber-i].getType() == checkMe) {
				checkerCount += 1;
				winnerCheckers.push_back(gameCells[rowNumber-i][colNumber-i]);
			} else {
				one = true;
			}
		}
		if (i != 0 && !two && rowNumber+i < height && colNumber+i >= 0) {
			if (gameCells[rowNumber+i][colNumber+i].getType() == checkMe) {
				checkerCount += 1;
				winnerCheckers.push_back(gameCells[rowNumber+i][colNumber+i]);
			} else {
				two = true;
			}
		}
		found = (checkerCount == 4) ? true : false;
	}
	if (!found)
		winnerCheckers.clear();
	return (found);
}

bool ConnectFour::checkRightDiagonal() {
	auto checkerCount = 0;
	auto found = false, one = false, two = false;
	int colNumber, rowNumber;

	ConnectFour::Checker checkMe;
	if (selectUser == 1) {
		checkMe = ConnectFour::Checker::X;
		colNumber = static_cast<int> (userMove.getPosition() - 'A');
		rowNumber = userMove.getRowNumber();
	} else {
		checkMe = ConnectFour::Checker::O;
		if (gameType == 'C' || gameType == 'c') {
			colNumber = static_cast<int> (computerMove.getPosition() - 'A');
			rowNumber = computerMove.getRowNumber();
		} else {
			colNumber = static_cast<int> (userMove.getPosition() - 'A');
			rowNumber = userMove.getRowNumber();
		}
	}

	for (int i = 0; i < 4 && !found; ++i) {
		if (!one && rowNumber+i < height && colNumber-i >= 0) {
			if (gameCells[rowNumber+i][colNumber-i].getType() == checkMe) {
				checkerCount += 1;
				winnerCheckers.push_back(gameCells[rowNumber+i][colNumber-i]);
			} else {
				one = true;
			}
		}
		if (i != 0 && !two && rowNumber-i >= 0 && colNumber+i < width) {
			if (gameCells[rowNumber-i][colNumber+i].getType() == checkMe) {
				checkerCount += 1;
				winnerCheckers.push_back(gameCells[rowNumber-i][colNumber+i]);
			} else {
				two = true;
			}
		}
		found = (checkerCount == 4) ? true : false;
	}
	if (!found)
		winnerCheckers.clear();
	return (found);
}

void ConnectFour::changeCheckers() {
	ConnectFour::Checker change = winnerCheckers[0].getType();

	for (int i = 0; i < 4; ++i) {
		int row = winnerCheckers[i].getRowNumber();
		int col = static_cast<int> (winnerCheckers[i].getPosition() - 'A');
		if (change == ConnectFour::Checker::X) {
			gameCells[row][col].setType(ConnectFour::Checker::WINX);
		} else if (change == ConnectFour::Checker::O) {
			gameCells[row][col].setType(ConnectFour::Checker::WINO);
		}
	}
}

void ConnectFour::showResult() const {
	switch (end) {
	case ConnectFour::State::ONE :
		cout << "Player ONE has just won!" << endl;
		break;
	case ConnectFour::State::TWO :
		cout << "Player TWO has just won!" << endl;
		break;
	case ConnectFour::State::COMPUTER :
		cout << "Computer has just won!" << endl;
		break;
	case ConnectFour::State::DEUCE :
		cout << "Nobody won this game!" << endl;
		break;
	default: ;
	}
	cout << endl;
}

// Load the game
bool ConnectFour::loadTheGame() {
	ifstream inputStream(filename.c_str());
	int w, h;

	if (inputStream.fail())
		return false;

	inputStream >> w;
	inputStream >> h;
	if (inputStream.eof())
		return false;

	winnerCheckers.clear();
	for (int i = height-1; i >= 0; --i) {
		for (int j = 0; j < width; ++j) {
			gameCells[i].pop_back();
		}
		gameCells.pop_back();
	}

	ConnectFour::livingCells -= cellSize;

	cellSize = 0;
	width = w;
	height = h;
	resize();

	inputStream >> gameType;
	inputStream >> selectUser;

	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			int get;
			char pos = static_cast<char> ('A'+j);
			inputStream >> get;
			gameCells[i][j].setRowNumber(i);
			gameCells[i][j].setPosition(pos);
			switch (get) {
				case 0:
					gameCells[i][j].setType(ConnectFour::Checker::EMPTY);
					break;
				case 1:
					gameCells[i][j].setType(ConnectFour::Checker::X);
					break;
				case 2:
					gameCells[i][j].setType(ConnectFour::Checker::O);
					break;
				case 3:
					gameCells[i][j].setType(ConnectFour::Checker::WINX);
					break;
				case 4:
					gameCells[i][j].setType(ConnectFour::Checker::WINO);
					break;
			}
		}
	}

	int row;
	char pos;
	char checker;

	inputStream >> row >> pos >> checker;
	userMove.setRowNumber(row);
	userMove.setPosition(pos);

	if (checker == 'X')
		userMove.setType(ConnectFour::Checker::X);
	else
		userMove.setType(ConnectFour::Checker::O);

	inputStream >> row >> pos >> checker;
	computerMove.setRowNumber(row);
	computerMove.setPosition(pos);

	if (checker == 'X')
		computerMove.setType(ConnectFour::Checker::X);
	else
		computerMove.setType(ConnectFour::Checker::O);

	inputStream >> cellSize;
	ConnectFour::livingCells += cellSize;

	inputStream.close();
	return true;
}

bool ConnectFour::saveTheGame() {
	ofstream outputStream(filename.c_str());
	if (outputStream.fail())
		return false;

	outputStream << width << ' ' << height << endl << endl;

	outputStream << gameType << endl << endl;

	outputStream << selectUser << endl << endl;

	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			switch (gameCells[i][j].getType()) {
				case ConnectFour::Checker::EMPTY:
					outputStream << 0 << ' ';
					break;
				case ConnectFour::Checker::X:
					outputStream << 1 << ' ';
					break;
				case ConnectFour::Checker::O:
					outputStream << 2 << ' ';
					break;
				case ConnectFour::Checker::WINX:
					outputStream << 3 << ' ';
					break;
				case ConnectFour::Checker::WINO:
					outputStream << 4 << ' ';
					break;
			}
		}
		outputStream << endl;
	}
	cout << endl;

	outputStream << endl;
	outputStream << userMove.getRowNumber() << ' '
				 << userMove.getPosition();
	if (userMove.getType() == ConnectFour::Checker::X)
		outputStream << " X";
	else
		outputStream << " O";
	outputStream << endl;

	outputStream << endl;
	outputStream << computerMove.getRowNumber() << ' '
				 << computerMove.getPosition();
	if (computerMove.getType() == ConnectFour::Checker::X)
		outputStream << " X";
	else
		outputStream << " O";
	outputStream << endl;

	outputStream << endl << cellSize;

	outputStream.close();
	return true;
}

bool ConnectFour::compareSaveLoad(const string& request) {
	bool saveOrLoad;
	bool retValue = false;

	// Eğer girdi SAVE ile başlıyor ve dosya ismi varsa...
	if (request.compare(0, 4, "SAVE") == 0 && request.length() > 5) {

		getFileName(request); // Dosya adı alınır.
		saveOrLoad = saveTheGame();
		if (saveOrLoad) { // Dosya kaydedildi.
			cout << "--------------------------------------------------";
			cout << endl << "#### Current game state has been saved to "
				 << filename << "! ####" << endl << endl;
			retValue = true;
		} else { // Dosya açılamadı.
			cerr << endl << "**** " << filename << " could not be opened! ****\n";
			retValue = false;
		}
	}

	if (request.compare(0, 4, "LOAD") == 0 && request.length() > 5) {
		getFileName(request);
		saveOrLoad = loadTheGame();
		if (saveOrLoad) {
			cout << "--------------------------------------------------";
			cout << endl << "#### Loading the game from " << filename
			 	 << "... ####" << endl << endl;
			retValue = true;
		} else {
			cerr << endl << "**** " << filename << " could not be opened! ****\n";
			retValue = false;
		}
	}
	return retValue;
}

// Split and get the file name
void ConnectFour::getFileName(const string& request) {
	filename = "";
	for (unsigned int i = 5; i < request.length(); ++i)
		if (request[i] != ' ')
			filename += request[i];
}

// Prevent some of the user moves
bool ConnectFour::preventUserMoves() {
	int rowNumber = userMove.getRowNumber();
	int colNumber = static_cast <int> (userMove.getPosition() - 'A');
	bool found = false;
	int count = 0;

	// Vertical check
	for (int i = 0; i < 3 && !found; ++i) {
		if (rowNumber + i < height) {
			if (gameCells[rowNumber+i][colNumber].getType() == userMove.getType())
				count += 1;
			if (count == 3) {
				found = true;
				computerMove.setRowNumber(rowNumber-1);
				computerMove.setPosition(userMove.getPosition());
			}
		}
	}

	// Horizontal check
	for (int i = 0; i < width && !found; ++i) {
		if (gameCells[rowNumber][i].getType() == ConnectFour::Checker::EMPTY) {
			Cell tmp = gameCells[rowNumber][i];
			gameCells[rowNumber][i].setPosition('A'+i);
			gameCells[rowNumber][i].setRowNumber(rowNumber);
			gameCells[rowNumber][i].setType(userMove.getType());
			if (checkHorizontal()) {
				if (rowNumber == height-1) {
					computerMove.setRowNumber(rowNumber);
					computerMove.setPosition('A' + i);
					found = true;
				}
				if (!found && returnTheChecker(rowNumber+1, i) != ConnectFour::Checker::EMPTY) {
					computerMove.setRowNumber(rowNumber);
					computerMove.setPosition('A' + i);
					found = true;
				}
			}
			gameCells[rowNumber][i] = tmp;
		}
	}
	return (found);
}

// Determine what move will computer make 
bool ConnectFour::determineComputerMoves() {
	srand(time(NULL));
	int col, row;

	if (ConnectFour::getLivingCells() == 1) {
		col = rand() % width;
		row = getValidRowNumber(col + 'A');
		if (returnTheChecker(row, col) != ConnectFour::Checker::EMPTY) {
			col = rand() % width;
			row = getValidRowNumber(col + 'A');
		}
		computerMove.setPosition(col + 'A');
		computerMove.setRowNumber(row);
		computerMove.setType(ConnectFour::Checker::O);
	} else {
		bool found = false;
		int count = 0;
		int maxCount = 0;
		int	maxRow;
		char maxCol;

		// Vertical test
		for (int i = 0; i < width && !found; ++i) {
			row = getValidRowNumber('A' + i)+1;
			if (row < height && returnTheChecker(row, i) == ConnectFour::Checker::O) {
				for (int j = 0; j < 3; ++j)
					if (j+row < height && returnTheChecker(j+row, i) == ConnectFour::Checker::O) {
						++count;
					if (count >= maxCount) {
						maxRow = row-1;
						maxCol = 'A'+i;
						maxCount = count;
					}
					if (count == 3)
						found = true;
				}
			}
			count = 0;
		}
		char ad;
		if (maxCount > 0 && maxRow >= 0 && maxRow < height) {
			ad = maxCol;
			row = maxRow;
		} else {
			ad = (rand() % width) + 'A';
			row = getValidRowNumber(ad);
			while (row < 0 || row >= height || returnTheChecker(row, ad-'A') != ConnectFour::Checker::EMPTY) {
				ad = (rand() % width) + 'A';
				row = getValidRowNumber(ad);
			}
		}
		computerMove.setType(ConnectFour::Checker::O);
		computerMove.setPosition(ad);
		computerMove.setRowNumber(row);
	}
	return false;
}

// Computer Play one move
void ConnectFour::play() {
	int row, col;
	computerMove.setType(ConnectFour::Checker::O);
	if (preventUserMoves()) {
		row = computerMove.getRowNumber();
		col = static_cast<int> (computerMove.getPosition() - 'A');
	} else {
		determineComputerMoves();
		row = computerMove.getRowNumber();
		col = static_cast<int> (computerMove.getPosition() - 'A');
	}
	gameCells[row][col].setRowNumber(row);
	gameCells[row][col].setPosition(computerMove.getPosition());
	gameCells[row][col].setType(ConnectFour::Checker::O);
	selectUser = 0;
	cellSize += 1;
	ConnectFour::livingCells += 1;
}


// 	Compares two game each other and if the first one is better than two,
//	the function return true, otherwise false.
bool ConnectFour::compareGames (const ConnectFour& other) const {
	int pointsForTheThis = 0;
	int pointsForTheOther = 0;

	// Searching for consecutive checkers verticallly
	for (int i = 0; i < width; ++i) {
		int count = 0, sumForFirst = 0;
		int first = getValidRowNumber('A' + i) + 1;
		bool pass = false;
		while (count < 3 && !pass) {
			if (first + count < height) {
				// This object
				if (returnTheChecker(count+first, i) == ConnectFour::Checker::X) {
					++sumForFirst;
				} else if (sumForFirst == 1) {
					--sumForFirst;
					pass = true;
				}
			}
			++count;
		}
		if (sumForFirst == 2)
			pointsForTheThis += 1; // One point for two checkers
		else if (sumForFirst == 3)
			pointsForTheThis += 2; // Two points for three checkers
	}
	for (int i = 0; i < other.getWidth(); ++i) {
		int count = 0, sumForSecond = 0;
		int second = other.getValidRowNumber('A' + i) + 1;
		bool pass = false;
		while (count < 3 && !pass) {
			if (second + count < other.getHeight()) {
				// Other object
				if (other.returnTheChecker(count+second, i) == ConnectFour::Checker::X) {
					++sumForSecond;
				} else if (sumForSecond == 1) {
					--sumForSecond;
					pass = true;
				}
			}
			++count;
		}
		if (sumForSecond == 2)
			pointsForTheOther += 1; // One point for two checkers
		else if (sumForSecond == 3)
			pointsForTheOther += 2; // Two points for three checkers
	}
	// The result returns in terms of points
	if (pointsForTheThis > pointsForTheOther)
		return (true);
	return (false);
}
