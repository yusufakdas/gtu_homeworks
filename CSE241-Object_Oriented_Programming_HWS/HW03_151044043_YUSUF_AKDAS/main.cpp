#include "ConnectFour.h"

int main(int argc, char *argv[]) {
	const int OBJECT_SIZE = 5;

	ConnectFour game;
	string mode;
	
	// Get the mode of game 
	do {
		cout << "Oyun modunu seçiniz: ";
		cin >> mode;

		if (mode.length() > 1) {
			cerr << endl << "#### Multiobject mod icin 'M' single mod "
				 << "icin 'S' giriniz! ####" << endl;
		}
		cout << endl;
	} while (mode[0] != 'S' && mode[0] != 's' && mode[0] != 'M' && mode[0] != 'm');

	// Control the game mode whether single or multi is
	if (mode[0] == 'S' || mode[0] == 's') {
		game.playGame();
	} else if (mode[0] == 'M' || mode[0] == 'm') {
		vector < ConnectFour > gameObjects; // The game objects for multiplayer

		// Init the game board using the class constructor
		for (int i = 0; i < OBJECT_SIZE; ++i) {
			cout << "Obje" << i+1 << ":\n";

			ConnectFour tmp;
			tmp.getSizeAndGameType();
			gameObjects.push_back(ConnectFour(tmp.getWidth(), tmp.getHeight(),
								  tmp.getGameType()));

			cout << endl;
		}

		// The end states of objects 
		vector <bool> finish(OBJECT_SIZE);
		for (auto index : finish)
			index = false;

		bool endGame = false; 
		int countEndedGame = 0;
		int previousGame = -1;
		int selectGame;
		
		// The game is started
		while (!endGame) {
		
			// Select the object
			do {
				cout << "Obje seçiniz: ";
				cin >> selectGame;
				if (!cin || (selectGame < 1 || selectGame > OBJECT_SIZE)) {
					cerr << endl << 1 << " ile " << OBJECT_SIZE
						 << " arasında giriniz!" << endl << endl;
					cin.clear();
					cin.ignore(256, '\n');
				}
			} while (selectGame < 1 || selectGame > OBJECT_SIZE);

			// For getline 
			string tmp;
			getline(cin, tmp);
			
			// Entry print
			gameObjects[selectGame-1].printGameBoard();
			
			// Control the game if end 
			if (!finish[selectGame-1]) {
				
				// Player vs player 
				if (gameObjects[selectGame-1].getGameType() == 'P' ||
					gameObjects[selectGame-1].getGameType() == 'p' )
				{
					string request;
					char position;
					bool get = false, slc = false;
				
					// Get the user input until it is valid
					while (!get && !slc) {
						get = gameObjects[selectGame-1].getRequest(request, position);
						slc = gameObjects[selectGame-1].compareSaveLoad(request);
					}
					// If the input is not a command
					if (get && !slc) {
						gameObjects[selectGame-1].play(position);
						gameObjects[selectGame-1].printGameBoard();
					}
				} else { // Player vs computer
					
					if (gameObjects[selectGame-1].getOrder() == 0) {
						string request;
						char position;
						bool get = false, slc = false;
						while (!get && !slc) {
							get = gameObjects[selectGame-1].getRequest(request, position);
							slc = gameObjects[selectGame-1].compareSaveLoad(request);
						}
						if (get && !slc) { // Play computer and user
							gameObjects[selectGame-1].play(position);
							gameObjects[selectGame-1].printGameBoard();
							gameObjects[selectGame-1].play();
							gameObjects[selectGame-1].printGameBoard();
						}
					}
				}
				
				// Compare the object each other. It is printed when the game is 
				// better than other.
				if (previousGame != -1 && previousGame != selectGame &&
					!finish[selectGame-1])
				{
					bool cmp = gameObjects[selectGame-1].compareGames(gameObjects[previousGame-1]);
					if (cmp) {
						cout << selectGame << " is better than "
							 << previousGame << endl << endl;
					}
				}
				// Control the specified game's end state
				if (gameObjects[selectGame-1].controlEnd()) {
					gameObjects[selectGame-1].printGameBoard();
					gameObjects[selectGame-1].showResult();
					finish[selectGame-1] = true;
					countEndedGame += 1;
				}
				previousGame = selectGame;
			} else {
				cout << endl <<  "~~~ This game has already ended. "
					 << "Choose another one! ~~~" << endl << endl;
			}
			// All of the games have ended
			if (countEndedGame == OBJECT_SIZE) {
				endGame = true;
				cout << "!!!! You have just finished all of the games. "
					 << "See you soon !!!!" << endl;
			}
		}
	}
	return (0);
}
