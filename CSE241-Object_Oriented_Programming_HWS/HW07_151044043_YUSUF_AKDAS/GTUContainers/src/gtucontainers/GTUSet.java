package gtucontainers;

import java.util.Arrays; // for hashCode
import java.lang.reflect.Array;
import java.util.NoSuchElementException;
import java.security.InvalidParameterException;

/**
 * <h1>Set Implementation for GTU</h1> 
 * 
 * <p>This generic class is a implementation of set and elements in the set are 
 * unique.</p>
 * 
 * @author yusufakdas
 * @param <T> Generic type of element that set keeps.
 * @since 2017-12-24
 * @version 1.4
 */
public class GTUSet <T> implements GTUSetInt <T> {
    /**
     * Generic java array that keeps elements of set. 
     */
    protected T[] data;
    
    /**
     * Element numbers in the set at any given time.
     */
    protected int currentSize;
    
    /**
     * Capacity of the array.When current size of array equals to capacity, it 
     * will be doubled.
     */
    protected int capacity;
   
    /**
     * This is the type of array for keeping data. 
     */
    protected Class<T[]> classType;
    
    /**
     * Constructor of the class that takes one parameter. 
     * 
     * @param cls The parameter is array type of data.
     */
    public GTUSet(Class<T[]> cls) {
        classType = cls;
        currentSize = 0;
        capacity = 64;
        data = classType.cast(Array.newInstance(classType.getComponentType(), 
                              capacity));
    }
    
    /**
     * Copy constructor of the class. 
     * 
     * <p>P.S: <a href="https://tinyurl.com/y9amsldx">Clone method</a> is not 
     * overridden because it has some problems.Instead, use copy constructor to 
     * make a copy of the object.</p>
     * @param copy Other GTUSet object.
     */
    public GTUSet(GTUSet<T> copy) {
        currentSize = copy.currentSize;
        capacity = copy.capacity;
        classType = copy.classType;
        data = classType.cast(Array.newInstance(classType.getComponentType(), 
                              capacity));
        for (int i = 0; i < currentSize; ++i)
            data[i] = copy.data[i];        
    }
        
    /**
     * Controls whether set is empty or not. 
     * 
     * @return If container is empty, then returns true otherwise false.
     */
    @Override
    public boolean empty() {
       return (currentSize == 0);
    }
    
    /** 
     * Current size.
     * 
     * @return Current size of container. 
     */
    @Override 
    public int size() {
        return (currentSize);
    }
    
    /**
     * Maximum size.
     * 
     * @return Capacity of container.
     */
    @Override 
    public int max_size() {
        return (capacity);
    }
    
    /**
     * Counts given value in the set.
     * 
     * @param val Value that is wanted its number in the container.
     * @return The number of particular element in the set.
     */
    @Override
    public int count(Object val) {
        int counter = 0;

        val = (T) val;
        
        // If found, increment counter by one. 
        for (int i = 0; i < currentSize; ++i)
            if (data[i].equals(val))
                ++counter;
     
        return (counter);
    }
    
    /**
     * Clears all data of container.
     */
    @Override
    public void clear() {
        currentSize = 0;
        capacity = 64;
        T[] cleanData;
        cleanData = classType.cast(Array.newInstance(classType.getComponentType(),
                    capacity));
    }
    
    /**
     * Erases an element from set.
     * 
     * @param val Value wanted to erase.
     * @return How many elements are erased.
     */
    @Override
    public int erase(Object val) {      
        int deleted = 0;

        val = (T) val;
        
        if (find(val) != null) { // If it is present then erase it. 
            // Allocates new spaces for erased value version of set.
            T[] newData;
            newData = classType.cast(Array.newInstance(classType.getComponentType(),
                      capacity));
            deleted = count(val);                   
            // Copies all elements to new allocated place except for given value
            for (int i = 0, j = 0; i < currentSize; ++i) {
                if (!data[i].equals(val)) {
                    newData[j] = data[i];
                    ++j;
                }
            }
            data = newData;
            --currentSize;
        }
        // Because set keeps unique element, if found returns 1, otherwise 0.
        return (deleted);
    }
    
    /**
     * Intersection of two set.
     * 
     * @param otherSet Given set as parameter to method.
     * @return A new set that is intersection of two set.
     */
    @Override
    public GTUSetInt <T> intersection(GTUSetInt<T> otherSet) {
        GTUSet <T> intersect = new GTUSet<>(classType);
        
        GTUIterator it1 = this.begin();
           
        while (it1.hasNext()) {
            T var1 = it1.next();
            GTUSet<T>.GTUIterator it2 = otherSet.begin();
            while (it2.hasNext()) {
                T var2 = it2.next();
                if (var1.equals(var2)) 
                    intersect.insert(var2);
            }
        }
        return (intersect);
    }
    
    /**
     * Inserts an element to set.
     * 
     * @param val The value that is wanted to insert to set.
     * @throws InvalidParameterException If the value is in the set then throw.
     * @see InvalidParameterException
     * @return A iterator for new element.
     */
    @Override
    public GTUIterator insert (T val) {
        // Searches for an element
        if (find(val) != null) { // If it is found then throw InvalidParameterException
            throw (new InvalidParameterException("Cannot add an existing "+
                                                 "element to set!"));
        } else {
            // Add given value to set.
            data[currentSize] = val;
            ++currentSize;
            // Controls whether currentSize is equal to capacity.
            resize();
        }
        return (find(val));
    }
    
    /**
     * Searches for given value in the container.
     * 
     * @param val The value that is wanted to find.
     * @return If it is found then returns iterator for its place, otherwise 
     *  returns null. 
     */
    @Override
    public GTUIterator find(Object val) {
        val = (T) val;
        
        for (int i = 0; i < currentSize; ++i)
            if (data[i].equals(val)) // found
                return (new GTUIterator(i)); 
        return (null); // not found
    }
    
    /**
     * Beginning of set.
     * 
     * @return Iterator that points to the data's first element.
     */
    @Override 
    public GTUIterator begin() {
        return (new GTUIterator());
    }
    
    /**
     * End of set.
     * 
     * @return Iterator that points to the data's last element.
     */
    @Override 
    public GTUIterator end() {
        return (new GTUIterator(currentSize-1));
    }

    /**
     * Resizes the set if current size equals to capacity.
     */
    protected void resize() {
        if (currentSize == capacity) {
            capacity *= 2; 
            T[] newData;
            newData = classType.cast(Array.newInstance(classType.getComponentType(), 
                                     capacity));
            for (int i = 0; i < currentSize; ++i)
                newData[i] = data[i];	
            data = newData;
        }
    }
        
    /**
     * Override of toString method for GTUSet class.
     * 
     * @return Contents of data as a string.
     */
    @Override
    public String toString() {
        String str = "";
        for (int i = 0; i < currentSize; ++i)
            str += data[i].toString() + " ";
        return (str);
    }
      
    /**
     * Override of equals method for GTUSet class.
     * 
     * @param other Object to be compared with this object.
     * @return If this object and given object equals, returns true otherwise 
     *  false.
     */
    @Override
    public boolean equals(Object other) {
        if (other == this) 
            return (true);
        if (other == null)
            return (false);
        if (!(other instanceof GTUSet)) 
            return (false);
        
        // Casts object of type Object to GTUSet
        GTUSet<T> castObj = (GTUSet<T>) other;
        
        // Control for capacity
        if (this.capacity != castObj.capacity)
            return (false);
        
        // Control for size
        if (this.currentSize != castObj.currentSize)
            return (false);
        
        // Control for element type
        if (!(this.classType.equals(castObj.classType)))
            return (false);
        
        // Control for array
        for (int i = 0; i < currentSize; ++i)
            if (!(this.data[i].equals(castObj.data[i])))
                return (false);
        
        return (true);
    }
    
    /**
     * Hash code of the object.
     * 
     * @return Hash code.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Arrays.deepHashCode(this.data);
        return hash;
    }

    /**
     * Iterator class implemented as inner class of GTUSet.
     */
    public class GTUIterator {
        /**
         * Position of iterator.
         */
        private int index;
        
        /**
         * No parameter constructor.
         */
        public GTUIterator() {
            index = 0;
        }
        
        /**
         * Constructor that takes one parameter.
         * 
         * @param i position of element.
         */
        public GTUIterator(int i) {
            index = i;
        }
        
        /**
         * Returns the next element in the list and advances the cursor position.
         *
         * @throws NoSuchElementException In state of out of bound of container
         * @see NoSuchElementException
         * @return The next element of set.
         */
        public T next() {
            if (!hasNext())
                throw (new NoSuchElementException("Cannot go forward anymore!"));
            return (data[index++]);   
        }
        
        /**
         * Returns the previous element in the list and moves the cursor 
         * position backwards.
         * 
         * @throws NoSuchElementException In state of out of bound of container
         * @see NoSuchElementException
         * @return The previous element of set.
         */
        public T previous() {
            if (!hasPrevious())
                throw (new NoSuchElementException("Cannot go backward anymore!"));
            index = (index == currentSize) ? --index : index;
            return (data[index--]);         
        }
        
        /**
         * Controls whether there is an element forward.
         * 
         * @return Returns true if this list iterator has more elements when 
         * traversing the list in the forward direction.
         */
        public boolean hasNext() {
            return (index <= currentSize-1);
        }
        
        /**
         * Controls whether there is an element backward.
         * 
         * @return Returns true if this list iterator has more elements when 
         * traversing the list in the reverse direction.
         */
        public boolean hasPrevious() {
            return (index >= 0);
        }
        
        /**
         * Override of toString method for GTUIterator class.
         * 
         * @return A string of current position and element to which points.
         */
        @Override
        public String toString() {
            return (String.format("%d -> %s" , index, data[index].toString()));
        }  
        
        /**
         * Override of equals method for GTUIterator class.
         * 
         * @param other Object to be compared with this object.
         * @return If this object and given object equals, returns true otherwise 
         *  false.
         */
        @Override
        public boolean equals(Object other) {
            if (this == null)
                return (false);
            if (this == other) 
                return (true);
            if (!(other instanceof GTUSet.GTUIterator))
                return (false);
            
            GTUSet<T>.GTUIterator castObj = (GTUSet<T>.GTUIterator) other;
            
            // Controls index
            if (!(this.index == castObj.index))
                return (false);
            
            return (true);
        }
        
        /**
         * Hash code of the object.
         * 
         * @return Hash code.
         */
        @Override
        public int hashCode() {
            int hash = 7;
            hash = 47 * hash + this.index;
            return hash;
        }

    }
} 