package gtucontainers;

import java.security.InvalidParameterException;
import javafx.util.Pair;

/**
 * Test class of GTUContainers.
 * 
 * @author yusufakdas
 */
public class GTUContainers {

    /**
     * Test method.
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            System.out.println("---------------- SET TEST -----------------"); 

            GTUSet <Integer> firstSet = new GTUSet <>(Integer[].class);
            GTUSet <Integer> secondSet = new GTUSet <>(Integer[].class);

            firstSet.insert(5);
            firstSet.insert(87);
            firstSet.insert(21);
            firstSet.insert(15);
            firstSet.insert(32);
            firstSet.insert(189);
            firstSet.insert(65);
            firstSet.insert(45);
            firstSet.insert(265);
            firstSet.insert(6545);
            firstSet.insert(776);
            firstSet.insert(656);
            firstSet.insert(6);

            print(firstSet);
            System.out.print("First set ");
            if (firstSet.empty()) {
                System.out.println("is empty and size of the first set " + 
                                    firstSet.size());
            } else {
                System.out.println("is not empty and size of the first set " + 
                                    firstSet.size());
            }
            System.out.println();
            
            System.out.print("Second set ");
            if (secondSet.empty()) {
                System.out.println("is empty and size of the first set " + 
                                    secondSet.size());
            } else {
                System.out.println("is not empty and size of the first set " + 
                                    secondSet.size());
            }
            
            System.out.println("Inserting some elements to second set.");
            secondSet.insert(123);
            secondSet.insert(87);
            secondSet.insert(43);
            secondSet.insert(6);
            secondSet.insert(736);
            secondSet.insert(985);
            secondSet.insert(32);
            secondSet.insert(15);
            secondSet.insert(656);
            secondSet.insert(265);
            secondSet.insert(5132);
            
            print(secondSet);
            System.out.println("Second set is not empty and size of the first set " + 
                                secondSet.size());
            System.out.println();
            
            GTUSetInt <Integer> s = firstSet.intersection(secondSet);
            System.out.println("Intersection set of first and second set.");
            System.out.println(s);
            
            System.out.println("##########################################\n");
            
            System.out.println("############ Erasing element #############");
            System.out.println("After erasing.\n");
            firstSet.erase(21);
            firstSet.erase(656);
            firstSet.erase(21);
            firstSet.erase(45);
            firstSet.erase(6545);
            System.out.println("First set: ");
            print(firstSet);
            System.out.println();
            
            secondSet.erase(123);
            secondSet.erase(87);
            secondSet.erase(15);
            secondSet.erase(5132);
            System.out.println("Second set: ");
            print(secondSet);
            System.out.println();
            
            System.out.println("Searching for 32 in intersection set");
            System.out.println("It is found: " + s.find(32));
            
            System.out.println("Count of 656 in second set: " + 
                                secondSet.count(656));
            s.clear();
            System.out.println("The size of intersection set after clear is " +
                                s.size());
            System.out.println("Printing s:" + s);
            System.out.println("##########################################\n");
            
            System.out.println("Adding same element to first set.");
            firstSet.insert(15);
            
        } catch (InvalidParameterException e) {
            System.out.println("#### Exception caught!! ####");
            System.out.println("#### " + e + " ####");
        }
        System.out.println();
        
        try {
            System.out.println("---------------- MAP TEST -----------------"); 
            
            GTUMap <String, Integer> mapFirst = new GTUMap(Pair[].class);
            GTUMap <String, Integer> mapSecond = new GTUMap(Pair[].class);
            
            mapFirst.insert(new Pair<>("Istanbul", 34));
            mapFirst.insert(new Pair<>("Ankara", 6));
            mapFirst.insert(new Pair<>("Erzurum", 25));
            mapFirst.insert(new Pair<>("Sivas", 59));
            mapFirst.insert(new Pair<>("Nigde", 51));
            mapFirst.insert(new Pair<>("Samsun", 55));
            mapFirst.insert(new Pair<>("Aksaray", 68));
            mapFirst.insert(new Pair<>("Konya", 42));
            System.out.println("First Map: ");
            System.out.println(mapFirst + "\n");
            
            mapSecond.insert(new Pair<>("Istanbul", 34));
            mapSecond.insert(new Pair<>("Ankara", 6));
            mapSecond.insert(new Pair<>("Izmir", 35));
            mapSecond.insert(new Pair<>("Gaziantep", 27));
            mapSecond.insert(new Pair<>("Nigde", 51));
            mapSecond.insert(new Pair<>("Trabzon", 61));
            mapSecond.insert(new Pair<>("Aksaray", 68));
            mapSecond.insert(new Pair<>("Adana", 1));
            mapSecond.insert(new Pair<>("Kocaeli", 41));
            System.out.println("Second Map: ");
            System.out.println(mapSecond + "\n");
            
            GTUSetInt <Pair<String, Integer>> m = mapFirst.intersection(mapSecond);
            System.out.println("Intersection set of first and second map.");
            System.out.println(m + "\n");

            System.out.println("After erasing: \n");
            
            mapFirst.erase("Istanbul");	
            mapFirst.erase("Erzurum");	
            mapFirst.erase("Nigde");	
            System.out.println("First map:");
            System.out.println(mapFirst + "\n");
            
            mapSecond.erase("Trabzon"); 
            mapSecond.erase("Aksaray");
            mapSecond.erase("Ankara");
            System.out.println("Second map:");
            System.out.println(mapSecond + "\n");
            
            System.out.println("Using at method:");
            System.out.println(mapFirst.at("Istanbul"));
            System.out.println(mapFirst.at("Konya"));
            System.out.println(mapFirst.at("Nigde"));
            System.out.println(mapFirst.at("Sivas"));
            System.out.println();
            
            System.out.println("Adding same element to first map.");
            mapSecond.insert(new Pair<>("Istanbul", 34));
            System.out.println(mapSecond);
            
        } catch (InvalidParameterException e) {
            System.out.println("#### Exception caught!! ####");
            System.out.println("#### " + e + " ####");
        }
        System.out.println("------------------------------------------"); 
        
    } 
    
    /**
     * Prints given set on the screen by using iterator.
     * 
     * @param <T> Generic type
     * @param s a set as parameter
     */
    public static<T> void print(GTUSet<T> s) {
        GTUSet.GTUIterator it = s.begin();
        while (it.hasNext()) 
            System.out.print(it.next() + " ");
        System.out.println();
    }
 
}
