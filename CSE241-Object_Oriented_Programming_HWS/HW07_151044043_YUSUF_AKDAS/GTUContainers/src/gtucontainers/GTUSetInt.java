package gtucontainers;

/**
 * Interface for Set
 * 
 * @author yusufakdas
 * @param <T> Generic type of element that set keeps.
 * @since 2017-12-24
 * @version 1.4
 */
public interface GTUSetInt <T> {

    /**
     * Controls whether container is empty or not.
     * 
     * @return True or false with respect to containers empty state.
     */
    boolean empty();
    
    /**
     * Current size.
     *
     * @return Total number of elements in the container.
     */
    int size();
    
    /**
     * Maximum size.
     * 
     * @return Capacity of container.
     */
    int max_size();
    
    /**
     * Insert a value to container
     * 
     * @param val Element to be inserted.
     * @return Iterator for new value added.
     */
    GTUSet<T>.GTUIterator insert (T val);
    
    /**
     * Make a new container that is intersection of two.
     * 
     * @param other Given map as parameter to method.
     * @return Intersection of containers.
     */
    GTUSetInt <T> intersection(GTUSetInt<T> other);
    
    /**
     * Erase given element from container.
     * 
     * @param val Element to be erased.
     * @return Number of element erased.
     */
    int erase(Object val);
    
    /**
     * Clears the container.
     */
    void clear();
    
    /**
     * Searches given element in the container.
     * 
     * @param val Element to be searched.
     * @return Iterator to position of element.
     */
    GTUSet<T>.GTUIterator find(Object val);
    
    /**
     * Counts given element in the container.
     * 
     * @param val Element to be counted.
     * @return Numbers of element that is in the container.
     */
    int count(Object val);
    
    /**
     * End of container.
     * 
     * @return Iterator to beginning of container.
     */
    GTUSet<T>.GTUIterator begin();

    /**
     * Beginning of container.
     * 
     * @return Iterator to end of container.
     */
    GTUSet<T>.GTUIterator end();
    
}
