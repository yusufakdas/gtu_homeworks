package gtucontainers;

import java.util.Arrays; // for hashCode
import javafx.util.Pair;
import java.lang.reflect.Array;
import java.security.InvalidParameterException;

/**
 * <h1>Map Implementation for GTU</h1> 
 * 
 * <p>This generic class is a implementation of map and elements in the map are 
 * unique.GTUMap class inherited from GTUSet class.</p>
 * 
 * @author yusufakdas
 * @param <K> Key for map.
 * @param <V> Value for map.
 * @since 2017-12-24
 * @version 1.4
 */
public class GTUMap<K, V> extends GTUSet < javafx.util.Pair<K, V> > {
    
    /**
     * Constructor of the class that takes one parameter. 
     * 
     * @param cls The parameter is array type of data.
     */
    public GTUMap(Class<Pair<K, V>[]> cls) {
        super(cls);
    }
    
    /**
     * Copy constructor of the class. 
     * 
     * @param copy Other GTUMap object.
     */
    public GTUMap(GTUMap<K, V> copy) {
        super(copy);
    }
    
    /**
     * Key as parameter to method.
     * 
     * @param k Key given as parameter.
     * @return If key is found, returns value of key, otherwise null.  
     */
    public V at(K k) { 
        // Search for given key value to determine whether it is present in map
        for (int i = 0; i < currentSize; ++i) 
            if (k.equals(data[i].getKey())) 
                return (data[i].getValue());
        return (null);
    }
    
    /**
     * Inserts a pair to map.
     * 
     * @param p Pair to be inserted to map.
     * @throws InvalidParameterException If the pair is in the map then throw.
     * @see InvalidParameterException
     * @return Iterator for inserted pair.
     */
    @Override
    public GTUIterator insert(Pair<K, V> p) {
        // Controls whether the key value is in map
        if (find(p.getKey()) != null) { // it is in the map, then throw InvalidParameterException.
            throw (new InvalidParameterException("Cannot add an existing "+ 
                                                 "element to map!"));
        } else {  // Cannot be found then add it to map.
            data[currentSize] = p;
            ++currentSize;
            resize();
        }
        return (find(p));
    }
       
    
    /**
     * Searches for given value with respect to key in the container.
     * 
     * @param val The key that is wanted to find.
     * @return If it is found then returns iterator for its place, otherwise 
     *  returns null. 
     */
    @Override
    public GTUIterator find(Object val) {       
        val = (K) val;
        
        for (int i = 0; i < currentSize; ++i)
            if (data[i].getKey().equals(val))  // found
                return (new GTUIterator(i)); 
        return (null); // not found TODO
    }
    
    /**
     * Erases an element from map with respect to key.
     * 
     * @param val Key wanted to erase its pair.
     * @return How many elements are erased.
     */
    @Override
    public int erase(Object val) {      
        int deleted = 0;
        
        val = (K) val;

        if (find(val) != null) { // If it is present then erase it. 
            // Allocates new spaces for erased value version of map.
            Pair<K, V>[] newData;
            newData = classType.cast(Array.newInstance(classType.getComponentType(),
                      capacity));
            deleted = count(val);                   
            // Copies all elements to new allocated place except for given value
            for (int i = 0, j = 0; i < currentSize; ++i) {
                if (!data[i].getKey().equals(val)) { // key
                    newData[j] = data[i];
                    ++j;
                }
            }
            data = newData;
            --currentSize;
        }
        // Because map keeps unique element, if found returns 1, otherwise 0.
        return (deleted);
    }
      
    /**
     * Counts given value with respect to key in the map.
     * 
     * @param val Key that is wanted its number in the container.
     * @return The number of particular element in the map.
     */
    @Override
    public int count(Object val) {
        int counter = 0;
        
        val = (K) val;

        // If found, increment counter by one. 
        for (int i = 0; i < currentSize; ++i)
            if (data[i].getKey().equals(val)) // key
                ++counter;
     
        return (counter);
    }
    
    /**
     * Intersection of two map.
     * 
     * @param otherMap Given map as parameter to method.
     * @return Intersection map.
     */
    @Override
    public GTUSetInt<Pair<K, V>> intersection(GTUSetInt<Pair<K, V>> otherMap) {
        GTUMap <K, V> intersect = new GTUMap <>(classType);
        
        GTUIterator it1 = this.begin();
           
        while (it1.hasNext()) {
            Pair<K, V> var1 = it1.next();
            GTUSet<Pair<K, V>>.GTUIterator it2 = otherMap.begin();
            while (it2.hasNext()) {
                Pair<K, V> var2 = it2.next();
                if (var1.equals(var2)) 
                    intersect.insert(var2);
            }
        }
        return (intersect);
    }
       
    /**
     * Override of toString method for GTUMap class.
     * 
     * @return Contents of map as a string.
     */
    @Override
    public String toString() {
        String str = "";
        
        for (int i = 0; i < size(); ++i) {
            str += data[i].getKey().toString() + " - " + 
                   data[i].getValue().toString();
            if (i != size()-1)
                str += "\n";
        }
        return (str);
    }
    
    /**
     * Override of toString method for GTUMap class.
     * 
     * @param other Object to be compared with this object.
     * @return If this object and given object equals, returns true otherwise 
     *  false.
     */
    @Override
    public boolean equals(Object other) {
        if (other == this)
            return (true);
        if (other == null)
            return (false);
        if (!(other instanceof GTUMap))
            return (false);
        
        // Casts object of type Object to GTUMap
        GTUMap<K, V> castObj = (GTUMap<K, V>) other;
        
        // Control for capacity
        if (this.capacity != castObj.capacity)
            return (false);
        
        // Control for size
        if (this.currentSize != castObj.currentSize)
            return (false);
        
        // Control for element type
        if (!(this.classType.equals(castObj.classType)))
            return (false);
        
        // Control for array
        for (int i = 0; i < currentSize; ++i)
            if (!(this.data[i].equals(castObj.data[i])))
                return (false);
        
        return (true);
    }
    
    /**
     * Hash code of the object.
     * 
     * @return Hash code.
     */
    @Override
    public int hashCode() {
        int hash = 15;
        hash = 57 * hash + Arrays.deepHashCode(this.data);
        return hash;
    }

}  
