#ifndef __IS_GTU_SET__
#define __IS_GTU_SET__
#include "gtuset.h"

namespace GTUContainers {
	
	/*
	 * Returns current size
	 */
	template <class T>
    int GTUSet<T>::size() const
    {
        return (this->currentSize);
    }

	/*
	 * Controls whether set is empty or not 
	 */
	template <class T>
    bool GTUSet<T>::empty() const
    {
        return (this->currentSize == 0 ? true : false);
    }
	
	/*
	 * Returns capacity of set
	 */
	template <class T>
    int GTUSet<T>::max_size() const
    {
        return (this->capacity);
    }

	/*
	 * Returns an iterator that points to the data's first element
	 */
	template <class T>
	typename GTUSetBase<T>::GTUIterator GTUSet<T>::begin() const
	{
		return (typename GTUSetBase<T>::GTUIterator(this->data.get()));
	}

	/*
	 * Returns an iterator that points to past-the-end element
	 */
	template <class T>
	typename GTUSetBase<T>::GTUIterator GTUSet<T>::end() const
	{
		return (typename GTUSetBase<T>::GTUIterator(this->data.get() + 
				this->currentSize));
	}

	/*
	 * Inserts an element to set. If the value is in the set than trows 
	 * invalid_argument exception, otherwise returns a pair for new element.
	 */
	template <class T>
	pair<typename GTUSetBase<T>::GTUIterator, bool> 
	GTUSet<T>::insert(const T& val) 
	{ int i;
		bool found = false;
		
		// Searches for an element
		for (i = 0; i < this->currentSize && !found; ++i)
			if (this->data.get()[i] == val) 
				found = true;
		if (found) { // If it is found then trow invalid_argument
			throw invalid_argument("You cannot add an element that is present!");
		} else {
		  	// Add given value to set.
			this->data.get()[this->currentSize] = val;
			++this->currentSize;
			
			// Controls whether currentSize is equal to capacity.
			this->resize();
			
			// Sort the set.
			this->sort();
		}
		// Returns pair.
		return (pair<typename GTUSetBase<T>::GTUIterator, bool>
				(typename GTUSetBase<T>::GTUIterator(this->data.get() + 
				this->currentSize), !found));
	}

	/*
	 * Erases an element from set and returns how many elements are deleted.
	 */
	template <class T>
    int GTUSet<T>::erase(const T& val)
    {
		bool found = false;

		// Controls whether the value is in set or not.
		for (int i = 0; i < this->currentSize && !found; ++i) 
			if (this->data.get()[i] == val) 
				found = true;

		if (found) { // If it is present then erase it. 
			// Allocates new spaces for erased value version of set.
			shared_ptr<T> newData(new T[this->capacity], 
				 		[](T *p){delete []p;}, allocator<T>());	 
				
			//Copies all elements to new allocated place except for given value
			for (int i = 0, j = 0; i < this->currentSize; ++i) {
				if (this->data.get()[i] != val) {
					newData.get()[j] = this->data.get()[i];
					++j;
				}
			}
			--this->currentSize;
			
			// Swaps data's pointer with newData's and shared_ptr will 
			// deallocate the old data's area.
			this->data.swap(newData);
		}
		// Because set keeps unique element, if found returns 1, otherwise 0.
		return (found ? this->count(val) : 0);
    }
	
	/*
	 * Counts given value in set
	 */
	template <class T>
	int GTUSet<T>::count(const T& val) const 
	{
		int counter = 0;
		
		for (int i = 0; i < this->currentSize; ++i) 
			if (val == this->data.get()[i])
				++counter;
		return (counter);
	}

	/*
	 * Clears all data from set
	 */
	template <class T>
	void GTUSet<T>::clear() 
	{
		this->data.reset();
		this->currentSize = 0;
		shared_ptr<T> newData(new T[this->capacity], 
					[](T *p){delete []p;}, allocator<T>());	 
		this->data = newData;
	}
	
	/* 
	 * Searches for value. If it is found then returns iterator for its place, 
	 * otherwise returns end().
	 */
	template <class T>
	typename GTUSetBase<T>::GTUIterator GTUSet<T>::find(const T& val) const 
	{
		for (auto it = this->begin(); it != this->end(); ++it) 
			if (*it == val) // it is found
				return (it);
		return (this->end()); // it cannot be found
	}

	/*
	 * Resizes the set.
	 */
	template <class T>
	void GTUSet<T>::resize() 
	{
		if (this->currentSize == this->capacity) {
			this->capacity *= 2; 
			shared_ptr<T> newData(new T[this->capacity], 
						[](T *p){delete []p;}, allocator<T>());	 
			for (int i = 0; i < this->currentSize; ++i)
				newData.get()[i] = this->data.get()[i];	

			this->data.swap(newData);
		}
	}

}
#endif
