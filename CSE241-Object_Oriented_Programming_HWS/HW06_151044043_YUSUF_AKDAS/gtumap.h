/*
 * gtumap.h
 */
#ifndef __GTU_MAP_H__
#define __GTU_MAP_H__
#include "gtuset.h"

namespace GTUContainers {

	template <class K, class V>
	class GTUMap : public GTUSet <pair<K, V> > {
	public:
		// Subscript operator
		V& operator[] (const K& k);

		// Searches for a key in map
		typename GTUMap<K, V>::GTUIterator find(const K& key) const;

		// Inserts given pair to map
        pair<typename GTUMap<K, V>::GTUIterator, bool> 
		insert(const pair<K, V>& key);

		// Erases given key's pair from map
        int erase(const K& key);
        
        // Count how many a value in container
        int count (const K& key) const;
	};

}
#include "gtumap.cpp"

#endif
