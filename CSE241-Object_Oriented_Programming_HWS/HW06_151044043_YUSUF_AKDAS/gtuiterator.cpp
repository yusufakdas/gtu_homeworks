#ifndef __IS_ITERATOR__
#define __IS_ITERATOR__
#include "gtusetbase.h"

namespace GTUContainers {
	
	/*
	 * Default constructor of iterator
	 */
	template <class T>
	GTUSetBase<T>::GTUIterator::GTUIterator() : ptr(nullptr) 
	{ }
	
	/*
	 * Constructor that takes a pointer of type T
	 */
	template <class T>
	GTUSetBase<T>::GTUIterator::GTUIterator(T *p) : ptr(p)
	{ }

	/*
	 * Iterator's assignment operator
	 */
	template <class T>
	typename GTUSetBase<T>::GTUIterator& GTUSetBase<T>::GTUIterator::operator =
								(const GTUSetBase<T>::GTUIterator& rightSide)
	{
		ptr = rightSide.ptr;
		return (*this);
	}

	/*
	 * Prefix increment operator
	 */
	template <class T>
	typename GTUSetBase<T>::GTUIterator& 
	GTUSetBase<T>::GTUIterator::operator ++() 
	{
		++ptr;
		return (*this);
	}
	
	/*
	 * Prefix decrement operator
	 */
	template <class T>
	typename GTUSetBase<T>::GTUIterator& 
	GTUSetBase<T>::GTUIterator::operator --() 
	{
		--ptr;
		return (*this);
	}
	
	/*
	 * Postfix increment operator
	 */
	template <class T>
	typename GTUSetBase<T>::GTUIterator 
	GTUSetBase<T>::GTUIterator::operator ++(int ignore) 
	{
		GTUSetBase<T>::GTUIterator tmp(ptr);		
		++ptr;
		return (tmp);
	}

	/*
	 * Postfix decrement operator
	 */
	template <class T>
	typename GTUSetBase<T>::GTUIterator 
	GTUSetBase<T>::GTUIterator::operator --(int ignore) 
	{
		GTUIterator tmp(ptr);		
		--ptr;
		return (tmp);
	}
	
	/*
	 * Dereference operator 
	 */
	template <class T>
	T& GTUSetBase<T>::GTUIterator::operator *()
	{
		return (*ptr);
	}

	/*
	 * Arrow operator operator 
	 */
	template <class T>
	T* GTUSetBase<T>::GTUIterator::operator ->()
	{
		return (ptr);
	}

	/*
	 * Dereference operator for constant
	 */
	template <class T>
	const T& GTUSetBase<T>::GTUIterator::operator *() const
	{
		return (*ptr);
	}

	/*
	 * Arrow operator for constant
	 */
	template <class T>
	const T* GTUSetBase<T>::GTUIterator::operator ->() const
	{
		return (ptr);
	}

	/* 
	 * Equality operator
	 */
	template <class T>
	bool GTUSetBase<T>::GTUIterator::operator ==(const GTUIterator& other) const 
	{
		return (ptr == other.ptr);
	}

	/* 
	 * Not equality operator
	 */
	template <class T>
	bool GTUSetBase<T>::GTUIterator::operator !=(const GTUIterator& other) const 
	{
		return (ptr != other.ptr);
	}

}
#endif
