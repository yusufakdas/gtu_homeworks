/*
 * gtuset.h
 */
#ifndef __GTU_SET_H__
#define __GTU_SET_H__
#include "gtusetbase.h"

namespace GTUContainers {

	template <class T>
    class GTUSet : public GTUSetBase<T> {
    public:

        // Size functions
        bool empty() const;
        int size() const;
        int max_size() const;

        // Insert functions
        std::pair<typename GTUSetBase<T>::GTUIterator, bool> insert(const T& val);

        // Erase functions
        int erase(const T& val);

        // Clear container
        void clear();

        // Find a specified value in container
        typename GTUSetBase<T>::GTUIterator find(const T& val) const;

        // Count how many a value in container
        int count(const T& val) const;

        // Iterators to begin and end
       	typename GTUSetBase<T>::GTUIterator begin() const;
	    typename GTUSetBase<T>::GTUIterator end() const;

		void resize();	
		
    };
}

#include "gtuset.cpp"

#endif
