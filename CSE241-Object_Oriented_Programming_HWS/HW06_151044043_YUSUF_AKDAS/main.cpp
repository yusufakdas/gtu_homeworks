/******************************************************************************
 * author: yusufakdas 														  *
 * last modified data: 13 Dec 2017 - 18:19									  *
 * site: github.com/yusufakdas												  *
 ******************************************************************************/
#include <iostream>
#include "gtusetbase.h"
#include "gtuset.h"
#include "gtumap.h"

using namespace GTUContainers;

using std::cout;
using std::cerr;
using std::endl;

/* 
 * Returns intersection of two object inherited from GTUSetBase.
 */ 
template <class T>
shared_ptr<GTUSetBase<T> > setIntersection(const GTUSetBase<T>& first,
										   const GTUSetBase<T>& second);
/* 
 * Prints GTUSet on screen.
 */
template <class T>
void print(const GTUSet<T>& s);

/* 
 * Prints GTUMap on screen.
 */
template <class K, class V>
void print(const GTUMap<K, V>& m);

int main(int argc, char *argv[])
{
	try {

		cout << "--------------------- SET TEST ----------------------\n";
		GTUSet <int> firstSet, secondSet;
	
		cout << endl;
		cout << "################# Inserting element #################" << endl;
		firstSet.insert(5);
		firstSet.insert(87);
		firstSet.insert(21);
		firstSet.insert(15);
		firstSet.insert(32);
		firstSet.insert(189);
		firstSet.insert(65);
		firstSet.insert(45);
		firstSet.insert(265);
		firstSet.insert(6545);
		firstSet.insert(776);
		firstSet.insert(656);
		firstSet.insert(6);
		
		print(firstSet);
		cout << "First set ";
		if (firstSet.empty())
			cout << "is empty and "<< "size of the first set " 
				 << firstSet.size() << endl;
		else 
			cout << "is not empty and " << "size of the first set " 
				 << firstSet.size() << endl;
		cout << endl;
	
		cout << "Second set ";
		if (secondSet.empty())
			cout << "is empty and "<< "size of the second set " 
				 << secondSet.size() << ".\n" ;
		else 
			cout << "is not empty and " << "size of the second set " 
				 << secondSet.size() << ".\n";
		cout << "Inserting some elements to second set.\n";
		
		secondSet.insert(123);
		secondSet.insert(87);
		secondSet.insert(43);
		secondSet.insert(6);
		secondSet.insert(736);
		secondSet.insert(985);
		secondSet.insert(45);
		secondSet.insert(32);
		secondSet.insert(15);
		secondSet.insert(656);
		secondSet.insert(265);
		secondSet.insert(5132);
		
		print(secondSet);
		cout << "Second set ";
		cout << "is not empty and " << "size of the second set " 
			 << secondSet.size() << ".\n";
		cout << endl;
		
		shared_ptr <GTUSetBase <int> > s; 
		s = setIntersection(firstSet, secondSet);
		cout << "Intersection set of first and second set.\n";
		for (auto it = s->begin(); it != s->end(); ++it)
			cout << *it << " ";
		cout << endl;

		cout << "#####################################################" << endl;
		cout << endl;
		
		cout << "################## Erasing element ##################" << endl;
		cout << "After erasing.\n\n";
		firstSet.erase(21);
		firstSet.erase(656);
		firstSet.erase(21);
		firstSet.erase(45);
		firstSet.erase(6545);
		cout << "First set: " << endl;
		print(firstSet);
		
		cout << endl;

		secondSet.erase(123);
		secondSet.erase(87);
		secondSet.erase(15);
		secondSet.erase(5132);
		cout << "Second set: " << endl;
		print(secondSet);
		cout << endl;

		auto it = s->find(32);
		cout << "Searching for 32 in intersection set.\n" ;
		cout << "It is found: " << *it << endl;
		cout << endl;

		cout << "Count of 656 in second set: " << secondSet.count(656) << ".\n";
		s->clear();
		cout << "The size of intersection set after clear is " << s->size() 
			 << ".\n";

		cout << "#####################################################" << endl;
		cout << endl;
		
		cout << "Adding same element to first set.\n";
		firstSet.insert(15);
		
	} catch (invalid_argument value) {

		cout << "Exception thrown!\n";
		cerr << value.what() << endl;
		
	}
	
	try {
	
		cout << "\n--------------------- MAP TEST ----------------------\n";
		cout << endl;

		GTUMap <string, int> mapFirst, mapSecond;
		
		cout << "First Map: " << endl;
		mapFirst["Istanbul"] = 34;
		mapFirst["Ankara"] = 6;
		mapFirst.insert(make_pair("Erzurum", 25));
		mapFirst.insert(make_pair("Sivas", 59));
		mapFirst.insert(make_pair("Nigde", 51));
		mapFirst["Samsun"] = 55;
		mapFirst["Aksaray"] = 68;
		print(mapFirst);
		cout << endl;

		cout << "Second Map: " << endl;
		mapSecond["Istanbul"] = 34;
		mapSecond["Ankara"] = 6;
		mapSecond.insert(make_pair("Izmir", 35));
		mapSecond.insert(make_pair("Gaziantep", 27));
		mapSecond.insert(make_pair("Nigde", 51));
		mapSecond["Trabzon"] = 61;
		mapSecond["Aksaray"] = 68;
		print(mapSecond);
		cout << endl;

		shared_ptr<GTUSetBase<pair<string, int> > > m;
		m = setIntersection(mapFirst, mapSecond);
		cout << "Intersection set of first and second map.\n";
		for (auto it = m->begin(); it != m->end(); ++it)
			cout << it->first << " - " << it->second << endl;
		cout << endl;
	
		cout << "After erasing." << endl;
		cout << endl;

		cout << "First map:" << endl;
		mapFirst.erase("Istanbul");	
		mapFirst.erase("Erzurum");	
		mapFirst.erase("Nigde");	
		print(mapFirst);
		cout << endl;

		cout << "Second map:" << endl;
		mapSecond.erase("Trabzon"); 
		mapSecond.erase("Aksaray");
		mapSecond.erase("Ankara");
		print(mapSecond);
		cout << endl;

		cout << "Adding same element to second map.\n";
		mapSecond.insert(make_pair("Istanbul", 34));

	} catch (invalid_argument value) {

		cout << "Exception thrown!\n";
		cerr << value.what();
		cout << "\n------------------------------------------------------\n";

	}
	return (0);
}

template <class T>
shared_ptr<GTUSetBase<T> > setIntersection(const GTUSetBase<T>& first, 
										   const GTUSetBase<T>& second)
{
	shared_ptr<GTUSet<T> >intersect(new GTUSet<T>);

	for (auto it1 : first)
		for (auto it2 : second)
			if (it1 == it2)
				intersect->insert(it1);

	return (intersect);
}

template <class T>
void print(const GTUSet<T>& s)
{
	typename GTUSet<T>::GTUIterator it;
	
	for (it = s.begin(); it != s.end(); ++it)
		cout << *it << ' ';
	cout << endl;
}

template <class K, class V>
void print(const GTUMap<K, V>& m)
{
	for (auto it = m.begin(); it != m.end(); ++it)
		cout << it->first << " - " << it->second << endl;
}
