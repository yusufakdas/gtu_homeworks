#ifndef __IS_MAP__
#define __IS_MAP__
#include "gtumap.h"

namespace GTUContainers {
	
	/*
	 * Subscript operator for Map. If element is in the map then returns 
	 * its value reference, otherwise element is added and returns its value 
	 * reference.
	 */
	template <class K, class V> 
	V& GTUMap<K, V>::operator [](const K& k) { 
		int index = -1;
		
		// Search for given key value to determine whether it is present in map
		for (int i = 0; i < this->currentSize; ++i) 
			if (k == this->data.get()[i].first) 
				index = i;
		
		// If it is not found, then add it to map.
		if (index == -1) {
			this->data.get()[this->currentSize] = make_pair(k, V());
			++this->currentSize;
			
			// Controls whether current size is equal to capacity
			this->resize();
			
			// Sort the map
			this->sort();
			
			// Search for the place of the element added after sorting
			for (int i = 0; i < this->currentSize; ++i) 
				if (k == this->data.get()[i].first) 
					index = i;
		}
		return (this->data.get()[index].second);
	}

	/* 
	 * Searches for an element's key value. If it is found then returns iterator
	 * for its place, otherwise returns end().
	 */
	template <class K, class V> 
	typename GTUMap<K, V>::GTUIterator GTUMap<K, V>::find(const K& key) const
	{
		for (auto it = this->begin(); it != this->end(); ++it) 
			if (*it = key) // it is found
				return (it);
		return (this->end()); // it cannot be found
	}

	/*
	 * Inserts an element to map and returns a pair that has of type iterator 
	 * as first element and of type bool as its second element. If the key value
	 * is found, then trows invalid_argument exception.
	 */
	template <class K, class V> 
	pair<typename GTUMap<K, V>::GTUIterator, bool> 
	GTUMap<K, V>::insert(const pair<K, V>& key)
	{
		int i;
		bool found = false;
		
		// Controls whether the key value is in map
		for (i = 0; i < this->currentSize && !found; ++i)
			if (this->data.get()[i].first == key.first) 
				found = true;
		if (found) { // it is found, then trow invalid_argument.
			throw invalid_argument("You cannot add an element that is present!");
		} else { // Cannot be found then add it to map.
			this->data.get()[this->currentSize] = key;
			++this->currentSize;
			this->resize();
		}
		this->sort(); // Of course, sort the map.
		return (pair<typename GTUMap<K, V>::GTUIterator, bool>
				(typename GTUMap<K, V>::GTUIterator(this->data.get() + 
				this->currentSize), !found));
	}
	
	/*
	 * Erases an element from map and returns how many elements are deleted.
	 */
	template <class K, class V>
	int GTUMap<K, V>::erase(const K& key) 
	{
		bool found = false;

		// Controls whether the key value is in map
		for (int i = 0; i < this->currentSize && !found; ++i) 
			if (this->data.get()[i].first == key) 
				found = true;

		if (found) { // If it is present then erase it. 
			// Allocates new spaces for erased value version of map.
			shared_ptr<pair<K, V> > newData(new pair<K, V>[this->capacity], 
					[](pair<K, V> *p){delete []p;}, allocator<pair<K, V> >());
					
			//Copies all elements to new allocated place except for given value.
			for (int i = 0, j = 0; i < this->currentSize; ++i) {
				if (this->data.get()[i].first != key) {
					newData.get()[j] = this->data.get()[i];
					++j;
				}
			}
			--this->currentSize;
			// Swaps data's pointer with newData's and shared_ptr will 
			// deallocate the old data's area.
			this->data.swap(newData);  
		}
		// Because map keeps unique element, if found returns 1, otherwise 0.
		return (found ? 1 : 0); 
	}
    
    /*
	 * Counts given key in map
	 */
    template <class K, class V>
    int GTUMap<K, V>::count (const K& key) const
    {
        int counter = 0;
		
		for (int i = 0; i < this->currentSize; ++i) 
			if (key == this->data.get()[i].first)
				++counter;
		return (counter);
    }

}
#endif
