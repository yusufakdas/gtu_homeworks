#ifndef __IS_SET_BASE__
#define __IS_SET_BASE__
#include "gtusetbase.h"

namespace GTUContainers {

 	/*
 	 * Default constructor
 	 */
	template <class T>
    GTUSetBase<T>::GTUSetBase() : currentSize(0), capacity(128)
    {
    	data = shared_ptr<T>(new T[capacity], 
					[](T *p){delete []p;}, allocator<T>());	 
    }

	/*
	 * Copy constructor
	 */
	template <class T>
	GTUSetBase<T>::GTUSetBase(const GTUSetBase& other) 		
		: capacity(other.capacity), currentSize(other.currentSize)
	{
    	data = shared_ptr<T>(new T[capacity], 
					[](T *p){delete []p;}, allocator<T>());	 
		for (int i = 0; i < currentSize; ++i) 
			data.get()[i] = other.data.get()[i];
	}
	
	/*
	 * Assignment operator
	 */ 
	template <class T>
	GTUSetBase<T>& GTUSetBase<T>::operator =(const GTUSetBase& rightSide)
	{
		if (this != &rightSide) {
			data.reset();

			currentSize = rightSide.currentSize;
			capacity = rightSide.capacity;

			data = shared_ptr<T>(new T[capacity], 
					[](T *p){delete []p;}, allocator<T>());	 
			for (int i = 0; i < currentSize; ++i)
				data.get()[i] = rightSide.data.get()[i];
		}
		return (*this);
	}

	/*
	 * Destructor
	 */
	template <class T>
	GTUSetBase<T>::~GTUSetBase()
	{
		// This function is empty because share pointer will relase the 
		// allocated space automatically.
	}

	/*
	 * Selection sort
	 */
	template <class T>
	void GTUSetBase<T>::sort() 
	{
		for (int i = 0; i < currentSize; ++i) {
			for (int j = 0; j < currentSize; ++j) {
				if (data.get()[i] < data.get()[j]) {
					T tmp = data.get()[i];
					data.get()[i] = data.get()[j];
					data.get()[j] = tmp;
				}
			}
		}
	}

}
#endif
