/*
 * gtusetbase.h
 * created date: 9 Dec 2017 - 14:32
 */
#ifndef __GTU_SET_BASE_H__
#define __GTU_SET_BASE_H__
#include <memory>
#include <utility> // For pair container
#include <exception>
using namespace std;

namespace GTUContainers {

	template <class T>
	class GTUSetBase {
	public:
		// Forward declaration of GTUIterator class
		class GTUIterator;

		// No parameter constructor
		GTUSetBase();

		// Copy constructor
		GTUSetBase(const GTUSetBase& other);

		// Assignment operator
		GTUSetBase& operator =(const GTUSetBase& rightSide);
		
		// Virtual desructor for future use
		virtual ~GTUSetBase();

		// Size functions
		virtual bool empty() const = 0;
		virtual int size() const = 0;
		virtual int max_size() const = 0;

		// Insert function
		virtual pair<GTUIterator, bool> insert(const T& val) = 0;

		// Erase function
		virtual int erase(const T& val) = 0;

		// Clear container
		virtual void clear() = 0;

		// Find a specified value in container
		virtual GTUIterator find(const T& val) const = 0;

		// Count how many a value in container
		virtual int count(const T& val) const = 0;
		
		// Iterators to begin and end
		virtual GTUIterator begin() const = 0;
		virtual GTUIterator end() const = 0;

		// Selection sort 
		void sort();

	public:
		// Iterator class
		class GTUIterator {
		public:
			/* Constructors */
			GTUIterator();
			GTUIterator(T *p);
			GTUIterator(int index);
			
			// Assignment operator
			GTUIterator& operator =(const GTUIterator& rightSide);

			// Pre increment and decrement operators
			GTUIterator& operator ++();
			GTUIterator& operator --();

			// Post increment and decrement operators
			GTUIterator operator ++(int ignore);
		 	GTUIterator operator --(int ignore);

			// Relation operators 
			bool operator ==(const GTUIterator& other) const;
			bool operator !=(const GTUIterator& other) const;

			// Dereference and arrow operators 
			T& operator *();
			T* operator ->();
			const T& operator *() const;
			const T* operator ->() const;
	
		private:
			T *ptr;
		};

    protected:
        shared_ptr <T> data;
        int currentSize;
        int capacity;
	};

}
#include "gtuiterator.cpp"
#include "gtusetbase.cpp"

#endif
