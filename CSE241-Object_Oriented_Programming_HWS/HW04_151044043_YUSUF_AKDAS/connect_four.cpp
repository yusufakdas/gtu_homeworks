/******************************************************************************
 * >> Connect Four v1.4  << 												  *
 *																			  *
 * filename: 'connect_four.cpp'											 	  *
 * author: yusufakdas														  *
 * last modified date: 11:06 p.m. - 7 Nov 2017								  *
 * site: github.com/yusufakdas												  *
 ******************************************************************************/
#include "connect_four.h"

int ConnectFour::livingCells = 0;

/*
 * Constructor that creates a 5x5 game board as default.
 */
ConnectFour::ConnectFour() : row(5), column(5), order(0),
						cellSize(0), gameType('P'), filename("")
{
	userMove = Cell(ConnectFour::EMPTY);
	computerMove = Cell(ConnectFour::EMPTY);

	// Allocates memory using allocate function.
	allocate();

	// Initializes the game board.
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < column; ++j) {
			gameBoard[i][j].setType(ConnectFour::EMPTY);
			gameBoard[i][j].setRowNumber(i);
			gameBoard[i][j].setPosition(static_cast<char> (j + 'A'));
		}
	}
	
	// Initializes lastRow array.
	for (int i = 0; i < column; ++i)
		lastRow[i] = row;
}

/*
 * Constructor that takes row and column.
 */
ConnectFour::ConnectFour(int r, int c) : row(r), column(c),
			  order(0), cellSize(0), gameType('P'), filename("")
{
	userMove = Cell(ConnectFour::EMPTY);
	computerMove = Cell(ConnectFour::EMPTY);

	// Allocates memory using allocate function.
	allocate();

	// Initializes the game board.
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < column; ++j) {
			gameBoard[i][j].setType(ConnectFour::EMPTY);
			gameBoard[i][j].setRowNumber(i);
			gameBoard[i][j].setPosition(static_cast<char> (j + 'A'));
		}
	}
	
	// Initializes lastRow array.
	for (int i = 0; i < column; ++i)
		lastRow[i] = row;
}

/*
 * Constructor that takes game board from a file.
 */
ConnectFour::ConnectFour(const string& filename)
					: row(5), column(5), order(0), cellSize(0),
			  		  gameType('P'), filename("")
{
	userMove = Cell(ConnectFour::EMPTY);
	computerMove = Cell(ConnectFour::EMPTY);

	resize(filename);
}

/*
 * Copy constructor for ConnectFour class
 */
ConnectFour::ConnectFour(const ConnectFour& copy)
{
	row = copy.row;
	column = copy.column;
	gameType = copy.gameType;
	order = copy.order;
	cellSize = copy.cellSize;
	filename = copy.filename;

	allocate();

	// Copies gameBoard from copy to this
	for (int i = 0; i < row; ++i)
		for (int j = 0; j < column; ++j)
			gameBoard[i][j] = copy.gameBoard[i][j];

	// Copies lastRow from copy to this object
	for (int i = 0; i < column; ++i)
		lastRow[i] = copy.lastRow[i];

	// Copies winnerCheckers array from copy to this object
	for (int i = 0; i < 4; ++i)
		winnerCheckers[i] = copy.winnerCheckers[i];
}

/*
 * Destruct the object when end of scope is reached
 */
ConnectFour::~ConnectFour()
{
	ConnectFour::livingCells -= cellSize;
	deallocate();
}

/*
 * Assignment operator for ConnectFour class
 */
ConnectFour& ConnectFour::operator =(const ConnectFour& rightSide)
{
	// Controls that the incoming object does not equal to this object
	if (this != &rightSide) {
		// Does deep copy
		deallocate();             // Deallocates this object's game board

		row = rightSide.row;
		column = rightSide.column;
		gameType = rightSide.gameType;
		order = rightSide.order;
		cellSize = rightSide.cellSize;
		filename = rightSide.filename;

		allocate();				  // Allocates the game board array

		// Copies gameBoard from rightSide to this
		for (int i = 0; i < row; ++i)
			for (int j = 0; j < column; ++j)
				gameBoard[i][j] = rightSide.gameBoard[i][j];

		// Copies lastRow array from rightSide to this object
		for (int i = 0; i < column; ++i)
			lastRow[i] = rightSide.lastRow[i];

		// Copies winnerCheckers array from rightSide to this object
		for (int i = 0; i < 4; ++i)
			winnerCheckers[i] = rightSide.winnerCheckers[i];
	}
	return (*this);
}

/*
 * Compares two ConnectFour object each other.
 */
bool ConnectFour::operator ==(const ConnectFour& other) const
{
	// Compares gameboards
	for (int i = 0; i < row; ++i)
		for (int j = 0; j < column; ++j)
			if (gameBoard[i][j] != other.gameBoard[i][j])
				return (false);

	// Compares other components
	if (row == other.row && column == other.column &&
		gameType == other.gameType && order == other.order &&
	 	userMove == other.userMove && cellSize == other.cellSize)
	{
		if (gameType == 'C') {
			if (computerMove == other.computerMove)
				return (true);
			else
				return (false);
		} else {
			return (true);
		}
	}
	return (false);
}

/*
 * Used equality operator.
 */
bool ConnectFour::operator !=(const ConnectFour& other) const
{
	return (!(*this == other));
}

/*
 * Prints whole game board on the screen
 */
void ConnectFour::printGameBoard() const
{
	// Letter column
	for (int i = 0; i < column; ++i)
		cout << static_cast<char> ('A' + i) << ' ';
	cout << endl;

	// Game board
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < column; ++j)
			cout << gameBoard[i][j] << ' ';
		cout << endl;
	}
	cout << "LivingCell: " << ConnectFour::getLivingCells() << endl;
	cout << endl;
}

/*
 * Allocates memory row x column
 */
void ConnectFour::allocate()
{
	// Allocates memory spaces for array that keeps last row numbers
	lastRow = new int[column];

	// Allocates memory space for rows
	gameBoard = new ConnectFour::Cell*[row];

	// Allocates memory space for column and
	for (int i = 0; i < row; ++i)
		gameBoard[i] = new ConnectFour::Cell[column];

	// Allocates memory space for winner's checkers.
	winnerCheckers = new ConnectFour::Cell[4];
}

/*
 * Deallocates memory row x column
 */
void ConnectFour::deallocate()
{
	// Deallocate arrays inside
	for (int i = 0; i < row; ++i)
		delete[] gameBoard[i];

	// Deallocate array of pointer arrays
	delete[] gameBoard;

	// Deallocate array of winners' checkers
	delete[] winnerCheckers;

	// Deallocate the lastRow array
	delete[] lastRow;

	// Prevents from dangling pointer
	gameBoard = nullptr;
	winnerCheckers = nullptr;
	lastRow = nullptr;
}

/*
 * s the game according to given file
 */
bool ConnectFour::resize(const string& filename)
{
	row = 0;
	column = 0;
	order = 0;

	// Opens file
	ifstream fileStream(filename.c_str());

	// If file couldn't be opened, function ended in false.
	if (fileStream.fail())
		return (false);

	// Gets the row column size
	char ch;
	int tmp = 0;
	while (fileStream.get(ch)) {
		if (ch == '\n') {
			if (column < tmp)
				column = tmp;
			++row;
			tmp = 0;
		} else {
			++tmp;
		}
	}

	// Allocate memory from heap
	allocate();

	// Rewinds the file to beginning.
	fileStream.clear();    // Cleans the current state of ifstream
	fileStream.seekg(0);   // Rewind the file

	// Creates the game board
	auto i = 0, j = 0;
	while (fileStream.get(ch)) {
		if (ch == '\n') {
			++i;  // Increment row number by one
			j = 0;
		} else {
			if (ch == '*') {
				gameBoard[i][j].setType(ConnectFour::EMPTY);
				gameBoard[i][j].setRowNumber(i);
				gameBoard[i][j].setPosition(static_cast<char> (j + 'A'));
			}
			++j;  // increment column number by one
		}
	}

	// Initializes lastRow dynamic array
	for (int i = 0; i < column; ++i) {
		for (int j = row - 1; j >= 0; --j) {
			if (gameBoard[j][i].getType() == ConnectFour::EMPTY) {
				lastRow[i] = j+1;
				break;
			} else {
				lastRow[i] = 0;
			}
		}
	}

	fileStream.close(); // Closes the file
	return (true);
}

/*
 * Resizes the game according to given row and column number
 */
void ConnectFour::resize(int r, int c)
{
	deallocate();
	row = r;
	column = c;
	allocate();

	for (int i = 0; i < column; ++i)
		lastRow[i] = row;
}

/*
 * Returns how many cells are in the game
 */
int ConnectFour::getLivingCells()
{
	return (ConnectFour::livingCells);
}

/*
 * Returns the row number
 */
int ConnectFour::getRowNumber() const
{
	return (row);
}

/*
 * Returns the column number
 */
int ConnectFour::getColumnNumber() const
{
	return (column);
}

/*
 * Returns the game type (P, C)
 */
char ConnectFour::getGameType() const
{
	return (gameType);
}

/*
 * Returns player order
 * 	  "0 -> PLAYER1",  " 1 -> PLAYER2",
 *	  "2 -> COMPUTER", "-1 -> INVALID"
 */
int ConnectFour::getPlayerOrder() const
{
	return (order);
}

/*
 * Control whether a column is legal or not;
 */
bool ConnectFour::isLegal(int r, int c, ConnectFour::Checker check) const
{
	return (r >= 0 && c >= 0 && c < column &&
			gameBoard[r][c].getType() == check);
}

/*
 * Plays the game for user for single time step
 */
void ConnectFour::play(char pos)
{
	int c = static_cast <int> (pos - 'A');
	int r = lastRow[c] - 1;

	if (isLegal(r, c, ConnectFour::EMPTY)) {
		if (order == 0)  // Player 1
			gameBoard[r][c].setType(ConnectFour::PLAYER1);
		else			 // Player 2
			gameBoard[r][c].setType(ConnectFour::PLAYER2);
		userMove = gameBoard[r][c];
		--lastRow[c];
		++cellSize;
		++ConnectFour::livingCells;
		if (gameType == 'P')  // Fixes order
			order = (order == 0) ? 1 : 0; // from p1 to p2, vice versa.
		else
			order = 2;
	} else {
		cerr << "#### You cannot move on this column."
			 << " Choose another one! ####" << endl;
	}
}

/*
 * Plays the game for computer for single time step
 */
void ConnectFour::play()
{
	int c, r;

	if (preventUserMoves())
		;
	else
		determineComputerMoves();

	r = computerMove.getRowNumber();						 // ROW
	c = static_cast<int> (computerMove.getPosition() - 'A'); // COLUMN

	cout << "Computer has made a move." << endl;
	gameBoard[r][c].setType(ConnectFour::COMP);
	--lastRow[c];
	++cellSize;
	++ConnectFour::livingCells;
	order = 0;
}

/*
 * Plays a whole game in pvp or pvc mode by taking a filename
 */
void ConnectFour::playGame()
{
	// Resize function (for filename) is called in this function.
	getGameTypeAndFileName();

	bool end = false;
	while (!end) {
		int get;
		string input;
		char position;
		printGameBoard();
		if (order != 2)
			get = getInput(input, position);
		if (get == 1)		// SAVE mode
			saveTheGame();
		else if (get == 2)
			loadTheGame();	// LOAD mode
		if (gameType == 'P' && get == 0) {
			play(position);
			end = controlEnd();
		} else if (gameType == 'C' && get == 0) {
			if (order == 0)
				play(position);
			else
				play();
			end = controlEnd();
		}
	}
	printGameBoard();
	showResult();
}

/*
 * Controls end of the game
 */
bool ConnectFour::controlEnd()
{
	ConnectFour::Cell move = userMove;

	// Checker control
	if (gameType == 'C' && order == 0) // Fixes move cell
		move = computerMove;

	if (checkDraw()) {
		order = -1;
		return (true);
	}
	if (checkVertical(move) || checkHorizontal(move) ||
	 	checkRightDiagonal(move) || checkLeftDiagonal(move)) {
		changeCheckers();
		if (gameType == 'P')  // Fixes order
			order = (order == 0) ? 1 : 0; // from p1 to p2, vice versa.
		else
			order = (order == 0) ? 2 : 0;
		return (true);
	}
	return (false);
}

/*
 * Prevents user from winning the game
 */
bool ConnectFour::preventUserMoves()
{
	int r = userMove.getRowNumber();
	int c = static_cast <int> (userMove.getPosition() - 'A');
	bool found = false;
	int count = 0;

	// Vertical check
	for (int i = 0; i < 3 && !found; ++i) {
		if (r + i < row) {
			if (isLegal(r + i, c, userMove.getType()))
				count += 1;
			if (count == 3 && gameBoard[r-1][c].getType() == ConnectFour::EMPTY) {
				found = true;
				computerMove.setRowNumber(r - 1);
				computerMove.setPosition(userMove.getPosition());
			}
		}
	}

	// Horizontal check
	for (int i = 0; i < column && !found; ++i) {
		if (gameBoard[r][i].getType() == ConnectFour::EMPTY) {
			Cell tmp = gameBoard[r][i];
			gameBoard[r][i].setPosition('A' + i);
			gameBoard[r][i].setRowNumber(r);
			gameBoard[r][i].setType(userMove.getType());
			if (checkHorizontal(userMove)) {
				if (r == row - 1) {
					computerMove.setRowNumber(r);
					computerMove.setPosition('A' + i);
					c = static_cast <int> ('A' + i);
					found = true;
				}
				if (!found && !isLegal(r + 1, i, ConnectFour::EMPTY)) {
					computerMove.setRowNumber(r);
					computerMove.setPosition('A' + i);
					c = static_cast <int> ('A' + i);
					found = true;
				}
			}
			gameBoard[r][i] = tmp;
		}
	}
	return (found);
}
/*
 *
 */
void ConnectFour::determineComputerMoves()
{
	srand(time(NULL));
	int c, r;

	if (ConnectFour::getLivingCells() == 1) {
		c = rand() % column;
		r = lastRow[c]-1;
		if (!isLegal(r, c, ConnectFour::EMPTY)) {
			c = rand() % column;
			r = lastRow[c] - 1;
		}
		computerMove.setPosition(c + 'A');
		computerMove.setRowNumber(r);
		computerMove.setType(ConnectFour::COMP);
	} else {
		bool found = false;
		int count = 0;
		int maxCount = 0;
		int	maxRow;
		char maxCol;

		// Vertical test
		for (int i = 0; i < column && !found; ++i) {
			r = lastRow[i]-1;
			if (r < row && isLegal(r, i, ConnectFour::COMP)) {
				for (int j = 0; j < 3; ++j)
					if (j + r < column && isLegal(r + j, i, ConnectFour::COMP)) {
						++count;
					if (count >= maxCount) {
						maxRow = r-1;
						maxCol = 'A'+i;
						maxCount = count;
					}
					if (count == 3)
						found = true;
				}
			}
			count = 0;
		}
		char ad;
		if (maxCount > 0 && maxRow >= 0 && maxRow < row) {
			ad = maxCol;
			r = maxRow;
		} else {
			ad = (rand() % column) + 'A';
			c = static_cast <int> (ad - 'A');
			r = lastRow[c] - 1;
			while (r < 0 || r >= row || !isLegal(r, c, ConnectFour::EMPTY)) {
				ad = (rand() % column) + 'A';
				c = static_cast <int> (ad - 'A');
				r = lastRow[c]-1;
			}
		}
		computerMove.setType(ConnectFour::COMP);
		computerMove.setPosition(ad);
		computerMove.setRowNumber(r);
	}
}
///////////////////////////////////////////////////////////////////////////////

/*
 * Displays who wins the game at end of the game
 */
void ConnectFour::showResult() const
{
	if (order == 0)
		cout << "Player ONE has just won!" << endl;
	else if (order == 1)
		cout << "Player TWO has just won!" << endl;
	else if (order == 2)
		cout << "Computer has just won!" << endl;
	else
		cout << "Nobody won this game!" << endl;
}

/*
 * Checks whether game ends in draw
 */
bool ConnectFour::checkDraw() const
{
	for (int i = 0; i < column; ++i)
		if (isLegal(lastRow[i] - 1 , i, ConnectFour::EMPTY))
			return (false);
	return (true);
}

/*
 * Checks game board vertically.
 */
bool ConnectFour::checkVertical(const ConnectFour::Cell& move)
{
	bool found = false;
	int checkerCount = 0;
	int c = static_cast <int> (move.getPosition() - 'A');

	int j = 0;
	for (int i = 0; i < row && !found; ++i) {
		if (isLegal(i, c, move.getType())) {
			winnerCheckers[j] = gameBoard[i][c];
			++checkerCount;
			++j;
		} else {
			j = 0;
			checkerCount = 0;
		}
		found = (checkerCount == 4) ? true : false;
	}
	return (found);
}

/*
 * Checks game board horizontally.
 */
bool ConnectFour::checkHorizontal(const ConnectFour::Cell& move)
{
	bool found = false;
	int checkerCount = 0;
	int r = move.getRowNumber();

	int j = 0;
	for (int i = 0; i < column && !found; ++i) {
		if (isLegal(r, i, move.getType())) {
			winnerCheckers[j] = gameBoard[r][i];
			++checkerCount;
			++j;
		} else {
			j = 0;
			checkerCount = 0;
		}
		found = (checkerCount == 4) ? true : false;
	}
	return (found);
}

/*
 * Checks game board left dioganally.
 */
bool ConnectFour::checkLeftDiagonal(const ConnectFour::Cell& move)
{
	bool found = false, one = false, two = false;
	int checkerCount = 0;
	int c = static_cast<int> (move.getPosition() - 'A');
	int r = move.getRowNumber();

	int j = 0;
	for (int i = 0; i < 4 && !found; ++i) {
		if (!one && r-i >= 0 && c-i >= 0) {
			if (isLegal(r-i, c-i, move.getType())) {
				winnerCheckers[j] = gameBoard[r-i][c-i];
				++checkerCount;
				++j;
			} else {
				one = true;
			}
		}
		if (i != 0 && !two && r+i < row && c+i >= 0) {
			if (isLegal(r+i, c+i, move.getType())) {
				winnerCheckers[j] = gameBoard[r+i][c+i];
				++checkerCount;
				++j;
			} else {
				two = true;
			}
		}
		found = (checkerCount == 4) ? true : false;
	}
	return (found);
}

/*
 * Checks game board right dioganally.
 */
bool ConnectFour::checkRightDiagonal(const ConnectFour::Cell& move)
{
	bool found = false, one = false, two = false;
	int checkerCount = 0;
	int c = static_cast<int> (move.getPosition() - 'A');
	int r = move.getRowNumber();

	int j = 0;
	for (int i = 0; i < 4 && !found; ++i) {
		if (!one && r+i < row && c-i >= 0) {
			if (isLegal(r+i, c-i, move.getType())) {
				winnerCheckers[j] = gameBoard[r+i][c-i];
				++checkerCount;
				++j;
			} else {
				one = true;
			}
		}
		if (i != 0 && !two && r-i >= 0 && c+i < column) {
			if (isLegal(r-i, c+i, move.getType())) {
				winnerCheckers[j] = gameBoard[r-i][c+i];
				++checkerCount;
				++j;
			} else {
				two = true;
			}
		}
		found = (checkerCount == 4) ? true : false;
	}
	return (found);
}

/*
 * Make winner's checker's small at the end of the game
 */
void ConnectFour::changeCheckers()
{
	ConnectFour::Checker check = winnerCheckers[0].getType();

	for (int i = 0; i < 4; ++i) {
		int r = winnerCheckers[i].getRowNumber();
		int c = static_cast<int> (winnerCheckers[i].getPosition() - 'A');
		if (check == ConnectFour::PLAYER1) {
			gameBoard[r][c].setType(ConnectFour::WINX);
		} else if (check == ConnectFour::PLAYER2 || check == ConnectFour::COMP) {
			gameBoard[r][c].setType(ConnectFour::WINO);
		}
	}
}
////////////////////////////////////////////////////////////////////////////////
/*
 * Gets user moves and commands.
 * Returns values: "0 --> MOVE", "1 --> SAVE", "2 --> LOAD", "-1 --> INVALID"
 */
int ConnectFour::getInput(string& request, char& letter)
{
	const char MIN = 'A';
	const char MAX = static_cast <char> ('A' + column - 1);
	int retValue = -1; // Default return value is -1 (invalid).
	if (order == 0)
		cout << "P1: ";
	else
		cout << "P2: ";
	if (!getline(cin, request)) {
		cerr << endl << "#### END OF FILE REACHED ####" << endl;
		exit(1);
	}
	if (request.length() == 1) {
		letter = request[0];
		if (letter >= MIN && letter <= MAX)
			retValue = 0; // MOVE return value
		else
			cerr << "#### You should enter a column between '" << MIN
				 << "' and '" << MAX << "'! ####" << endl;
	} else if (request.compare(0, 4, "SAVE") == 0 && request.length() > 5) {
		getFileName(request);
		retValue = 1;  // SAVE return value
	} else if (request.compare(0, 4, "LOAD") == 0 && request.length() > 5) {
		getFileName(request);
		retValue = 2;  // LOAD return value
	} else {
		cerr << "#### Enter a column letter or to save the game "
			 << "'SAVE filename' and to load a game 'LOAD filename' ####"
			 << endl;
	}
	return (retValue);
}

/*
 * Empties buffer
 */
void ConnectFour::emptyBuffer() const {
	while (cin.get() != '\n')
		;
}

/*
 * Splits input and get file name from it
 */
void ConnectFour::getFileName(const string& request)
{
	filename = "";
	for (unsigned int i = 5; i < request.length(); ++i)
		if (request[i] != ' ')
			filename += request[i];
}


/*
 * Gets game type and file name from user
 */
void ConnectFour::getGameTypeAndFileName()
{
	bool valid = false;

	// Gets file name until it is valid.
	do {
		cout << "Enter file name for game board> ";
		cin >> filename;
		emptyBuffer();
		if (resize(filename)) // Resizes the game board
			valid= true;
		else
			cerr << "#### Given file could not be opened! ####" << endl;
	} while (!valid);

	// Gets game type until it is valid.
	valid = false;
	do {
		cout << "Player | Computer> ";
		cin >> gameType;
		emptyBuffer();
		if (gameType == 'P' || gameType == 'C')
			valid = true;
		else
			cerr << "#### Invalid game type. Enter 'P' or 'C'. ####" << endl;
	} while (!valid);
}

/*
 * Loads game from given file
 */
bool ConnectFour::loadTheGame()
{
	ifstream inputStream(filename.c_str());

	// Controls whether the file is opened.
	if (inputStream.fail()) {
		cerr << "#### " << filename << " could not be opened! ####" << endl;
		return (false);
	}

	// Reads row and column number values to tmp variables.
	int tmpRow, tmpCol;
	inputStream >> tmpRow >> tmpCol;

	// Controls whether file is empty or not.
	if (inputStream.eof()) {
		cerr << "#### " << filename << " might be empty! ####" << endl;
		return (false);
	}

	// Regulates livingCells counter.
	ConnectFour::livingCells -= cellSize;

	// Cleans gameBoard, lastRow and winnerCheckers arrays.
	deallocate();

	// Assigns temporary variables to permanent variables.
	row = tmpRow;
	column = tmpCol;

	// Creates clean gameBoard, lastRow and winnerCheckers arrays.
	allocate();

	// Reads game type.
	inputStream >> gameType;

	// Reads user order.
	inputStream >> order;

	// Reads lastRow array.
	for (int i = 0; i < column; ++i)
		inputStream >> lastRow[i];

	// Reads gameBoard
	char pos;
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < column; ++j) {
			int get;
			pos = static_cast<char> ('A' + j);
			inputStream >> get;
			gameBoard[i][j].setRowNumber(i);
			gameBoard[i][j].setPosition(pos);
			switch (get) {
				case 0: gameBoard[i][j].setType(ConnectFour::EMPTY);   break;
				case 1: gameBoard[i][j].setType(ConnectFour::PLAYER1); break;
				case 2: gameBoard[i][j].setType(ConnectFour::PLAYER2); break;
				case 3: gameBoard[i][j].setType(ConnectFour::COMP);	   break;
				case 8: gameBoard[i][j].setType(ConnectFour::ILLEGAL); break;
			}
		}
	}

	// Reads user moves.
	inputStream >> tmpRow >> pos;
	userMove.setRowNumber(tmpRow);
	userMove.setPosition(pos);

	// Determines user checker type.
	if (order == 0 || order == 2)
		userMove.setType(ConnectFour::PLAYER1);
	else if (order == 1)
		userMove.setType(ConnectFour::PLAYER2);

	// If game type is PvC, then reads computer moves.
	if (gameType == 'C') {
		inputStream >> tmpRow >> pos;
		computerMove.setRowNumber(tmpRow);
		computerMove.setPosition(pos);
		computerMove.setType(ConnectFour::COMP);
	}

	inputStream >> cellSize;

	ConnectFour::livingCells += cellSize;

	// Closes the file
	inputStream.close();

	// Prints informative message on the screen
	cout << "--------------------------------------------------" << endl;
	cout << "**** Loading the game from " << filename << "... ****" << endl;
	
	return (true);
}

/*
 * Saves game to given file
 */
bool ConnectFour::saveTheGame() const
{
	ofstream outputStream(filename.c_str());

	// Controls whether file is opened
	if (outputStream.fail()) {
		cerr << "#### " << filename << " could not be opened! ####" << endl;
		return (false);
	}

	// Board row and column are written
	outputStream << row << ' ' << column << endl << endl;

	// Gametype is written
	outputStream << gameType << endl << endl;

	// User order is written
	outputStream << order << endl << endl;

	// Last row counter is written
	for (int i = 0; i < column; ++i)
		outputStream << lastRow[i] << ' ';
	outputStream << endl;

	// Game board is written
	outputStream << endl;
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < column; ++j) {
			if (gameBoard[i][j].getType() == ConnectFour::EMPTY)
				outputStream << 0 << ' ';
			else if (gameBoard[i][j].getType() == ConnectFour::PLAYER1)
				outputStream << 1 << ' ';
			else if (gameBoard[i][j].getType() == ConnectFour::PLAYER2)
				outputStream << 2 << ' ';
			else if (gameBoard[i][j].getType() == ConnectFour::COMP)
				outputStream << 3 << ' ';
			else if (gameBoard[i][j].getType() ==  ConnectFour::ILLEGAL)
				outputStream << 8 << ' ';
		}
		outputStream << endl;
	}
	outputStream << endl;

	// User move is written
	outputStream << userMove.getRowNumber() << ' ' << userMove.getPosition()
 				 << endl;

	// If game type is computer, computer move is written too
	if (gameType == 'C') {
		outputStream << endl;
		outputStream << computerMove.getRowNumber() << ' '
 					 << computerMove.getPosition();
		outputStream << endl;
	}

	//
	outputStream << endl << cellSize;

	// Closes the file
	outputStream.close();

	// informative message to screen
	cout << "--------------------------------------------------" << endl;
	cout << "**** Current game state has been saved to " << filename << "! ****"
		 << endl;
		 
	return (true);
}

/*******************************************************************************
 *	Inner Cell class function implementations                                  *
 ******************************************************************************/
ConnectFour::Cell::Cell() : position('\0'), rowNumber(0),
							type(ConnectFour::ILLEGAL)
{/* Intentionally left blank */}

ConnectFour::Cell::Cell(ConnectFour::Checker t) : position('\0'), rowNumber(0),
												  type(t)
{/* Intentionally left blank */}

/*
 * Compare two cells each other
 */
bool ConnectFour::Cell::operator ==(const ConnectFour::Cell& other) const
{
	return (position == other.position && rowNumber == other.rowNumber &&
			type == other.type);
}

/*
 * Compare two cells each other
 */
bool ConnectFour::Cell::operator !=(const ConnectFour::Cell& other) const
{
	return (!(*this == other));
}

/*
 * Returns the current position of the cell
 */
inline char ConnectFour::Cell::getPosition() const
{
	return (position);
}

/*
 * Returns the current row number of the cell
 */
inline int ConnectFour::Cell::getRowNumber() const
{
	return (rowNumber);
}

/*
 * Returns the type of this cell
 */
inline ConnectFour::Checker ConnectFour::Cell::getType() const
{
	return (type);
}

/*
 * Sets position according to p
 */
inline void ConnectFour::Cell::setPosition(char p)
{
	position = p;
}

/*
 * Sets row number according to r
 */
inline void ConnectFour::Cell::setRowNumber(int r)
{
	rowNumber = r;
}

/*
 * Sets checker type according to t
 */
inline void ConnectFour::Cell::setType(ConnectFour::Checker t)
{
	type = t;
}

/*
 * Prints one cell on the screen
 */
ostream& operator <<(ostream& outStream, const ConnectFour::Cell& cell)
{
	switch (cell.getType()) {
		case ConnectFour::EMPTY   : outStream << '.'; break;
		case ConnectFour::PLAYER1 : outStream << 'X'; break;
		case ConnectFour::PLAYER2 : outStream << 'O'; break;
		case ConnectFour::COMP    : outStream << 'O'; break;
		case ConnectFour::WINX    : outStream << 'x'; break;
		case ConnectFour::WINO    : outStream << 'o'; break;
		case ConnectFour::ILLEGAL : outStream << ' '; break;
	}
	return (outStream);
}

/*
 * Stream extraction operator for getting input for cell class
 */
istream& operator >>(istream& inpStream, ConnectFour::Cell& cell)
{
	char check;

	cout << "Enter the position of cell> ";
	inpStream >> cell.position;

	// Row number control
	do {
		cout << "Enter the row number of cell> ";
		inpStream >> cell.rowNumber;
		if (cell.rowNumber < 0)
			cerr << "#### Row number must be non-negative! ####" << endl;
	} while (cell.rowNumber < 0);

  	// Checker control
	do {
		cout << "Enter the checker type of cell> ";
		inpStream >> check;
		if (check != 'O' && check != 'X')
			cerr << "#### Checker type must be 'X' or 'O'! ####" << endl;
	} while (check != 'O' && check != 'X');

	if (check == 'X')
		cell.type = ConnectFour::PLAYER1;
	else
		cell.type = ConnectFour::PLAYER2;

	return (inpStream);
}

/*
 * Prefix increment operator. Changes cell state
 * EMPTY --> PLAYER1 --> PLAYER2 --> COMPUTER --> EMPTY
 * Returns: New State
 */
ConnectFour::Cell& ConnectFour::Cell::operator ++()
{
	if (type == ConnectFour::EMPTY)
		type = ConnectFour::PLAYER1;
	else if (type == ConnectFour::PLAYER1)
		type = ConnectFour::PLAYER2;
	else if (type == ConnectFour::PLAYER2)
		type = ConnectFour::COMP;
	else if (type == ConnectFour::COMP)
		type = ConnectFour::EMPTY;

	return (*this); // Returns new state.
}

/*
 * Prefix decrement operator. Changes cell state
 * COMPUTER <-- EMPTY <-- PLAYER1 <-- PLAYER2 <-- COMPUTER
 * Returns: New State
 */
ConnectFour::Cell& ConnectFour::Cell::operator --()
{
	if (type == ConnectFour::EMPTY)
		type = ConnectFour::COMP;
	else if (type == ConnectFour::COMP)
		type = ConnectFour::PLAYER2;
	else if (type == ConnectFour::PLAYER2)
		type = ConnectFour::PLAYER1;
	else if (type == ConnectFour::PLAYER1)
		type = ConnectFour::EMPTY;

	return (*this); // Returns new state.
}

/*
 * Postfix increment operator. Changes cell state
 * EMPTY --> PLAYER1 --> PLAYER2 --> COMPUTER --> EMPTY
 * Returns: Current State
 */
ConnectFour::Cell ConnectFour::Cell::operator ++(int ignore)
{
	ConnectFour::Cell tmp = *this;  // Assign current state to a temp variable.

	if (type == ConnectFour::EMPTY)
		type = ConnectFour::PLAYER1;
	else if (type == ConnectFour::PLAYER1)
		type = ConnectFour::PLAYER2;
	else if (type == ConnectFour::PLAYER2)
		type = ConnectFour::COMP;
	else if (type == ConnectFour::COMP)
		type = ConnectFour::EMPTY;

	return (tmp); // Returns current state.
}

/*
 * Postfix decrement operator. Changes cell state
 * COMPUTER <-- EMPTY <-- PLAYER1 <-- PLAYER2 <-- COMPUTER
 * Returns: Current State
 */
ConnectFour::Cell ConnectFour::Cell::operator --(int ignore)
{
	ConnectFour::Cell tmp = *this; // Assigns current state to a temp variable.

	if (type == ConnectFour::EMPTY)
		type = ConnectFour::COMP;
	else if (type == ConnectFour::COMP)
		type = ConnectFour::PLAYER2;
	else if (type == ConnectFour::PLAYER2)
		type = ConnectFour::PLAYER1;
	else if (type == ConnectFour::PLAYER1)
		type = ConnectFour::EMPTY;

	return (tmp); // Returns current state.
}
