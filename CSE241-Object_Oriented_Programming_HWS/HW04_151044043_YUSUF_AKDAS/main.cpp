/******************************************************************************
 * >> Connect Four v1.4  << 												  *
 *																			  *
 * filename: 'main.cpp'				    						  	 	      *
 * author: yusufakdas						      							  *
 * last modified date: 11:06 p.m. - 7 Nov 2017	          					  *
 * site: github.com/yusufakdas												  *
 ******************************************************************************/
#include "connect_four.h"

int main(int argc, char *argv[])
{
	cout << "Hello, Welcome to Connect Four v1.4!"   << endl << endl;
	cout << "Connect Four is a simple game. You need to collect four of your "
	     << "checker " << endl << "in a row to win the game. This row can be "
		 << "horizontal, vertical or diagonal." << endl << endl;

	char gameMode;
	// Gets input from user to select game mode (SINGLE or MULTI)
	do {
		cout << "Enter the game mode (S - M)> ";
		cin >> gameMode;
		if (gameMode != 'S' && gameMode != 'M')  // ERROR MESSAGE!
			cerr << "#### You must enter S or M! ####" << endl;
	} while (gameMode != 'S' && gameMode != 'M');

	if (gameMode == 'S') {			// Single game mode
		ConnectFour singleGame;
		singleGame.playGame();
	} else { 						// Multi game mode
		ConnectFour *multiGames = new ConnectFour[5];
		bool *endTest = new bool[5];

		// Gets file name and play mode for per object.
		for (int i = 0; i < 5; ++i) {
			multiGames[i].getGameTypeAndFileName();
			endTest[i] = false;
		}

		// For controlling end of all games.
		int countEndedGames = 0;
		int counter = 0;
		// Starts the games' loops
		while (countEndedGames != 5) {
			int chooseObject;
			if (counter == 0) { // FOR PLAYING TWO MOVES IN PVP MODE
				do { // GET GAME OBJECT NUMBER UNTIL IT IS VALID
					cout << "Enter game object number> ";
					cin >> chooseObject;
					if (chooseObject < 1 || chooseObject > 5) {
						cerr << "#### Invalid object number ####" << endl;
						cin.clear();
						cin.ignore(256, '\n');
					}
					if (cin.eof()) {
						cerr << "#### ERROR ####" << endl;
						exit(1);
					}
				} while (chooseObject < 1 || chooseObject > 5);
				// For getline
				string tmp;
				getline(cin, tmp);

				cout << "***************************************************\n";
				multiGames[chooseObject-1].printGameBoard();
			}

			if (multiGames[0] == multiGames[1])
				cout << ">>>> OBJECT 1 == OBJECT 2 <<<<" << endl;
			else
				cout << ">>>> OBJECT 1 != OBJECT 2 <<<<" << endl;
			cout << endl;

			if (!endTest[chooseObject-1]) {
				string input;
				char position;
				int get;
				get = multiGames[chooseObject-1].getInput(input, position);
				char gameType = multiGames[chooseObject-1].getGameType();
				
				bool valid = false;
				if (get == 1)		// SAVE GAME
					valid = multiGames[chooseObject-1].saveTheGame();
				else if (get == 2)  // LOAD GAME
					valid = multiGames[chooseObject-1].loadTheGame();

				if (valid)
					counter = 0;
				// PVP and PVC mode
				if (gameType == 'P' && get == 0 && !valid) {
					multiGames[chooseObject-1].play(position);
					endTest[chooseObject-1] = multiGames[chooseObject-1].controlEnd();
					++counter;
				} else if (gameType == 'C' && get == 0) {
					multiGames[chooseObject-1].play(position);
					if (multiGames[chooseObject-1].getPlayerOrder() == 2) {
						multiGames[chooseObject-1].printGameBoard();
						multiGames[chooseObject-1].play();
					}
					endTest[chooseObject-1] = multiGames[chooseObject-1].controlEnd();
					counter = 0;
				}

				if (endTest[chooseObject-1]) {
					multiGames[chooseObject-1].printGameBoard();
					multiGames[chooseObject-1].showResult();
					++countEndedGames;
				} else {
					multiGames[chooseObject-1].printGameBoard();
				}
			} else {
				cerr << "#### Choose another object because this has ended. ####"
					 << endl;
			}
			if (counter == 2 || endTest[chooseObject-1])
				counter = 0;
		}
		cout << "You have just finished all of the games. See you soon!" << endl;
		delete[] endTest;
 		delete[] multiGames;
	}
	return (0);
}
