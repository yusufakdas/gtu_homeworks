/******************************************************************************
 * >> Connect Four v1.4  << 												  *
 *																			  *
 * filename: 'connect_four.h'												  *
 * author: yusufakdas														  *
 * last modified date: 11:06 p.m. - 7 Nov 2017								  *
 * site: github.com/yusufakdas												  *
 ******************************************************************************/
#ifndef CONNECT_FOUR_H_
#define CONNECT_FOUR_H_

#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
using namespace std;

class ConnectFour {
	class Cell;  // Forward declaration for Cell class
	enum Checker { EMPTY, PLAYER1, PLAYER2, COMP, WINX, WINO, ILLEGAL };
public:
    // No parameter constructor. Default size 5X5.
    ConnectFour();

    // Constructor that takes row and column number
    ConnectFour(int r, int c);

    // Constructor that takes game board shape from a file.
    ConnectFour(const string& filename);

    // Copy Constructor for ConnectFour class
    ConnectFour(const ConnectFour& copy);

    // Desructor that deallocates memory
    ~ConnectFour();

    // Assingment operator for ConnecFour class
    ConnectFour& operator =(const ConnectFour& rightSide);

	// Determines whether two ConnecFour objects are same or not
	bool operator ==(const ConnectFour& other) const;
	bool operator !=(const ConnectFour& other) const;

    // The function to print game board on the screen
    void printGameBoard() const;

	// Accessor Functions
	int getRowNumber() const;
	int getColumnNumber() const;
	char getGameType() const;
	int getPlayerOrder() const;

	// Plays the game for user for single time step.
	void play(char pos);

	// Plays the game for computer for single time step
	void play();

	// Plays a whole game in pvp or pvc mode
	void playGame();

	// Controls end of game
	bool controlEnd();

	// Gets input and commands (LOAD or SAVE) from user
	int getInput(string& request, char& letter);

	// Gets game type and and filename from user
	void getGameTypeAndFileName();

	// Loads game from file
	bool loadTheGame();

	// Saves game to file
	bool saveTheGame() const;

	// Prints informative message on the screen at end of the game
	void showResult() const;

// KONTROL ET
private:
    /**************************************************************************/
	class Cell {
	public:
		// No parameter constructor
		Cell();

		// Constructor that takes type as parameter
		Cell(Checker t);

		// Accessor Functions
		char getPosition() const;
		int getRowNumber() const;
		Checker getType() const;

		// Mutator Functions
		void setPosition(char p);
		void setRowNumber(int r);
		void setType(Checker t);

		// Pre-post increment/decrement operators
		Cell& operator ++();
		Cell& operator --();
		Cell operator ++(int ignore);
		Cell operator --(int ignore);

		// Compare two cells each other
		bool operator ==(const Cell& other) const;
		bool operator !=(const Cell& other) const;

		// Extraction operator for Cell class
		friend istream& operator >>(istream& inpStream, Cell& cell);

		// Insertion operator for Cell class
		friend ostream& operator <<(ostream& outStream, const Cell& cell);
    private:
		char position; 		// Position of a cell
		int rowNumber;      // Row number of a cell
		Checker type;		// X or O
	};
	// Re-declares operator >> to define it out of the outer class.
	friend istream& operator >>(istream& inpStream, Cell& cell);

	// Re-declares operator << to define it out of the outer class.
	friend ostream& operator <<(ostream& outStream, const Cell& cell);
    /**************************************************************************/

    // For memory allocation operations in the class
    void allocate();

    // For memory deallocation operations in the class
    void deallocate();

	// Resizes the game according to file
	bool resize(const string& filename);

	// Overloads resize function according to row and column number
	void resize(int r, int c);

	// Determines whether given cell is valid or not.
	bool isLegal(int r, int c, Checker check) const;

	// Split and get the file name
	void getFileName(const string& request);

 	// Returns how many cells are in the game
	static int getLivingCells();

	// Prevent user to win the game
	bool preventUserMoves();

	// Determine the move of computer
	void determineComputerMoves();

	// Empty buffer
	void emptyBuffer() const;

	// Check the board
	bool checkVertical(const Cell& move);
	bool checkHorizontal(const Cell& move);
	bool checkLeftDiagonal(const Cell& move);
	bool checkRightDiagonal(const Cell& move);
	bool checkDraw() const;

	// Make checkers small in end of the game
	void changeCheckers();

	// ****Connect Four game board (2d dynamic array)****
	Cell **gameBoard;

	// Winner's four checkers' coordinates on the board
	Cell *winnerCheckers;

	// Move made by user
	Cell userMove;

	// Move made by computer
	Cell computerMove;

	// Keeps track of how the last checker's row number in cells
	int *lastRow;

	int row; 		 		// Row number of the board.
	int column; 	 		// Column number of the board.
	int order;				// "0 --> P1(X)", "1 --> P2(O)", "2 --> C(O)"
	int cellSize;			// Keeps checker size for a object.
	char gameType;			// "'P' --> PvP", "'C' --> PvC"
	static int livingCells; // Sum of the printable cells(X, O) in the game
	string filename; 		// Filename
};
#endif
