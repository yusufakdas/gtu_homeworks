(defconstant *NULL* -1)
(defconstant *DIGIT* 0)			; To indicate digit state.
(defconstant *LETTER* 1)		; To indicate letter state.
(defconstant *OTHER* 2)			; To indicate other state.
(defconstant *MINUS* 3)			; To indicate minus state.
(defconstant *MULTIPLY* 4)		; To indicate multiply state.
(defconstant *SPACE* 5)			; To indicate space state.
(defconstant *BINARY-VALUE-LIST* '(true false)) ; To indicate binary value list.
(defconstant *OPERATOR-CHAR-LIST* (list #\( #\) #\+ #\- #\* #\/)) 
(defconstant *KEYWORD-LIST* '(and or not equal append concat 
							  set deffun for while if exit)) 

(defun get-state (ch)
 	"Get the state of the character."

	(cond ((null ch) *NULL*)
	  	  ((alpha-char-p ch) *LETTER*)
	      ((is-space ch) *SPACE*)
	      ((is-digit ch) *DIGIT*)
	      ((char-equal ch #\*) *MULTIPLY*)
	      ((char-equal ch #\-) *MINUS*)
	      (t *OTHER*)
	)
)

(defun is-digit (ch)
  	"Check character out whether it is digit or not."

	(if (and (char>= ch #\0) (char<= ch #\9)) 
	  	t
		nil
	)
)

(defun is-binary-value (lexeme)
  	"Determine whether it is binary value or not."

	(if (or (string-equal lexeme (car *BINARY-VALUE-LIST*)) 
			(string-equal lexeme (cadr *BINARY-VALUE-LIST*)))
	  	t
	  	nil
	)
)

(defun is-keyword (lexeme lst)
  	"Determine whether it is keyword or not."

	(cond ((null lst) nil)
		  ((string-equal lexeme (car lst)) t)
		  (t (is-keyword lexeme (cdr lst)))
	)
)

(defun is-operator (ch lst)
  	"Determine whether it is operator or not."

	(cond ((null lst) lst)
		  ((char-equal ch (car lst))  t)
		  (t (is-operator ch (cdr lst)))
	)
)

(defun list-to-str (lexeme)
  	"Convert list to string."

	(format nil "~{~A~}" lexeme)
)

(defun get-token (lexeme)
  	"Get the token with respect to given list."

  	(let ((lexeme-to-string (list-to-str lexeme))) 
  	  	(cond ((null lexeme) nil)
			((or (= (get-state (car lexeme)) *DIGIT*) 
				 (= (get-state (cadr lexeme)) *DIGIT*))
			 	(list "integer" lexeme-to-string)
			)
			((= (get-state (car lexeme)) *LETTER*)
		       	(cond ((is-keyword lexeme-to-string *KEYWORD-LIST*)
		            		(list "keyword" lexeme-to-string))
					  ((is-binary-value lexeme-to-string)
		                	(list "binary_value" lexeme-to-string))
		              (t (list "identifier" lexeme-to-string))
		    	)
		  	)
		  	((= (get-state (car lexeme)) *OTHER*)
		  	    (if (is-operator (car lexeme) *OPERATOR-CHAR-LIST*)
		  	 		(list "operator" lexeme-to-string)
		  	 		nil
		  		)
		  	)
		  	((= (get-state (car lexeme)) *MINUS*)
		  	 	(if (null (cadr lexeme))
		  	 		(list "operator" lexeme-to-string)
		  	 		nil
		  	 	)
		  	)
		  	((= (get-state (car lexeme)) *MULTIPLY*)
		  	 	(list "operator" lexeme-to-string)
		  	)
		)
	)
)

(defun is-space (ch)
  "Control whether it is space."

  (or (char-equal ch #\space) 
	  (char-equal ch #\linefeed)
	  (char-equal ch #\newline)
  	  (char-equal ch #\tab))
)

(defun err (ch ch-next) 
  "Control whether it is space."
	; If an error occurs, exit the program.
	(format t "**** ERROR: begins with ~a~a ... ****~%" ch ch-next) 
	(quit)
)

(defun lexer (filename)
  	"THE LEXER FUNCTION"
	
	; Open the file.
	(with-open-file (instream filename :direction :input :if-does-not-exist nil)
    	(let ((coffee-file (make-string (file-length instream))) (lexeme nil) 
    		  (token-list nil) (ch) (curr-state nil) (token))
		  	(read-sequence coffee-file instream) ; File has been read as string.

			(dotimes (x (length coffee-file)) ; Iterates the string until its length.
			  	(setf ch (char coffee-file x)) 
			  	(cond ((and (not (char-equal ch #\newline)) (not (char-equal ch #\linefeed))) ; Control end of line character.
			  			(setf ch-next (char coffee-file (+ x 1))) ; Get the next character.
			  			(setf curr-state (get-state ch)) ; Get the state of the current character.
						(cond ((= curr-state *OTHER*) ; If the character is not minus, multiplication.
							   (setf lexeme (append lexeme (list ch)))
							   (setf token (get-token lexeme))
							   (if (null token) 
							   		(err ch ch-next)
									(setf token-list (append token-list (list token) ))
							   )
							   (setf lexeme nil)
						   	)
							  ((= curr-state *MINUS*) 
								(cond ((= (get-state ch-next) *DIGIT*)
										(setf lexeme (append lexeme (list ch)))
							   	   	)
							   	   	  ((is-space ch-next)
										(setf lexeme (append lexeme (list ch)))
							   	   		(setf token-list (append token-list 
							   	   						(list (get-token lexeme))))
							   	   		(setf lexeme nil)
							   	   	)
							   	   	  (t (err ch ch-next))
							   	)
							)
							  ((= curr-state *MULTIPLY*)
								(setf lexeme (append lexeme (list ch)))
								(cond ((/= (get-state ch-next) *MULTIPLY*)
							   	   	   (cond ((is-space ch-next)
							   	   			(setf token-list (append token-list 
							   	   					(list (get-token lexeme))))
							   	   			)
							   	   			(t (err ch ch-next))
							   	   	   )
							   	   	)
							   	   	  (t (setf x (+ x 1))
										(setf lexeme (append lexeme (list ch)))
							   	   	  	(setf token-list (append token-list 
							   	   						(list (get-token lexeme))))
							   		)
							   	)
							   	(setf lexeme nil)
							)
							  ((= curr-state *LETTER*)
								(cond ((or (is-space ch-next) (char= ch-next #\)))
										(setf lexeme (append lexeme (list ch)))
							   	   		(setf token-list (append token-list 
							   	   						(list (get-token lexeme))))
							   	   		(setf lexeme nil)
							   	   	)
							   	   	  ((/= (get-state ch-next) *LETTER*)
							   	   	   	(err ch ch-next))
							   	   	  (t (setf lexeme (append lexeme (list ch))))
							   	)
							)
							  ((= curr-state *DIGIT*)
								(cond ((or (is-space ch-next) (char= ch-next #\)))
										(setf lexeme (append lexeme (list ch)))
							   	   		(setf token-list (append token-list 
							   	   						(list (get-token lexeme))))
							   	   		(setf lexeme nil)
							   	   	)
							   	   	  ((/= (get-state ch-next) *DIGIT*)
							   	   	   	(err ch ch-next))
							   	   	  (t (setf lexeme (append lexeme (list ch))))
							   	)
							)
						)
			  		)	
			  	)
			)
			token-list
		)
	)
)

(write (lexer "example.coffee"))
(terpri)

