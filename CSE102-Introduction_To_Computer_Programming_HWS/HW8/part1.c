#include <stdio.h>
#include <string.h>
#define TRUE 1
#define FALSE 0

#define NAMELEN 50
#define MAXRECORD 500

typedef struct person_s {
	int id;
	char name[NAMELEN];
} Person;

typedef struct record_s {
	int id;
	char number[NAMELEN];
} Record;

typedef struct Records_s {
	Record data[MAXRECORD];
	int size;
} Records;

typedef struct people_s {
	Person data[MAXRECORD];
	int size;
} People;

/* ========== IMPLEMENT THE FUNCTIONS BELOW ========== */

/*
 * Write Records.data array to the filename.
 * Note that file should be binary.
 */
void writeRecords(Records records, char* filename) {
    FILE *binp;
    int i;

    /* Opens a binary file in write mode and assigns to file pointer. */
	binp = fopen(filename, "wb");
    
     /* Controls whether any error occurs or not. If there is, exits the function */
    if (binp == NULL)
        return;

    /* Writes records.data to file opened. */ 
    for (i = 0; i < records.size; ++i)
	   fwrite(&records.data[i], sizeof(Record), 1, binp);

    /* Closes the file. */
	fclose(binp); 
}

/*
 * Reads Record structs from the binary file and places them to Records.data array.
 * Note that the number of Record structs in the file is unkown.
 */
void readRecords(Records *records, char* filename) {
    FILE *binp;
	
	/* Opens a binary file in read mode and assigns to file pointer. */
	binp = fopen(filename, "rb");

    /* Assigns 0 to records size. */
    records->size = 0;

    /* Controls whether any error occurs or not. If there is, exits the function */
    if (binp == NULL)
        return;
    
    /* Reads data from the file and adds them to people struct. */
    while (fread(&records->data[records->size], sizeof(Record), 1, binp) > 0)
        records->size++;
    
    /* Closes the file. */
    fclose(binp);
}

/*
 * Write People.data array to the filename.
 * Note that file should be binary.
 */
void writePeople(People people, char* filename) {
	FILE *binp;
    int i;
    
    /* Opens a binary file in write mode and assigns to file pointer. */
	binp = fopen(filename, "wb");
	
	/* Controls whether any error occurs or not. If there is, exits the function */
    if (binp == NULL)
        return;

    /* Writes people.data to file opened. */ 
    for (i = 0; i < people.size; ++i)
	   fwrite(&people.data[i], sizeof(Person), 1, binp);

    /* Closes the file. */
	fclose(binp); 
}

/*
 * Reads Person structs from the binary file and places them to People.data array.
 * Note that the number of Person structs in the file is unkown.
 */
void readPeople(People *people, char* filename) {
    FILE *binp;

    /* Opens a binary file in read mode and assigns to file pointer. */
	binp = fopen(filename, "rb");

    /* Assigns 0 to people size. */
    people->size = 0;

    /* Controls whether any error occurs or not. If there is, exits the function */
    if (binp == NULL)
        return;

    /* Reads data from the file and adds them to people struct. */
    while (fread(&people->data[people->size], sizeof(Person), 1, binp) > 0)
        people->size++;
    
    /* Closes the file. */
	fclose(binp);  
}

/* 
 * Controls whether same phone numbers are. If there are, returns 0, otherwise 1.
 */
int same(Records *records, char num[]) {
    int i;

    for (i = 0; i < records->size; ++i)
        if(strcmp(num, records->data[i].number) == 0)
            return (0); 
    return (1);
}

/*
 * Reads the input file and constructs People and Records structs.
 * Note that each Record in Records is unique (should not present multiple times).
 */
void read(char* filename, People *people, Records *records) {
	FILE *inp;                     
	char tmp[2][NAMELEN];
	int i, id, num, status,
        quit = FALSE;    /* For executing the while loop until end of file */
    
    /* Opens text file in read mode and assigns to file pointer. */
	inp = fopen(filename, "rt");

    /* Exits the function if file is not opened */
    if (inp == NULL)
        return;

	while (!quit) {
		/* Takes id, name, surname, number of phone numbers from the text file */
		status = fscanf(inp, "%d%s%s%d", &id, tmp[0], tmp[1], &num);
        if (status != EOF) { /* Controls whether end of file reached or not */
    	    people->data[people->size].id = id; 
    	    /* Concatenates name and surname in a determined form and adds to
    	     * people structure. */
    		sprintf(people->data[people->size].name, "%s %s", tmp[0], tmp[1]);
    		/* For getting phone numbers */
    		for (i = 0; i < num; ++i) { 
    			fscanf(inp, "%s", tmp[0]); /* Gets phone number */
                /* If there is any identical phone numbers, then forwards the 
                 * file pointer. */
                if (!same(records, tmp[0])) {
                    fscanf(inp, "%s", tmp[0]);
                } else {
                    records->data[records->size].id = id;
                    strcpy(records->data[records->size++].number, tmp[0]);
                }
    		}
    		people->size += 1; /* Increments people size by one */
	    } else {
            quit = TRUE; /* If EOF is reached, then makes it TRUE to exit loop. */
        }
    }
	fclose(inp); /* Closes the file. */
}

/* ========== IMPLEMENT THE FUNCTIONS ABOVE ========== */

void print(People people, Records records) {
	int i,j,found = 0;
	/* header */
	printf("%-5s %-30s %-20s\n", "ID","NAME","NUMBER(s)");
	/* line */
	for (i = 0; i < 57; ++i)
		printf("-");
	printf("\n");

	for (i = 0; i < people.size; ++i) {
		found = 0;
		printf("%-5d %-30s", people.data[i].id, people.data[i].name);
		for (j = 0; j < records.size; ++j) {
			if(records.data[j].id == people.data[i].id) {
				if(found)
					printf("%36s", "");
				printf("%-20s\n", records.data[j].number);
				found = 1;
			}
		}
		printf("\n");
	}
}

int isPeopleEq(People ppl1, People ppl2) {
	int i;
	if(ppl1.size != ppl2.size)
		return 0;
	for (i = 0; i < ppl1.size; ++i)
		if(strcmp(ppl1.data[i].name,ppl2.data[i].name) ||
			ppl1.data[i].id != ppl2.data[i].id)
			return 0;
	return 1;
}

int isRecordsEq(Records rec1, Records rec2) {
	int i;
	if(rec1.size != rec2.size)
		return 0;
	for (i = 0; i < rec1.size; ++i)
		if(strcmp(rec1.data[i].number,rec2.data[i].number) ||
			rec1.data[i].id != rec2.data[i].id)
			return 0;
	return 1;
}

int main(int argc, char** argv) {
	People people1,people2;
	Records records1,records2;
	people1.size = 0;
	records1.size = 0;
	read(argv[1],&people1, &records1);
	print(people1, records1);
	writePeople(people1,"people.bin");
	writeRecords(records1,"records.bin");
	readRecords(&records2,"records.bin");
	readPeople(&people2,"people.bin");
	print(people2, records2);
	printf("%s\n", isRecordsEq(records1,records2) ? "RECORDS ARE SAME" : "RECORDS ARE DIFFERENT!");
	printf("%s\n", isPeopleEq(people1,people2) ? "PEOPLE ARE SAME" : "PEOPLE ARE DIFFERENT!");
	return 0;
}
