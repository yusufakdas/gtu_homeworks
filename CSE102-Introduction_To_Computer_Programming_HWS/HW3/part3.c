/*******************************************************************************
 * Yusuf Akdas 151044043 - HW_3_Part_3                                         *
 ******************************************************************************/

#include <stdio.h>

int charge(int cardType, int *monthlyUse, double *balance);

int main(void) {
	int monthlyUse = 120;
	double balance = 20.0;
	int ret = charge(1, &monthlyUse, &balance); 
	
	if(ret == 0)
		printf("Remaining monthly use: %d – Remaining Balance: %.2f\n", 
			   monthlyUse, balance);
	else
		printf("Insufficient balance.\n");
	
	return (0);
}

/*
 * IstanbulCard
 */
int charge(int cardType, int *monthlyUse, double *balance) {
    int    status = 0;
    double decrease_amount;

    switch (cardType) {
        case 1: /* Normal Card */
            decrease_amount = 2.30;
            break;
        case 2: /* Student Card */
            decrease_amount = 1.15;
            break;
        case 3: /* Teacher Card */
            decrease_amount = 1.65;
            break;
        default:
            status = -2;
    }
    if (status == 0) {
         if (*monthlyUse < 1 && *monthlyUse >= 0) {
            if (cardType == 1) {
                if(*balance >= decrease_amount)
                    *balance -= decrease_amount;
                else
                    status = -1;
            } else if (cardType == 2) {
                if(*balance >= decrease_amount)
                    *balance -= decrease_amount;
                else
                    status = -1;
            } else if (cardType == 3) {
                if(*balance >= decrease_amount)
                    *balance -= decrease_amount;
                else
                    status = -1;
            }
         } else if (*monthlyUse >= 1) {
            *monthlyUse -= 1;
         } else {
            status = -1;
         }
    }
    return (status);
}

/*******************************************************************************
 *                             End of the Part 3                               *
 ******************************************************************************/
