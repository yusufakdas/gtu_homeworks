/*******************************************************************************
 * Yusuf Akdas 151044043 - HW_3_Part_1                                         *
 ******************************************************************************/
 
#include <stdio.h>

/* Functions prototypes */
int isPrime(int num);
int goldbach(int num, int *p1, int *p2);

int main(void) {
	int num, p1, p2;
	
	printf("Enter a number> ");
	scanf("%d", &num);
	if (goldbach(num, &p1, &p2))
		printf("%d is goldbach number! (%d + %d = %d)\n", num, p1, p2, num);
	else
		printf("%d is not a goldbach number!\n", num);

	return (0);
}

/*
 * This function determines whether a given number is prime or not. 
 */
int isPrime(int num) {
    int n, status = 1;          
    
    /* If number is a prime, then status is 1 */
    if (num >= 2) {
        for (n = 2; n < num; n++) {
        /* If a number can be divided by 2 then the number is not a prime number 
         * and status is 0 */
            if (num % n == 0) 
                status = 0;
        }
    } else {
        status = 0;     /* If not status is 0 */
    }
    return (status);    /* Returns status */
}

/*
 * This function divides a given even number that is different from two as sum of
 * two prime number and returns these prime numbers as output. 
 */
int goldbach(int num, int *p1, int *p2) {
    int i, j;
    int status = 1,
        flag   = 1;  /* This is for breaking loop */
    
    /* p1 and p2 are initialized as 0 */   
    *p1 = *p2 = 0;
    
    /* If number is even and greater than 2 */
    if (num % 2 == 0 && num > 2) {
        for (i = 2; i < num && flag; i++) {
            if (isPrime(i)) {
                *p1 = i;
            }
            for (j = num; j > 0; j--) {
            /* If j is prime and *p1 + j (both of two is prime) is equal to even 
             * number entered */
                if (isPrime(j) && j + *p1 == num) {
                    *p2 = j;
                    flag = 0; /* If flag is 0 then break the loop */
                }  /* end of if  */
            }  /* end of for */
        }  /* end of if  */
    } else {
        status = 0;  /* If not status is 0 */
    }
    return (status); /* Returns status     */
}

/*******************************************************************************
 *                             End of the Part 1                               *
 ******************************************************************************/
