/*******************************************************************************
 * Yusuf Akdas 151044043 - HW_3_Part_2                                         *
 ******************************************************************************/

#include <stdio.h>

/* Function prototype */
int dispenseChange(double paid, double due, int *tl1, int *krs50, int *krs25,
                   int *krs10, int *krs5, int *krs1);

int main(void) {
	double paid, due; 
	int tl1, krs50, krs25, krs10, krs5, krs1;
	
	paid = 15;
	due = 9.68;
	
	if(dispenseChange(paid,due,&tl1,&krs50, &krs25, &krs10, &krs5, &krs1)) /* returns 1 */
		printf("Change: 1TL:%d, Kurus-50:%d, 25:%d, 10:%d, 5:%d, 1:%d\n", 
			   tl1, krs50, krs25, krs10, krs5, krs1);
	else
		printf("Unable to dispense change.");

	return (0);
}

/* This function dispenses change */
int dispenseChange(double paid, double due, int *tl1, int *krs50, int *krs25,
                   int *krs10, int *krs5, int *krs1) {
    int status = 1;              /* This is for return value. It is set 1 
                                  * in the beginning and if there is a problem, 
                                  * then it will be set 0  */
    double change = paid - due;  /* For calculating change */

    /* All of them are initialized as 0 in the beginning */
    *tl1 = *krs50 = *krs25 = *krs10 = *krs5 = *krs1 = 0;

    /*
     * If change, paid and due are greater than 0, this condition executes. 
     * For example if change is greater than 1, substract 1 from change and 
     * add 1 to *tl1 until change is less than 1. 
     */
    if (change >= 0 && paid > 0 && due > 0) {
        while (change >= 1) {                      
            change -= 1;       
            *tl1 += 1;
        }
        while (change >= 0.5) {
            change -= 0.5;
            *krs50 += 1;
        }
        while (change >= 0.25) {
            change -= 0.25;
            *krs25 += 1;
        }
        while (change >= 0.1) {
            change -= 0.1;
            *krs10 += 1;
        }
        while (change >= 0.05) {
            change -= 0.05;
            *krs5 += 1;
        }
        while (change > 0.009) {
            change -= 0.01;
            *krs1 += 1;
        }
    } else {            
        status = 0;  /* If not, return 0 */
    }
    return (status);
}

/*******************************************************************************
 *                             End of the Part 2                               *
 ******************************************************************************/
