/******************************************************************************
 * author: @yusufakdas 151044043                                              *
 * last update: 25 April 2017                                                 *
 * HW6 Part2 - 'Counts occurence of words until "end" in a sentence.'         *
 ******************************************************************************/
#include <stdio.h>
#include <string.h>
#define MAX_NUM_WORDS 500
#define WORD_LENGTH 50

/* Fuction Prototypes */
int getAndFilterWord(char *w);
void addWord(char *w, char words[MAX_NUM_WORDS][WORD_LENGTH],
             int occur[MAX_NUM_WORDS], int *word_count);
void sortWords(char words[MAX_NUM_WORDS][WORD_LENGTH], int occur[MAX_NUM_WORDS],
               int word_count);
int myTest(void);

/* Prototypes of additional three functions to make some operations happen. */
void lowercase(char *w);
int whereIs(char *w, char words[MAX_NUM_WORDS][WORD_LENGTH], int word_count);
void printOccurence(char words[MAX_NUM_WORDS][WORD_LENGTH], int *occur,
                    int word_count);
/* 
 * My main function
 */
int main(void)
{
    char words[MAX_NUM_WORDS][WORD_LENGTH];
    char w[WORD_LENGTH];
    int occur[MAX_NUM_WORDS];
    int word_count, i, status;
    
    /* Makes 0 all elements of occur array */
    for (i = 0; i < MAX_NUM_WORDS; i++)
        occur[i] = 0;
    
    /* Gets words untill "end" */
    while (strcmp(w, "end") != 0)
    {
        status = getAndFilterWord(w);                 /* Gets word from user */
        if (status)         /* Sends the word addWord fuction if it is valid */
            addWord(w, words, occur, &word_count);
    }
    printf("\n");
    printf("Unsorted List\n");
    printOccurence(words, occur, word_count);
    
    /* Sorts the arrays in descending order. */
    sortWords(words, occur, word_count);
    
    printf("Sorted List\n");
    printOccurence(words, occur, word_count);
    printf("Word Count: %d\n", word_count);
    return (0);
}
/* 
 * Makes words lowercase. 
 */
void lowercase(char *w)
{
    int i;
    for (i = 0; i < strlen(w); i++)
        if ('A' <= w[i] && 'Z' >= w[i])
            w[i] += 'a' - 'A';
}
/*
 * Discards characters apart from letters from word. If w is word returns 1,
 * otherwise 0.
 */
int getAndFilterWord(char *w)
{
    int i, counter = 0;
    char temp[WORD_LENGTH];             /* Temporary array for cleaning word */

    scanf("%s", temp);                  /* Gets a word to temp array */
    lowercase(temp);                    /* Makes letters of a word lowercase */
    /* For control of characters of temp */ 
    for (i = 0; i <= strlen(temp); i++)
    {
        if (temp[i] == '\0') {
            w[counter] = '\0';          /* Adds '\0' to end of w */
        }
        /* If it is lowercase letter then adds to w array. */
        if (temp[i] >= 'a' && temp[i] <= 'z') {
            w[counter] = temp[i];
            counter++;
        }
    }
    if (w[0] == 0)
        return (0);     /* If it is a word */
    else
        return (1);     /* If it is not a word */
}
/*
 * Adds a word to words array according to its occurrence if it is valid.
 */
void addWord(char *w, char words[MAX_NUM_WORDS][WORD_LENGTH],
             int occur[MAX_NUM_WORDS], int *word_count)
{
    int where;
    /* Finds the subscript of the word.*/
    where = whereIs(w, words, *word_count);
    if (where >= *word_count) {
        strcpy(words[where], w);
        *word_count += 1;
    }
    occur[where] += 1;
}
/*
 * Looks for word whether it is in words array. If it is in the array, then
 * it returns an empty places of words array and occur array.
 */
int whereIs(char *w, char words[MAX_NUM_WORDS][WORD_LENGTH], int word_count)
{
    int found = 0, i;
    
    for (i = 0; i < word_count && !found; i++)
        if (strcmp(w, words[i]) == 0)
            found = 1;
    if (found)
        return (i-1); /* Returns where the word is in words array */
    else
        return (i);   /* Returns an empty place in words array */ 
}
/*
 * Sorts the words and according to their occurrences in descending order.
 */
void sortWords(char words[MAX_NUM_WORDS][WORD_LENGTH], int occur[MAX_NUM_WORDS],
               int word_count)
{
    int i, j, temp;
    char w_temp[WORD_LENGTH];
    /* Used selection sort algorithm */
    for (i = 0; i < word_count-1; i++)
    {
        for (j = i; j < word_count; j++)
        {
            if (occur[j] > occur[i])
            {
                temp = occur[j];
                occur[j] = occur[i];
                occur[i] = temp;
                strcpy(w_temp, words[j]);
                strcpy(words[j], words[i]);
                strcpy(words[i], w_temp);
            }
        }
    }
}
/*
 * Prints words and their occurrences on the screen.
 */
void printOccurence(char words[MAX_NUM_WORDS][WORD_LENGTH], int *occur,
                    int word_count)
{
    int i;
    
    for (i = 0; i < word_count; i++)
        printf("%s\t-> %d\n", words[i], occur[i]);
    printf("\n");
}
/******************************************************************************
 *                          ~End of Part2 of HW6~                             *
 ******************************************************************************/
