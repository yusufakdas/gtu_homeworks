/*****************************************************************************
 * HW1_PART1 Yusuf AKDAS 151044043                                           *
 *****************************************************************************/

#include <stdio.h>
#define ONE_YEAR 365      /* constant for one year  */
#define ONE_MONTH 30      /* constant for one month */

/* functions prototypes */
int theAge(int day, int month, int year, int today, int this_month, int this_year);
int daysLeft(int day, int month, int today, int this_month);

int main(void) {
    int birth_year, birth_month, birth_day;
    int this_year, this_month, this_day;
    int age, dayz;

    printf("Enter your birth year, birth month and birth day in this order> ");
    scanf("%d%d%d", &birth_year, &birth_month, &birth_day);
    printf("Enter the current year, month and day in this order> ");
    scanf("%d%d%d", &this_year, &this_month, &this_day);

    age = theAge(birth_day, birth_month, birth_year, this_day, this_month, this_year);
    dayz = daysLeft(birth_day, birth_month, this_day, this_month);

    return (0);
}

/* Calculates age */
int theAge(int day, int month, int year, int today, int this_month, int this_year) {
    int day_type;
    int age;

    day_type = (this_year - year) * ONE_YEAR + (this_month - month) * ONE_MONTH+
               (today - day);

    age = day_type / ONE_YEAR;
    printf("%d\n", age);

    return (age);
}

/* Calculates the number of days to the birthday */
int daysLeft(int day, int month, int today, int this_month) {
    int days;

    days = (month * ONE_MONTH + day) - (this_month * ONE_MONTH + today);
    days = (days + ONE_YEAR) % ONE_YEAR;
    printf("%d\n", days);

    return (days);
}
