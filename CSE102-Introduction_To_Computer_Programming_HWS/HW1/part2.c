/*****************************************************************************
 * HW1_PART2 Yusuf AKDAS 151044043                                           *
 * This C code calculates the age of a tree.                                 *
 *****************************************************************************/

#include <stdio.h>
#define PI 3.14
#define AMERICAN_ELM 4.0
#define AUSTRIAN_PINE 4.5
#define BLACK_CHERRY 5.0
#define SCOTCH_PINE 3.5

/* fuctions prototypes */
float diameter(float circumference);
float ageOfTree(float diameter, float growth_factor);

int main(void) {
    float circumference, dia, age;

    printf("Enter the circumference of the tree> ");
    scanf("%f", &circumference);
    dia = diameter(circumference);
    age = ageOfTree(dia, AMERICAN_ELM);

    printf("The diameter ot the tree is %.2f.\n", dia);
    printf("The age of the tree is %.2f.\n", age);

    return (0);
}

/* Calculates diameter of tree */
float diameter(float circumference) {
    return (circumference / PI);
}

/* Calculates age of tree */
float ageOfTree(float diameter, float growth_factor) {
    return (diameter * growth_factor);
}
