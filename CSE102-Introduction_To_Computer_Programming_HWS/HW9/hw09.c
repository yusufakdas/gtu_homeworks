#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0
#define NAMELEN 50
#define MAXRECORD 500

typedef struct record_s {
    int id;
    char number[NAMELEN];
    struct record_s *next;
} Record;

typedef struct person_s {
    int id;
    char name[NAMELEN];
    double expenditure;
    Record *numbers;
} Person;

typedef struct people_s {
    Person data[MAXRECORD];
    int size;
} People;


/*
 Write People.data array to the filename.
*/
void writePeople(People people, char* filename) {
    FILE *binp;
    Record *head;
    int i, phone_size = 0;

    /* Opens a binary file in write mode and assigns to file pointer. */
    binp = fopen(filename, "wb");

    /* Controls whether any error occurs or not. If there is, exits the function */
    if (binp == NULL)
        return;

    /* Writes people.data to file opened. */
    for (i = 0; i < people.size; ++i) {
        fwrite(&people.data[i].id, sizeof(int), 1, binp);
        
        /* Writes name of every person by length of the string including NULL */
        fwrite(&people.data[i].name, sizeof(char)*(strlen(people.data[i].name)+1), 1, binp);
        
        /* Writes expenditure of every person */
        fwrite(&people.data[i].expenditure, sizeof(double), 1, binp);
        
        /* 
         * Calculates the number of phone numbers of every person and writes  
         * them to binary file because they will be used when data is read.
         */ 
        for (head = people.data[i].numbers; head != NULL; head = head->next) {
            phone_size++;
        }
        fwrite(&phone_size, sizeof(int), 1, binp);
    
        /* Writes phone numbers of every person by their length of including NULL */ 
        for (head = people.data[i].numbers; head != NULL; head = head->next) {
            fwrite(&head->number, sizeof(char)*(strlen(head->number)+1), 1, binp);
        }
        phone_size = 0;
    }

    /* Closes the file. */
    fclose(binp);
}

/*
 Reads Person structs from file and places them to People.data array.
 Note that the number of Person structs in the file is unkown.
*/
void readPeople(People *people, char* filename) {
    FILE *binp;
    Record *head;
    char ch;
    int i, j, phone_size = 0;

    /* Opens a binary file in read mode and assigns to file pointer. */
    binp = fopen(filename, "rb");

    /* Assigns 0 to people size. */
    people->size = 0;


    /* Controls whether any error occurs or not. If there is, exits the function */
    if (binp == NULL)
        return;

    /* Reads data from the file and adds them to people struct. */
    while (fread(&people->data[people->size].id, sizeof(int), 1, binp) == 1) {
        /* Reads name of every person character by character until NULL */
        i = 0;
        ch = ' ';
        while (ch != '\0') { 
            fread(&ch, sizeof(char), 1, binp);
            people->data[people->size].name[i] = ch;
            i++;
        }
    
        /* Reads expenditure of every person */
        fread(&people->data[people->size].expenditure, sizeof(double), 1, binp);
        
        /* Reads number of phone numbers of every person */
        fread(&phone_size, sizeof(int), 1, binp);
    

        /**********************************************************************
         * Reads phone numbers of every person character by character add them*
         * to linked list.                                                    *
         **********************************************************************/
        people->data[people->size].numbers = (Record *)malloc(sizeof(Record));
        head = people->data[people->size].numbers;
        i = 0;
        ch = ' ';
        while (ch != '\0') {
            fread(&ch, sizeof(char), 1, binp);
            head->number[i] = ch;
            i++;
        }
        head->id = people->data[people->size].id; /* Person id */

        j = 0;
        while (j < phone_size-1) {
            head->next = (Record *)malloc(sizeof(Record));
            i = 0;
            ch = ' ';
            while (ch != '\0') {
                fread(&ch, sizeof(char), 1, binp);
                head->next->number[i] = ch;
                i++;
            }
            head->next->id = people->data[people->size].id;
            head->next->next = NULL;
            head = head->next;
            j++;
        }
        people->size++;
        /**********************************************************************/
    }

    /* Closes the file. */
    fclose(binp);
}


/*
 * Calculates the expected expenditure for the person with unknown expenditure.
 * Persons' expenditure is -1 if their expenditure is unknown.
 * You should calculate average bill amount for a phone number and multiply it by
 * the phone number count for the person.
 */
void imputation(People *people) {
    int valid = 0, invalid = 0, i;   /* Counters */
    double total_bill = 0;
    Record *head;                    /* For traversing the list */

    /*
     * Traverse the list until NULL to find the total expenditure and total phone
     * numbers of known expenditure. 
     */
    for (i = 0; i < people->size; ++i) {
        if (people->data[i].expenditure != -1) {
            total_bill += people->data[i].expenditure;
            for (head = people->data[i].numbers; head != NULL; head = head->next)
                valid += 1;
        }
    }

    /* Calculates the unknown expenditures in the list. */
    for (i = 0; i < people->size; ++i) {
        if (people->data[i].expenditure == -1) {
            for (head = people->data[i].numbers; head != NULL; head = head->next)
                invalid += 1;
        /* Expected expenditure formula */
        people->data[i].expenditure = (total_bill / valid) * invalid;
        }
    }
}

/*
 * This fuction makes linked list typed Record for phone numbers and it returns
 * the address of the head of list.
 */
Record *form_list(FILE *inp, People *people) {
    Record *head, *temp;
    char ch = ' ';
    int quit = FALSE;

    /* Memory allocation for the head node of list */
    head = (Record *)malloc(sizeof (Record));

    /* Copies the address of the head of list to an temporary variable not to
     * lose it */
    temp = head;

    /* Copies first elements of the list */
    temp->id = people->data[people->size].id;
    fscanf(inp, "%s", temp->number);

    /*
     * Gets rest of phone numbers until ch is newline character, which indicates
     * the end of line. On the other hand ch provides for skipping space character
     * between phone numbers and in end of line. 
     */
    while (!quit) {
        while (ch == ' ') 
            ch = getc(inp);
        fseek(inp, -1, SEEK_CUR);
        
        if (ch != '\n') {
            temp->next = (Record *)malloc(sizeof (Record));
            temp->next->id = people->data[people->size].id;
            fscanf(inp, "%s", temp->next->number);
            temp = temp->next;
            ch = getc(inp);
        } else {
            quit = TRUE;
        }
    }
    temp->next = NULL; /* Assigns NULL pointer to next of the last node of list */

    return (head); /* Returns head node */
}

/*
 * Reads the input file and constructs People struct.
 * Note that the number of phone numbers in file is unknown unlike your
 * previous homework. You should keep phone numbers in the linked list
 * (numbers field)
 */
void read(char* filename, People *people) {
    FILE *inp;
    int status,
        quit = FALSE;       /* For executing the while loop until end of file */
    char *name, *surname;

    /* Allocates some of memory for name and surname from heap. */
    name = (char *)calloc(NAMELEN, sizeof (char));
    surname = (char *)calloc(NAMELEN, sizeof (char));

    /* Opens text file in read mode and assigns to file pointer. */
    inp = fopen(filename, "rt");

    /* Exits the function if file is not opened */
    if (inp == NULL)
        return;

    while (!quit) {
        /* Takes id, name, surname and expenditure from the text file */
        status = fscanf(inp, "%d%s%s%lf", &people->data[people->size].id, name,
                        surname, &people->data[people->size].expenditure);
        if (status != EOF) {
            /* Concatenates name and surname in a determined form and adds to
    	     * people structure. */
            sprintf(people->data[people->size].name, "%s %s", name, surname);
            /* Gets the linked list for phone numbers using form_list function */
            people->data[people->size].numbers = form_list(inp, people);
            people->size += 1; /* Increments people size by one */
        } else {
            quit = TRUE; /* If EOF is reached, then makes it TRUE to exit loop.*/
        }
    }
    free(name);     /* Frees the allocated space for name */
    free(surname);  /* Frees the allocated space for surname */
    fclose(inp);    /* Closes the file. */
}

void print(People people) {
    int i,found = 0;
    Record *rec;
    /* header */
    printf("%-5s %-30s %-20s %-20s\n", "ID","NAME","EXPENDITURE","NUMBER(s)");
    /* line */
    for (i = 0; i < 78; ++i)
        printf("-");
    printf("\n");

    for (i = 0; i < people.size; ++i) {
        found = 0;
        printf("%-5d %-30s %-20.4f", people.data[i].id, people.data[i].name, people.data[i].expenditure);
        rec = people.data[i].numbers;
        while(rec) {
            if(found)
                printf("%57s", "");
            else
                found = 1;
            printf("%-20s\n", rec->number);
            rec = rec->next;
        }
        printf("\n");
    }
}

int isPeopleEq(People ppl1, People ppl2) {
    Record *rec1,*rec2;
    int i,found = 0;
    int p1size = 0, p2size = 0;
    if(ppl1.size != ppl2.size)
        return 0;
    for (i = 0; i < ppl1.size; ++i) {
        if(strcmp(ppl1.data[i].name,ppl2.data[i].name))
            return 0;
        if(ppl1.data[i].id != ppl2.data[i].id)
            return 0;

        p1size = p2size = 0;
        rec1 = ppl1.data[i].numbers;
        while(rec1) {
            ++p1size;
            rec1 = rec1->next;
        }

        rec2 = ppl2.data[i].numbers;
        while(rec2) {
            ++p2size;
            rec2 = rec2->next;
        }

        if(p1size != p2size) {
            return 0;
        }

        rec1 = ppl1.data[i].numbers;
        while(rec1) {
            rec2 = ppl2.data[i].numbers;
            found = 0;
            while(!found && rec2) {
                if(strcmp(rec1->number,rec2->number) == 0) {
                    found = 1;
                    break;
                }
                rec2 = rec2->next;
            }
            if(!found) {
                return 0;
            }
            rec1 = rec1->next;
        }
    }
    return 1;
}

int main(int argc, char** argv) {
    People people1,people2;
    people1.size = 0;
    read(argv[1],&people1);
    print(people1);
    writePeople(people1,"people.bin");
    readPeople(&people2,"people.bin");
    print(people2);
    printf("%s\n", isPeopleEq(people1,people2) ? "PEOPLE ARE SAME" : "PEOPLE ARE DIFFERENT!");
    printf("Making imputation\n");
    imputation(&people1);
    print(people1);
    return 0;
}
