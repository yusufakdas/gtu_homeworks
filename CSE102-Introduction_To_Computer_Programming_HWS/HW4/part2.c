/******************************************************************************
 * Yusuf Akdas 151044043 - HW_4_Part_2                                        *
 ******************************************************************************/
#include <stdio.h>
#define MAX 255

/* Defines new type named PaddingType */
typedef enum _paddingType {ZERO, HALF, SAME} PaddingType;

/* Function prototypes */
void addPadding(double inputArr[], int inputArraySize, double outputArr[], int
    *outputArraySize, int paddingWidth, void paddingMethod(double[], int, int));
void zeroPadding(double outputArr[], int outputArraySize, int paddingWidth);
void samePadding(double outputArr[], int outputArraySize, int paddingWidth);
void halfPadding(double outputArr[], int outputArraySize, int paddingWidth);
int convolution(double inputArr[], int inputArraySize, double kernelArr[], int
                kernelArraySize, double outputArr[], int *outputArraySize, int
                stride, PaddingType padding);

int main(void) {
	double inputArr[] = {3, 5, 7, 9, 11, 13, 15};
	double kernelArr[] = {-1, 2, 0.5, 4, 1.7};
	double outputArr[MAX];
	int outputArrSize = 0, i;
	
	convolution(inputArr, 7, kernelArr, 5, outputArr, &outputArrSize, 1, SAME);
	for (i = 0; i < outputArrSize; ++i) 
		printf("%.1f ",  outputArr[i]);
	printf("\n");

	return (0);
}

/*
 * This function makes convolution operation happen.
 */
int convolution(double inputArr[], int inputArraySize, double kernelArr[], int
                kernelArraySize, double outputArr[], int *outputArraySize, int
                stride, PaddingType padding) {
    int i, j, paddingWidth, temp_array_size, status,
        k = -stride;
    double sum = 0;
    double temp_array[255]; /* Used for addPadding function */ 

    /* This formula calculates how many times padding is made */
    paddingWidth = (kernelArraySize - 1) / 2;

    /* 
     * if input array size is greater than kernel array size, 
     * this operation is made.
     */
    if (inputArraySize > kernelArraySize) {
        status = 0;        /* Indicates that there is no problem */

        *outputArraySize = ((inputArraySize - kernelArraySize + 2 * paddingWidth) /
                            stride) + 1;
        
        /* Selects which padding method will be used */
        switch (padding) {
          case ZERO:
               addPadding(inputArr, inputArraySize, temp_array,
                          &temp_array_size, paddingWidth, zeroPadding);
               break;
          case SAME:
               addPadding(inputArr, inputArraySize, temp_array,
                          &temp_array_size, paddingWidth, samePadding);
               break;
          case HALF:
               addPadding(inputArr, inputArraySize, temp_array,
                          &temp_array_size, paddingWidth, halfPadding);
               break;
        }
        
        /* This is the actual part that makes convolution operation happen */
        for (i = 0; i < *outputArraySize; i++) {
            k += stride;
            sum = 0;
            for (j = 0; j < kernelArraySize; j++) {
                sum += kernelArr[j] * temp_array[j + k];
            }
            outputArr[i] = sum;
        }
    } else {
        status = -1;
    }

    return (status);
}

/* 
 * This function copies content of inputArr into outputArr and shifts outputArr
 * by paddingWidth. In addition to this it adds some padding using zeroPadding
 * samePadding and halfPadding functions.
 */
void addPadding(double inputArr[], int inputArraySize, double outputArr[], int
    *outputArraySize, int paddingWidth, void paddingMethod(double[], int, int))
{
    int i, j; 
    
    /* Calculates the size of output array */
    *outputArraySize = 2 * paddingWidth + inputArraySize;

    /* Copies input array to output array*/
    for (i = 0; i < inputArraySize; i++) {
         outputArr[i] = inputArr[i];
    }
    
    /* Shifts output array */
    for (i = 0; i < paddingWidth; i++) {
        for (j = (*outputArraySize - 1); j > 0; j--) {
            outputArr[j] = outputArr[j - 1];
        }
    }

    /* Adds padding using other three function apart from addPadding */
    paddingMethod(outputArr, *outputArraySize, paddingWidth);
}

/*
 * This function adds zero to the beginning and end of outputArr.
 */
void zeroPadding(double outputArr[], int outputArraySize, int paddingWidth)
{
    int i;
    
    /* Adds zero to beginning and end of the array */
    for (i = 0; i < paddingWidth; i++) {
        outputArr[i] = 0;
        outputArr[outputArraySize - i - 1] = 0; 
    }
}

/*
 * This function adds first and last element of input array to the beginning and 
 * end of outputArr.
 */
void samePadding(double outputArr[], int outputArraySize, int paddingWidth)
{
    int    i;
    double beginning = outputArr[paddingWidth],
           end = outputArr[outputArraySize - paddingWidth - 1];
    
    /* Adds the first and last element of input array */
    for (i = 0; i < paddingWidth; i++) {
        outputArr[i] = beginning;
        outputArr[outputArraySize - i - 1] = end; 
    }
}

/*
 * This function adds half of first and last element of input array to 
 * the beginning and end of outputArr.
 */
void halfPadding(double outputArr[], int outputArraySize, int paddingWidth)
{
    int    i;
    double beginning = outputArr[paddingWidth] / 2,            
           end = outputArr[outputArraySize - paddingWidth - 1] / 2;
    
    /* Adds half of first and last element of input array */
    for (i = 0; i < paddingWidth; i++) {
        outputArr[i] = beginning;
        outputArr[outputArraySize - i - 1] = end; 
    }
}

/******************************************************************************
 *                             End of the Part 2                              *
 ******************************************************************************/
