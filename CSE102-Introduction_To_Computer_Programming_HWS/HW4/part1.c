/******************************************************************************
 * Yusuf Akdas 151044043 - HW_4_Part_1                                        *
 ******************************************************************************/

#include <stdio.h>
#define MAX 255

/* Function prototypes */
void addPadding(double inputArr[], int inputArraySize, double outputArr[], int
    *outputArraySize, int paddingWidth, void paddingMethod(double[], int, int));
void zeroPadding(double outputArr[], int outputArraySize, int paddingWidth);
void samePadding(double outputArr[], int outputArraySize, int paddingWidth);
void halfPadding(double outputArr[], int outputArraySize, int paddingWidth);

int main(void) {
	double inputArr[] = {5, 6, 7, 8, 9};
	double outputArr[MAX];
	int outputArrSize = 0, i, size = 4;
	
	addPadding(inputArr, 5 , outputArr, &outputArrSize, size, halfPadding);

	for (i = 0; i < outputArrSize; ++i)
		printf("%.1f ", outputArr[i]);
	printf("\n");

	return (0);
}

/* 
 * This function copies content of inputArr into outputArr and shifts outputArr
 * by paddingWidth. In addition to this it adds some padding using zeroPadding
 * samePadding and halfPadding functions.
 */
void addPadding(double inputArr[], int inputArraySize, double outputArr[], int
    *outputArraySize, int paddingWidth, void paddingMethod(double[], int, int)) {
    int i, j; 
    
    /* Calculates the size of output array */
    *outputArraySize = 2 * paddingWidth + inputArraySize;

    /* Copies input array to output array*/
    for (i = 0; i < inputArraySize; i++) {
         outputArr[i] = inputArr[i];
    }
    
    /* Shifts output array */
    for (i = 0; i < paddingWidth; i++) {
        for (j = (*outputArraySize - 1); j > 0; j--) {
            outputArr[j] = outputArr[j - 1];
        }
    }

    /* Adds padding using other three function apart from addPadding */
    paddingMethod(outputArr, *outputArraySize, paddingWidth);
}

/*
 * This function adds zero to the beginning and end of outputArr.
 */
void zeroPadding(double outputArr[], int outputArraySize, int paddingWidth)
{
    int i;
    
    /* Adds zero to beginning and end of the array */
    for (i = 0; i < paddingWidth; i++) {
        outputArr[i] = 0;
        outputArr[outputArraySize - i - 1] = 0; 
    }
}

/*
 * This function adds first and last element of input array to the beginning and 
 * end of outputArr.
 */
void samePadding(double outputArr[], int outputArraySize, int paddingWidth)
{
    int    i;
    double beginning = outputArr[paddingWidth],
           end = outputArr[outputArraySize - paddingWidth - 1];
    
    /* Adds the first and last element of input array */
    for (i = 0; i < paddingWidth; i++) {
        outputArr[i] = beginning;
        outputArr[outputArraySize - i - 1] = end; 
    }
}

/*
 * This function adds half of first and last element of input array to 
 * the beginning and end of outputArr.
 */
void halfPadding(double outputArr[], int outputArraySize, int paddingWidth)
{
    int    i;
    double beginning = outputArr[paddingWidth] / 2,            
           end = outputArr[outputArraySize - paddingWidth - 1] / 2;
    
    /* Adds half of first and last element of input array */
    for (i = 0; i < paddingWidth; i++) {
        outputArr[i] = beginning;
        outputArr[outputArraySize - i - 1] = end; 
    }
}

/******************************************************************************
 *                             End of the Part 1                              *
 ******************************************************************************/
