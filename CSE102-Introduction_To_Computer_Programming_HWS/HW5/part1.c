/******************************************************************************
 * author: @yusufakdas 151044043                                              *
 * last update: 6 April 2017 10:04 PM                                         *
 * HW5 - 'This is minesweeper game that can be played on the terminal screen' *
 ******************************************************************************/

#include <stdio.h>

/* For GRIDSIZE X GRIDSIZE grid */
#define GRIDSIZE 4

typedef enum {
     mined,
	   empty,
	   flaggedMined,
	   flaggedEmpty,
	   closedEmpty,
	   closedMined
} cell;

/* Function prototypes */
void printGrid (cell grid[][GRIDSIZE]);
int openCell(cell grid[][GRIDSIZE], int x, int y);
void flagCell(cell grid[][GRIDSIZE], int x, int y);
int isCellEmpty(cell grid[][GRIDSIZE], int x, int y);
int isLocationLegal(int x, int y);
int asMain(void);
void initGrid(cell grid[][GRIDSIZE]);

/* Prototypes of additional two functions to make some operations happen. */
int isThereAnyFlag(cell grid[][GRIDSIZE], int x, int y);
int controlEnd(cell grid[][GRIDSIZE]);

int main(void) {

    asMain();

    return (0);
}

/*
 * This function takes players choices, counts valid moves made by player and
 * calls functions needed according to the player's wish. Invalid moves are to
 * open a cell that has been opened, to flag empty cell that has been opened,
 * to enter invalid location and to enter operation apart from 'o' or 'f'.
 */
int asMain(void)
{
    int x, y, stat,
        counter = 0,  /* Variable for counting moves */
        cont    = 1;  /* Variable for determining whether the game ends */
    char select;
    cell main_board[GRIDSIZE][GRIDSIZE];
    initGrid(main_board); /* Initializes game board */
    printf("Welcome to Minesweeper!\n\nIf you want to open a cell, enter only");
    printf(" 'o' or if you want to flag or unflag, enter only 'f'. Sample ");
    printf("usage 'o 2 3'. This opens (2 3).\n\n"); /* Introduction massage */
    do {
        printf("Enter your choice with a location > ");
        scanf(" %c%d%d", &select, &x, &y); /* Takes choices from user */
        switch (select)                    /* For player's choice */
        {
          case 'o': /* Opens a cell in particular location. */
                if (main_board[x][y] == empty) /* Prevents to open a cell that
                                                has been opened. */
                {
                    printf("This cell has already been opened. Please enter ");
                    printf("differrent one!\n");
                    counter--;           /* For elimination of invalid moves */
                }
                stat = openCell(main_board, x, y);
                if (stat < 0) /* If invalid location or flagged location is
                               entered then prints this massage on the screen.*/
                {
                    printf("The location may be invalid or flagged. Please ");
                    printf("enter differrent one!\n");
                    counter--;           /* For elimination of invalid moves */
                }
                break;
          case 'f': /* Flags or unflags a cell in particular location. */
                if (main_board[x][y] == empty) /* Prevents to open a cell that
                                                has been opened. */
                {
                    printf("This cell has already been opened. Please enter ");
                    printf("differrent one!\n");
                    counter--;          /* For elimination of invalid moves */
                }
                flagCell(main_board, x, y);
                break;
          default: /* If invalid operation is entered then prints this
                    massage on the screen. */
                printf("You can use only 'o' to open a cell or 'f' to ");
                printf("flag and unflag!\n");
                counter--;               /* For elimination of invalid moves */
        }
        printGrid(main_board);           /* Prints current grid on the screen.*/
        counter++;                       /* Counts moves done by user. */
        cont = controlEnd(main_board);   /* Controls whether the game ends. */
    } while (cont > 0);
    if (cont < 0) /* Loose massage with valid moves made */
        printf("Oh no! You have hit a mine. GAME OVER! - moves: %d\n", counter);
    else          /* Win massage with valid moves made */
        printf("Congratulations! You win the game with %d moves.\n", counter);
    return (0);
}
/*
 * Initializes the answer grid that shows where closed mines and
 * closed empty places are.
 */
void initGrid(cell grid[][GRIDSIZE])
{
    int row, col;
    cell initialize[GRIDSIZE][GRIDSIZE] = {
        {closedMined, closedEmpty, closedEmpty, closedEmpty},
        {closedEmpty, closedEmpty, closedEmpty, closedMined},
        {closedEmpty, closedMined, closedEmpty, closedMined},
        {closedEmpty, closedEmpty, closedEmpty, closedEmpty}
    };
    /* Writes the answer array to called grid. */
    for (row = 0; row < GRIDSIZE; row++)
      for (col = 0; col < GRIDSIZE; col++)
        grid[row][col] = initialize[row][col];
}
/*
 * Determines whether the location entered by user is legal or not.
 * If location is legal, then this function returns 1, otherwise 0.
 */
int isLocationLegal(int x, int y)
{
    return ((x < GRIDSIZE && x >= 0) && (y < GRIDSIZE && y >= 0));
}
/*
 * Prints current grid on the screen. It prints 'e' for empty, 'X' for mine and
 * 'f' for flag and '.' for something else on the screen.
 */
void printGrid(cell grid[][GRIDSIZE])
{
    int row, col;
    for (row = 0; row < GRIDSIZE; row++)
    {
        for (col = 0; col < GRIDSIZE; col++)
        {
            if (grid[row][col] == empty)
                printf("e ");
            else if (grid[row][col] == flaggedEmpty ||
                     grid[row][col] == flaggedMined)
                printf("f ");
            else if (grid[row][col] == mined)
                printf("X ");
            else
                printf(". ");
        }
        printf("\n");
    }
}
/*
 * Controls whether location entered by user is empty or not.
 */
int isCellEmpty(cell grid[][GRIDSIZE], int x, int y)
{
    return (grid[x][y] == closedEmpty);
}
/*
 * Opens any legal location entered by user.
 */
int openCell(cell grid[][GRIDSIZE], int x, int y)
{
    int i, j, stat;
    if(isLocationLegal(x,y) && (!(isThereAnyFlag(grid, x, y))))
    {
        if (grid[x][y] == closedEmpty) /* Opens closeEmpty cells with its 8
                                        closedEmpty neigbours. */
        {
            for (i = x-1; i <= x+1; i++)
              for (j = y-1; j <= y+1; j++)
                if (isLocationLegal(i, j) && isCellEmpty(grid, i, j) &&
                   (!(isThereAnyFlag(grid, i, j))))
                  grid[i][j] = empty;
            stat = 1; /* If there is no problem */
        /* If user hits a mine, then change closedMined cells as mined cells and
         print all of them on the screen. */
        } else if (grid[x][y] == closedMined) {
            for (i = 0; i < GRIDSIZE; i++)
              for (j = 0; j < GRIDSIZE; j++)
                if (grid[i][j] == closedMined)
                  grid[i][j] = mined;
        }
    } else {
        stat = -2; /* If location is illegal and there is a flag */
    }
    return (stat);
}
/*
 * Determines whether there is a flag specified location or not. If there is a
 * flag, then returns 1, otherwise 0.
 */
int isThereAnyFlag(cell grid[][GRIDSIZE], int x, int y)
{
    return (grid[x][y] == flaggedEmpty || grid[x][y] == flaggedMined);
}
/*
 * Controls whether all empty cells are opened and whether user hit a mine.
 */
int controlEnd(cell grid[][GRIDSIZE])
{
    int row, col,
        stat = 0; /* If stat does not change, this means that all of closedEmpty
                   cells is opened. */
    for (row = 0; row < GRIDSIZE; row++)
      for (col = 0; col < GRIDSIZE; col++)
        if (grid[row][col] == mined)
          return (-1);  /* If player hits a mine, returns -1. */
        else if (grid[row][col] == closedEmpty ||
                 grid[row][col] == flaggedEmpty)
          stat = 1; /* If there is any closedEmpty cell, continue game. */
    return (stat);
}
/*
 * Flags or unflags any legal location.
 */
void flagCell(cell grid[][GRIDSIZE], int x, int y)
{
    if(isLocationLegal(x, y))
    {
        if (grid[x][y] == closedEmpty)
            grid[x][y] = flaggedEmpty; /* Flags.   */
        else if (grid[x][y] == closedMined)
            grid[x][y] = flaggedMined; /* Flags.   */
        else if (grid[x][y] == flaggedEmpty)
            grid[x][y] = closedEmpty;  /* Unflags. */
        else if (grid[x][y] == flaggedMined)
            grid[x][y] = closedMined;  /* Unflags. */
    }
}
/******************************************************************************
 *                              ~End of HW5~                                  *
 ******************************************************************************/
