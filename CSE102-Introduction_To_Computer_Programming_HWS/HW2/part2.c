/******************************************************************************
 * Yusuf Akdas 151044043 Homework_2 - Part_2                                  *
 ******************************************************************************/

#include <stdio.h>

/* Function prototypes */
void drawRectangle(int width, int height, int startingPoint, int printLastLine);
void drawDoubleCircle(int radius, int startingPoint , int wheelDistance);
void drawCar(void);

int main(void) {
	drawCar();
	return (0);
}

/*
 * This function draws two rectangular parts of the car.    
 */
void drawRectangle(int width, int height, int startingPoint, int printLastLine) {
    int j = 0,  /* It is for height of rectangle */    
        i = 0;  
    
    /* This is for how many times function draws a line */
    for (i = 0; i < height; i++) {
        /* Sets where rectangle starts */
        for (j = 0; j < startingPoint; j++) {
            printf(" ");
        }
        /* 
         * If i is equal to 0, then it draws a line by filling in asterisks,
         * or if i is equal to height - 1 and printLastLine is equal to 1, 
         * then draw the base of rectangle.
         */
        if (i == 0 || (i == height - 1 && printLastLine == 1)) {
            for (j = 0; j < width; j++) {
                printf("*");
            }
            printf("\n");
        /* This is for void in the rectangle */
        } else if (i > 0) {
            printf("*");
            for (j = 2; j < width; j++) {
                printf(" ");
            }
            printf("*\n");
        }
    }
}
/*
 * This function draws wheels of the car. In this function, circle equation was
 * used to draw the wheels. (x² + y² = r²)
 */
void drawDoubleCircle(int radius, int startingPoint , int wheelDistance) {
    int i = 0,
        j = 0;

    for (i = radius; i >= -radius; i--) {
        /* Sets where wheel is drawed. */
        for (j = 0; j < startingPoint; j++) {
           printf(" ");
        }
        /* Draws the first wheel */
        for (j = radius; j >= -radius; j--) {
            /* Puts space between two asterisk */
            printf(" ");
            /* When this equation is less or equal to the square of radius, 
             * it indicates interior of circle. Therefore if this is true then
             * it puts asterisk, otherwise space*/
            if ((i * i) + (j * j) <= (radius * radius)) {
               printf("*");
            } else {
               printf(" ");
            }
        }
        /* Sets space between two wheels. */
        for (j = 0; j < wheelDistance; j++) {
            printf(" ");
        }
        /* Draws other wheel*/
        for (j = radius; j >= -radius; j--) {
            printf(" ");
            if ((i * i) + (j * j) <= (radius * radius)) {
               printf("*");
            } else {
               printf(" ");
            }
        }
        printf("\n");
    }
}
/*
 * This function draws a car by using other two function.
 */ 
void drawCar(void)
{
    drawRectangle(30, 5, 33, 0);
    drawRectangle(60, 10, 18, 1);
    drawDoubleCircle(4, 24, 11);
}
