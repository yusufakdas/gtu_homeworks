/*******************************************************************************
 *                  Yusuf Akdas 151044043 Homework_2 - Part_1                  *                                       *
 ******************************************************************************/

#include <stdio.h>

/* Function prototypes */
void drawFunction(int xAxis, int yAxis, int functionNumber);
int fTheFunction(int x, int functionNumber);

int main(void) {
    int xAxis, yAxis, functionNumber;

    printf("Enter x axis, y axis and function number> ");
    scanf("%d%d%d", &xAxis, &yAxis, &functionNumber);
    drawFunction(xAxis, yAxis, functionNumber);

    return (0);
}

int fTheFunction(int x, int functionNumber) {
    switch (functionNumber) {
        case 1:
            return (x * x + 5 * x - 5);
        case 2:
            return (x + 2 - x * x);
        case 3:
            return (x - 5);
        case 4:
            return (2 * x * x + 18 * x - 4);
        default:
            return (2 * x + 10);
    }
}

/* This function plots the graph of given function */
void drawFunction(int xAxis, int yAxis, int functionNumber) {
    int i, j;  /* for iteration */

    /* This is for how many times repetition is done by loop. */
    for (i = yAxis; i >= 0; i--) {
        printf("|");
        if (i != 0) {
            /* this is for scanning each line */
            for (j = 0; j < xAxis; j++) {
                /* if i is equal to result obtained by using j,
                 * then print a asterisk */
                if (i == fTheFunction(j, functionNumber))
                    printf("*");
                printf(" ");
            }
        } else { /* if i is equal to 0, then print x axis */
            for (j = 0; j < xAxis; j++)
                printf("-");
        }
        printf("\n");   /* newline after each repetition */
    }
}
