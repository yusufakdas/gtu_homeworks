package com.example.android.connectfour;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

public class PlayGameSelectionActivity extends AppCompatActivity {
    private int mBoardSize;
    private int mGameType; // 0 pvp, 1 pvc
    private int mTimer = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_game_selection);

        final EditText sizeText = findViewById(R.id.board_size_text);
        handleBoardSize(sizeText);

        final Spinner typeSpinner = findViewById(R.id.game_type_spinner);
        handleGameTypeSelection(typeSpinner);

        final CheckBox checkTimerMode = findViewById(R.id.check_timer_mode);
        handleTimerMode(checkTimerMode);

        findViewById(R.id.start_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (CanContinueNextStep(checkTimerMode)) {
                Intent goToTheGame = new Intent(PlayGameSelectionActivity.this,
                                     GameActivity.class);
                goToTheGame.putExtra("BOARD_SIZE", mBoardSize);
                goToTheGame.putExtra("GAME_TYPE", mGameType);
                goToTheGame.putExtra("TIMER_MODE", mTimer);
                startActivity(goToTheGame);
                finish();
            }
            }
        });
    }

    private void handleGameTypeSelection(Spinner typeSpinner) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.game_type, R.layout.spinner_selection);
        typeSpinner.setAdapter(adapter);

        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mGameType = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void handleBoardSize(EditText sizeText) {
        sizeText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    mBoardSize = Integer.parseInt(s.toString());
                } catch (NumberFormatException e) {
                    mBoardSize = 0;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void handleTimerMode(CheckBox checkBox) {
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                LinearLayout timerLayout = findViewById(R.id.set_time_layout);
                EditText timer = findViewById(R.id.timer_edit_text);
                if (isChecked) {
                    timerLayout.setVisibility(View.VISIBLE);
                    handleSetTimer(timer);
                } else {
                    timer.setText("");
                    timerLayout.setVisibility(View.GONE);
                }
            }
        });
    }

    private void handleSetTimer(EditText timer) {
        timer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    mTimer = Integer.parseInt(s.toString());
                } catch (NumberFormatException e) {
                    mTimer = -1;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private boolean CanContinueNextStep(CheckBox check) {
        if (mBoardSize > 40 || mBoardSize < 5) {
            Toast error = Toast.makeText(PlayGameSelectionActivity.this,
                "Please type a number that is between 5 and 40!", Toast.LENGTH_SHORT);
            error.show();
            return (false);
        }

        if (check.isChecked() && mTimer < 5) {
            Toast error = Toast.makeText(PlayGameSelectionActivity.this,
                "Timer mode on and time value must be greater than 5!", Toast.LENGTH_SHORT);
            error.show();
            return (false);
        }
        return (true);
    }
}