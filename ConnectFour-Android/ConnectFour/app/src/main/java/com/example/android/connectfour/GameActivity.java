package com.example.android.connectfour;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class GameActivity extends AppCompatActivity {
    private final Pair<Integer, Integer> CHECK_VERTICAL = new Pair<>(1, 0);
    private final Pair<Integer, Integer> CHECK_HORIZONTAL = new Pair<>(0, 1);
    private final Pair<Integer, Integer> CHECK_RIGHT_DIAGONAL = new Pair<>(1, 1);
    private final Pair<Integer, Integer> CHECK_LEFT_DIAGONAL = new Pair<>(1, -1);

    private int mBoardSize;
    private int mGameType;
    private int mTimer;
    private int mOrder;    // "0 --> P1(X)", "1 --> P2(O)", "2 --> C(O)"
    private ArrayList<Cell> mGameCells;
    private int[] mRowCount;
    private Cell mUserMove;
    private Cell mComputerMove;
    private Cell[] winnerCheckers = new Cell[4];
    private ArrayList<Cell> mMoveCoordinates = new ArrayList<>();
    private CountDownTimer mCountDown;
    private TextView mTimerPanel;
    private boolean mIsEnd = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);

        setContentView(R.layout.activity_game);
        getDataPrevious();
        initialize();
    }

    private void getDataPrevious() {
        Bundle extraData = getIntent().getExtras();
        if (extraData == null) {
            Log.e("GameActivity", "extraData is null");
            return;
        }
        mBoardSize = (int) extraData.get("BOARD_SIZE");
        mGameType = (int) extraData.get("GAME_TYPE");
        mTimer = (int) extraData.get("TIMER_MODE");
    }

    private void initialize() {
        GridLayout board = findViewById(R.id.game_grid);
        board.setColumnCount(mBoardSize);
        board.setRowCount(mBoardSize);

        mGameCells = new ArrayList<>();
        for (int i = 0; i < mBoardSize; ++i) {
            for (int j = 0; j < mBoardSize; ++j) {
                View v = LayoutInflater.from(this).inflate(R.layout.grid_element, board,
                         false);
                v.setOnClickListener(new ViewListener(j));
                board.addView(v);
                mGameCells.add(new Cell(i, j, v));
            }
        }
        mRowCount = new int[mBoardSize];
        Arrays.fill(mRowCount, mBoardSize);

        mTimerPanel = findViewById(R.id.timer);
        if (mTimer != -1) {
            findViewById(R.id.timer_linear).setVisibility(View.VISIBLE);
            final Random rand = new Random();
            mCountDown = new CountDownTimer((mTimer + 1) * 1000, 1000) {
                int col = rand.nextInt(mBoardSize);

                @Override
                public void onTick(long millisUntilFinished) {
                    final long second = millisUntilFinished;
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            String str = "Time: " + String.valueOf(second / 1000);
                            mTimerPanel.setText(str);
                        }
                    });
                }

                @Override
                public void onFinish() {
                    col = rand.nextInt(mBoardSize);
                    while (!isLegal(mRowCount[col] - 1, col, Cell.Checker.EMPTY))
                        col = rand.nextInt(mBoardSize);
                    playGame(col);
                }
            }.start();
        }
        findViewById(R.id.undo_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        undo();
                    }
                });
            }
        });

        mComputerMove = new Cell(-1, -1, new View(this));
    }

    private void undo() {
        if (!mMoveCoordinates.isEmpty()) {
            if (mTimer != -1)
                mCountDown.cancel();

            Cell lastElement;
            if (mGameType == 1) {
                lastElement = mMoveCoordinates.get(mMoveCoordinates.size() - 1);
                lastElement.setType(Cell.Checker.EMPTY);
                lastElement.getChecker().setBackgroundResource(R.drawable.defaultback);
                ++mRowCount[lastElement.getCol()];
                mMoveCoordinates.remove(lastElement);
            }

            lastElement = mMoveCoordinates.get(mMoveCoordinates.size() - 1);
            lastElement.setType(Cell.Checker.EMPTY);
            lastElement.getChecker().setBackgroundResource(R.drawable.defaultback);
            ++mRowCount[lastElement.getCol()];
            mMoveCoordinates.remove(lastElement);

            if (mGameType == 0)
                mOrder = (mOrder == 0) ? 1 : 0;

            if (mTimer != -1)
                mCountDown.start();
        } else {
            Toast.makeText(this, "Cannot undo anymore!", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isLegal(int r, int c, Cell.Checker type) {
        return (r >= 0 && c >= 0 && c < mBoardSize && r < mBoardSize
                && mGameCells.get(getIndex(r, c)).getType() == type);
    }

    private int getIndex(int r, int c) {
        return (r * mBoardSize + c);
    }

    private boolean play(int pos) {
        int r = mRowCount[pos] - 1;
        if (isLegal(r, pos, Cell.Checker.EMPTY)) {
            View b = mGameCells.get(getIndex(r, pos)).getChecker();
            mMoveCoordinates.add(mGameCells.get(getIndex(r, pos)));
            if (mOrder == 0) {
                mGameCells.get(getIndex(r, pos)).setType(Cell.Checker.P1);
                b.setBackgroundResource(R.drawable.red);
            } else {
                mGameCells.get(getIndex(r, pos)).setType(Cell.Checker.P2);
                b.setBackgroundResource(R.drawable.blue);
            }
            if (mGameType == 0) { // Fixes order
                if (mOrder == 0)
                    mOrder = 1;
                else
                    mOrder = 0;
            } else {
                mOrder = 2;
            }
            mUserMove = mGameCells.get(getIndex(r, pos));
            --mRowCount[pos];
        } else {
            return (false);
        }
        if (mTimer != -1)
            mCountDown.start();
        return (true);
    }

    private void play() {
        int c, r;
        Random rand = new Random();

        if (!preventOrWin(Cell.Checker.COMP) && !preventOrWin(Cell.Checker.P1)) {
            mComputerMove.setCol(rand.nextInt(mBoardSize));
            mComputerMove.setRow(mRowCount[mComputerMove.getCol()] - 1);
            while (!isLegal(mComputerMove.getRow(), mComputerMove.getCol(), Cell.Checker.EMPTY)) {
                mComputerMove.setCol(rand.nextInt(mBoardSize));
                mComputerMove.setRow(mRowCount[mComputerMove.getCol()] - 1);
            }
        }
        r = mComputerMove.getRow();
        c = mComputerMove.getCol();
        mComputerMove.setType(Cell.Checker.COMP);
        mMoveCoordinates.add(mGameCells.get(getIndex(r, c)));

        View b = mGameCells.get(getIndex(r, c)).getChecker();
        mGameCells.get(getIndex(r, c)).setType(Cell.Checker.COMP);
        b.setBackgroundResource(R.drawable.blue);
        --mRowCount[c];

        mOrder = 0;
    }

    private void playGame(int pos) {
        if (mTimer != -1 || mIsEnd)
            mCountDown.cancel();
        if (mGameType == 0) {
            play(pos);
            mIsEnd = controlEnd(mUserMove);
        } else {
            if (play(pos)) {
                mIsEnd = controlEnd(mUserMove);
                if (!mIsEnd) {
                    play();
                    mIsEnd = controlEnd(mComputerMove);
                }
            } else {
                Toast.makeText(this, "Cannot play on this column!", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        if (mIsEnd) {
            if (mTimer != -1 )
                mCountDown.cancel();
            resultDialog();
        }
    }

    private void changeCheckers() {
        for (int i = 0; i < 4; ++i)
            winnerCheckers[i].getChecker().setBackgroundResource(R.drawable.end);
    }

    private boolean controlEnd(Cell move) {
        if (checkDraw()) {
            mOrder = -1;
            return (true);
        }
        if (check(move, CHECK_VERTICAL) || check(move, CHECK_HORIZONTAL)
                || check(move, CHECK_RIGHT_DIAGONAL) || check(move, CHECK_LEFT_DIAGONAL)) {
            changeCheckers();
            if (mGameType == 0)  // Fixes order
                mOrder = (mOrder == 0) ? 1 : 0; // from p1 to p2, vice versa.
            else
                mOrder = (mOrder == 0) ? 2 : 0;
            return (true);
        }
        return (false);
    }

    private String showResult() {
        switch (mOrder) {
            case 0: return ("Player one has just won!");
            case 1: return ("Player two has just won!");
            case 2: return ("Computer has just won!");
            default:return ("Nobody won this game!");
        }
    }

    private boolean checkDraw() {
        for (int i = 0; i < mBoardSize; ++i)
            if (isLegal(0, i, Cell.Checker.EMPTY))
                return (false);
        return (true);
    }

    private boolean check(Cell move, Pair<Integer, Integer> checkType) {
        int c = move.getCol(), r = move.getRow(), j = 1, checkerCount = 1;
        int dxR = checkType.first, dxC = checkType.second;
        boolean one = false, two = false;

        if (mGameType == 1 && r != mBoardSize - 1 && isLegal(r + 1, c, Cell.Checker.EMPTY))
            return (false);

        winnerCheckers[0] = mGameCells.get(getIndex(r, c));
        for (int i = 1; i < 4; ++i) {
            int varRow = r - (i * dxR), varCol = c - (i * dxC);
            if (!one && isLegal(varRow, varCol, move.getType())) {
                winnerCheckers[j] = mGameCells.get(getIndex(varRow, varCol));
                ++checkerCount;
                ++j;
            } else {
                one = true;
            }
            varRow = r + (i * dxR);
            varCol = c + (i * dxC);
            if (!two && isLegal(varRow, varCol, move.getType())) {
                winnerCheckers[j] = mGameCells.get(getIndex(varRow, varCol));
                ++checkerCount;
                ++j;
            } else {
                two = true;
            }
            if (checkerCount == 4)
                return (true);
        }
        return (false);
    }

    private boolean checkForComputer(Cell.Checker type, int row, int col) {
        for (int j = -1; j <= 1; ++j) {
            for (int k = -1; k <= 1; ++k) {
                if (isLegal(row + j, col + k, Cell.Checker.EMPTY)) {
                    mGameCells.get(getIndex(row + j, col + k)).setType(type);
                    Cell move = mGameCells.get(getIndex(row + j, col + k));
                    mComputerMove.setRow(move.getRow());
                    mComputerMove.setCol(move.getCol());
                    mComputerMove.setType(Cell.Checker.COMP);
                    --mRowCount[col + k];

                    if (check(move, CHECK_VERTICAL) || check(move, CHECK_HORIZONTAL)
                         || check(move, CHECK_RIGHT_DIAGONAL) || check(move, CHECK_LEFT_DIAGONAL)) {
                        move.setType(Cell.Checker.EMPTY);
                        ++mRowCount[col + k];
                        return (true);
                    }

                    move.setType(Cell.Checker.EMPTY);
                    ++mRowCount[col + k];
                }
            }
        }
        return (false);
    }

    private boolean preventOrWin(Cell.Checker type) {
        for (int i = 0; i < mBoardSize; ++i) {
            int row = mRowCount[i];
            if (row < mBoardSize && mGameCells.get(getIndex(row, i)).getType() == type)
                if (checkForComputer(Cell.Checker.P1, row, i))
                    return (true);
        }
        return (false);
    }

    private void resultDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("GAME OVER!");
        dialog.setMessage(showResult());
        dialog.setCancelable(false);

        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        dialog.create().show();
    }

    class ViewListener implements View.OnClickListener {
        private int mPos;

        ViewListener(int p) {
            mPos = p;
        }

        @Override
        public void onClick(View view) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    if (!mIsEnd)
                        playGame(mPos);
                }
            });
        }
    }
}