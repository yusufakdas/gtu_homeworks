package com.example.android.connectfour;

import android.view.View;

class Cell {
    private int mRow;
    private int mCol;
    private View mChecker;
    private Checker mType;

    Cell(int r, int c, View v) {
        mRow = r;
        mCol = c;
        mType = Checker.EMPTY;
        mChecker = v;
    }

    int getRow() {
        return (mRow);
    }

    int getCol() {
        return (mCol);
    }

    Checker getType() {
        return (mType);
    }

    void setRow(int row) {
        mRow = row;
    }

    void setCol(int col) {
        mCol = col;
    }

    void setType(Checker t) {
        mType = t;
    }

    View getChecker() {
        return (mChecker);
    }

    enum Checker {
        EMPTY, P1, P2, COMP
    }
}