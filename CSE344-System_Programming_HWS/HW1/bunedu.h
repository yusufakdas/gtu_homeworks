/* Yusuf AKDAS 151044043 */

#ifndef __BUNEDU_H__
#define __BUNEDU_H__
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#define FALSE 0
#define TRUE 1
#define DOTDIR 2 /* For '.' and '..' directories. */

/* 
 * Flag for '-z' option.
 */
extern short option;

/*
 * Traverse the given path and print the directory usage in KB.
 * Returns: sum of the positive values of pathfun on success,
 * 		    -1 if it failed to traverse any subdirectory.
 */
extern int postOrderApply(char *path, int pathfun(char *path1));

/*
 * Get the size of an ordinary file.
 * Returns: the size in blocks of the file given by path,
 * 			-1 if path does not correspond to an ordinary file.
 */ 
extern int sizepathfun(char *path);

#endif

