/* Yusuf AKDAS 151044043 */
#include "bunedufork.h" 

/*
 * Check the given file is directory or not.
 */
static int isDirectory(const char *path)
{
	struct stat attributes;				
	int isDot = DOTDIR, end;

	if (lstat(path, &attributes) == -1)
		return FALSE;

	/* Check whether the given file is dot or dotdot. */
	for (end = strlen(path) - 1; path[end] != '/'; --end)
		if (path[end] != '.') 
			isDot = FALSE;
	if (S_ISDIR(attributes.st_mode) != 0) 
		return isDot == DOTDIR ? DOTDIR : TRUE;
	return FALSE;
}

/*
 * Concatenates two files by putting '/' character between them.
 * Returns the concatenated string.
 */
static char *concatenatePathWithFile(const char *d_name, const char *path)
{
	size_t filePathSize = strlen(d_name) + strlen(path) + 1 + 1;
	char *filePath = (char *) malloc(sizeof(char) * filePathSize);
	sprintf(filePath, "%s/%s", path, d_name);  /* Concatenation */
	return filePath;
}

/*
 * Reads the file written by processes and adds pid, size and path to struct 
 * array.
 * Returns the line number and the struct.
 */
static struct line_t *readAndParseFile(int *lineCount)
{
	char *buffer, *lineBuffer, *chPtr;
	off_t bytes;
	struct line_t *lines;
	short tabCount = 0;
	int i, j, k, fd;
	
	/* Open file descriptor */
	if ((fd = open(FILE_NAME, O_RDONLY)) == -1) {
		perror("open readFile");
		exit(EXIT_FAILURE);
	}

	/* Seek to the end of the file to get the total bytes file includes. */
	bytes = lseek(fd, 0, SEEK_END);
	buffer = (char *) malloc(sizeof(char) * (bytes + 1));

	/* Seek to the beginning of the file */
	lseek(fd, 0, SEEK_SET);
	if (read(fd, buffer, bytes) == -1) {
		perror("read");
		free(buffer);
		exit(EXIT_FAILURE);
	}

	/* Close the file descriptor */
	if (close(fd) == -1) {
		perror("close readFile");
		free(buffer);
		exit(EXIT_FAILURE);
	}

	buffer[bytes] = '\0';
	chPtr = buffer;

	/* Iterates in order to get the line number of the file */
	*lineCount = 0;
	while (*chPtr++ != '\0')
		if (*chPtr == '\n')
			++(*lineCount);
	
	lineBuffer = (char *) malloc(sizeof(char) * BUFF_LEN);
	lines = (struct line_t *) malloc(sizeof(struct line_t) * (*lineCount));

	/* Parse the string and add related fields to the struct */
	for (i = 0, j = 0; i < *lineCount; ++i) {
		for (k = 0; buffer[j] != '\n'; ++j) {
			if (buffer[j] == '\t') {
				lineBuffer[k] = '\0';
				if (tabCount == 0)
					lines[i].pid = atol(lineBuffer);
				else
					lines[i].size = atol(lineBuffer);
				tabCount = tabCount == 1 ? 0 : tabCount + 1;
				k = 0;
			} else {
				lineBuffer[k++] = buffer[j];
			}
		}
		lineBuffer[k] = '\0';
		lines[i].path = (char *) malloc(sizeof(char) * (k + 1));
		sprintf(lines[i].path, "%s", lineBuffer);
		++j;
	}
	free(lineBuffer);
	free(buffer);

	return lines;
}

/*
 * Traverse the given path and print the directory usage in KB.
 * Returns: sum of the positive values of pathfun on success,
 * 		    -1 if it failed to traverse any subdirectory.
 */
int postOrderApply(char *path, int pathfun(char *path1))
{
	DIR *dirStream = NULL;
	struct dirent *entry = NULL;
	char *pathOperation, *filePath;
	char buffer[BUFF_LEN];
	int status, checkDir, totalSize = 0, returnValue, fd;
	pid_t childPid;

	pathOperation = (char *) calloc(sizeof(char), strlen(path) + 1);
	sprintf(pathOperation, "%s", path);

	if ((dirStream = opendir(pathOperation)) == NULL) {
		perror("opendir");
		free(pathOperation);
		exit(EXIT_FAILURE);
	}

	switch (childPid = fork()) {
		case -1:	/* Error occured */
			perror("fork1");
			free(pathOperation);
			exit(EXIT_FAILURE);

		case  0:	/* Child execution point */
			if ((fd = open(FILE_NAME, FILE_FLAG, FILE_MODE)) == -1) {
				perror("open");
				free(pathOperation);
				_exit(EXIT_FAILURE);
			}

			while ((entry = readdir(dirStream)) != NULL) {
				filePath = concatenatePathWithFile(entry->d_name,pathOperation);
				checkDir = isDirectory(filePath);
				if (checkDir == TRUE) {
					switch (childPid = fork()) {
						case -1:
							free(pathOperation);
							free(filePath);
							close(fd);
							perror("fork1");
							exit(EXIT_FAILURE);

						case  0:
							/* Close file descriptor to open the directory */
							if (closedir(dirStream) == -1) {
								free(pathOperation);
								free(filePath);
								close(fd);
								perror("closedir");
								_exit(EXIT_FAILURE);
							}
							/* Set the new path */
							pathOperation = (char *)realloc(pathOperation, 
											strlen(filePath) + 1);
							sprintf(pathOperation, "%s", filePath);

							/* Open the file */
							if ((dirStream = opendir(pathOperation)) == NULL) {
								free(pathOperation);
								free(filePath);
								close(fd);
								perror("opendir1");
								_exit(EXIT_FAILURE);
							}
							totalSize = 0;
							break;

						default: ; /* Parent continues */
					}
				} else if (checkDir == FALSE) {
					if ((returnValue = pathfun(filePath)) != -1) {
						totalSize += returnValue / 1024;
					} else {

						/* When writing file, -1 is added as to make parsing 
						 * operation easier */
						sprintf(buffer, "%ld\t-1\tSpecial file %s\n", 
								(long)getpid(), entry->d_name);

						/* Lock the file to write special file info */
						if (flock(fd, LOCK_EX) != 0) {
							perror("flock - lock");	
							free(pathOperation);
							free(filePath);
							_exit(EXIT_FAILURE);
						}

						/* Write to the file */
						if ((write(fd, buffer, strlen(buffer)) == -1)) {
							free(pathOperation);
							free(filePath);
							close(fd);
							perror("write");
							_exit(EXIT_FAILURE);
						}

						/* Unlock the file to let other process use */
						if (flock(fd, LOCK_UN) != 0) {
							perror("flock - unlock");	
							free(pathOperation);
							free(filePath);
							_exit(EXIT_FAILURE);
						}
					}
				}
				free(filePath);
			}

			/* Waiting for all child processes before death */
			while (wait(NULL) != -1)
				;

			sprintf(buffer, "%ld\t%d\t%s\n", (long)getpid(), totalSize, 
					pathOperation);
			free(pathOperation);

			if (flock(fd, LOCK_EX) != 0) {
				perror("flock - lock");	
				_exit(EXIT_FAILURE);
			}

			if ((write(fd, buffer, strlen(buffer)) == -1)) {
				perror("write");
				_exit(EXIT_FAILURE);
			}

			if (flock(fd, LOCK_UN) != 0) {
				perror("flock - unlock");	
				_exit(EXIT_FAILURE);
			}

			if (closedir(dirStream) == -1) {
				perror("closedir");
				_exit(EXIT_FAILURE);
			}

			if (close(fd) == -1) {
				perror("close fd");
				_exit(EXIT_FAILURE);
			}
			_exit(EXIT_SUCCESS);
		default:	/* Parent execution point */
			childPid = wait(&status);
			free(pathOperation);
			if (closedir(dirStream) == -1) {
				perror("closedir");
				exit(EXIT_FAILURE);
			}
	}
	return 0;
}

/*
 * Prints the result with respect to the given argument.
 */
void printResult(short option)
{
	int lineCount, i, j;
	struct line_t * lines;
	long processCount = 0;
	size_t len;
	char *subStr;
	int slashCount = 0;

	lines = readAndParseFile(&lineCount);

	/* BONUS PART: Calculate the total size for '-z' option. */
	for (i = 0; i < lineCount && option; ++i) {
		for (j = 0; j < lineCount && lines[i].size != -1; ++j) {
			len = strlen(lines[i].path);
			if (lines[j].size != -1 
					&& strncmp(lines[i].path, lines[j].path, len) == 0) {
				for (subStr = &lines[j].path[len]; *subStr; ++subStr)
					if (*subStr == '/')
						++slashCount;
				if (slashCount == 1)
					lines[i].size += lines[j].size;
				slashCount = 0;
			}
		}
	}

	printf("PID\tSIZE\tPATH\n");
	for (i = 0; i < lineCount; ++i) {
		printf("%ld\t", lines[i].pid);
		if (lines[i].size == -1) {
			printf("\t");
		} else {
			printf("%ld\t", lines[i].size);
			++processCount;
		}
		printf("%s\n", lines[i].path);
	}
	printf("%ld child processes created. Main process is %ld.\n", 
			processCount, (long) getpid());

	/* Free the struct */
	for (i = 0; i < lineCount; ++i) 
		free(lines[i].path);
	free(lines);
}

/*
 * Get the size of an ordinary file.
 * Returns: the size in blocks of the file given by path,
 * 			-1 if path does not correspond to an ordinary file.
 */ 
int sizepathfun(char *path)
{
	struct stat attributes;

	/* If the non-existent path is passed, print an error message and exit. */
	if (lstat(path, &attributes) == -1) {
		perror("sizepathfun");
		exit(EXIT_FAILURE);
	}
	/* If the given file is not regular return -1, otherwise the size of file.*/
	return S_ISREG(attributes.st_mode) == 0 ? -1 : attributes.st_size;
}

