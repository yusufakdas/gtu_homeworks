/* Yusuf AKDAS 151044043 */
#ifndef __BUNEDU_H__
#define __BUNEDU_H__
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/file.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#define FALSE 0
#define TRUE 1
#define DOTDIR 2 /* For '.' and '..' directories. */
#define FILE_NAME "151044043sizes.txt"
#define FILE_MODE S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH
#define FILE_FLAG O_CREAT | O_WRONLY | O_TRUNC
#define BUFF_LEN 1024

/* Holding the process id, size and path of the file */
struct line_t {
	long pid;
	long size;
	char *path;
};

/*
 * Traverse the given path and print the directory usage in KB.
 * Returns: sum of the positive values of pathfun on success,
 * 		    -1 if it failed to traverse any subdirectory.
 */
extern int postOrderApply(char *path, int pathfun(char *path1));

/*
 * Get the size of an ordinary file.
 * Returns: the size in blocks of the file given by path,
 * 			-1 if path does not correspond to an ordinary file.
 */ 
extern int sizepathfun(char *path);

/*
 * Prints the result with respect to the given argument.
 */
extern void printResult(short option);

#endif

