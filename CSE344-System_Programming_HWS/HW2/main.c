/* Yusuf AKDAS 151044043 */

#include "bunedufork.h"

int main(int argc, char *argv[])
{
	char *path = NULL;
	short option = FALSE;

	if (argc == 2) {
		path = argv[1];
	} else if (argc == 3) {
		if (strcmp(argv[1], "-z") != 0) {
			fprintf(stderr, "Usage: ./buNeDuFork [-z] rootpath\n");
			exit(EXIT_FAILURE);
		}
		option = TRUE;
		path = argv[2];
	} else {
		fprintf(stderr, "Usage: ./buNeDuFork [-z] rootpath\n");
		exit(EXIT_FAILURE);
	}

	postOrderApply(path, sizepathfun);
	printResult(option);

	return EXIT_SUCCESS;
}

