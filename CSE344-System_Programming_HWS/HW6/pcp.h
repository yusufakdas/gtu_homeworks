/* Yusuf AKDAS - 151044043 */
#ifndef __PCP_H__
#define __PCP_H__
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <dirent.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/resource.h>
#include <fcntl.h>
#include <signal.h>
#define FALSE 0
#define TRUE 1
#define MAX_COPY_STEP 8192
#define MAXFDPERPROCESS 1024
#define DESTFLAG (O_WRONLY | O_TRUNC | O_CREAT)
#define DESTMODE (S_IRWXU | S_IRWXG | S_IROTH)

typedef enum { REGULAR, FIFO, DIRECTORY, DOT, OTHER, ERR } filetype_t;

struct item {
	char name[PATH_MAX];
	int in;
	int out;
};

/* Copy buffer is an circular queue designed for ThreadPool */
typedef struct {
	struct item *data;
	int size;
	int currAdd;
	int currRemove;
	int currCount;
} CopyBuffer;

typedef struct {
	pthread_t *threadIDs;
	int threadCount;
	struct {
		long regularCount;
		long fifoCount;
		long dirCount;
		long long bytesCopied;
	} result;
} ThreadPool;

extern void operate(char *dirs[2], int consumerCount, int bufferSize);

#endif

