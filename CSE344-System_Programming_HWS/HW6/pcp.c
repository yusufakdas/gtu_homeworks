/* Yusuf AKDAS - 151044043 */
#include "pcp.h"

/* Copy buffer is a circular queue that is shared with thread pool */
static CopyBuffer buffer;
static ThreadPool copyPool;
static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t lockStdout = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t lockResult = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t lockFD = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t empty = PTHREAD_COND_INITIALIZER;
static pthread_cond_t full = PTHREAD_COND_INITIALIZER;
static int prodDone = FALSE;
static int sigDone = FALSE;
static int currentFDCount = 3;
static pthread_t signalThreadID; 
static pthread_t prodId;

static void initThreadPool(int threadCount, void *(*threadFunc)(void *arg), void *arg);
static void destroyThreadPool(void);
static filetype_t getFileType(const char *path);
static void clean(void);
static void *producerThreadFunc(void *arg);
static void *consumerThreadFunc(void *arg);
static int generateData(const char *source, const char *dest, const char *name, struct item *data);
static int readDirectory(const char *source, const char *dest);
static void addTheItem(struct item *val);
static void copyFile(const struct item *val);
static CopyBuffer initCopyBuffer(int size);
static void destroyCopyBuffer(CopyBuffer *buf);
static int addAnItem(CopyBuffer *buf, const struct item *val);
static int removeAnItem(CopyBuffer *buf, struct item *val);
static void printStatistics(long long totalTime, int consumer, int bufferSize);
static int increaseProducerThreadStackSize(void); /* Because of recursive calls */
static void increaseResult(filetype_t type);
static int installSIGINTHandler(void);
static void *signalHandlerThread(void *arg);
static int increaseFileDescriptorCount(void);
static void decreaseFileDescriptorCount(void);

void operate(char *dirs[2], int consumerCount, int bufferSize)
{
	struct timeval start;
	struct timeval end;
	long long totalTime = 0;
	int errCode = 0;

	signalThreadID = pthread_self();
	if (installSIGINTHandler() == -1) {
		fprintf(stderr, "SIGINT signal handler could not be installed properly!\n");
		exit(EXIT_FAILURE);
	}
	if (atexit(clean) == -1) {
		perror("atexit");
		exit(EXIT_FAILURE);
	}
	if (mkdir(dirs[1], DESTMODE) == -1 && errno != EEXIST) {
		perror("mkdir destination");
		exit(EXIT_FAILURE);
	}
	buffer = initCopyBuffer(bufferSize);
	gettimeofday(&start, NULL);
	initThreadPool(consumerCount, consumerThreadFunc, NULL);

	prodId = pthread_self();
	errCode = pthread_create(&prodId, NULL, producerThreadFunc, (void **) dirs);
	if (errCode != 0) {
		fprintf(stderr, "pthread_create: %s\n", strerror(errCode));
		exit(EXIT_FAILURE);
	}
	errCode = pthread_join(prodId, NULL);
	if (errCode != 0)
		fprintf(stderr, "pthread_join: %s\n", strerror(errCode));
	prodId = pthread_self();
	destroyThreadPool();

	gettimeofday(&end, NULL);
	if (!sigDone) {
		totalTime = 1E+3*(end.tv_sec - start.tv_sec)+(end.tv_usec - start.tv_usec)*1E-3;
		printStatistics(totalTime, consumerCount, bufferSize);
	}
}

static void printStatistics(long long totalTime, int consumer, int bufferSize)
{
	long miliseconds = totalTime % 1000;
	long seconds = totalTime / 1000;
	long minutes = seconds / 60;

	seconds %= 60;
	printf("\n----------------------------------------------------\n");
	printf("STATISTICS: Consumers: %d - Buffer Size: %d\n", consumer,
			bufferSize);
	printf("Regular File: %ld\n", copyPool.result.regularCount);
	printf("FIFO File: %ld\n", copyPool.result.fifoCount);
	printf("Directory: %ld\n", copyPool.result.dirCount);
	printf("TOTAL BYTES COPIED: %lld\n", copyPool.result.bytesCopied);
	printf("TOTAL TIME: %02ld:%02ld.%03ld (min:sec.mili)\n", minutes,
			seconds, miliseconds);
	printf("----------------------------------------------------\n");
}

static void *producerThreadFunc(void *arg)
{
	char **dirs = (char **) arg;
	char *sourceDir = *dirs;
	char *destDir = *(dirs + 1);

	increaseProducerThreadStackSize();
	readDirectory(sourceDir, destDir);

	pthread_mutex_lock(&lock);
	prodDone = TRUE;
	pthread_mutex_unlock(&lock);
	pthread_cond_broadcast(&empty);

	return NULL;
}

static void *consumerThreadFunc(void *arg) {
	int isEnd = FALSE;
	struct item val;

	memset(&val, 0, sizeof(struct item));
	for (;;) {
		/* CAUTION: Critical section to check conditional variable 'empty' */
		if (pthread_mutex_lock(&lock) != 0) {
			fprintf(stderr, "pthread_mutex_lock: consumerThreadFunc\n");
			exit(EXIT_FAILURE);
		}
		while (prodDone == FALSE && buffer.currCount == 0) {
			if (pthread_cond_wait(&empty, &lock) != 0) {
				fprintf(stderr, "pthread_cond_wait: consumerThreadFunc\n");
				exit(EXIT_FAILURE);
			}
		}
		isEnd = (prodDone == TRUE && buffer.currCount == 0);
		if (isEnd == FALSE)
			removeAnItem(&buffer, &val);
		if (pthread_mutex_unlock(&lock) != 0) {
			fprintf(stderr, "pthread_mutex_unlock: consumerThreadFunc\n");
			exit(EXIT_FAILURE);
		}
		if (pthread_cond_signal(&full) != 0) {
			fprintf(stderr, "pthread_cond_signal: consumerThreadFunc\n");
			exit(EXIT_FAILURE);
		}
		if (isEnd == TRUE)
			break;
		copyFile(&val);
	}
	return NULL;
}

static void copyFile(const struct item *val)
{
	ssize_t readBytes = 0;
	size_t fileBytes = 0;
	size_t writBytes = 0;
	char *bytes = NULL;
	int notDone = FALSE;

	bytes = (char *) calloc(MAX_COPY_STEP, sizeof(char));
	while (!sigDone) {
		readBytes = read(val->in, bytes, MAX_COPY_STEP);
		if (readBytes == -1) {
			fprintf(stderr, "copyFile: read\n");
			notDone = TRUE;
			break;
		} else if (readBytes == 0) {
			break;
		}
		writBytes = write(val->out, bytes, readBytes);
		if (writBytes == -1) {
			fprintf(stderr, "copyFile: write\n");
			notDone = TRUE;
			break;
		} else if (readBytes != writBytes) {
			fprintf(stderr, "copyFile: file corrupted\n");
			notDone = TRUE;
			break;
		}
		fileBytes += readBytes;
	}
	free(bytes);
	if (close(val->in) == -1)
		fprintf(stderr, "close: consumerThreadFunc - val.in\n");
	decreaseFileDescriptorCount();
	if (close(val->out) == -1)
		fprintf(stderr, "close: consumerThreadFunc - val.out\n");
	decreaseFileDescriptorCount();
	if (!sigDone || notDone) {
		pthread_mutex_lock(&lockStdout);
		printf("%s has been copied successfully.\n", val->name);
		pthread_mutex_unlock(&lockStdout);

		pthread_mutex_lock(&lockResult);
		copyPool.result.bytesCopied += fileBytes;
		pthread_mutex_unlock(&lockResult);
	}
}

static int readDirectory(const char *source, const char *dest)
{
	DIR *dir = NULL;
	struct dirent *entry = NULL;
	struct item val;
	char pathSource[PATH_MAX];
	char pathDest[PATH_MAX];
	filetype_t type;
	int err = 0;

	if (increaseFileDescriptorCount()) {
		pthread_mutex_lock(&lockStdout);
		printf("----MAXIMUM FILE DESCRIPTOR FOR PER PROCESS HAS BEEN REACHED!----\n");
		pthread_mutex_unlock(&lockStdout);
		return -1;
	}
	dir = opendir(source);
	if (dir == NULL) {
		pthread_mutex_lock(&lockStdout);
		printf("%s directory could not be opened.\n", source);
		pthread_mutex_unlock(&lockStdout);
		return -1;
	}
	memset(&val, 0, sizeof(struct item));
	while (!sigDone && (entry = readdir(dir)) != NULL) {
		snprintf(pathSource, PATH_MAX, "%s/%s", source, entry->d_name);
		snprintf(pathDest, PATH_MAX, "%s/%s", dest, entry->d_name);
		type = getFileType(pathSource);

		if (type == FIFO) {
			if (mkfifo(pathDest, DESTMODE) == -1 && errno != EEXIST) { 
				perror("mkfifo");
			} else {
				pthread_mutex_lock(&lockStdout);
				printf("%s has been copied successfully.\n", entry->d_name);
				pthread_mutex_unlock(&lockStdout);
				increaseResult(type);
			}
		} else if (type == REGULAR) {
			err = generateData(pathSource, pathDest, entry->d_name, &val);
			if (err == 0) {
				addTheItem(&val);
				increaseResult(type);
			} else if (err == -1) {
				pthread_mutex_lock(&lockStdout);
				printf("%s file could not be opened!\n", entry->d_name);
				pthread_mutex_unlock(&lockStdout);
			}
		} else if (type == DIRECTORY) {
			if (mkdir(pathDest, DESTMODE) == -1 && errno != EEXIST) {
				perror("mkdir");
				return -1;
			}
			if (readDirectory(pathSource, pathDest) != -1)
				increaseResult(type);
		}
	}
	if (closedir(dir) == -1) {
		perror("closedir");
		return -1;
	}
	decreaseFileDescriptorCount();
	return 0;
}

static void increaseResult(filetype_t type)
{
	pthread_mutex_lock(&lockResult);
	if (type == FIFO)
		++copyPool.result.fifoCount;
	else if (type == DIRECTORY)
		++copyPool.result.dirCount;
	else if (type == REGULAR)
		++copyPool.result.regularCount;
	pthread_mutex_unlock(&lockResult);
}

static void addTheItem(struct item *val)
{
	/* CAUTION: Critical Section to check conditional variable 'full' */
	if (pthread_mutex_lock(&lock) != 0) {
		fprintf(stderr, "pthread_mutex_lock: producerThreadFunc\n");
		exit(EXIT_FAILURE);
	}
	while (buffer.currCount == buffer.size) {
		if (pthread_cond_wait(&full, &lock) != 0) {
			fprintf(stderr, "pthread_cond_wait: producerThreadFunc\n");
			exit(EXIT_FAILURE);
		}
	}
	if (pthread_mutex_unlock(&lock) != 0) {
		fprintf(stderr, "pthread_mutex_unlock: producerThreadFunc\n");
		exit(EXIT_FAILURE);
	}
	/* CAUTION: Critical Section */
	if (pthread_mutex_lock(&lock) != 0) {
		fprintf(stderr, "pthread_mutex_lock: readDirectory\n");
		exit(EXIT_FAILURE);
	}
	addAnItem(&buffer, val);

	if (pthread_mutex_unlock(&lock) != 0) {
		fprintf(stderr, "pthread_mutex_unlock: readDirectory\n");
		exit(EXIT_FAILURE);
	}
	if (pthread_cond_signal(&empty) != 0) {
		fprintf(stderr, "pthread_cond_signal: producerThreadFunc\n");
		exit(EXIT_FAILURE);
	}
}

/* Modify this function to copy FIFO files */
static int generateData(const char *source, const char *dest, const char *name, 
						struct item *data)
{
	if (increaseFileDescriptorCount()) {
		pthread_mutex_lock(&lockStdout);
		printf("----MAXIMUM FILE DESCRIPTOR FOR PER PROCESS HAS BEEN REACHED!----\n");
		pthread_mutex_unlock(&lockStdout);
		return -2;
	}
	data->in = open(source, O_RDONLY);
	if (data->in == -1) {
		perror("DATA_IN");
		return -1;
	}
	if (increaseFileDescriptorCount()) {
		pthread_mutex_lock(&lockStdout);
		printf("----MAXIMUM FILE DESCRIPTOR FOR PER PROCESS HAS BEEN REACHED!----\n");
		pthread_mutex_unlock(&lockStdout);
		return -2;
	}
	data->out = open(dest, DESTFLAG, DESTMODE);
	if (data->out == -1) {
		perror("DATA_OUT");
		close(data->in);
		decreaseFileDescriptorCount();
		return -1;
	}
	snprintf(data->name, PATH_MAX, "%s", name);

	return 0;
}

static filetype_t getFileType(const char *path)
{
	struct stat buf;
	filetype_t type;
	int i, isDot = TRUE;

	/* File information could not be acquired. */
	if (lstat(path, &buf) == -1)
		return ERR;
	type = S_ISREG(buf.st_mode) ? REGULAR
			: S_ISFIFO(buf.st_mode) ? FIFO
			: S_ISDIR(buf.st_mode) ? DIRECTORY : OTHER;
	if (type == DIRECTORY) {
		for (i = strlen(path) - 1; path[i] != '/'; --i)
			if (path[i] != '.')
				isDot = FALSE;
		return isDot ? DOT : type;
	}
	return type;
}

static void clean(void)
{
	int i;
	int retVal = 0;

	destroyCopyBuffer(&buffer);
	if (copyPool.threadIDs != NULL) {
		for (i = 0; i < copyPool.threadCount; ++i)
			pthread_kill(copyPool.threadIDs[i], SIGINT);
		destroyThreadPool();
	}
	pthread_kill(signalThreadID, SIGINT);
	if ((retVal = pthread_equal(signalThreadID, pthread_self())) == 0)
		retVal = pthread_join(signalThreadID, NULL);
	if ((retVal = pthread_equal(prodId, pthread_self())) == 0)
		pthread_join(prodId, NULL);
}

static int increaseProducerThreadStackSize(void)
{
	pthread_attr_t attribute;
	size_t bytes;
	int error;

	if ((error = pthread_attr_init(&attribute)) != 0) {
		fprintf(stderr, "pthread_attr_init: %s\n", strerror(error));
		return -1;
	}
	if ((error = pthread_attr_getstacksize(&attribute, &bytes)) != 0) {
		fprintf(stderr, "pthread_attr_getstacksize: %s\n", strerror(error));
		return -1;
	}
	if ((error = pthread_attr_setstacksize(&attribute, bytes * 2)) != 0) {
		fprintf(stderr, "pthread_attr_setstacksize: %s\n", strerror(error));
		return -1;
	}
	if ((error = pthread_attr_destroy(&attribute)) != 0) {
		fprintf(stderr, "pthread_attr_destroy: %s\n", strerror(error));
		return -1;
	}
	return 0;
}

/* Thread pool functions */
static void initThreadPool(int threadCount, void *(*threadFunc)(void *arg), void *arg)
{
	int i;

	memset(&copyPool, 0, sizeof(ThreadPool));
	copyPool.threadCount = threadCount;
	copyPool.threadIDs = (pthread_t *) calloc(threadCount, sizeof(pthread_t));
	for (i = 0; i < threadCount; ++i) {
		if (pthread_create(&copyPool.threadIDs[i], NULL, threadFunc, arg) != 0) {
			fprintf(stderr, "initThreadPool: %d\n", i);
			exit(EXIT_FAILURE);
		}
	}
}

static void destroyThreadPool(void)
{
	int i;

	for (i = 0; i < copyPool.threadCount; ++i)
		if (pthread_join(copyPool.threadIDs[i], NULL) != 0) 
			fprintf(stderr, "destroyThreadPool: %d\n", i);
	free(copyPool.threadIDs);
	copyPool.threadIDs = NULL;
}

/* CopyBuffer functions */
static CopyBuffer initCopyBuffer(int size)
{
	CopyBuffer buffer;

	memset(&buffer, 0, sizeof(CopyBuffer));
	buffer.size = size;
	buffer.data = (struct item*) calloc(size, sizeof(struct item));

	return buffer;
}

static void destroyCopyBuffer(CopyBuffer *buf)
{
	struct item val;

	memset(&val, 0, sizeof(struct item));
	while (removeAnItem(buf, &val) != -1) {
		close(val.in);
		close(val.out);
	}
	free(buf->data);
}

static int addAnItem(CopyBuffer *buf, const struct item *val)
{
	if (buf->currCount == buf->size) return -1;

	snprintf(buf->data[buf->currAdd].name, PATH_MAX, "%s", val->name);
	buf->data[buf->currAdd].in = val->in;
	buf->data[buf->currAdd].out = val->out;
	buf->currAdd = (buf->currAdd + 1) % buf->size;
	++buf->currCount;

	return 0;
}

static int removeAnItem(CopyBuffer *buf, struct item *val)
{
	if (buf->currCount == 0) return -1;

	snprintf(val->name, PATH_MAX, "%s", buf->data[buf->currRemove].name);
	val->in = buf->data[buf->currRemove].in;
	val->out = buf->data[buf->currRemove].out;
	buf->currRemove = (buf->currRemove + 1) % buf->size;
	--buf->currCount;

	return 0;
}

static int installSIGINTHandler(void)
{
	int retVal = 0;
	sigset_t signalMask;

	if (sigemptyset(&signalMask) == -1) {
		perror("sigemptyset");
		return -1;
	}
	if (sigaddset(&signalMask, SIGINT) == -1) {
		perror("sigaddset");
		return -1;
	}
	if ((retVal = pthread_sigmask(SIG_BLOCK, &signalMask, NULL)) != 0) {
		fprintf(stderr, "pthread_sigmask: %s\n", strerror(retVal));
		return -1;
	}
	if ((retVal = pthread_create(&signalThreadID, NULL, signalHandlerThread, NULL) != 0)) {
		fprintf(stderr, "pthread_create: %s\n", strerror(retVal));
		return -1;
	}
	return 0;
}

static void *signalHandlerThread(void *arg)
{
	int signo;
	sigset_t signalMask;

	if (sigemptyset(&signalMask) == -1) {
		perror("sigemptyset");
		return NULL;
	}
	if (sigaddset(&signalMask, SIGINT) == -1) {
		perror("sigaddset");
		return NULL;
	}
	sigwait(&signalMask, &signo);
	sigDone = TRUE;
	return NULL;
}

static int increaseFileDescriptorCount(void)
{
	pthread_mutex_lock(&lockFD);
	if (currentFDCount >= MAXFDPERPROCESS - 1) {
		pthread_kill(signalThreadID, SIGINT);
		printf("CURRENT FILE DESCRIPTOR COUNT-> %d\n", currentFDCount);
		pthread_mutex_lock(&lock);
		prodDone = TRUE;
		pthread_mutex_unlock(&lock);
		pthread_mutex_unlock(&lockFD);
		pthread_cond_broadcast(&full);
		pthread_cond_broadcast(&empty);
		return 1;
	}
	++currentFDCount;
	pthread_mutex_unlock(&lockFD);
	return 0;
}

static void decreaseFileDescriptorCount(void)
{
	pthread_mutex_lock(&lockFD);
	--currentFDCount;
	pthread_mutex_unlock(&lockFD);
}

