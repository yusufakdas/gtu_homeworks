/* Yusuf AKDAS - 151044043 */
#include "pcp.h"

int main(int argc, char *argv[])
{
	int bufferSize = 0;
	int consumerCount = 0;
	char *dirs[2];

	if (argc != 5) { fprintf(stderr, "Usage: %s consumers buffersize "
				"/from/source/dir /to/destination/dir\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	consumerCount = atoi(argv[1]);
	bufferSize = atoi(argv[2]);
	if (consumerCount < 1 || bufferSize < 1) {
		fprintf(stderr, "Consumer count and buffer size must be "
				"greater than one!\n");
		exit(EXIT_FAILURE);
	}
	dirs[0] = argv[3];
	dirs[1] = argv[4];

	operate(dirs, consumerCount, bufferSize);

	exit(EXIT_SUCCESS);
}

