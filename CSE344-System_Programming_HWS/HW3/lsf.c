#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <dirent.h>
#include <sys/stat.h>
#define PERMLEN 9

void listFiles(DIR *dir, const char *cwd);
int getPermissionsAndFileType(const char *path, char *perm, off_t *size);

int main(int argc, char *argv[])
{
	char *cwd;
	DIR *dir;

	if (argc > 1) {
		fprintf(stderr, "Usage: lsf\n");
		exit(EXIT_FAILURE);
	}

	if ((cwd = getcwd(NULL, 0)) == NULL) {
		perror("lsf - getcwd");
		exit(EXIT_FAILURE);
	}

	if ((dir = opendir(cwd)) == NULL) {
		perror("lsf - opendir");
		free(cwd);
		exit(EXIT_FAILURE);
	}

	listFiles(dir, cwd);
	
	free(cwd);
	if (closedir(dir) == -1) {
		perror("lsf - closedir");
		exit(EXIT_FAILURE);
	}
	exit(EXIT_SUCCESS);
}

void listFiles(DIR *dir, const char *cwd)
{
	size_t len, cwdLen = strlen(cwd);
	struct dirent *entry = NULL;
	char *path = NULL;
	char perm[PERMLEN + 2]; /* +2 for type and '\0' */
	off_t size = 0;

	while ((entry = readdir(dir)) != NULL) {
		len = cwdLen + strlen(entry->d_name) + 2; /* '\0' and '/' */
		path = (char *) realloc(path, sizeof(char) * len);
		sprintf(path, "%s/%s", cwd, entry->d_name);
		if (getPermissionsAndFileType(path, perm, &size)) {
			if (perm[0] != 'D')		/* If it is not directory... */
				printf("%s %10ld %s\n", perm, (long)size, entry->d_name);
		} else {
			perror("lsf - lstat");
		}
	}
	free(path);
}

int getPermissionsAndFileType(const char *path, char *perm, off_t *size)
{
	struct stat statbuf;
	mode_t m;

	if (lstat(path, &statbuf) == -1)
		return 0;

	m = statbuf.st_mode;
	perm[0] = S_ISREG(m) != 0 ? 'R' : S_ISDIR(m) != 0 ? 'D' : 'S';
	perm[1] = S_IRUSR & m ? 'r' : '-';
	perm[2] = S_IWUSR & m ? 'w' : '-';
	perm[3] = S_IXUSR & m ? 'x' : '-';
	perm[4] = S_IRGRP & m ? 'r' : '-';
	perm[5] = S_IWGRP & m ? 'w' : '-';
	perm[6] = S_IXGRP & m ? 'x' : '-';
	perm[7] = S_IROTH & m ? 'r' : '-';
	perm[8] = S_IWOTH & m ? 'w' : '-';
	perm[9] = S_IXOTH & m ? 'x' : '-';
	perm[10] = '\0';
	*size = statbuf.st_size;

	return 1;
}

