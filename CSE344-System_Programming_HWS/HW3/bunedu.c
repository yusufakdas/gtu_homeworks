#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <linux/limits.h>
#define FALSE 0
#define TRUE 1
#define DOTDIR 2 /* For '.' and '..' directories. */

/*
 * Traverse the given path and print the directory usage in KB.
 * Returns: sum of the positive values of pathfun on success,
 * 		    -1 if it failed to traverse any subdirectory.
 */
int postOrderApply(char *path, int pathfun(char *path1));

/*
 * Get the size of an ordinary file.
 * Returns: the size in blocks of the file given by path,
 * 			-1 if path does not correspond to an ordinary file.
 */ 
int sizepathfun(char *path);

/*
 * Check the given file is directory or not.
 */
int isDirectory(const char *path);

/* 
 * Flag for '-z' option.
 */
short option = FALSE;

int main(int argc, char *argv[])
{
	char *path = NULL;
	char buffer[PATH_MAX];
	
	if (argc == 1) {
		fgets(buffer, PATH_MAX, stdin);
		buffer[strlen(buffer)-1] = '\0';
		path = buffer;
	} else if (argc == 2) {
		if (strcmp(argv[1], "-z") == 0) {
			fgets(buffer, PATH_MAX, stdin);
			buffer[strlen(buffer)-1] = '\0';
			path = buffer;
			option = TRUE;
		} else {
			path = argv[1];
		}
	} else if (argc == 3) {
		if (strcmp(argv[1], "-z") != 0) {
			fprintf(stderr, "Usage: ./buNeDu [-z] rootpath\n");
			exit(EXIT_FAILURE);
		}
		option = TRUE;
		path = argv[2];
	} else {
		fprintf(stderr, "Usage: ./buNeDu [-z] rootpath\n");
		exit(EXIT_FAILURE);
	}
	postOrderApply(path, sizepathfun);

	return EXIT_SUCCESS;
}

/*
 * Check the given file is directory or not.
 */
int isDirectory(const char *path)
{
	struct stat attributes;				
	int isDot = DOTDIR, end;

	if (lstat(path, &attributes) == -1)
		return FALSE;

	/* Check whether the given file is dot or dotdot. */
	for (end = strlen(path) - 1; path[end] != '/'; --end)
		if (path[end] != '.') 
			isDot = FALSE;

	if (S_ISDIR(attributes.st_mode) != 0) 
		return isDot == DOTDIR ? DOTDIR : TRUE;
	return FALSE;
}

/*
 * Traverse the given path and print the directory usage in KB.
 * Returns: sum of the positive values of pathfun on success,
 * 		    -1 if it failed to traverse any subdirectory.
 */
int postOrderApply(char *path, int pathfun(char *path1))
{
	int totalSize = 0, returnValue = 0, regularSize = 0, checkDir;
	struct dirent *entry = NULL;
	char *filePath = NULL;
	size_t filePathSize;
	DIR *dirStream = opendir(path);

	/* Check whether the stream is opened or not. */
	if (dirStream == NULL && errno != EACCES) {
		perror("opendir");	
		return -1;
	}

	/* If permission is denied, print an informative message on the screen */
	if (errno == EACCES) { 
		printf("Cannot read folder %s\n", path);
	} else {
		while ((entry = readdir(dirStream)) != NULL) {
			filePathSize = strlen(entry->d_name) + strlen(path) + 1 + 1;
			filePath = (char *) malloc(sizeof(char) * filePathSize);
			sprintf(filePath, "%s/%s", path, entry->d_name); /* Concatenation */
			checkDir = isDirectory(filePath);
			if (checkDir == TRUE) {  /* If it is directory...*/ 
				if (option) 
					totalSize += postOrderApply(filePath, pathfun);
				else 
					totalSize = postOrderApply(filePath, pathfun);
			} else if (checkDir == FALSE) {  /* If it is not dot dotdot directory... */
				returnValue = pathfun(filePath);
				if (returnValue != -1)
					regularSize += returnValue / 1024;
				else
					printf("Special file %s\n", entry->d_name);
			}
			free(filePath);
		}
		printf("%d\t%s\n", option ? totalSize + regularSize 
				: regularSize, path); /* Print size and path */
	}
	closedir(dirStream);

	return totalSize + regularSize;
}

/*
 * Get the size of an ordinary file.
 * Returns: the size in blocks of the file given by path,
 * 			-1 if path does not correspond to an ordinary file.
 */ 
int sizepathfun(char *path)
{
	struct stat attributes;

	/* If the non-existent path is passed, print an error message and exit. */
	if (lstat(path, &attributes) == -1) {
		perror("sizepathfun");
		exit(EXIT_FAILURE);
	}
	/* If the given file is not regular return -1, otherwise the size of file. */
	return S_ISREG(attributes.st_mode) == 0 ? -1 : attributes.st_size;
}
