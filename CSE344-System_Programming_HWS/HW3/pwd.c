#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(void)
{
	char *cwd;

	if ((cwd = getcwd(NULL, 0)) == NULL) {
		perror("pwd");
		exit(EXIT_FAILURE);
	}
	printf("%s\n", cwd);

	free(cwd);
	exit(EXIT_SUCCESS);
}
