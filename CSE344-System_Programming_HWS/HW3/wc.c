#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

int main(int argc, char *argv[])
{
	ssize_t readBytes = 0;
	size_t len = 0, lineCount = 0;
	off_t totalBytes;
	char *data = NULL;
	int fd, i;

	switch (argc) {
		case 1:
			while (getline(&data, &len, stdin) != -1) 
				++lineCount;
			break;

		case 2:
			if ((fd = open(argv[1], O_RDONLY)) == -1) {
				perror("wc - open");
				exit(EXIT_FAILURE);
			}

			if ((totalBytes = lseek(fd, 0, SEEK_END)) == -1) {
				perror("wc - lseek");
				close(fd);
				exit(EXIT_FAILURE);
			}

			if (lseek(fd, 0, SEEK_SET) == -1) {
				perror("wc - lseek");
				close(fd);
				exit(EXIT_FAILURE);
			}
			
			data = (char *) malloc(sizeof(char) * totalBytes + 1);
			if ((readBytes = read(fd, data, totalBytes)) != totalBytes) {
				perror("wc - read");
				close(fd);
				free(data);
				exit(EXIT_FAILURE);
			}
			data[readBytes] = '\0';

			for (i = 0; data[i]; ++i)
				if (data[i] == '\n')
					++lineCount;

			if (close(fd) == -1) {
				perror("wc - close");
				free(data);
				exit(EXIT_FAILURE);
			}
			break;

		default:
			fprintf(stderr, "Usage: wc [/path/to/file]\n");
			exit(EXIT_FAILURE);
	}
	printf("%ld\n", lineCount);
	free(data);
	exit(EXIT_SUCCESS);
}
