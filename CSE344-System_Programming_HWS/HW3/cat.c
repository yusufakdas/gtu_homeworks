#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

int main(int argc, char *argv[])
{
	ssize_t readBytes = 0;
	size_t len = 0;
	off_t totalBytes;
	char *data = NULL;
	int fd;

	switch (argc) {
		case 1:
			while (getline(&data, &len, stdin) != -1) 
				printf("%s", data);
			break;

		case 2:
			if ((fd = open(argv[1], O_RDONLY)) == -1) {
				perror("cat - open");
				exit(EXIT_FAILURE);
			}

			if ((totalBytes = lseek(fd, 0, SEEK_END)) == -1) {
				perror("cat - lseek");
				close(fd);
				exit(EXIT_FAILURE);
			}

			if (lseek(fd, 0, SEEK_SET) == -1) {
				perror("cat - lseek");
				close(fd);
				exit(EXIT_FAILURE);
			}
			
			data = (char *) malloc(sizeof(char) * totalBytes + 1);
			if ((readBytes = read(fd, data, totalBytes)) != totalBytes) {
				perror("cat - read");
				free(data);
				close(fd);
				exit(EXIT_FAILURE);
			}
			data[readBytes] = '\0';
			printf("%s", data);

			if (close(fd) == -1) {
				perror("cat - close");
				free(data);
				exit(EXIT_FAILURE);
			}
			break;

		default:
			fprintf(stderr, "Usage: cat [/path/to/file]\n");
			exit(EXIT_FAILURE);
	}
	free(data);
	exit(EXIT_SUCCESS);
}

