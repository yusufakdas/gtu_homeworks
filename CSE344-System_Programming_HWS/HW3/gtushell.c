/* Yusuf AKDAS - 151044043 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <ctype.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <linux/limits.h>
#define MODES S_IRWXU | S_IRWXG | S_IRGRP
#define WRITE_FLAGS O_WRONLY | O_TRUNC | O_CREAT
#define NO_OP 0
#define STDOUT_OP 1
#define STDIN_OP 2
#define PIPE_OP 3
#define MAX_ARG 3
#define MAX_CMD 2

struct input_t {
	struct command_t {
		char *cmd;
		char *args[MAX_ARG];
	} command[MAX_CMD];
	char *redirectFile;
	unsigned int op:4;		/* 0: No op, 1: '>', 2: '<', 3: '|' */
};

const char *SHELL_COMMANDS[] = { "help", "cd", "exit" };
extern char **environ;

struct input_t **cmdList = NULL;
char *inp = NULL;
char *buffer = NULL;
char *program = NULL;
char cwd[PATH_MAX];
char *arguments[MAX_ARG + 2];
size_t cmdListLength = 10;
size_t currentCmdCount = 0;
int pipefd[2] = {-1, -1};
int fd = -1;

int handleShellCommands(const struct input_t *cmd, char *cwdForPrompt);
void handleWithoutPipe(const struct input_t *cmd);
void handlePipe(const struct input_t *cmd);
void handleHistoryCommand(struct input_t **cmd);

void split(char **ptr, char *buffer);
ssize_t prompt(char **inp, size_t *len, char *cwd);
void freeInput(struct input_t *inp);
int checkShellCommand(const char *shcmd);
int checkError(const struct input_t *inp, int cmdCount, int redirectCount);
struct input_t *parseCommand(char *inp, char **buffer);
void printHelpMenu();
char *getTheCurrentDirName(char *cwd);
void execPreparation(int cmdCount, const struct input_t *cmd);
void signalHandler(int signalNo);
void reallocCmdList();
void clean();		/* For atexit() */

int main(int argc, char *argv[])
{
	ssize_t length = 0;
	size_t len = 0;
	char cwdForPrompt[PATH_MAX];
	struct input_t *cmd = NULL;
	struct sigaction sa;
	int exitShell = 0;

	/* setbuf(stdout, NULL); */

	if (atexit(&clean) == -1) {
		perror("-gtushell: atexit:");
		exit(EXIT_FAILURE);
	}

	memset(&sa, 0, sizeof(struct sigaction));
	sa.sa_handler = &signalHandler;
	sigaction(SIGTERM, &sa, NULL);
	sigaction(SIGINT, &sa, NULL);

	if ((getcwd(cwd, PATH_MAX)) == NULL) {
		perror("-gtushell: cwd:");
		exit(EXIT_FAILURE);
	}
	sprintf(cwdForPrompt, "%s", cwd);

	cmdList = (struct input_t **) malloc(sizeof(struct input_t *)*cmdListLength);
	while (!exitShell && (length = prompt(&inp, &len, cwdForPrompt)) != -1) {
		if (inp[0] != '\0' && ((cmd = parseCommand(inp, &buffer)) != NULL)) {

			if (currentCmdCount >= cmdListLength)
				reallocCmdList();
			cmdList[currentCmdCount] = cmd;	
			++currentCmdCount;

			handleHistoryCommand(&cmd);
			if (checkShellCommand(cmd->command[0].cmd)) {
				exitShell = handleShellCommands(cmd, cwdForPrompt);
			} else if (cmd->op != PIPE_OP) {
				handleWithoutPipe(cmd);
			} else {
				handlePipe(cmd);
			}

		}
	}
	if (length == -1) 
		putchar('\n');
	exit(EXIT_SUCCESS);
}

void execPreparation(int cmdCount, const struct input_t *cmd)
{
	int i;

	size_t programLength = strlen(cwd)+strlen(cmd->command[0].cmd)+2;
	program = (char *) malloc(sizeof(char) * programLength);
	sprintf(program, "%s/%s", cwd, cmd->command[cmdCount].cmd);
	arguments[0] = cmd->command[cmdCount].cmd;
	for (i = 0; cmd->command[cmdCount].args[i]; ++i) 
		arguments[i+1] = cmd->command[cmdCount].args[i];
	arguments[i+1] = (char *) NULL;
}

ssize_t prompt(char **inp, size_t *len, char *cwd)
{
	ssize_t length;
	
	printf("[gtushell | %s]$ ", getTheCurrentDirName(cwd));
	length = getline(inp, len, stdin);
	if (length > 0) (*inp)[length-1] = '\0';

	return length;
}

struct input_t *parseCommand(char *command, char **buffer)
{
	char *ptr = command;
	int argCount = 0;
	int cmdCount = 0;
	int redirectCount = 0;
	struct input_t *cmd = NULL;

	if (*buffer == NULL || strlen(*buffer) < strlen(command))
		*buffer = (char *) realloc(*buffer, sizeof(char)*strlen(command)+1);

	cmd = (struct input_t *) malloc(sizeof(struct input_t)); 
	memset(cmd, 0, sizeof(struct input_t));

	while (*ptr != '\0' && redirectCount != -1 && cmdCount != -1) {
		split(&ptr, *buffer);
		if (**buffer != '\0' && cmd->command[cmdCount].cmd == NULL) {
			cmd->command[cmdCount].cmd = (char *) 
				malloc(sizeof(char)*strlen(*buffer)+1);
			sprintf(cmd->command[cmdCount].cmd, "%s", *buffer);
		}
		while (*ptr != '\0') {
			split(&ptr, *buffer);		
			if (strcmp(*buffer, ">") != 0 && strcmp(*buffer, "<") != 0
					&& strcmp(*buffer, "|") != 0) {
				if (argCount < 3 && strlen(*buffer) != 0) {
					cmd->command[cmdCount].args[argCount] = (char *)
						malloc(sizeof(char)*strlen(*buffer)+1);
					sprintf(cmd->command[cmdCount].args[argCount], "%s", *buffer);
				} 
				++argCount;
			} else if (strcmp(*buffer, ">") == 0 || strcmp(*buffer, "<") == 0) {
				if (redirectCount == 0) {
					cmd->op = **buffer == '>' ? STDOUT_OP : STDIN_OP;
					split(&ptr, *buffer);
					cmd->redirectFile = (char *) malloc(sizeof(char)*strlen(*buffer)+1);
					sprintf(cmd->redirectFile, "%s", *buffer);
					redirectCount = 1;
				} else {
					redirectCount = -1;
					break;
				}
			} else if (strcmp(*buffer, "|") == 0) {
				if (cmdCount == 0) {
					cmd->op = PIPE_OP;
					argCount = 0;
					cmdCount = 1;
				} else {
					cmdCount = -1;
				}
				break;
			}
		}
	}
	if (checkError(cmd, cmdCount, redirectCount) == -1) {
		freeInput(cmd);
		return NULL;
	}
	return cmd;
}

void freeInput(struct input_t *inp)
{
	int i, j;
	
	if (inp) {
		if (inp->redirectFile) free(inp->redirectFile);
		for (i = 0; i < MAX_CMD; ++i) {
			if (inp->command[i].cmd) free(inp->command[i].cmd);
			for (j = 0; j < MAX_ARG; ++j)
				if (inp->command[i].args[j])
					free(inp->command[i].args[j]);
		}
		free(inp);
	}
}

void split(char **ptr, char *buffer)
{
	int i;

	while (**ptr != '\0' && isspace(**ptr)) ++(*ptr);
	for (i = 0; **ptr != '\0' && !isspace(**ptr); ++i, ++(*ptr))
		buffer[i] = **ptr;
	buffer[i] = '\0';
	while (**ptr != '\0' && isspace(**ptr)) ++(*ptr);
}

int checkError(const struct input_t *inp, int cmdCount, int redirectCount)
{
	if (cmdCount == -1) {
		fprintf(stderr, "-gtushell: only one pipe can be used.\n");
		return -1;
	}
	if (redirectCount == -1) {
		fprintf(stderr, "-gtushell: only one redirection can be done.\n");
		return -1;
	}
	if ((inp->op == STDIN_OP || inp->op == STDOUT_OP) && inp->redirectFile == NULL) {
		fprintf(stderr, "-gtushell: invalid usage of redirection.\n");
		return -1;
	}
	if (inp->op == PIPE_OP && inp->command[1].cmd == NULL) {
		fprintf(stderr, "-gtushell: invalid usage of pipe.\n");
		return -1;
	}
	if (inp->op == PIPE_OP && (checkShellCommand(inp->command[0].cmd) 
				|| checkShellCommand(inp->command[1].cmd))) {
		fprintf(stderr, "-gtushell: pipe can be used between utilities.\n");
		return -1;
	}
	if (strcmp(inp->command[0].cmd, "cd") == 0 && inp->command[0].args[0] == NULL) {
		fprintf(stderr, "-gtushell: cd path/to/where/you/want/to/go\n");
		return -1;
	}
	if (inp->command[0].cmd[0] == '!' && inp->command[0].args[0] != NULL) {
		fprintf(stderr, "-gtushell: usage of ! operator: !n\n");
		return -1;
	}
	if (inp->command[0].cmd[0] == '!' && atoi(&inp->command[0].cmd[1]) == 0
			&& strlen(&inp->command[0].cmd[1]) > 1) {
		fprintf(stderr, "-gtushell: usage of !n operator: n must be an integer.\n");
		return -1;
	}
	if (inp->command[0].cmd[0] == '!' && (atoi(&inp->command[0].cmd[1]) <= 0 
			|| atoi(&inp->command[0].cmd[1]) > currentCmdCount)) {
		fprintf(stderr, "-gtushell: usage of !n operator: n must be between 1 and %ld.\n", 
				currentCmdCount);
		return -1;
	}
	return 0;
}

int checkShellCommand(const char *shcmd)
{
	int i;

	for (i = 0; i < 3; ++i)
		if (strcmp(shcmd, SHELL_COMMANDS[i]) == 0)
			return 1;
	return 0;
}

void printHelpMenu()
{
	printf("-lsf: list the current working directory.\n");
	printf("-pwd: print name of current working directory.\n");
	printf("-cd path: change the current working directory to given path.\n");
	printf("-help: shows the supported commands.\n");
	printf("-cat [path/to/file]: print the content of the given file or standard input.\n");
	printf("-wc [path/to/file]: print the number of lines given file or standard input.\n");
	printf("-bunedu [-z] [path/to/file]: summarizes the directories with their sizes.\n");
	printf("-exit: exit the gtushell.\n");
}

char *getTheCurrentDirName(char *cwd)
{
	int i;

	for (i = strlen(cwd) - 1; i != 0 && cwd[i] != '/'; --i)
		;
	return i == 0 ? cwd : &cwd[i+1];
}

void clean()
{
	int i;

	if (inp) free(inp);
	if (buffer) free(buffer);
	if (program) free(program);
	if (pipefd[0] != -1) close(pipefd[0]);
	if (pipefd[1] != -1) close(pipefd[1]);
	if (fd != -1) close(fd);
	if (cmdList != NULL) {
		for (i = 0; i < currentCmdCount; ++i)
			freeInput(cmdList[i]);
		free(cmdList);
	}
}

void reallocCmdList()
{
	struct input_t **tmp;
	size_t oldSize = cmdListLength;
	int i;

	cmdListLength *= 2;
	tmp = (struct input_t **) malloc(sizeof(struct input_t *) * cmdListLength);
	for (i = 0; i < oldSize; ++i)
		tmp[i] = cmdList[i];
	free(cmdList);
	cmdList = tmp;
}

int handleShellCommands(const struct input_t *cmd, char *cwdForPrompt)
{
	int retVal = 0;

	if (cmd->command[0].cmd[0] == 'h') {
		printHelpMenu();
	} else if (cmd->command[0].cmd[0] == 'c') {
		if (chdir(cmd->command[0].args[0]) == -1)
			perror("-gtushell: cd");
		else
			if (getcwd(cwdForPrompt, PATH_MAX) == NULL)
				perror("-gtushell: cd");
	} else {
		retVal = 1;
	}
	return retVal;
}

void handleWithoutPipe(const struct input_t *cmd)
{
	switch (fork()) {
		case -1:
			perror("-gtushell: fork");
			return;

		case  0:
			if (cmd->op == STDOUT_OP) {
				if ((fd = open(cmd->redirectFile, WRITE_FLAGS, 
								MODES)) == -1) {
					perror("-gtushell");
					fd = -1;
					exit(EXIT_FAILURE);
				}
				if (dup2(fd, STDOUT_FILENO) == -1) {
					perror("-gtushell:");
					fd = -1;
					exit(EXIT_FAILURE);
				}
				if (close(fd) == -1) {
					perror("-gtushell");
					fd = -1;
					exit(EXIT_FAILURE);
				}
			} else if (cmd->op == STDIN_OP) {
				if ((fd = open(cmd->redirectFile, O_RDONLY)) == -1) {
					perror("-gtushell");
					fd = -1;
					exit(EXIT_FAILURE);
				}
				if (dup2(fd, STDIN_FILENO) == -1) {
					perror("-gtushell");
					fd = -1;
					exit(EXIT_FAILURE);
				}
				if (close(fd) == -1) {
					perror("-gtushell");
					fd = -1;
					exit(EXIT_FAILURE);
				}
			}
			execPreparation(0, cmd);
			execve(program, arguments, environ);
			fprintf(stderr, "-gtushell: %s: %s\n", 
					arguments[0], strerror(errno));
			exit(EXIT_FAILURE);
		default:
			wait(NULL);
	}
}

void handlePipe(const struct input_t *cmd)
{
	if (pipe(pipefd) == -1) {
		perror("-gtushell");
		return;
	}
	switch (fork()) {
		case -1:
			perror("-gtushell");
			return;

		case  0:
			if (close(pipefd[0]) == -1) {
				perror("-gtushell");
				pipefd[0] = -1;
				exit(EXIT_FAILURE);
			}
			if (dup2(pipefd[1], STDOUT_FILENO) == -1) {
				perror("-gtushell");
				pipefd[0] = -1;
				exit(EXIT_FAILURE);
			}
			if (close(pipefd[1]) == -1) {
				perror("-gtushell");
				pipefd[1] = -1;
				exit(EXIT_FAILURE);
			}
			execPreparation(0, cmd);
			execve(program, arguments, environ);
			fprintf(stderr, "-gtushell: %s: %s\n", 
					arguments[0], strerror(errno));
			exit(EXIT_FAILURE);
	}
	switch (fork()) {
		case -1:
			perror("-gtushell");
			return;

		case  0:
			if (close(pipefd[1]) == -1) {
				perror("-gtushell");
				pipefd[1] = -1;
				exit(EXIT_FAILURE);
			}
			if (dup2(pipefd[0], STDIN_FILENO) == -1) {
				perror("-gtushell");
				pipefd[0] = -1;
				exit(EXIT_FAILURE);
			}
			if (close(pipefd[0]) == -1) {
				perror("-gtushell");
				pipefd[0] = -1;
				exit(EXIT_FAILURE);
			}
			execPreparation(1, cmd);
			execve(program, arguments, environ);
			fprintf(stderr, "-gtushell: %s: %s\n", 
					arguments[0], strerror(errno));
			exit(EXIT_FAILURE);
	}
	if (close(pipefd[0]) == -1) {
		perror("-gtushell");
		exit(EXIT_FAILURE);
	}
	if (close(pipefd[1]) == -1) {
		perror("-gtushell");
		exit(EXIT_FAILURE);
	}
	while (wait(NULL) != -1) 
		;
}

void handleHistoryCommand(struct input_t **cmd)
{
	long history = 0;

	if ((*cmd)->command[0].cmd[0] == '!') {
		history = atol(&(*cmd)->command[0].cmd[1]);
		--currentCmdCount;
		freeInput(cmdList[currentCmdCount]);
		cmdList[currentCmdCount] = NULL;
		*cmd = cmdList[currentCmdCount-history];
	}
}

void signalHandler(int signalNo)
{
	if (signalNo == SIGTERM)
		printf("\n***** gtushell has been terminated with SIGTERM signal. *****\n");
	exit(EXIT_SUCCESS);
}

