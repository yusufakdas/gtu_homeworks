/* Yusuf AKDAS 151044043 */

/* BUGS:
 * 		1. It cannot count the child processes correctly.
 * 		2. Sometimes, the order of the folders is not post order.
 */

#include "bunedufpf.h" 

/*
 * Check the given file is directory or not.
 */
static int isDirectory(const char *path)
{
	struct stat attributes;				
	int isDot = DOTDIR, end;

	if (lstat(path, &attributes) == -1)
		return FALSE;

	/* Check whether the given file is dot or dotdot. */
	for (end = strlen(path) - 1; path[end] != '/'; --end)
		if (path[end] != '.') 
			isDot = FALSE;
	if (S_ISDIR(attributes.st_mode) != 0) 
		return isDot == DOTDIR ? DOTDIR : TRUE;
	return FALSE;
}

/*
 * Concatenates two files by putting '/' character between them.
 * Returns the concatenated string.
 */
static char *concatenatePathWithFile(const char *d_name, const char *path)
{
	size_t filePathSize = strlen(d_name) + strlen(path) + 1 + 1;
	char *filePath = (char *) malloc(sizeof(char) * filePathSize);
	sprintf(filePath, "%s/%s", path, d_name);  /* Concatenation */
	return filePath;
}

/*
 * Traverse the given path and print the directory usage in KB.
 * Returns: sum of the positive values of pathfun on success,
 * 		    -1 if it failed to traverse any subdirectory.
 */
int postOrderApply(char *path, int pathfun(char *path1))
{
	DIR *dirPtr = NULL;
	struct dirent *entry = NULL;
	pid_t childPid;
	char *currDirPath = NULL;
	char *mainDirPath = NULL;
	int isDir, size = 0, retVal;
	int pipeFd[2];
	char buffer[BUFF_LEN];
	ssize_t readBytes;
	int isPipeCreated = FALSE;
	int writeEnd, readEnd, readValue = 0;
	int fifoWrite, fifoRead;
	int processCount = 0;
	long pid;

	mainDirPath = (char *) malloc(sizeof(path) * (strlen(path) + 1));
	sprintf(mainDirPath, "%s", path);
	if ((dirPtr = opendir(mainDirPath)) == NULL) {
		perror("opendir");
		free(mainDirPath);
		exit(EXIT_FAILURE);
	}

	if (mkfifo(FIFO_FILE_NAME, FIFO_PERMISSIONS) == -1) {
		if (errno != EEXIST) {
			perror("mkfifo");
			free(mainDirPath);	
			exit(EXIT_FAILURE);
		}
	}

	switch (childPid = fork()) {
		case -1:
			perror("fork - first");
			closedir(dirPtr);
			free(mainDirPath);
			exit(EXIT_FAILURE);

		case  0:
			if ((fifoWrite = open(FIFO_FILE_NAME, O_WRONLY)) == -1) {
				perror("open fifo - write");
				closedir(dirPtr);
				free(mainDirPath);
				_exit(EXIT_FAILURE);
			}

			writeEnd = readEnd = -1;
			while ((entry = readdir(dirPtr)) != NULL) {
				currDirPath = concatenatePathWithFile(
							entry->d_name, mainDirPath);
				isDir = isDirectory(currDirPath);
				if (isDir == TRUE) {

					/* Create only one pipe between tree levels */
					if (isPipeCreated == FALSE && pipe(pipeFd) == -1) {
						perror("pipe");
						free(mainDirPath);
						free(currDirPath);
						closedir(dirPtr);
						_exit(EXIT_FAILURE);
					}

					switch (childPid = fork()) {
						case -1:
							perror("fork - second");
							free(currDirPath);
							free(mainDirPath);
							closedir(dirPtr);
							_exit(EXIT_FAILURE);

						case  0:	/* I am a child */
							if (closedir(dirPtr) == -1) {
								perror("closedir - child");
								free(currDirPath);
								free(mainDirPath);
								_exit(EXIT_FAILURE);
							}

							mainDirPath = (char *) realloc(mainDirPath, 
								sizeof(currDirPath) * (strlen(currDirPath) + 1));

							sprintf(mainDirPath, "%s", currDirPath);
							if ((dirPtr = opendir(mainDirPath)) == NULL) {
								perror("opendir - child");
								free(currDirPath);
								free(mainDirPath);
								_exit(EXIT_FAILURE);
							}
							if (option == TRUE) {
								if (close(pipeFd[0]) == -1) {
									free(currDirPath);
									free(mainDirPath);
									close(pipeFd[1]);
									closedir(dirPtr);
									_exit(EXIT_FAILURE);
								}

								if (isPipeCreated == TRUE) {
									readEnd = -1;
								}
								writeEnd = dup(pipeFd[1]);
								close(pipeFd[1]);
								isPipeCreated = FALSE;
							}
							size = 0;
							break;
						default: /* Parent process continues */
							if (option == TRUE) {
								isPipeCreated = TRUE;
								readEnd = dup(pipeFd[0]);
							}
					}
				} else if (isDir == FALSE) {
					if ((retVal = pathfun(currDirPath)) != -1) {
						size += retVal / ONE_KB;
					} else {
						sprintf(buffer, "%ld\t-1\tSpecial_file_%s\n", (long) getpid(),
								entry->d_name);
						if (write(fifoWrite, buffer, sizeof(char) * strlen(buffer)) == -1) {
							perror("write-specials");
							_exit(EXIT_FAILURE);
						}
					}
				}
				free(currDirPath);
			}

			if (option == TRUE) {
				close(pipeFd[1]);
				if (readEnd == -1 && writeEnd != -1) {
					write(writeEnd, &size, sizeof(int));
					close(writeEnd);
				}
				
				if (readEnd != -1) {
					while ((readBytes = read(readEnd, &readValue, sizeof(int))) != 0) {
						if (readBytes != -1) {
							size += readValue;
						} else {
							perror("read - pipe");
							close(pipeFd[0]);
							close(readEnd);
							close(writeEnd);
							free(mainDirPath);
							close(fifoWrite);
							_exit(EXIT_FAILURE);
						}
					}
					if (close(readEnd) == -1) {
						perror("close - readEnd");
						close(pipeFd[0]);
						close(readEnd);
						close(writeEnd);
						free(mainDirPath);
						close(fifoWrite);
						_exit(EXIT_FAILURE);
					}
					
					if (writeEnd != -1) {
						write(writeEnd, &size, sizeof(int));
						if (close(writeEnd) == -1) {
							perror("close - writeEnd");
							close(fifoWrite);
							close(pipeFd[0]);
							free(mainDirPath);
							_exit(EXIT_FAILURE);
						}
					}
				}
			} else {
				/* Wait until all children die */
				while (wait(NULL) != -1)
					;
			}

			sprintf(buffer, "%ld\t%d\t%s\n", (long) getpid(), size, mainDirPath);
			if (write(fifoWrite, buffer, sizeof(char) * strlen(buffer)) == -1) {
				perror("fifoWrite");
				free(mainDirPath);
				close(fifoWrite);
				closedir(dirPtr);
				exit(EXIT_FAILURE);
			}
			
			if (close(fifoWrite) == -1) {
				perror("close - fifowrite");
				closedir(dirPtr);
				_exit(EXIT_FAILURE);
			}

			free(mainDirPath);
			if (closedir(dirPtr) == -1) {
				perror("closedir - child");
				_exit(EXIT_FAILURE);
			}
			_exit(EXIT_SUCCESS);
		default:
			if ((fifoRead = open(FIFO_FILE_NAME, O_RDONLY)) == -1) {
				perror("open fifo - write");
				closedir(dirPtr);
				free(mainDirPath);
				_exit(EXIT_FAILURE);
			}

			/*if (option == FALSE) wait(NULL);*/
		
			processCount = 0;
			printf("PID\tSIZE\tPATH\n");
			while ((readBytes = read(fifoRead, buffer, BUFF_LEN)) != 0) {
				if (readBytes != -1) {
					buffer[readBytes] = '\0';
					sscanf(buffer, "%ld\t%d\t", &pid, &size);
					if (size != -1) ++processCount;
					printf("%s", buffer);
				} else {
					perror("read from fifo");
					exit(EXIT_FAILURE);
				}
			}
			printf("%d child processes created. Main process is %ld.\n", 
					processCount, (long) getpid());

			free(mainDirPath);
			if (closedir(dirPtr) == -1) {
				perror("closedir - parent");
				close(fifoRead);
				exit(EXIT_FAILURE);
			}

			if (close(fifoRead) == -1) {
				perror("close fifo - read");
				exit(EXIT_FAILURE);
			}
		if (unlink(FIFO_FILE_NAME) == -1) {
			perror("unlink");
			exit(EXIT_FAILURE);
		}
	}
	return 0;
}

/*
 * Get the size of an ordinary file.
 *
 * Returns: the size in blocks of the file given by path,
 * 			-1 if path does not correspond to an ordinary file.
 */ 
int sizepathfun(char *path)
{
	struct stat attributes;

	/* If the non-existent path is passed, print an error message and exit. */
	if (lstat(path, &attributes) == -1) {
		perror("sizepathfun");
		exit(EXIT_FAILURE);
	}
	/* If the given file is not regular return -1, otherwise the size of file.*/
	return S_ISREG(attributes.st_mode) == 0 ? -1 : attributes.st_size;
}

