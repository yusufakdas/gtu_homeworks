/* Yusuf Akdas - 151044043 */
#include "bank.h"
#define SERVERFF_FLAGS O_WRONLY | O_TRUNC

void operate(int procCount);
void clean(void);
void handleSignal(int signo);

char clientFifoFileName[BUFLEN] = "\0";
int serverFifoFd = -1;
int clientFifoFd = -1;

int main(int argc, char *argv[])
{
	struct sigaction act;

	if (argc != 2 || !isdigit(argv[1][0])) {
		fprintf(stderr, "Usage: %s [musteriProcessSayisi]\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	if (atexit(clean) == -1) {
		perror("atexit");
		exit(EXIT_FAILURE);
	}
	if ((serverFifoFd = open(SERVERFF, SERVERFF_FLAGS)) == -1) {
		perror("open serverFifoFd");
		exit(EXIT_FAILURE);
	}
	memset(&act, 0, sizeof(struct sigaction));
	act.sa_handler = handleSignal;
	if (sigaction(SIGINT, &act, NULL) == -1 
			|| sigaction(SIGTERM, &act, NULL) == -1) {
		perror("sigaction");
		exit(EXIT_FAILURE);
	}
	operate(atoi(argv[1]));

	exit(EXIT_SUCCESS);
}

void handleSignal(int signo)
{
	clean();
	exit(EXIT_SUCCESS);
}

void operate(int procCount)
{
	int money = EOF, i;
	long childPid;
	ssize_t readBytes;

	for (i = 0; i < procCount; ++i) {
		switch(fork()) {
		case -1:
			perror("fork");		
			exit(EXIT_FAILURE);

		case  0:
			childPid = (long) getpid();
			sprintf(clientFifoFileName, "%s%ld", CLIENTFF_TMPLT, childPid); 
			if (mkfifo(clientFifoFileName, FIFO_MODE) == -1) {
				perror("mkfifo client");
				exit(EXIT_FAILURE);
			}
			if (write(serverFifoFd, &childPid, sizeof(long)) == -1) {
				perror("write serverfifo");
				exit(EXIT_FAILURE);
			}
			if ((clientFifoFd = open(clientFifoFileName, O_RDONLY)) == -1) {
				fprintf(stderr, "%s: %s\n", clientFifoFileName, strerror(errno));
				exit(EXIT_FAILURE);
			}
			if ((readBytes = read(clientFifoFd, &money, sizeof(int)) == -1)) {
				perror("read clientFifoFd");
				exit(EXIT_FAILURE);
			}
			if (money != EOF)
				printf("Musteri %ld %d lira aldi :)\n", (long)getpid(), money);
			else
				printf("Musteri %ld parasini alamadi :(\n", (long)getpid());

			exit(EXIT_SUCCESS);
		default: ; /* Parent continues to create other child procesesses. */
		}
	}
	/* Wait all children before the death. */
	while (wait(NULL) != -1)
		;
}

void clean(void)
{
	if (clientFifoFd != -1)
		close(clientFifoFd);
	if (serverFifoFd != -1) 
		close(serverFifoFd);
	if (strlen(clientFifoFileName) != 0)
		unlink(clientFifoFileName);
}
