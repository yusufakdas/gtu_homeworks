/* Yusuf AKDAS - 151044043 */
#ifndef __BANK_H__
#define __BANK_H__
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <string.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <signal.h>
#include <fcntl.h>

/* 
 * FIFO cannot be created if it is created in the shared folder.
 * For this reason, I chose to create them under tmp folder.
 */
#define SERVERFF "/tmp/bank_server"
#define CLIENTFF_TMPLT "/tmp/bank_client_" /* Client PID comes after the underscore*/
#define FIFO_MODE S_IRWXU | S_IRWXG | S_IROTH
#define BUFLEN 100

#endif

