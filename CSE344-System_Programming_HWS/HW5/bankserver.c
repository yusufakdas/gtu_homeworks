/* Yusuf Akdas - 151044043 */
#include "bank.h"
#define MAXPROC 4 
#define FALSE 0
#define TRUE 1
#define READ 0
#define WRITE 1
#define SERVERFF_FLAGS O_RDONLY | O_NONBLOCK
#define LOG_FILENAME "Banka.log"

struct commpipes {
	int toChild[2];
	int toParent[2];
};

struct result {
	long clientPid;
	int processId;
	int money;
	long opTime;
}; 

void signalHandler(int signo);
timer_t installTimer(long sec, long mili, int signo); 
void installSignalHandler(int signo, void (*func) (int));
void createPipes(void);
void closePipes(int except);
int withdrawRandomMoney(void);
void handleClient(int procid);
void createAndOpenServerFIFO(void);
void serveClients(long second);
void clean(void);					/* Used in atexit */
void waitFor1500(void);
int openClientFifo(long clientPid);
void employChildren(long second);
void sendEOFtoClients(int readfd, long early);
void closePipesForParent(void);

const char *months[] = {"Ocak", "Subat", "Mart", "Nisan", "Mayis", "Haziran", 
                    "Temmuz", "Agustos", "Eylul", "Ekim", "Kasim", "Aralik"};
static volatile sig_atomic_t done = FALSE;
struct timespec start;
timer_t timerid = NULL;
struct commpipes comm[MAXPROC]; 		/* Communication pipes */
int serverFifoFd = -1;
int clientFifoFd = -1;
FILE *logFilePtr = NULL;
pid_t childPidList[MAXPROC];

int main(int argc, char *argv[])
{
	long second = 0;

	if (argc != 2 || !isdigit(argv[1][0])) {
		fprintf(stderr, "Usage: %s [ServisZamani]\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	second = atol(argv[1]);
	if (second == 0) {
		fprintf(stderr, "**** Time must be greater than 0! ****\n");
		exit(EXIT_FAILURE);
	}
	if (atexit(clean) == -1) {
		perror("atexit");
		exit(EXIT_FAILURE);
	}
	memset(&comm, -1, sizeof(struct commpipes) * MAXPROC);
  createAndOpenServerFIFO();
	srand(time(NULL));
	createPipes();
	installSignalHandler(SIGINT, signalHandler);
	installSignalHandler(SIGTERM, signalHandler);
	installSignalHandler(SIGUSR1, signalHandler);
	serveClients(second);
	exit(EXIT_SUCCESS);
}

void createAndOpenServerFIFO()
{
	if (mkfifo(SERVERFF, FIFO_MODE) == -1) {
		perror("mkfifo");
		exit(EXIT_FAILURE);
	}
	if ((serverFifoFd = open(SERVERFF, SERVERFF_FLAGS)) == -1) {
		if (errno == EINTR) exit(EXIT_SUCCESS);
		perror("open serverFifoFd");
		exit(EXIT_FAILURE);
	}
}

int openClientFifo(long clientPid)
{
	int fifoFd;
	char filename[BUFLEN];

	sprintf(filename, "%s%ld", CLIENTFF_TMPLT, clientPid);
	if ((fifoFd = open(filename, O_RDWR)) == -1) {
		fprintf(stderr, "open clientFifoFd %s: %s\n", filename, strerror(errno));
		exit(EXIT_FAILURE);
	}
	return fifoFd;
}

void signalHandler(int signo)
{
	int i;

	done = TRUE;
	if (signo == SIGALRM)
		for (i = 0; i < MAXPROC; ++i) 
			kill(childPidList[i], SIGUSR1);
	else if (signo == SIGINT || signo == SIGTERM)
		exit(EXIT_SUCCESS);
}

void clean(void)
{
	closePipes(-1);
	unlink(SERVERFF);
	if (timerid != NULL)    timer_delete(timerid);		
	if (serverFifoFd != -1) close(serverFifoFd);
	if (clientFifoFd != -1) close(clientFifoFd);
	if (logFilePtr != NULL) fclose(logFilePtr);
}

void createPipes(void)
{
	int i, flags;
	
	for (i = 0; i < MAXPROC; ++i) {
		if (pipe(comm[i].toChild) == -1) {
			perror("pipe create toChild");
			exit(EXIT_FAILURE);
		}
		flags = fcntl(comm[i].toChild[READ], F_GETFL, 0);
		fcntl(comm[i].toChild[READ], F_SETFL, flags | O_NONBLOCK);

		if (pipe(comm[i].toParent) == -1) {
			perror("pipe create toParent");
			exit(EXIT_FAILURE);
		}
		flags = fcntl(comm[i].toParent[READ], F_GETFL, 0);
		fcntl(comm[i].toParent[READ], F_SETFL, flags | O_NONBLOCK);
	}
}

void closePipes(int except)
{
	int i, j;
	int *to;

	for (i = 0; i < MAXPROC; ++i) {
		for (j = 0; i != except && j < 2; ++j) {
			to = j == 0 ? comm[i].toChild : comm[i].toParent;
			if (to[READ] != -1) {
				if (close(to[READ]) == -1) {
					fprintf(stderr, "READ: %d: %s\n", i, strerror(errno));
					exit(EXIT_FAILURE);
				}
			}
			to[READ] = -1;
			if (to[WRITE] != -1) {
				if (close(to[WRITE]) == -1) {
					fprintf(stderr, "WRITE: %d: %s\n", i, strerror(errno));
					exit(EXIT_FAILURE);
				}
			}
			to[WRITE] = -1;
		}
	}
}

int withdrawRandomMoney(void)
{
	return rand() % 101;
}

void serveClients(long second)
{
	int i; 
	pid_t pid;

	if (clock_gettime(CLOCK_REALTIME, &start) == -1) {
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}
	for (i = 0; i < MAXPROC; ++i) {
		switch (pid = fork()) {
		case -1:
			perror("fork");
			exit(EXIT_FAILURE);
		case  0:
			closePipes(i);

			if (close(comm[i].toChild[WRITE]) == -1) {
				fprintf(stderr, "close %d toChild[WRITE]: %s\n", i, 
						strerror(errno));
				exit(EXIT_FAILURE);
			}
			comm[i].toChild[WRITE] = -1;
			
			if (close(comm[i].toParent[READ]) == -1) {
				fprintf(stderr, "close %d toParent[READ]: %s\n", i, 
						strerror(errno));
				exit(EXIT_FAILURE);
			}
			comm[i].toParent[READ] = -1;

			if (close(serverFifoFd) == -1) {
				fprintf(stderr, "close %d serverFifoFd: %s\n", i, 
						strerror(errno));
				exit(EXIT_FAILURE);
			}
			serverFifoFd = -1;
			handleClient(i);

			exit(EXIT_SUCCESS);
		default:  /* Parent continues to create other child processes. */
			childPidList[i] = pid;
		}
	}
	/* Ignore the SIGUSR1 signal in parent process */
	installSignalHandler(SIGUSR1, NULL);

	/* Set the SIGALRM signal for timer */
	installSignalHandler(SIGALRM, signalHandler);
	timerid = installTimer(second, 0, SIGALRM);
	employChildren(second);
}

void installSignalHandler(int signo, void (*func) (int))
{
	struct sigaction act;

	memset(&act, 0, sizeof(struct sigaction));
	act.sa_handler = func != NULL ? func : SIG_IGN;
	if (sigaction(signo, &act, NULL) == -1) {
		perror("sigaction");
		exit(EXIT_FAILURE);
	}
}

timer_t installTimer(long sec, long mili, int signo)
{
	struct sigevent event;
	struct itimerspec value;
	timer_t id;

	memset(&event, 0, sizeof(struct sigevent));
	event.sigev_notify = signo < 0 ? SIGEV_NONE : SIGEV_SIGNAL;
	event.sigev_signo = signo;
	
	value.it_interval.tv_sec = 0;
	value.it_interval.tv_nsec = 0;
	value.it_value.tv_sec = sec;
	value.it_value.tv_nsec = mili * 1E+6;

	if (timer_create(CLOCK_REALTIME, &event, &id) == -1) {
		perror("timer_create");
		exit(EXIT_FAILURE);
	}
	if (timer_settime(id, 0, &value, NULL)) {
		perror("timer_settime");
		exit(EXIT_FAILURE);
	}
	return id;
}

void waitFor1500(void)
{
	struct itimerspec val;

	timerid = installTimer(1, 500, -1);
	while (timer_gettime(timerid, &val) != -1)
		if (done || (val.it_value.tv_sec == 0 && val.it_value.tv_nsec == 0))
			break;

	timer_delete(timerid);
	timerid = NULL;
}

void closePipesForParent(void)
{
	int i;

	for (i = 0; i < MAXPROC; ++i) {
		if (close(comm[i].toParent[WRITE]) == -1) {
			perror("employChildren pipe write close");
			exit(EXIT_FAILURE);
		}
		comm[i].toParent[WRITE] = -1;
		if (close(comm[i].toChild[READ]) == -1) {
			perror("employChildren pipe read close");
			exit(EXIT_FAILURE);
		}
		comm[i].toChild[READ] = -1;
	}
}

void employChildren(long second)
{
	int procTaskCount[MAXPROC] = {0}, writeProc = 0, readProc = 0, counter = 0;
	long clientPid = 0;
	struct result res;
	ssize_t readBytes = 0;
	time_t currentTime = time(NULL);
	struct tm *local = localtime(&currentTime);

	if ((logFilePtr = fopen(LOG_FILENAME, "w")) == NULL) {
		perror("fopen employChildren");
		exit(EXIT_FAILURE);
	}
	fprintf(logFilePtr, "%d %s %d tarihinde islem basladi. Bankamiz %ld saniye " 
			"hizmet verecek.\n", local->tm_mday, months[local->tm_mon], 
			1900 + local->tm_year, second);
	fprintf(logFilePtr, "clientPid\tprocessNo\t\tPara\tislem bitis zamani\n");
	fprintf(logFilePtr, "----\t\t--------------\t------\t----------------------\n");

	closePipesForParent();
	errno = 0;
	while (!done) {
		readBytes = read(serverFifoFd, &clientPid, sizeof(long));
		if ((readBytes <= 0 && errno == EAGAIN) || (readBytes == 0 && errno == 0)) {
			;
		} else if (readBytes == -1) {
			perror("read from serverFifoFd");
			exit(EXIT_FAILURE);
		} else {
			if (write(comm[writeProc].toChild[WRITE], &clientPid, sizeof(long)) == -1) {
				perror("write toChild");
				exit(EXIT_FAILURE);
			}
			writeProc = writeProc == MAXPROC - 1 ? 0 : writeProc + 1;
		}

		errno = 0;
		readBytes = read(comm[readProc].toParent[READ], &res, sizeof(struct result));
		if ((readBytes <= 0 && errno == EAGAIN) || (readBytes == 0 && errno == 0)) {
			readProc = readProc == MAXPROC - 1 ? 0 : readProc + 1;
		} else if (readBytes == -1) {
			perror("read from toParent[READ]");
			exit(EXIT_FAILURE);
		} else {
			fprintf(logFilePtr, "%ld\t\t\tprocess%d\t\t%d\t\t%ld msec\n",
					res.clientPid, res.processId, res.money, res.opTime);
			++procTaskCount[readProc];
			++counter;
		}
		clientPid = 0;
	}
	/* Wait all child processes */
	while (wait(NULL) != -1)
		;
	fprintf(logFilePtr, "\n%ld saniye dolmustur %d musteriye hizmet verdik\n", 
			second, counter);
	fprintf(logFilePtr, "process1 %d\n", procTaskCount[0]);
	fprintf(logFilePtr, "process2 %d\n", procTaskCount[1]);
	fprintf(logFilePtr, "process3 %d\n", procTaskCount[2]);
	fprintf(logFilePtr, "process4 %d musteriye hizmet sundu ....\n", procTaskCount[3]);
	fclose(logFilePtr);
	logFilePtr = NULL;
	sendEOFtoClients(serverFifoFd, clientPid);
}

void handleClient(int procid)
{
	int readfd = comm[procid].toChild[READ];
	int writefd = comm[procid].toParent[WRITE];
	struct result operationResult;
	struct timespec end;
	ssize_t readBytes = 0;
	long clientPid = 0;
	int money = 0, early = FALSE;
	double nanoToMili = 0;

	while (!done) {
		readBytes = read(readfd, &clientPid, sizeof(long));
		if (readBytes < 0 && errno == EAGAIN) continue;

		waitFor1500(); 
		if (done) {
			early = TRUE;
			break;
		}
		money = withdrawRandomMoney();
		clientFifoFd = openClientFifo(clientPid);
		if (write(clientFifoFd, &money, sizeof(int)) == -1) {
			perror("handleClient client fifo write");
			exit(EXIT_FAILURE);
		}
		if (close(clientFifoFd) == -1) {
			perror("handleClient close client fifo");
			exit(EXIT_FAILURE);
		}
		clientFifoFd = -1;
		operationResult.clientPid = clientPid;
		operationResult.processId = procid + 1;
		operationResult.money = money;
		if (clock_gettime(CLOCK_REALTIME, &end) == -1) {
			perror("clock_gettime end");
			exit(EXIT_FAILURE);
		}
		nanoToMili = (end.tv_nsec - start.tv_nsec) * 1E-6; 
		operationResult.opTime = (end.tv_sec - start.tv_sec) * 1000 + (long) nanoToMili;
		if (write(writefd, &operationResult, sizeof(struct result)) == -1) {
			perror("handleClient parent pipe write");
			exit(EXIT_FAILURE);
		}
	}
	sendEOFtoClients(readfd, early ? clientPid : 0);
}

void sendEOFtoClients(int readfd, long early)
{
	ssize_t readBytes = 0;
	int eof = EOF;
	long clientPid = 0;

	errno = 0;
	for (;;) {
		if (early == 0) {
			readBytes = read(readfd, &clientPid, sizeof(long));
			if (readBytes == 0 || (readBytes == -1 && errno == EAGAIN)) break;
		} else {
			clientPid = early;
			early = 0;
		}
		clientFifoFd = openClientFifo(clientPid);
		if (write(clientFifoFd, &eof, sizeof(int)) == -1) {
			perror("sendEOFtoClients client fifo write");
			exit(EXIT_FAILURE);
		}
		if (close(clientFifoFd) == -1) {
			perror("sendEOFtoClients close client fifo");
			exit(EXIT_FAILURE);
		}
		clientFifoFd = -1;
	}
}
