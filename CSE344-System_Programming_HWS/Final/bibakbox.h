#ifndef __BIBAKBOX_H__
#define __BIBAKBOX_H__
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <signal.h>
#include <stdint.h>
#include <dirent.h>
#include <fcntl.h>
#include <linux/limits.h>
#include <errno.h>
#include <sys/file.h>
#include <time.h>
#define FILE_PERM S_IRWXU | S_IRWXG | S_IROTH
#define WRITE_MODE O_WRONLY | O_TRUNC | O_CREAT
#define TRANSFER_BYTES 8192
#define FROM_SOCK_TO_FILE 0
#define FROM_FILE_TO_SOCK 1

#define FALSE 0
#define TRUE 1
#define MAXBUF 100
#define SERVER 0
#define CLIENT 1
#define DISCONNECTED 0
#define CONNECTED 1
#define DEBUG

#define INIT 0
#define ADD 1
#define MODIFY 2
#define DELETE 3
#define UNKNOWN 4
#define FROM_SRV 7
#define FETCH 8
#define SYNC 9
#define OK 10
#define UPTODATE 11
#define REALTIMESYNC 12
#define NORMAL 13
#define END 99
#define SHUTDOWN 100

struct operation {
	int opcode;
	int type;
	off_t bytes;
	char name[PATH_MAX];
};

struct entry {
	char *filename;
	int lastStatus;
	int type;
	struct timespec lastModify;
};

struct client {
	char *dirName;
	struct entry *files;
	size_t current;
	size_t max;
	int connection;
};

typedef enum { REGULAR=0, DIRECTORY, DOT, OTHER, ERR } filetype_t;

extern char *serializeOperation(const struct operation *op, char *data);
extern void deserializeOperation(const char *data, struct operation *op);

extern filetype_t getFileType(const char *path);
extern off_t getBytes(const char *file);

extern ssize_t readLine(int sfd, char *buffer, size_t size);
extern int readFromFile(int sfd, const char *path, off_t size);
extern int writeToFile(int sfd, const char *path, off_t size);
extern int sendRequest(int sfd, int opcode);
extern int getResponse(int sfd, int *opcode);

extern void createClient(struct client *cl, const char *dirName);
extern void destroyClient(struct client *cl);

extern int synchronize(int type, int fd, void *var1, void *var2,
						int (process(int, int, void*, void*, struct operation*)));
/* CALLBACK FUNCTIONS */
extern int fetchFile(int type, int clFd, void *var1, void *var2, struct operation *op);
extern int addFile(struct client *cl, struct operation *op);
extern int compareAndUpdate(int type, int fd, void *var1, void *var2,
							struct operation *op);

#endif

