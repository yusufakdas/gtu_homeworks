/* Yusuf AKDAS - 151044043 */
#include "bibakbox.h"
#define BACKLOG 10
#define DIR_MODE S_IRWXU | S_IRWXG | S_IROTH

static int initServerSocket(unsigned short portNumber);
static void clean(void);
static int acceptor(void);
static void *handleClient(void *arg);
static void initClients(void);
static void checkFiles(const struct operation *op, int clFd);
static int searchClient(const char *dirName);
static int createDirectory(char *name);
static int searchDirectory(const char *dirName);
static int getFilesFromServerDir(struct client *cl);
static int sendAllFilesInformation(int clFd, struct client *cl);
static int syncInInterval(int clFd, struct client *cl);
static void makeNormalAllStatus(struct client *cl);
static void identifyDeletedFiles(struct client *cl);
static int deleteTheFile(struct client *cl, char *path, int index);
static void deleteOperation(struct client *cl, int index);
static int createThreadPool(void);
static void destroyThreadPool(void);
static void deleteRecursively(struct client *cl, char *path);
static int installSignalHandler(void);
static void *signalHandlerThread(void *arg);
static void blockHandler(int signo);

static int svSockFD = -1;					/* Server socket file descriptor */
static int sigDone = FALSE;
static struct client *clients = NULL;
static int threadPoolSize = 0;
static const char *SERVER_DIR_NAME = NULL;
static pthread_t signalThreadID;
static pthread_t mainThread;

static int clientSockFd = -1;
static pthread_mutex_t clientMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t clientCreateMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t clientCond = PTHREAD_COND_INITIALIZER;

static pthread_t *threadIDs = NULL;
static int connectedCount = 0;

int main(int argc, char *argv[])
{
	int portNumber;

	if (argc != 4) {
		fprintf(stderr, "Usage: %s [directory] [threadPoolSize] [portnumber]\n",
				argv[0]);
		exit(EXIT_FAILURE);
	}

	SERVER_DIR_NAME = argv[1];
	if (createDirectory(NULL) == -1) {
		fprintf(stderr, "Server directory could not be created.\n");
		exit(EXIT_FAILURE);
	}

	threadPoolSize = atoi(argv[2]);
	if (threadPoolSize <= 0) {
		fprintf(stderr, "Thread pool size cannot be less than 0: %d.\n",
				threadPoolSize);
		exit(EXIT_FAILURE);
	}

	portNumber = atoi(argv[3]);
	if (portNumber <= 0 || portNumber > 65535) {
		fprintf(stderr, "Invalid port number: %d.\n", portNumber);
		exit(EXIT_FAILURE);
	}

	if (atexit(clean) == -1) {
		perror("atexit");
		exit(EXIT_FAILURE);
	}

	signalThreadID = pthread_self();
	if (installSignalHandler() == -1) {
		fprintf(stderr, "signal handler could not be installed properly!\n");
		exit(EXIT_FAILURE);
	}

	if (initServerSocket((unsigned short) portNumber) == -1) {
		fprintf(stderr, "Server socket could not be initialized.\n");
		exit(EXIT_FAILURE);
	}
	if (createThreadPool() == -1)
		fprintf(stderr, "Some threads may not be created properly\n");

	mainThread = pthread_self();
	initClients();
	if (acceptor() == -1) {
		fprintf(stderr, "Some errors occured while accepting.\n");
		exit(EXIT_FAILURE);
	}

	exit(EXIT_SUCCESS);
}

static int initServerSocket(unsigned short portNumber)
{
	int opt = 1;
	struct sockaddr_in addr;

	if ((svSockFD = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		return -1;
	}
	if (setsockopt(svSockFD, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int)) == -1) {
		perror("setsockopt");
		return -1;
	}
	memset(&addr, 0, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(portNumber);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(svSockFD, (struct sockaddr *) &addr, sizeof(struct sockaddr_in)) == -1) {
		perror("bind");
		return -1;
	}
	if (listen(svSockFD, BACKLOG) == -1) {
		perror("listen");
		return -1;
	}
	return 0;
}

static void clean(void)
{
	int i;
	int retVal;

	if (svSockFD != -1)
		close(svSockFD);
	if (clientSockFd != -1)
		close(clientSockFd);
	if (clients != NULL) {
		for (i = 0; i < threadPoolSize; ++i)
			destroyClient(&clients[i]);
		free(clients);
	}
	if (threadIDs != NULL)
		destroyThreadPool();
	pthread_kill(signalThreadID, SIGINT);
	if ((retVal = pthread_equal(signalThreadID, pthread_self())) == 0)
		retVal = pthread_join(signalThreadID, NULL);
}

static int acceptor(void)
{
	int clFd = 0, err = 0, i;
	struct sockaddr_in clAddr;
	socklen_t len = sizeof(struct sockaddr_in);

	memset(&clAddr, 0, len);
	while (!sigDone) {
		clFd = accept(svSockFD, (struct sockaddr *) &clAddr, &len);
		if ((err = pthread_mutex_lock(&clientMutex)) != 0)
			fprintf(stderr, "Lock Mutex acceptor: %s\n", strerror(err));
		clientSockFd = clFd;
		if ((err = pthread_mutex_unlock(&clientMutex)) != 0)
			fprintf(stderr, "Unlock Mutex acceptor: %s\n", strerror(err));
		if (!sigDone) pthread_cond_signal(&clientCond);
	}
	for (i = 0; i < threadPoolSize; ++i)
		pthread_cancel(threadIDs[i]);
	return 0;
}

static void *handleClient(void *arg)
{
	int clFd = -1, err = 0;
	struct operation request;
	ssize_t readBytes = 0;
	char buffer[PATH_MAX] = {0};

	if ((err = pthread_mutex_lock(&clientMutex)) != 0) {
		fprintf(stderr, "Lock Mutex handleClient: %s\n", strerror(err));
		return NULL;
	}
	while (clientSockFd == -1) {
		if ((err = pthread_cond_wait(&clientCond, &clientMutex) != 0)) {
			fprintf(stderr, "Lock Mutex handleClient: %s\n", strerror(err));
			break;
		}
	}
	if (err == 0) {
		clFd = clientSockFd;
		clientSockFd = -1;
	}
	if ((err = pthread_mutex_unlock(&clientMutex) != 0)) {
		fprintf(stderr, "Unlock Mutex handleClient: %s\n", strerror(err));
		return NULL;
	}
	if (err != 0)
		return NULL;

	readBytes = readLine(clFd, buffer, PATH_MAX);
	if (readBytes == -1) {
		perror("read");
		return NULL;
	}
	memset(&request, 0, sizeof(struct operation));
	deserializeOperation(buffer, &request);
	checkFiles(&request, clFd);

	if (close(clFd) == -1)
		perror("close");
	return NULL;
}

static void initClients(void)
{
	clients	= (struct client *) calloc(threadPoolSize, sizeof(struct client));
	if (clients == NULL) {
		perror("initClients calloc");
		exit(EXIT_FAILURE);
	}
}

static int getFilesFromServerDir(struct client *cl)
{
	DIR *dirPtr = NULL;
	struct dirent *entry = NULL;
	char *path = NULL;
	char *subPath = NULL;
	size_t size = 0, subSize = 0;
	struct operation op;

	size = strlen(SERVER_DIR_NAME) + strlen(cl->dirName) + 2;
	path = (char *) calloc(size, sizeof(char));
	if (path == NULL) {
		perror("getFilesFromServerDir calloc");
		return -1;
	}

	sprintf(path, "%s/%s", SERVER_DIR_NAME, cl->dirName);
	if ((dirPtr = opendir(path)) == NULL) {
		perror("getFilesFromServerDir opendir");
		free(path);
		return -1;
	}

	memset(&op, 0, sizeof(struct operation));
	while ((entry = readdir(dirPtr)) != NULL) {
		subSize = size + strlen(entry->d_name) + 2;
		subPath = (char *) calloc(subSize, sizeof(char));
		sprintf(subPath, "%s/%s", path, entry->d_name);
		if (getFileType(subPath) == REGULAR) {
			strcpy(op.name, entry->d_name);
			op.opcode = FROM_SRV;
			addFile(cl, &op);
		}
		free(subPath);
	}
	closedir(dirPtr);
	free(path);

	return 0;
}

static void deleteOperation(struct client *cl, int index)
{
	int i;
	size_t size = strlen(SERVER_DIR_NAME) + strlen(cl->dirName)
				  + strlen(cl->files[index].filename) + 3;
	char *path = (char *) calloc(size, sizeof(char));

	sprintf(path, "%s/%s/%s", SERVER_DIR_NAME, cl->dirName,
			cl->files[index].filename);
	cl->files[index].lastStatus = DELETE;

	if (cl->files[index].type == REGULAR) {
		printf("%s has been deleted from '%s' directory.\n",
				cl->files[index].filename, cl->dirName);
		deleteTheFile(cl, path, index);
	}
	else ;
		/*deleteRecursively(cl, &path[strlen(SERVER_DIR_NAME)
						  + strlen(cl->dirName) + 2]);*/
	free(path);
}

/* TODO */
static void deleteRecursively(struct client *cl, char *path)
{
	int i;

	for (i = 0; i < cl->current; ++i) {
		if (strncmp(path, cl->files[i].filename, strlen(path)) == 0) {
			if (cl->files[i].type == REGULAR) {
				printf("  %s   %s\n", cl->files[i].filename, path);
			}
		}
	}
}

static void identifyDeletedFiles(struct client *cl)
{
	int i;

	for (i = 0; i < cl->current; ++i) {
		if (cl->files[i].lastStatus == NORMAL)
			deleteOperation(cl, i);
	}
}

static int deleteTheFile(struct client *cl, char *path, int index)
{
	int i;

	free(cl->files[index].filename);
	cl->files[index].filename = NULL;
	for (i = index; i < cl->current; ++i) {
		cl->files[i] = cl->files[i+1];
	}
	--cl->current;

	if (unlink(path) == -1) {
		perror("deleteTheFile unlink");
		free(path);
		return -1;
	}
	return 0;
}

static int syncInInterval(int clFd, struct client *cl)
{
	int response = 0;
	int retVal = 0;

	makeNormalAllStatus(cl);
	getResponse(clFd, &response);
	retVal = synchronize(SERVER, clFd, (void *)cl, (void *)SERVER_DIR_NAME,
						compareAndUpdate);
	identifyDeletedFiles(cl);

	return retVal;
}

static void checkFiles(const struct operation *op, int clFd)
{
	struct client *cl = NULL;
	int index = -1;

	if (searchDirectory(op->name) == TRUE) {
		index = searchClient(op->name);
		if (index == -1) {
			pthread_mutex_lock(&clientCreateMutex);
			cl = &clients[connectedCount];
			++connectedCount;
			pthread_mutex_unlock(&clientCreateMutex);

			createClient(cl, op->name);
			getFilesFromServerDir(cl);
		} else {
			pthread_mutex_lock(&clientCreateMutex);
			cl = &clients[index];
			pthread_mutex_unlock(&clientCreateMutex);
		}
		sendRequest(clFd, SYNC);
		synchronize(SERVER, clFd, (void *)cl, (void *)SERVER_DIR_NAME, compareAndUpdate);
		sendAllFilesInformation(clFd, cl);
	} else {
		pthread_mutex_lock(&clientCreateMutex);
		cl = &clients[connectedCount];
		++connectedCount;
		pthread_mutex_unlock(&clientCreateMutex);

		createClient(cl, op->name);
		createDirectory(cl->dirName);
		sendRequest(clFd, FETCH);
		synchronize(SERVER, clFd, (void *)cl, (void *)SERVER_DIR_NAME, fetchFile);
	}
	while (!sigDone) {
		if (syncInInterval(clFd, cl) < 0)
			break;
	}
}

static int sendAllFilesInformation(int clFd, struct client *cl)
{
	int i;
	struct operation op;
	char *serializedData = NULL;
	char *path = NULL;
	int response;
	size_t size = 0;

	memset(&op, 0, sizeof(struct operation));
	for (i = 0; i < cl->current; ++i) {
		op.opcode = SYNC;
		snprintf(op.name, PATH_MAX-1, "%s", cl->files[i].filename);

		size = strlen(SERVER_DIR_NAME) + strlen(cl->dirName) +
			   strlen(cl->files[i].filename) + 3;
		path = (char *) calloc(size, sizeof(char));
		sprintf(path, "%s/%s/%s", SERVER_DIR_NAME, cl->dirName,
				cl->files[i].filename);
		op.bytes = getBytes(path);
		serializedData = serializeOperation(&op, serializedData);
		if (write(clFd, serializedData, strlen(serializedData)*sizeof(char)) == -1) {
			perror("sendAllFilesInformation write");
			return -1;
		}
		getResponse(clFd, &response);
		if (response == ADD) {
			readFromFile(clFd, path, op.bytes);
		}
		free(path);
	}
	sendRequest(clFd, END);

	free(serializedData);
	return 0;
}

static int searchClient(const char *dirName)
{
	int i;

	for (i = 0; i < threadPoolSize; ++i) {
		if (clients[i].dirName == NULL)
			continue;
		if (strcmp(clients[i].dirName, dirName) == 0)
			return i;
	}
	return -1;
}

static int createDirectory(char *name)
{
	const char *path = NULL;
	char buffer[PATH_MAX] = {0};

	if (name != NULL) {
		sprintf(buffer, "%s/%s", SERVER_DIR_NAME, name);
		path = buffer;
	} else {
		path = SERVER_DIR_NAME;
	}
	if (mkdir(path, DIR_MODE) == -1 && errno != EEXIST) {
		perror("createDirectory mkdir");
		return -1;
	}
	return 0;
}

static int searchDirectory(const char *dirName)
{
	DIR *dirPtr = NULL;
	struct dirent *entry = NULL;
	int found = FALSE;

	if ((dirPtr = opendir(SERVER_DIR_NAME)) == NULL) {
		perror("searchDirectory opendir");
		return -1;
	}
	while (!found && (entry = readdir(dirPtr)) != NULL)
		if (strcmp(dirName, entry->d_name) == 0)
			found = TRUE;

	closedir(dirPtr);
	return found;
}

static void makeNormalAllStatus(struct client *cl)
{
	int i;

	for (i = 0; i < cl->current; ++i) {
		if (cl->files[i].lastStatus != DELETE)
			cl->files[i].lastStatus = NORMAL;
	}
}

static int createThreadPool(void)
{
	int i;
	int errCode = 0;

	threadIDs = (pthread_t *) calloc(threadPoolSize, sizeof(pthread_t));
	for (i = 0; i < threadPoolSize; ++i) {
		if (pthread_create(&threadIDs[i], NULL, handleClient, NULL) != 0) {
			fprintf(stderr, "Thread %d cannot be created\n", i);
			threadIDs[i] = pthread_self();
			errCode = -1;
		}
	}
	return errCode;
}

static void destroyThreadPool(void)
{
	int i;

	for (i = 0; i < threadPoolSize; ++i)
		if (threadIDs[i] != pthread_self())
			pthread_join(threadIDs[i], NULL);
	free(threadIDs);
}


static int installSignalHandler(void)
{
	int retVal = 0;
	sigset_t signalMask;
	struct sigaction sa;

	memset(&sa, 0, sizeof(struct sigaction));
	sa.sa_handler = blockHandler;
	sigaction(SIGUSR1, &sa, NULL);

	if (sigemptyset(&signalMask) == -1) {
		perror("sigemptyset");
		return -1;
	}
	if (sigaddset(&signalMask, SIGINT) == -1) {
		perror("sigaddset");
		return -1;
	}
	if (sigaddset(&signalMask, SIGTERM) == -1) {
		perror("sigaddset");
		return -1;
	}
	if ((retVal = pthread_sigmask(SIG_BLOCK, &signalMask, NULL)) != 0) {
		fprintf(stderr, "pthread_sigmask: %s\n", strerror(retVal));
		return -1;
	}
	if ((retVal = pthread_create(&signalThreadID, NULL, signalHandlerThread,
					NULL) != 0)) {
		fprintf(stderr, "pthread_create: %s\n", strerror(retVal));
		return -1;
	}
	return 0;
}

static void *signalHandlerThread(void *arg)
{
	int signo;
	sigset_t signalMask;

	if (sigemptyset(&signalMask) == -1) {
		perror("sigemptyset");
		return NULL;
	}
	if (sigaddset(&signalMask, SIGINT) == -1) {
		perror("sigaddset");
		return NULL;
	}
	sigwait(&signalMask, &signo);
	pthread_kill(mainThread, SIGUSR1);
	sigDone = TRUE;
	return NULL;
}

static void blockHandler(int signo)
{ }
