#include "bibakbox.h"

char *serializeOperation(const struct operation *op, char *data)
{
	char buffer[MAXBUF] = {0};
	size_t size = 0;
	char *tmp = NULL;

	snprintf(buffer, MAXBUF, "%d %d %ld", op->opcode, op->type, op->bytes);
	size = strlen(buffer) + 2 + strlen(op->name) + 2; /* 1 for CR, 1 for null */
	tmp = (char *) realloc(data, size * sizeof(char));
	if (tmp == NULL) {
		perror("realloc");
		exit(EXIT_FAILURE);
	}
	data = tmp;
	snprintf(data, size, "%s %s\n", buffer, op->name);

	return data;
}

void deserializeOperation(const char *data, struct operation *op)
{
	size_t fetchedBytes = 0;
	char buffer[MAXBUF] = {0};

	sscanf(data, "%d%d%ld", &op->opcode, &op->type, &op->bytes);
	sprintf(buffer, "%d %d %ld ", op->opcode, op->type, op->bytes);
	fetchedBytes = strlen(buffer);
	if (fetchedBytes > 0) {
		sprintf(op->name, "%s", &data[fetchedBytes]);
		op->name[strlen(op->name)-1] = '\0';
	} else {
		op->name[0] = '\0';
	}
}

filetype_t getFileType(const char *path)
{
	struct stat buf;
	filetype_t type;
	int i, isDot = TRUE;

	/* File information could not be acquired. */
	if (lstat(path, &buf) == -1)
		return ERR;

	type = S_ISREG(buf.st_mode) ? REGULAR : S_ISDIR(buf.st_mode)
								? DIRECTORY : OTHER;
	if (type == DIRECTORY) {
		for (i = strlen(path) - 1; path[i] != '/'; --i)
			if (path[i] != '.')
				isDot = FALSE;
		return isDot ? DOT : type;
	}
	return type;
}

off_t getBytes(const char *file)
{
	struct stat buf;
	if (lstat(file, &buf) == -1) perror("lstat");
	return buf.st_size;
}

ssize_t readLine(int sfd, char *buffer, size_t size)
{
	ssize_t readBytes = 0, totalRead = 0;
	char ch;

	for (;;) {
		readBytes = read(sfd, &ch, 1);
		if (readBytes == -1) {
			if (readBytes == EINTR) continue;
			return -1;
		} else if (readBytes == 0) {
			break;
		} else {
			if (totalRead < size - 1) {
				++totalRead;
				*buffer++ = ch;
			}
			if (ch == '\n') break;
		}
	}
	*buffer = '\0';
	return totalRead;
}

int sendRequest(int sfd, int opcode)
{
	struct operation op;
	char *val = NULL;
	ssize_t writeBytes = 0;

	memset(&op, 0, sizeof(struct operation));
	op.opcode = opcode;
	strcpy(op.name, " ");

	val = serializeOperation(&op, val);
	if ((writeBytes = write(sfd, val, strlen(val) * sizeof(char))) == -1) {
		if (writeBytes == EINTR) {
			perror("EINTR");
			exit(1);
		}
		return -1;
	}
	free(val);
	return 0;
}

int getResponse(int sfd, int *opcode)
{
	struct operation op;
	char buffer[PATH_MAX] = {0};
	ssize_t readBytes = 0;

	memset(&op, 0, sizeof(struct operation));
	if ((readBytes = read(sfd, buffer, PATH_MAX - 1)) == -1) {
		if (readBytes == EINTR) {
			perror("EINTR");
			exit(1);
		}
		return -1;
	}
	deserializeOperation(buffer, &op);
	if (opcode != NULL) *opcode = op.opcode;

	return 0;
}

int readFromFile(int sfd, const char *path, off_t size)
{
	int fd;
	char *bytes = NULL;
	char *p = NULL;
	int err = 0;
	ssize_t readBytes = 0;
	ssize_t sendBytes = 0;
	ssize_t untilOff_t = 0;
	ssize_t loc = (ssize_t) size;
	ssize_t transfer = 0;
	int response = 0;

	if ((fd = open(path, O_RDONLY)) == -1) {
		perror("readFromFile open");
		getResponse(sfd, NULL);
		close(fd);
		free(bytes);
		return -1;
	}
	bytes = (char *) calloc(TRANSFER_BYTES, sizeof(char));
	for (;;) {
		transfer = size > TRANSFER_BYTES ? TRANSFER_BYTES : size;
		readBytes = read(fd, bytes, transfer);
		size -= readBytes;

		if (readBytes == 0)
			break;
		if (readBytes == -1) {
			perror("readFromFile read");
			err = -1;
			break;
		}
		p = bytes;
		while (readBytes > 0) {
			sendBytes = send(sfd, bytes, readBytes, 0);
			p += sendBytes;
			readBytes -= sendBytes;
		}
		if (size <= 0) break;
	}
	free(bytes);
	getResponse(sfd, &response);

	if (close(fd) == -1) {
		perror("readFromFile close");
		err = -1;
	}
	return err;
}

int writeToFile(int sfd, const char *path, off_t size)
{
	int fd;
	char *bytes = NULL;
	int err = 0;
	ssize_t recvBytes = 0;
	ssize_t writeBytes = 0;
	ssize_t tot = 0;

	if ((fd = open(path, WRITE_MODE, FILE_PERM)) == -1) {
		perror("writeToFile open");
		sendRequest(sfd, OK);
		close(fd);
		free(bytes);
		return -1;
	}

	bytes = (char *) calloc(TRANSFER_BYTES, sizeof(char));
	for (;;) {
		recvBytes = recv(sfd, bytes, TRANSFER_BYTES, 0);
		if (recvBytes == -1) {
			perror("writeToFile recv");
			err = -1;
			break;
		}
		writeBytes = write(fd, bytes, recvBytes);
		if (writeBytes < 0) {
			perror("writeBytes write");
			err = -1;
			break;
		}
		tot += recvBytes;
		if (tot == size) break;
	}
	free(bytes);
	sendRequest(sfd, OK);

	if (close(fd) == -1) {
		perror("writeToFile close");
		err = -1;
	}
	return err;
}

int synchronize(int type, int fd, void *var1, void *var2,
				 int (process(int, int, void*, void*, struct operation*)))
{
	char buffer[PATH_MAX] = {0};
	struct operation req;
	ssize_t readBytes = 0;
	int a = 0;

	memset(&req, 0, sizeof(struct operation));
	while (req.opcode != END) {
		readBytes = readLine(fd, buffer, PATH_MAX);
		if (readBytes < 0) {
			perror("read");
			exit(EXIT_FAILURE);
		} else if (readBytes == 0) {
			fprintf(stderr, "Connection is terminated\n");
			return -2;
		}
		buffer[readBytes] = '\0';
		deserializeOperation(buffer, &req);

		if (req.opcode != END) {
			if (process == NULL) {
				printf("synchronize: %s", buffer);
			} else if (process(type, fd, var1, var2, &req) == -1) {
				fprintf(stderr, "processFile: an error occured\n");
				exit(EXIT_FAILURE);
			}
		}
	}
	return 0;
}


int addFile(struct client *cl, struct operation *op)
{
	struct entry *var = NULL;

	if (cl->files == NULL)
		cl->files = (struct entry *) calloc(cl->max, sizeof(struct entry));
	if (cl->current == cl->max) {
		var = (struct entry *) realloc(cl->files,
					sizeof(struct entry) * cl->max * 2);
		if (var == NULL) {
			perror("processFile realloc");
			free(cl->files);
			exit(EXIT_FAILURE);
		}
		memset(&var[cl->max], 0, sizeof(struct entry) * cl->max);
		cl->max *= 2;
		cl->files = var;
	}
	cl->files[cl->current].lastStatus = op->opcode;
	cl->files[cl->current].type = op->type;
	cl->files[cl->current].filename = (char *) calloc(strlen(op->name) + 1, sizeof(char));
	snprintf(cl->files[cl->current].filename, strlen(op->name) + 1, "%s", op->name);
	cl->current++;

	return 0;
}

void destroyClient(struct client *cl)
{
	int i;

	if (cl->files != NULL) {
		for (i = 0; i < cl->max; ++i)
			if (cl->files[i].filename != NULL)
				free(cl->files[i].filename);
		free(cl->files);
	}
	if (cl->dirName != NULL)
		free(cl->dirName);
}

void createClient(struct client *cl, const char *dirName)
{
	size_t size = 0;

	if (dirName != NULL) {
		size = strlen(dirName) + 1;		/* 1 for null byte */
		cl->dirName = (char *) calloc(size, sizeof(char));
		snprintf(cl->dirName, size, "%s", dirName);
	}
	cl->connection = CONNECTED;
	cl->max = 20;
	cl->current = 0;
	cl->files = NULL;
}

int compareAndUpdate(int type, int fd, void *var1, void *var2,
					 struct operation *op)
{
	int i;
	struct client *cl = (struct client *) var1;

	for (i = 0; i < cl->current; ++i) {
		if (strcmp(op->name, cl->files[i].filename) == 0) {
			cl->files[i].lastStatus = UPTODATE;
			sendRequest(fd, OK);
			return 0;
		}
	}
	sendRequest(fd, ADD);
	fetchFile(type, fd, var1, var2, op);

	return 0;
}

int fetchFile(int type, int fd, void *var1, void *var2, struct operation *op)
{
	char *path = NULL;
	const char *dir = NULL;
	struct client *cl = NULL;
	size_t size = 0;

	if (type == SERVER) {
		cl = (struct client *) var1;
		dir = (const char *) var2;

		size = strlen(dir) + strlen(cl->dirName) + strlen(op->name) + 3;
		path = (char *) calloc(size, sizeof(char));
		sprintf(path, "%s/%s/%s", dir, cl->dirName, op->name);
		if (op->type == 1) {
			if (mkdir(path, FILE_PERM) == -1 && errno != EEXIST) {
				perror("mkdir: fetchFile");
				exit(EXIT_FAILURE);
			}
		}
		addFile(cl, op);
	} else {
		dir = (const char *) var2;
		size = strlen(dir) + strlen(op->name) + 2;
		path = (char *) calloc(size, sizeof(char));
		sprintf(path, "%s/%s", dir, op->name);
	}
	if (op->type == 0) {
		printf("%s has been added to '%s' directory.\n", op->name,
				cl->dirName);
		writeToFile(fd, path, op->bytes);
	}
	free(path);
	return 0;
}

