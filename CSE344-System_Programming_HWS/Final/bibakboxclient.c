/* Yusuf AKDAS - 151044043 */
#include "bibakbox.h"

static int initClientSocket(const char *IPAddress, unsigned short portNumber);
static int operate(char *path);
static const char *getOnlyDirName(char *dirName);
static void clean(void);
static int syncInInterval(long ms, char *path);
static int readTheDirectoryHelper(const char *dirName, const char *path,
								const char *subDir, int opcode);
static int readTheDirectory(const char *dirName, const char *path, int opcode);
static int clSockFD = -1;					/* Client socket file descriptor */
static volatile sig_atomic_t done = FALSE;	/* Flag for signal handlers      */
static const char *CLIENT_DIR_NAME;
static struct client cl;

int main(int argc, char *argv[])
{
	int portNumber;

	if (argc != 4) {
		fprintf(stderr, "Usage: %s [dirName] [ip address] [portnumber]\n",
				argv[0]);
		exit(EXIT_FAILURE);
	}

	if (atexit(clean) == -1) {
		perror("atexit");
		exit(EXIT_FAILURE);
	}

	CLIENT_DIR_NAME = argv[1];
	portNumber = atoi(argv[3]);
	if (portNumber <= 0 || portNumber > 65535) {
		fprintf(stderr, "Invalid port number: %d.\n", portNumber);
		exit(EXIT_FAILURE);
	}

	createClient(&cl, NULL);
	if (initClientSocket(argv[2], (unsigned short) portNumber) == -1) {
		fprintf(stderr, "Client socket could not be initialized.\n");
		exit(EXIT_FAILURE);
	}

	if (operate(argv[1]) == -1) {
		exit(EXIT_FAILURE);
	}

	exit(EXIT_SUCCESS);
}

static int initClientSocket(const char *IPAddress, unsigned short portNumber)
{
	struct sockaddr_in addr;
	int retVal = 0;

	if ((clSockFD = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		return -1;
	}

	memset(&addr, 0, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(portNumber);

	retVal = inet_pton(AF_INET, IPAddress, &addr.sin_addr.s_addr);
	if (retVal != 1) {
		if (retVal == -1)
			perror("inet_pton");
		else
			fprintf(stderr, "Invalid IP address representation.\n");
		return -1;
	}

	if (connect(clSockFD, (struct sockaddr *) &addr, sizeof(struct sockaddr_in)) == -1) {
		perror("connect");
		return -1;
	}

	return 0;
}

static void clean(void)
{
	if (clSockFD != -1)
		close(clSockFD);
	destroyClient(&cl);
}

/*
 * TODO: error handling
 */
static int operate(char *path)
{
	struct operation op;
	char *serializedData = NULL;
	int responseOp = 0;
	int err = 0;
	const char *dirName = getOnlyDirName(path);

	memset(&op, 0, sizeof(struct operation));
	op.opcode = INIT;
	snprintf(op.name, PATH_MAX-1, "%s", dirName);

	serializedData = serializeOperation(&op, serializedData);
	if (write(clSockFD, serializedData, strlen(serializedData)) == -1) {
		perror("operate write");
		return -1;
	}
	free(serializedData);

	getResponse(clSockFD, &responseOp);
	err = readTheDirectory(dirName, path, responseOp);
	sendRequest(clSockFD, END);
	if (responseOp == SYNC) {
		synchronize(CLIENT, clSockFD, (void *) &cl, (void *) CLIENT_DIR_NAME,
					compareAndUpdate);
	}
	while (1) {
		sleep(2);
		syncInInterval(750, path);
	}
	return 0;
}


static int syncInInterval(long ms, char *path)
{
	const char *dirName = getOnlyDirName(path);

	sendRequest(clSockFD, 1589);
	readTheDirectory(dirName, path, SYNC);
	sendRequest(clSockFD, END);

	return 0;
}

static int readTheDirectoryHelper(const char *dirName, const char *path,
								  const char *subDir, int opcode)
{
	DIR *dirPtr = NULL;
	char *serializedData = NULL;
	struct dirent *entry = NULL;
	struct operation op;
	char buffer[PATH_MAX] = {0};
	int response = 0, err = 0;
	filetype_t t;

	if ((dirPtr = opendir(path)) == NULL) {
		perror("opendir");
		return -1;
	}
	memset(&op, 0, sizeof(struct operation));
	while ((entry = readdir(dirPtr)) != NULL) {
		snprintf(buffer, PATH_MAX - 1, "%s/%s", path, entry->d_name);
		t = getFileType(buffer);

		if (t == REGULAR || t == DIRECTORY) {
			op.opcode = INIT;
			op.type = t == REGULAR ? 0 : 1;	/* Regular: 0, Directory: 1 */
			op.bytes = getBytes(buffer);
			if (strcmp(subDir, "") == 0)
				snprintf(op.name, PATH_MAX-1, "%s", entry->d_name);
			else
				snprintf(op.name, PATH_MAX-1, "%s/%s", subDir, entry->d_name);
			serializedData = serializeOperation(&op, serializedData);

			if (write(clSockFD, serializedData, strlen(serializedData)) == -1) {
				perror("operate write");
				err = -1;
				break;
			}
			if (t == REGULAR) {
				if (opcode == FETCH) {
					readFromFile(clSockFD, buffer, op.bytes);
					addFile(&cl, &op);
				}
				if (opcode == SYNC) {
					getResponse(clSockFD, &response);
					if (response == ADD) {
						readFromFile(clSockFD, buffer, op.bytes);
					}
				}
			} else if (t == DIRECTORY) {
				if (opcode == SYNC) {
					getResponse(clSockFD, &response);
					if (response == ADD) {
						printf("Directory created\n");
						addFile(&cl, &op);
					}
				}
				readTheDirectoryHelper(entry->d_name, buffer,
							&buffer[strlen(CLIENT_DIR_NAME)+1],opcode);
			}
		}
	}
	free(serializedData);
	if (closedir(dirPtr) == -1) {
		perror("closedir");
		return -1;
	}
	return err;
}

static int readTheDirectory(const char *dirName, const char *path, int opcode)
{
	return readTheDirectoryHelper(dirName, path, "", opcode);
}


static const char *getOnlyDirName(char *path)
{
	int index = strlen(path) - 1;

	if (path[index] == '/') {
		path[index] = '\0';
	}
	while (index > 0 && path[index] != '/') --index;
	if (path[index] == '/') ++index;

	return &path[index];
}

