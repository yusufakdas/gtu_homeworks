module _1bit_to_32bits(out, in);
	output [31:0] out;
	input in;

	buf bit_0(out[0], in),     bit_1(out[1], 1'b0),
		 bit_2(out[2], 1'b0),   bit_3(out[3], 1'b0),
		 bit_4(out[4], 1'b0),   bit_5(out[5], 1'b0),
		 bit_6(out[6], 1'b0),   bit_7(out[7], 1'b0),
		 bit_8(out[8], 1'b0),   bit_9(out[9], 1'b0),
		 bit_10(out[10], 1'b0), bit_11(out[11], 1'b0),
		 bit_12(out[12], 1'b0), bit_13(out[13], 1'b0),
		 bit_14(out[14], 1'b0), bit_15(out[15], 1'b0),
		 bit_16(out[16], 1'b0), bit_17(out[17], 1'b0),
		 bit_18(out[18], 1'b0), bit_19(out[19], 1'b0), 
		 bit_20(out[20], 1'b0), bit_21(out[21], 1'b0),
		 bit_22(out[22], 1'b0), bit_23(out[23], 1'b0),
		 bit_24(out[24], 1'b0), bit_25(out[25], 1'b0),
		 bit_26(out[26], 1'b0), bit_27(out[27], 1'b0),
		 bit_28(out[28], 1'b0), bit_29(out[29], 1'b0),
		 bit_30(out[30], 1'b0), bit_31(out[31], 1'b0);
		 
endmodule
