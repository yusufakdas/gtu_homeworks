module _32bit_input_4x1_mux(R, S1, S0, I3, I2, I1, I0);
	input  S1, S0;
	input  [31:0] I3, I2, I1, I0;
	output [31:0] R;
	wire 	 [31:0] first_mux_wires, second_mux_wires;
	
	_32bit_input_2x1_mux first_32bit_mux(first_mux_wires, S0, I1, I0);
	_32bit_input_2x1_mux second_32bit_mux(second_mux_wires, S0, I3, I2);
	_32bit_input_2x1_mux result_32bit_mux(R, S1, second_mux_wires, first_mux_wires);
	
endmodule
