module _2x1_mux(R, S0, I1, I0);
	input  S0, I0, I1;
	output R;
	wire 	 not_S0, I0_and, I1_and;
	
	not negate_S0(not_S0, S0);
	and and_of_I0(I0_and, I0, not_S0);
	and and_of_I1(I1_and, I1, S0);
	or result(R, I1_and, I0_and);
	
endmodule
