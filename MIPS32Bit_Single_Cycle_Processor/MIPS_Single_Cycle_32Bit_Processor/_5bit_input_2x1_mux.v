module _5bit_input_2x1_mux(R, S0, I1, I0);
	output [4:0] R;
	input [4:0] I1, I0;
	input S0;
	
	_2x1_mux R0 (R[0],  S0, I1[0],  I0[0]),
				R1 (R[1],  S0, I1[1],  I0[1]),
				R2 (R[2],  S0, I1[2],  I0[2]),
				R3 (R[3],  S0, I1[3],  I0[3]),
				R4 (R[4],  S0, I1[4],  I0[4]);
	
endmodule
