module control_unit(jump, reg_dst, branch, mem_read, mem_to_reg, ALU_op, mem_write, ALU_src, reg_write, opcode);
	output jump;
	output reg_dst;
	output branch;
	output mem_read;
	output mem_to_reg;
	output [2:0] ALU_op;
	output mem_write;
	output ALU_src;
	output reg_write;
	input  [5:0] opcode;

	wire reg_dst_temp;
	
	wire sig_mem_to_reg_temp;
	wire sig_ALU_src_temp;
	
	wire sig_reg_write_temp_AND1;
	wire sig_reg_write_temp_AND2;
	wire sig_reg_write_temp_AND3;
	
	wire sig_mem_read_temp_AND;

	wire not_opcode_4;	
	wire not_opcode_3;
	wire not_opcode_2;
	wire not_opcode_1;
	
	wire sig_branch_temp_XOR;
	wire sig_branch_temp_NOT;

	not not_of_opcode_3(not_opcode_3, opcode[3]); //C
	not not_of_opcode_2(not_opcode_2, opcode[2]); //D
	not not_of_opcode_1(not_opcode_1, opcode[1]); //E
	
	// Handle reg_dst signal.
	or sig_reg_dst_first_step(reg_dst_temp, opcode[5], opcode[4], opcode[3], opcode[2], opcode[1], opcode[0]);
	not sig_reg_dst_second_step(reg_dst, reg_dst_temp);
	
	// Handle ALUsrc signal.
	or sig_ALU_src_first_step(ALU_src, opcode[5], opcode[4], opcode[3], opcode[1], opcode[0]);
	
	// Handle mem_to_reg signal.
	and sig_mem_to_reg_first_step(mem_to_reg, opcode[5], opcode[1], opcode[0], not_opcode_3);
	
	// Handle reg_write signal.
	and sig_reg_write_first_step(sig_reg_write_temp_AND1, not_opcode_2, not_opcode_1);
	and sig_reg_write_second_step(sig_reg_write_temp_AND2, opcode[3], not_opcode_1);
	and sig_reg_write_third_step(sig_reg_write_temp_AND3, opcode[0], not_opcode_3);
	or sig_reg_write_fourth_step(reg_write, sig_reg_write_temp_AND1, sig_reg_write_temp_AND2, sig_reg_write_temp_AND3);
	
	// Handle mem_read signal.
	and sig_mem_read_first_step(sig_mem_read_temp_AND, opcode[5], opcode[0], opcode[1]);
	and sig_mem_read_second_step(mem_read, not_opcode_3, sig_mem_read_temp_AND);
	
	// Handle mem_write signal.
	and sig_mem_read(mem_write, opcode[3], sig_mem_read_temp_AND);
	
	// Handle branch signal.
	xor sig_branch_first_step(sig_branch_temp_XOR, opcode[2], opcode[1], opcode[0]);
	not sig_branch_second_step(sig_branch_temp_NOT, opcode[3]);
	and sig_branch_third_step(branch, sig_branch_temp_XOR, sig_branch_temp_NOT, opcode[2]);
	
	// Handle jump signal.
	and sig_jump(jump, sig_branch_temp_XOR, sig_branch_temp_NOT, opcode[1]);
	
	/****************** Handle ALU_op signal. ******************/
	// ALUOp 2
	nor sig_alu_op_2_second_step(ALU_op[2], opcode[3], opcode[1]);
	
	// ALUOp 1
	nor sig_alu_op_1(ALU_op[1], opcode[4], opcode[2]);	
	
	// ALUOp 0
	and sig_alu_op_0(ALU_op[0], opcode[3], opcode[2], opcode[0]);
	/***********************************************************/
	
endmodule
