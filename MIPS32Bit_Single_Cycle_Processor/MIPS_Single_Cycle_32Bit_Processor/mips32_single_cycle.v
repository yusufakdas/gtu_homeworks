module mips32_single_cycle(clk);
	input clk;
	
	// Instruction taken from instruction memory
	wire [31:0] instruction;
	
	// For instruction
	wire [5:0] opcode, funct;
	wire [4:0] rs, rt, rd, shamt;
	wire [31:0] read_reg_top, read_reg_down;
	wire [15:0] immediate_field;
	
	wire [4:0] write_register;

	// For ALU outputs
	wire zero, carry, overflow;
	wire [31:0] alu_result;

	wire [31:0] write_data;

	// Control signals
	wire jump;
	wire reg_dst;
	wire branch;
	wire mem_read;
	wire mem_to_reg;
	wire mem_write;
	wire ALU_src;
	wire reg_write;
	wire [2:0] ALU_op;
	
	wire [31:0] next_address;
	
	// Seletion between zero and sign extend.
	wire [31:0] zero_extend;
	wire [31:0] sign_extend;
	wire [31:0] extension;
	wire select_bit_for_extension;

	// SELECT BITS OF ALU
	wire [2:0] select_bits_ALU;
	
	// For shift operation
	wire shift_select_xor, shift;
	wire [31:0] extended_shamt;
	wire [31:0] shift_RT_mux_res, shift_RS_mux_res;
	
	// ALU down input
	wire [31:0] ALU_down_input;
	
	// For SLTU Operation
	wire [31:0] sltu_extended_msb;
	wire get_for_R_type_OR;
	wire get_for_R_type_NOT;
	wire sltu_select;
	
	// Memory data
	wire [31:0] read_data;
	
	// Result of mem to reg.
	wire [31:0] res;
	
	// Jump address.
	wire [25:0] jump_address;
	wire [31:0] _32bits_jump_address;
	wire [31:0] value;
	
	// Check branch
	wire is_branch;
	
	// Get the next address.
	nextPC get_pc(next_address, value, jump, is_branch, clk);
	
	// Get the next instruction from instruction memory.
	instruction_memory get_instruction(instruction, next_address, clk);

	// Parse instruction.
	instruction_divide parse_instruction(opcode, rs, rt, rd, shamt, funct, immediate_field, jump_address, instruction);
	_26bits_to_32bits get_32bits_jump(_32bits_jump_address, jump_address, next_address);

	// Control signals for instruction.
	control_unit get_control_signals(jump, reg_dst, branch, mem_read, mem_to_reg, ALU_op, mem_write, ALU_src, reg_write, opcode);

	// Get the register to be written,
	_5bit_input_2x1_mux get_write_register(write_register, reg_dst, rd, rt);
	
	// Read data from registers.
	mips_registers get_register_contents(read_reg_top, read_reg_down, res, rs, rt, write_register, reg_write, clk);
	
	// Zero extend or Sign extend.
	and get_select_bit_for_extension(select_bit_for_extension, opcode[3], opcode[2]);
	sign_extend_imm get_sign_ext(sign_extend, immediate_field);
	zero_extend_imm get_zero_ext(zero_extend, immediate_field);
	_32bit_input_2x1_mux get_the_correct_extension(extension, select_bit_for_extension, zero_extend, sign_extend);
	
	// Get the select bits of ALU
	alu_control_unit get_the_alu_control_bits(select_bits_ALU, funct, ALU_op);
	
	// Shift operation selects.
	xor s_select_xor(shift_select_xor, select_bits_ALU[0], select_bits_ALU[1]);
	and shift_select(shift, select_bits_ALU[2], shift_select_xor);
	
	_5bits_to_32bits extender(extended_shamt, shamt); // Extend the 5 bits shamt to 32 bits result
	_32bit_input_2x1_mux shift_rt(shift_RT_mux_res, shift, extended_shamt, read_reg_down); // GO TO RT INPUT OF ALU
	_32bit_input_2x1_mux shift_rs(shift_RS_mux_res, shift, read_reg_down, read_reg_top);     // GO TO RS INPUT OF ALU
	
	// Get the down input of ALU.
	_32bit_input_2x1_mux down_side(ALU_down_input, ALU_src, extension, shift_RT_mux_res);
	
	// Get the result using Arithmetic Logic Unit
	alu32 get_ALU_result(alu_result, zero, carry, overflow, select_bits_ALU, shift_RS_mux_res, ALU_down_input);
	and check_brach(is_branch, branch, zero);
	_32bit_input_2x1_mux get_jump(value, jump, _32bits_jump_address, extension);
	
	// For SLTU operation
	_1bit_to_32bits msb_extent(sltu_extended_msb, alu_result[31]);
	or get_opcode_or(get_for_R_type_OR, opcode[5], opcode[4], opcode[3], opcode[2], opcode[1], opcode[0]);
	not get_opcode_not(get_for_R_type_NOT, get_for_R_type_OR);
	and get_sltu_select_bit(sltu_select, get_for_R_type_NOT, funct[3]);
	_32bit_input_2x1_mux result_selection(write_data, sltu_select, sltu_extended_msb, alu_result);
	
	// Memory Operation
	data_memory use_memory(read_data, alu_result, read_reg_down, mem_read, mem_write, clk);
	
	// Memory to Register
	_32bit_input_2x1_mux reg_to_mem(res, mem_to_reg, read_data, write_data);  //(R, S0, I1, I0)


endmodule 