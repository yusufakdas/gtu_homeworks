module check_for_sum_and_sub(result, select, check_for);
	input  check_for;
	input  [2:0] select;
	output result;
	wire xor_tmp, mux_result;
	
	xor select_xor(xor_tmp, select[2], select[1]);
	_2x1_mux selector(mux_result, select[0], 1'b0, xor_tmp);
	and select_and(result, mux_result, check_for);
	
endmodule
