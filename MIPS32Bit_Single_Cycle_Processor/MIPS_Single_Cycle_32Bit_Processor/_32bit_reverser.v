module _32bit_reverser(reverse_inp, inp);
	input  [31:0] inp;
	output [31:0] reverse_inp;
	
	buf rev0 (reverse_inp[31], inp[0]),
		 rev1 (reverse_inp[30], inp[1]),
		 rev2 (reverse_inp[29], inp[2]),
		 rev3 (reverse_inp[28], inp[3]),
		 rev4 (reverse_inp[27], inp[4]),
		 rev5 (reverse_inp[26], inp[5]),
		 rev6 (reverse_inp[25], inp[6]),
		 rev7 (reverse_inp[24], inp[7]),
		 rev8 (reverse_inp[23], inp[8]),
		 rev9 (reverse_inp[22], inp[9]),
		 rev10(reverse_inp[21], inp[10]),
		 rev11(reverse_inp[20], inp[11]),
		 rev12(reverse_inp[19], inp[12]),
		 rev13(reverse_inp[18], inp[13]),
		 rev14(reverse_inp[17], inp[14]),
		 rev15(reverse_inp[16], inp[15]),
		 rev16(reverse_inp[15], inp[16]),
		 rev17(reverse_inp[14], inp[17]),
		 rev18(reverse_inp[13], inp[18]),
		 rev19(reverse_inp[12], inp[19]),
		 rev20(reverse_inp[11], inp[20]),
		 rev21(reverse_inp[10], inp[21]),
		 rev22(reverse_inp[9],  inp[22]),
		 rev23(reverse_inp[8],  inp[23]),
		 rev24(reverse_inp[7],  inp[24]),
		 rev25(reverse_inp[6],  inp[25]),
		 rev26(reverse_inp[5],  inp[26]),
		 rev27(reverse_inp[4],  inp[27]),
		 rev28(reverse_inp[3],  inp[28]),
		 rev29(reverse_inp[2],  inp[29]),
		 rev30(reverse_inp[1],  inp[30]),
		 rev31(reverse_inp[0],  inp[31]);

endmodule
