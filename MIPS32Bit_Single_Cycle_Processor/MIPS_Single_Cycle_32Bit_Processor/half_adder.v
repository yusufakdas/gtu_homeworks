module half_adder(sum, cout, A, B);
	input  A, B;
	output sum, cout;
	
	xor sum_of_them(sum, A, B);
	and carry_out(cout, A, B);
	
endmodule
