module instruction_memory(instruction, pc, clk);
	output  [31:0] instruction;
	input [31:0] pc;
	input clk;
	
	reg [31:0] instructions [255:0];
	
	initial $readmemh("instructions.mem", instructions);

	assign instruction = instructions[pc];
		
/*
	always begin
		if (instruction === 32'bx)
			$stop();
	end
*/
		
endmodule
