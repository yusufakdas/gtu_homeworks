module data_memory(read_data, address, write_data, mem_read, mem_write, clk);
	output reg[31:0] read_data;
	input [31:0] address;
	input [31:0] write_data;
	input mem_read;
	input mem_write;
	input clk;
	
	reg [31:0] memory [255:0];

	initial $readmemh("data.mem", memory);
	
	always @(negedge clk) 
	begin
		if (mem_read === 1)
			read_data = memory[address];
	end
	
	always @(posedge clk) 
	begin
		if (mem_write === 1) 
			memory[address] = write_data;
			$writememh("result_data_memory.mem", memory); 
	end
	
endmodule
