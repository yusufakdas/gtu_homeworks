module alu32(R, zero, carry, overflow, select, A, B);
	input  [31:0] A, B;
	input  [2:0] select;
	output zero, carry, overflow;
	output [31:0] R;
	wire   [31:0] res_and, res_or, res_sum, res_sub, 
					  res_xor, res_shift, res_nor;
	wire	 [31:0] B_not, B_not_tmp;
	wire 	 carry_sum, carry_subtraction, tmp, temp_carry, tmp_sub_carry,
			 temp_xnor, temp_xor, temp_xnor_sum, temp_xnor_sub, temp_overflow;
		
	// And
	_32bit_and and_result(res_and, A, B);
	
	// Or
	_32bit_or or_result(res_or, A, B);
	
	// Sum
	_32bit_adder sum_result(res_sum, carry_sum, A, B, 1'b0);
	
	// Xor
	_32bit_xor xor_result(res_xor, A, B);

	// Subtraction (2's complement)
	_32bit_not negate_B(B_not_tmp, B);
	_32bit_adder add_one(B_not, tmp_sub_carry, B_not_tmp, 32'b1, 1'b0);
	_32bit_adder subtraction_result(res_sub, carry_subtraction, A, B_not, 1'b0);

	// Shifter. This module can shift both arithmetic right and logical left.
	_32bit_shifter shift(res_shift, A, B, select[0]);
		
	// Nor
	_32bit_nor nor_result(res_nor, A, B);
	
	// Select carry
	_2x1_mux select_carry(temp_carry, select[2], carry_subtraction, carry_sum);
	
	// Carry bit is only set if operation is either sum or subtraction.
	check_for_sum_and_sub carry_check(carry, select, temp_carry);
	
	// Select operation
	_32bit_input_8x1_mux selection(R, select[2], select[1], select[0], res_nor, res_shift, 
											 res_shift, res_sub, res_xor, res_sum, res_or, res_and);
	
	// BONUS Part (Overflow Bit) ***********************************************/
	xnor xnor_for_sum(temp_xnor_sum, A[31], B[31]);
	xnor xnor_for_sub(temp_xnor_sub, A[31], B_not[31]);
	_2x1_mux operate(temp_xnor, select[2], temp_xnor_sub, temp_xnor_sum);
	xor xor_for_overflow(temp_overflow, R[31], carry);
	_2x1_mux get_overflow_bit(tmp, temp_xnor, temp_overflow, 1'b0);
	check_for_sum_and_sub fl_check(overflow, select, tmp);
	/***************************************************************************/
	
	// BONUS Part (Zero Bit) ***************************************************/
	zero_bit_checker zero_setter(zero, R);												
	/***************************************************************************/
	
endmodule
