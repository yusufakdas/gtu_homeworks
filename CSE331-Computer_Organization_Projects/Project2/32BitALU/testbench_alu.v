`define DELAY 50

module testbench_alu;
	reg  [31:0] A, B;
	reg  [2:0] select;
	wire [31:0] R;
	wire zero, carry, overflow;
	
	alu32 action(R, zero, carry, overflow, select, A, B);
	
	initial
	begin
		/****************************************************************/
		$display("[INPUT1] (A: 0xb79e80df, B: 0x8e5c6f09) -------------------------------------");
		A = 32'hb79e80df; B = 32'h8e5c6f09;
		select = 3'b000;  // AND
		#`DELAY;
		
		select = 3'b001;	// OR
		#`DELAY;
		
		select = 3'b010;	// A + B
		#`DELAY;
		
		select = 3'b011;	// XOR
		#`DELAY;
		
		select = 3'b100;	// A - B
		#`DELAY;
		
		select = 3'b101;	// >>
		#`DELAY;
		
		select = 3'b110;	// <<
		#`DELAY;
		
		select = 3'b111;	// NOR
		#`DELAY;
		$display("-----------------------------------------------------------------------------\n");
		/****************************************************************/
		
		/****************************************************************/
		$display("[INPUT2] (A: 0x8181cf1, B: 0xa) ---------------------------------------------");
		A = 32'h8181cf1; B = 32'ha;
		select = 3'b000;  // AND
		#`DELAY;
		
		select = 3'b001;	// OR
		#`DELAY;
		
		select = 3'b010;	// A + B
		#`DELAY;
		
		select = 3'b011;	// XOR
		#`DELAY;
		
		select = 3'b100;	// A - B
		#`DELAY;
		
		select = 3'b101;	// >>
		#`DELAY;
		
		select = 3'b110;	// <<
		#`DELAY;
		
		select = 3'b111;	// NOR
		#`DELAY;
		$display("-----------------------------------------------------------------------------\n");
		/****************************************************************/
		
		/****************************************************************/
		$display("[INPUT3] (A: 0x7fffffff, B: 0x3) --------------------------------------------");
		A = 32'h7fffffff; B = 32'h3;
		select = 3'b000;  // AND
		#`DELAY;
		
		select = 3'b001;	// OR
		#`DELAY;
		
		select = 3'b010;	// A + B
		#`DELAY;
		
		select = 3'b011;	// XOR
		#`DELAY;
		
		select = 3'b100;	// A - B
		#`DELAY;
		
		select = 3'b101;	// >>
		#`DELAY;
		
		select = 3'b110;	// <<
		#`DELAY;
		
		select = 3'b111;	// NOR
		#`DELAY;
		$display("-----------------------------------------------------------------------------\n");
		/****************************************************************/
	end

	initial
	begin
		$monitor("Opcode: %3b, A = %32b, B = %32b, R = %32b, carry_out = %1b, zero = %1b, overflow = %1b", 
					select, A, B, R, carry, zero, overflow);
	end

endmodule
