# author: @yusufakdas
    .data
max_byte:        .word      256     # Maximum byte to be read
filename_length: .word      40      # Maximum length of filename
buffer:          .space     256     # Empty space for string to be read
filename:        .space     40
asteriks:        .asciiz    "**** "
intro_message:   .asciiz    "Enter the filename: "		
error_message:   .asciiz    " could not be found! ****\n"
numbers:         .asciiz    "zeroonetwothreefourfivesixseveneightnine" # All numbers
initial_of_nums: .word      0, 4, 7, 10, 15, 19, 23, 26, 31, 36, 40    # Start index of each number 
    .globl  main
    .text	
main:
    # Some constant values for convenience
    addi $s5, $zero, 1                  # Constant one
    addi $s4, $zero, 4                  # A Word is 4-byte.
    addi $s6, $zero, '/'            	# Preceeded by zero ('/')
    addi $s7, $zero, ':'            	# Followed by nine (':')
	addi $s2, $zero, ' '				# Space character
	
    jal file_operations                 # Read string from file. 
	
    la   $t0, buffer                    # Get the initial address of the buffer.
    addi $t1, $zero, 0                  # Loop counter
    solve_loop:
        beq $t1, $s1, exit
        lb  $a1, ($t0)
        jal is_valid
        beq $v0, $s5, print_number
        bne $v0, $s5, print_char
        print_number: jal get_the_number
                      j increment
        print_char:   jal putchar
		increment: addi $t1, $t1, 1
                   addi $t0, $t0, 1
        j solve_loop
	
exit: addi $v0, $zero, 10               # Service number of halt
      syscall                           # System call

putchar:   # $a1 is the register containing the char to be printed. 
    addi $v0, $zero, 11     # System call of print char.
    add  $a0, $zero, $a1    # Put the given char. into $a0 argument register.
    syscall                 # System call
    jr   $ra                # Go to where it was in advance.

is_digit:  # $a1 is the register containing the char to be checked.
    slt $t2, $a1, $s7
    slt $t3, $s6, $a1
    and $v0, $t2, $t3
    jr  $ra
	
is_valid:  # $a1 is the register containing the char to be checked.
    add  $s3, $zero, $ra
    jal  is_digit
    beq  $zero, $v0, no

    # Check the beginning of the buffer.
   	beq  $zero, $t1, rest

    # Check the number's one predecessor.    
    addi $t3, $t0, -1
    lb   $t4, ($t3)
    beq  $t4, $s2, rest
    addi $t5, $zero, '\n'
    bne  $t4, $t5, no
    rest: # Check the number's one successor.
        addi $t3, $t0, 1
        lb   $t4, ($t3) 
        beq  $t4, $s2, yes
        addi $t5, $zero, '.'
        beq  $t4, $t5, one_step
        addi $t5, $zero, '?'
        beq  $t4, $t5, one_step
        addi $t5, $zero, '!'
        beq  $t4, $t5, one_step
        addi $t5, $zero, '\n'
        beq  $t5, $t4, yes
        bne  $t4, $t5, no
        one_step: addi $t3, $t3, 1
    		      lb   $t4, ($t3) 
    		      addi $t5, $zero, ' ' 
    	 	      beq  $t4, $t5, yes
    		      addi $t5, $zero, '\0'
    		      beq  $t4, $t5, yes
    		      addi $t5, $zero, '\n'
    		      beq  $t4, $t5, yes
    		      addi $t5, $zero, '.'
    		      beq  $t4, $t5, yes
    		      j  no
    
    yes: jr $s3
    no: add $v0, $zero, $zero
    	jr  $s3
    
get_the_number: # $a1 is the register containing the number.
    addi $t2, $a1, -48          # ASCII '0'
    add  $t6, $zero, $ra        # Save the return address of procedure. 
    la   $t3, initial_of_nums   # Load the address of numbers' start bytes
    mult $t2, $s4
    mflo $t2

    add  $t3, $t3, $t2
    lw   $t4, ($t3)  # Current number's index
    lw   $t5, 4($t3) # Next number's index

	la   $t3, numbers
    add  $t3, $t3, $t4
    lb   $a1, ($t3)
  	beq  $t1, $zero, make_upper # Check number whether number is at the beginning of the sentence
	
	addi $t8, $t0, -1
    lb   $t7, ($t8)
	
    beq  $s2, $t7, space_loop
    bne	 $s2, $t7, control
    space_loop:
              bne  $t7, $s2, control
              addi $t8, $t8, -1 
              lb   $t7, ($t8)
              j  space_loop

    control: addi $t8, $zero, '.'
             beq  $t7, $t8, make_upper
             addi $t8, $zero, '!'
             beq  $t7, $t8, make_upper
             addi $t8, $zero, '?'
             beq  $t7, $t8, make_upper

    bne  $t1, $zero, number_loop
    make_upper: addi $a1, $a1, -32
    number_loop:
        beq	 $t4, $t5, end_of_loop
        jal  putchar
        addi $t4, $t4, 1
        addi $t3, $t3, 1
        lb   $a1, ($t3)
        j    number_loop	
    end_of_loop: jr	$t6

file_operations:
    add  $t0, $zero, $ra    # Save the return address of this procedure.
    jal get_filename        # Get the filename from the user.

    addi $v0, $zero, 13     # Service number of OPEN operation
    la   $a0, filename      # Send filename address as argument.
    addi $a1, $zero, 0      # Read mode
    addi $a2, $zero, 0      # Mode is 0 as default.
    syscall                 # System call
    add  $s0, $zero, $v0    # Save the file descriptor for other processes. ($s0 -> file desc.)

    slt  $t1, $zero, $s0    # Check if file is opened.
    beq  $t1, $zero, err    # If it is not opened, print an error message.

    addi $v0, $zero, 14     # Service number of READ operation
    add	 $a0, $zero, $s0    # Provide file descriptor.
    la   $a1, buffer        # Address of input buffer
    lw   $a2, max_byte      # Maximum bytes to be read
    syscall                 # System call
    add	 $s1, $zero, $v0    # Save how many characters were read. ($s1 -> # of chars read)

    addi $v0, $zero, 16     # Service number of CLOSE operation
    add	 $a0, $zero, $s1    # Provide file descriptor.
    syscall                 # System call
    jr 	 $t0

err: # Error message is printed and program halts.
    addi $v0, $zero, 4
    la   $a0, asteriks
    syscall
    la   $a0, filename
    syscall
    la   $a0, error_message
    syscall
    j    exit

get_filename:
    addi $v0, $zero, 4
    la   $a0, intro_message
    syscall

    addi $v0, $zero, 8
    la   $a0, filename
    lw 	 $a1, filename_length
    syscall

    lb 	 $t1, ($a0)
    addi $t2, $zero, '\n'
    remove_newline:  # The newline character at the end of the filename eliminated.
        beq  $t1, $t2, found
        addi $a0, $a0, 1
        lb 	 $t1, ($a0)
        j    remove_newline
    found: addi $t2, $zero, 0
           sb   $t2, ($a0)
    jr	 $ra
