module control_unit(select_bits_ALU, function_code);
	input [5:0] function_code;
	output [2:0] select_bits_ALU;
	
	// Negation of all function code bits
	wire func_5_neq, func_4_neq, func_3_neq,
		  func_2_neq, func_1_neq, func_0_neq;
	
	wire select_bit_1_first_and, select_bit_1_second_and,
		  select_bit_1_third_and;
		  
	wire select_bit_0_first_and, select_bit_0_second_and;
	
//this module use just structural verilog
//contol unit part = convert to function code(6bits) to select bits(3bits)(needs ALU)
//for control unit part, have to design circuit that can only do this job. 
//(Remember the lecture,first step use K-map.......)

//example
//input = function_code = 20(hex) = 32(dec) = add
//output = select_bits_ALU = 010(binary) = 2(dec) = add

//function_code = 02(srl) -> select bits == 101 (alu32 can work srl)
	
	not neq_bit_5 (func_5_neq, function_code[5]), //~A
		 neq_bit_4 (func_4_neq, function_code[4]), //~B
		 neq_bit_3 (func_3_neq, function_code[3]), //~C
		 neq_bit_2 (func_2_neq, function_code[2]), //~D
		 neq_bit_1 (func_1_neq, function_code[1]), //~E
		 neq_bit_0 (func_0_neq, function_code[0]); //~F
		 
	// Get the second bit of ALU select
	or select_bit_2 (select_bits_ALU[2], function_code[1], func_5_neq);
	
	// Get the first bit of ALU select
	and s_bit_1_f_and(select_bit_1_first_and, func_4_neq, func_3_neq, func_2_neq, func_1_neq, func_0_neq),
		 s_bit_1_s_and(select_bit_1_second_and, function_code[5], func_4_neq, func_3_neq, func_2_neq, func_1_neq),
		 s_bit_1_t_and(select_bit_1_third_and, function_code[5], func_4_neq, func_3_neq, function_code[2], function_code[1], function_code[0]);
	or select_bit_1(select_bits_ALU[1], select_bit_1_first_and, select_bit_1_second_and, select_bit_1_third_and);
	
	// Get the zeroth bit of ALU select
	and s_bit_0_f_and(select_bit_0_first_and, function_code[5], func_4_neq, func_3_neq, function_code[2], function_code[0]),
		 s_bit_0_s_and(select_bit_0_second_and, func_5_neq, func_4_neq, func_3_neq, func_2_neq, function_code[1], func_0_neq);
	or select_bit_0(select_bits_ALU[0], select_bit_0_first_and, select_bit_0_second_and);
		 
endmodule
