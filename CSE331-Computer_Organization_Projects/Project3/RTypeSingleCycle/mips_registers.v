module mips_registers
( read_data_1, read_data_2, write_data, read_reg_1, read_reg_2, write_reg, signal_reg_write, clk );

	output reg [31:0] read_data_1, read_data_2;
	input [31:0] write_data;
	input [4:0] read_reg_1, read_reg_2, write_reg;
	input signal_reg_write; 
	input clk;
	
	reg [31:0] registers [31:0];

	initial begin
		$readmemb("registers.mem", registers);
	end
	
	always @(negedge clk) begin
		read_data_1 = registers[read_reg_1];
		read_data_2 = registers[read_reg_2];
	end
	
	always @(posedge clk) begin
		if (write_data !== 32'bx && write_reg != 5'b0 && signal_reg_write == 1'b1) begin
			registers[write_reg] = write_data;
			$writememb("registers_result.mem", registers);  //  You can change the file name as "registers.mem" if you want to continue with new register contents.
																			//  The purpose of writting to new file is to make test process easier and to see differences between 
																			// old and new register contents.
		end
	end
	
endmodule
