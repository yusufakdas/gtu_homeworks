module instruction_divide(opcode, rs, rt, rd, shamt, funct, instruction);
	input  [31:0] instruction;
	output [5:0] opcode, funct;
	output [4:0] rs, rt, rd, shamt;
	
	// Get the opcode field.
	buf opcode_bit_5(opcode[5], instruction[31]),
		 opcode_bit_4(opcode[4], instruction[30]),
		 opcode_bit_3(opcode[3], instruction[29]),
		 opcode_bit_2(opcode[2], instruction[28]),
		 opcode_bit_1(opcode[1], instruction[27]),
		 opcode_bit_0(opcode[0], instruction[26]);
		 
	// Get the $rs field.
	buf rs_bit_4(rs[4], instruction[25]),
		 rs_bit_3(rs[3], instruction[24]),
		 rs_bit_2(rs[2], instruction[23]),
		 rs_bit_1(rs[1], instruction[22]),
		 rs_bit_0(rs[0], instruction[21]);
		
	// Get the $rt field.
	buf rt_bit_4(rt[4], instruction[20]),
		 rt_bit_3(rt[3], instruction[19]),
		 rt_bit_2(rt[2], instruction[18]),
		 rt_bit_1(rt[1], instruction[17]),
		 rt_bit_0(rt[0], instruction[16]);
		 
	// Get the $rd field.
	buf rd_bit_4(rd[4], instruction[15]),
		 rd_bit_3(rd[3], instruction[14]),
		 rd_bit_2(rd[2], instruction[13]),
		 rd_bit_1(rd[1], instruction[12]),
		 rd_bit_0(rd[0], instruction[11]);
	
	// Get the shamt field.
	buf shamt_bit_4(shamt[4], instruction[10]),
		 shamt_bit_3(shamt[3], instruction[9]),
		 shamt_bit_2(shamt[2], instruction[8]),
		 shamt_bit_1(shamt[1], instruction[7]),
		 shamt_bit_0(shamt[0], instruction[6]);
		 
	// Get the funct field.
	buf funct_bit_5(funct[5], instruction[5]),
		 funct_bit_4(funct[4], instruction[4]),
		 funct_bit_3(funct[3], instruction[3]),
		 funct_bit_2(funct[2], instruction[2]),
		 funct_bit_1(funct[1], instruction[1]),
		 funct_bit_0(funct[0], instruction[0]);
					
endmodule 
