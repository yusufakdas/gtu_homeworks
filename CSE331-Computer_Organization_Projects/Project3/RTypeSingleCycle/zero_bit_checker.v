module zero_bit_checker(result, inp);
	input  [31:0] inp;
	output result;
	wire	 [31:0] xor_result, xnor_result;
	
	_32bit_xor xor_with_zero(xor_result, inp, 32'b0);
	_32bit_not get_not(xnor_result, xor_result);
	
	and set_zero(result, xnor_result[0], 
								xnor_result[1], 
								xnor_result[2], 
								xnor_result[3],
								xnor_result[4], 
								xnor_result[5], 
								xnor_result[6], 
								xnor_result[7], 
								xnor_result[8], 
								xnor_result[9], 
								xnor_result[10],
								xnor_result[11], 
								xnor_result[12], 
								xnor_result[13], 
								xnor_result[14], 
								xnor_result[15], 
								xnor_result[16], 
								xnor_result[17], 
								xnor_result[18], 
								xnor_result[19], 
								xnor_result[20], 
								xnor_result[21], 
								xnor_result[22], 
								xnor_result[23], 
								xnor_result[24], 
								xnor_result[25], 
								xnor_result[26], 
								xnor_result[27], 
								xnor_result[28], 
								xnor_result[29], 
								xnor_result[30], 
								xnor_result[31]);

endmodule
