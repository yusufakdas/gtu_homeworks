module mips32(instruction, result);
	input [31:0] instruction;
	output [31:0] result;
	
	reg clk;
	
	// For instruction
	wire [5:0] opcode, funct;
	wire [4:0] rs, rt, rd, shamt;
	wire [31:0] RT_content, RS_content;
	
	// For ALU Control
	wire [2:0] select_bits_ALU;
	
	// For shift operation
	wire shift_select_xor, shift;
	wire [31:0] extended_shamt;
	wire [31:0] shift_RT_mux_res, shift_RS_mux_res;
	
	// For ALU outputs
	wire zero, carry, overflow;
	wire [31:0] alu_result;
	
	// For SLTU Operation
	wire [31:0] sltu_extended_msb;
	
	instruction_divide get_fields(opcode, rs, rt, rd, shamt, funct, instruction);
	
	mips_registers get_data(RS_content, RT_content, result, rs, rt, rd, 1'b1, clk);

	control_unit get_alu_select(select_bits_ALU, funct);
	
	// Shift operation selects.
	xor s_select_xor(shift_select_xor, select_bits_ALU[0], select_bits_ALU[1]);
	and shift_select(shift, select_bits_ALU[2], shift_select_xor);
	
	_5bits_to_32bits extender(extended_shamt, shamt); // Extend the 5 bits shamt to 32 bits result
	_32bit_input_2x1_mux shift_rt(shift_RT_mux_res, shift, extended_shamt, RT_content); // GO TO RT INPUT OF ALU
	_32bit_input_2x1_mux shift_rs(shift_RS_mux_res, shift, RT_content, RS_content);     // GO TO RS INPUT OF ALU
	
	// Get the result using Arithmetic Logic Unit
	alu32 get_ALU_result(alu_result, zero, carry, overflow, select_bits_ALU, shift_RS_mux_res, shift_RT_mux_res);
	
	// For SLTU operation
	_1bit_to_32bits msb_extent(sltu_extended_msb, alu_result[31]);
	_32bit_input_2x1_mux result_selection(result, funct[3], sltu_extended_msb, alu_result);
	
endmodule
