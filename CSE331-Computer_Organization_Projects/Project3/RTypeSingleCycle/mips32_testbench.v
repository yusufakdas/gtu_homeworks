`timescale 1ps/1ps
`define CLOCK_TIME 5
`define DELAY 10

module mips32_testbench;
	wire [31:0] result;
	reg [31:0] instruction_set = 32'b0;
		
	mips32 execute(instruction_set, result);
	
	initial begin
		execute.clk = 0;
	end
	
	always #`CLOCK_TIME execute.clk = ~execute.clk;
	
	initial begin		 	//   opcode rs	   rt		rd	   shamt funct
		instruction_set = 32'b_000000_01001_01100_00011_00000_100000; #`DELAY; // rs=$9  rt=$12 rd=$3  shmat=0  (add $3, $9, $12)    [240 + 32 = 272]
		instruction_set = 32'b_000000_01010_01011_00101_00000_100001; #`DELAY; // rs=$10 rt=$11 rd=$5  shmat=0  (addu $5, $10, $11)  [3840 + 2048 = 5888]
		instruction_set = 32'b_000000_01111_11010_01101_00000_100100; #`DELAY; // rs=$15 rt=$26 rd=$13 shmat=0  (and $13, $15, $26)  [96 & 26 = 0]
		instruction_set = 32'b_000000_10110_11110_01000_00000_100111; #`DELAY; // rs=$22 rt=$30 rd=$15 shmat=0  (nor $15, $22, $30)  [~(22 | 30)]
		instruction_set = 32'b_000000_11011_00111_11000_00000_100101; #`DELAY; // rs=$27 rt=$7  rd=$24 shmat=0  (or $24, $27, $7)    [27 | 7 = 31]  
		instruction_set = 32'b_000000_01011_01010_11001_00000_101011; #`DELAY; // rs=$11 rt=$10 rd=$25 shamt=0  (sltu $25, $11, $10) [2048 sltu 3840 = 0x0001]
		instruction_set = 32'b_000000_10111_01100_01100_10001_000000; #`DELAY; // rs=$23 rt=$12 rd=$12 shamt=17 (sll $12, $12, 17)	  [32 << 17 = 4194304]
		instruction_set = 32'b_000000_10001_11111_10101_01010_000010; #`DELAY; // rs=$17 rt=$31 rd=$21 shmat=10 (srl $21, $31, 10)	  [4294967295 >> 10 = 4194303]
		instruction_set = 32'b_000000_11101_10000_00100_00000_100010; #`DELAY; // rs=$29 rt=$16 rd=$4  shamt=0  (sub $4, $29, $16)   [29 - 16 = 13]
		instruction_set = 32'b_000000_01000_10101_00110_00000_100011; #`DELAY; // rs=$8  rt=$21 rd=$6  shamt=0  (subu $6, $8, $21)   [‭4294967265‬ - ‭4194303 = 4290772962‬]
		instruction_set = 32'b_000000_10011_00111_00111_00000_101011; #`DELAY; // rs=$19 rt=$7  rd=$7  shamt=0  (sltu $7, $19, $7)   [19 sltu 7 = 0x0000]

		$display("ATTEMPT TO WRITE $zero REGISTER!");
		instruction_set = 32'b_000000_10110_11010_00000_00000_100101; #`DELAY; // ATTEMPT TO WRITE $zero REGISTER.

		$display("The new register contents has been written into file!");
		$stop;
	end
	
	always @(posedge execute.clk)  begin
		$display("posedge of clk.........................................................................");
		$display("current time : %3d", $stime);
		$display("opcode: %6b, rs: %5b, rt: %5b, rd: %5b, shamt: %5b, funct: %6b", 
					execute.opcode, execute.rs, execute.rt, execute.rd, execute.shamt, execute.funct);
		$display("RS content: %32b, RT content: %32b, RD content: %32b, Result: %32b", execute.RS_content, 
					execute.RT_content, execute.get_data.registers[execute.rd], result);
	   $display(".......................................................................................\n");	
	end
	
endmodule
