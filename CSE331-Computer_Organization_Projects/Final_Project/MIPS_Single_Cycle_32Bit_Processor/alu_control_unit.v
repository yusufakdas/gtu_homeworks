module alu_control_unit(select_bits_ALU, function_code, ALU_op);
	input [5:0] function_code;
	input [2:0] ALU_op;
	output [2:0] select_bits_ALU;

	wire is_r_type_temp;
	wire is_r_type;
	wire [2:0] select_bits_temp;
	
	// Negation of all function code bits
	wire func_5_neq, func_4_neq, func_3_neq,
		  func_2_neq, func_1_neq, func_0_neq;
	
	wire select_bit_1_first_and, select_bit_1_second_and,
		  select_bit_1_third_and;
		  
	wire select_bit_0_first_and, select_bit_0_second_and;
	
	not neq_bit_5 (func_5_neq, function_code[5]), //~A
		 neq_bit_4 (func_4_neq, function_code[4]), //~B
		 neq_bit_3 (func_3_neq, function_code[3]), //~C
		 neq_bit_2 (func_2_neq, function_code[2]), //~D
		 neq_bit_1 (func_1_neq, function_code[1]), //~E
		 neq_bit_0 (func_0_neq, function_code[0]); //~F
		 
	// Get the second bit of ALU select
	or select_bit_2 (select_bits_temp[2], function_code[1], func_5_neq);
	
	// Get the first bit of ALU select
	and s_bit_1_f_and(select_bit_1_first_and, func_4_neq, func_3_neq, func_2_neq, func_1_neq, func_0_neq),
		 s_bit_1_s_and(select_bit_1_second_and, function_code[5], func_4_neq, func_3_neq, func_2_neq, func_1_neq),
		 s_bit_1_t_and(select_bit_1_third_and, function_code[5], func_4_neq, func_3_neq, function_code[2], function_code[1], function_code[0]);
	or select_bit_1(select_bits_temp[1], select_bit_1_first_and, select_bit_1_second_and, select_bit_1_third_and);
	
	// Get the zeroth bit of ALU select
	and s_bit_0_f_and(select_bit_0_first_and, function_code[5], func_4_neq, func_3_neq, function_code[2], function_code[0]),
		 s_bit_0_s_and(select_bit_0_second_and, func_5_neq, func_4_neq, func_3_neq, func_2_neq, function_code[1], func_0_neq);
	or select_bit_0(select_bits_temp[0], select_bit_0_first_and, select_bit_0_second_and);

	// Check R type
	or check_r_type_first(is_r_type_temp, ALU_op[0], ALU_op[1]);
	and check_r_type_second(is_r_type, is_r_type_temp, ALU_op[2]);
		 
	_2x1_mux second_bit(select_bits_ALU[2], is_r_type, select_bits_temp[2], ALU_op[2]);
	_2x1_mux first_bit(select_bits_ALU[1], is_r_type, select_bits_temp[1], ALU_op[1]);
	_2x1_mux zeroth_bit(select_bits_ALU[0], is_r_type, select_bits_temp[0], ALU_op[0]);
	
endmodule
