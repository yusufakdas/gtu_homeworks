module _5bits_to_32bits(out, in);
	input  [4:0] in;
	output [31:0] out;
	
	buf bit0(out[0], in[0]),
		 bit1(out[1], in[1]),
		 bit2(out[2], in[2]),
		 bit3(out[3], in[3]),
		 bit4(out[4], in[4]),
		 bit5(out[5], 1'b0),
		 bit6(out[6], 1'b0),
		 bit7(out[7], 1'b0),
		 bit8(out[8], 1'b0),
		 bit9(out[9], 1'b0),
		 bit10(out[10], 1'b0),
		 bit11(out[11], 1'b0),
		 bit12(out[12], 1'b0),
		 bit13(out[13], 1'b0),
		 bit14(out[14], 1'b0),
		 bit15(out[15], 1'b0),
		 bit16(out[16], 1'b0),
		 bit17(out[17], 1'b0),
		 bit18(out[18], 1'b0),
		 bit19(out[19], 1'b0),
		 bit20(out[20], 1'b0),
		 bit21(out[21], 1'b0),
		 bit22(out[22], 1'b0),
		 bit23(out[23], 1'b0),
		 bit24(out[24], 1'b0),
		 bit25(out[25], 1'b0),
		 bit26(out[26], 1'b0),
		 bit27(out[27], 1'b0),
		 bit28(out[28], 1'b0),
		 bit29(out[29], 1'b0),
		 bit30(out[30], 1'b0),
		 bit31(out[31], 1'b0);

endmodule
