module _32bit_adder(sum, cout, A, B, cin);
	input  cin;
	input  [31:0] A, B;
	output [31:0] sum;
	output cout;
	wire	 [30:0] carry_out_wires;

	full_adder FA0 (sum[0],  carry_out_wires[0],  A[0],  B[0],  cin),
				  FA1 (sum[1],  carry_out_wires[1],  A[1],  B[1],  carry_out_wires[0]),
				  FA2 (sum[2],  carry_out_wires[2],  A[2],  B[2],  carry_out_wires[1]),
				  FA3 (sum[3],  carry_out_wires[3],  A[3],  B[3],  carry_out_wires[2]),
				  FA4 (sum[4],  carry_out_wires[4],  A[4],  B[4],  carry_out_wires[3]),
				  FA5 (sum[5],  carry_out_wires[5],  A[5],  B[5],  carry_out_wires[4]),
				  FA6 (sum[6],  carry_out_wires[6],  A[6],  B[6],  carry_out_wires[5]),
				  FA7 (sum[7],  carry_out_wires[7],  A[7],  B[7],  carry_out_wires[6]),
				  FA8 (sum[8],  carry_out_wires[8],  A[8],  B[8],  carry_out_wires[7]),
				  FA9 (sum[9],  carry_out_wires[9],  A[9],  B[9],  carry_out_wires[8]),
				  FA10(sum[10], carry_out_wires[10], A[10], B[10], carry_out_wires[9]),
				  FA11(sum[11], carry_out_wires[11], A[11], B[11], carry_out_wires[10]),
				  FA12(sum[12], carry_out_wires[12], A[12], B[12], carry_out_wires[11]),
				  FA13(sum[13], carry_out_wires[13], A[13], B[13], carry_out_wires[12]),
				  FA14(sum[14], carry_out_wires[14], A[14], B[14], carry_out_wires[13]),
				  FA15(sum[15], carry_out_wires[15], A[15], B[15], carry_out_wires[14]),
				  FA16(sum[16], carry_out_wires[16], A[16], B[16], carry_out_wires[15]),
				  FA17(sum[17], carry_out_wires[17], A[17], B[17], carry_out_wires[16]),
				  FA18(sum[18], carry_out_wires[18], A[18], B[18], carry_out_wires[17]),
				  FA19(sum[19], carry_out_wires[19], A[19], B[19], carry_out_wires[18]),
				  FA20(sum[20], carry_out_wires[20], A[20], B[20], carry_out_wires[19]),
				  FA21(sum[21], carry_out_wires[21], A[21], B[21], carry_out_wires[20]),
				  FA22(sum[22], carry_out_wires[22], A[22], B[22], carry_out_wires[21]),
				  FA23(sum[23], carry_out_wires[23], A[23], B[23], carry_out_wires[22]),
				  FA24(sum[24], carry_out_wires[24], A[24], B[24], carry_out_wires[23]),
				  FA25(sum[25], carry_out_wires[25], A[25], B[25], carry_out_wires[24]),
				  FA26(sum[26], carry_out_wires[26], A[26], B[26], carry_out_wires[25]),
				  FA27(sum[27], carry_out_wires[27], A[27], B[27], carry_out_wires[26]),
				  FA28(sum[28], carry_out_wires[28], A[28], B[28], carry_out_wires[27]),
				  FA29(sum[29], carry_out_wires[29], A[29], B[29], carry_out_wires[28]),
				  FA30(sum[30], carry_out_wires[30], A[30], B[30], carry_out_wires[29]),
				  FA31(sum[31], cout, 					 A[31], B[31], carry_out_wires[30]);
	
endmodule
