module nextPC(next, value, jump, branch, clk);
	output reg[31:0] next;
	input [31:0] value;
	input clk;
	input jump;
	input branch;
	
	initial
	begin
		next = -32'b10;
	end
	
	always @(posedge clk)
	begin
		if (branch === 1)
			next = next + value + 1;
		else if (jump === 1)
			next = value;
		else
			next = next + 1;
	end
	
endmodule
