module _26bits_to_32bits(_32bits, _26bits, pc);
	output [31:0] _32bits;
	input [25:0] _26bits;
	input [31:0] pc;
	
	buf bit31(_32bits[31], pc[31]),
		 bit30(_32bits[30], pc[30]),
		 bit29(_32bits[29], pc[29]),
		 bit28(_32bits[28], pc[28]),
		 bit27(_32bits[27], pc[27]),
		 bit26(_32bits[26], pc[26]),
		 bit25(_32bits[25], _26bits[25]),
		 bit24(_32bits[24], _26bits[24]),
		 bit23(_32bits[23], _26bits[23]),
		 bit22(_32bits[22], _26bits[22]),
		 bit21(_32bits[21], _26bits[21]),
		 bit20(_32bits[20], _26bits[20]),
		 bit19(_32bits[19], _26bits[19]),
		 bit18(_32bits[18], _26bits[18]),
		 bit17(_32bits[17], _26bits[17]),
		 bit16(_32bits[16], _26bits[16]),
		 bit15(_32bits[15], _26bits[15]),
		 bit14(_32bits[14], _26bits[14]),
		 bit13(_32bits[13], _26bits[13]),
		 bit12(_32bits[12], _26bits[12]),
		 bit11(_32bits[11], _26bits[11]),
		 bit10(_32bits[10], _26bits[10]),
		 bit9(_32bits[9], _26bits[9]),
		 bit8(_32bits[8], _26bits[8]),
		 bit7(_32bits[7], _26bits[7]),
		 bit6(_32bits[6], _26bits[6]),
		 bit5(_32bits[5], _26bits[5]),
		 bit4(_32bits[4], _26bits[4]),
		 bit3(_32bits[3], _26bits[3]),
		 bit2(_32bits[2], _26bits[2]),
		 bit1(_32bits[1], _26bits[1]),
		 bit0(_32bits[0], _26bits[0]);

endmodule
