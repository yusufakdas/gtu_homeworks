module _32bit_not(not_inp, inp);
	input  [31:0] inp;
	output [31:0] not_inp;
	
	not NotInp0 (not_inp[0],  inp[0]),
		 NotInp1 (not_inp[1],  inp[1]),
	 	 NotInp2 (not_inp[2],  inp[2]),
		 NotInp3 (not_inp[3],  inp[3]),
		 NotInp4 (not_inp[4],  inp[4]),
		 NotInp5 (not_inp[5],  inp[5]),
		 NotInp6 (not_inp[6],  inp[6]),
		 NotInp7 (not_inp[7],  inp[7]),
		 NotInp8 (not_inp[8],  inp[8]),
		 NotInp9 (not_inp[9],  inp[9]),
		 NotInp10(not_inp[10], inp[10]),
		 NotInp11(not_inp[11], inp[11]),
		 NotInp12(not_inp[12], inp[12]),
		 NotInp13(not_inp[13], inp[13]),
		 NotInp14(not_inp[14], inp[14]),
		 NotInp15(not_inp[15], inp[15]),
		 NotInp16(not_inp[16], inp[16]),
		 NotInp17(not_inp[17], inp[17]),
		 NotInp18(not_inp[18], inp[18]),
		 NotInp19(not_inp[19], inp[19]),
		 NotInp20(not_inp[20], inp[20]),
		 NotInp21(not_inp[21], inp[21]),
		 NotInp22(not_inp[22], inp[22]),
		 NotInp23(not_inp[23], inp[23]),
		 NotInp24(not_inp[24], inp[24]),
		 NotInp25(not_inp[25], inp[25]),
		 NotInp26(not_inp[26], inp[26]),
	 	 NotInp27(not_inp[27], inp[27]),
		 NotInp28(not_inp[28], inp[28]),
		 NotInp29(not_inp[29], inp[29]),
		 NotInp30(not_inp[30], inp[30]),
		 NotInp31(not_inp[31], inp[31]);
		 
endmodule
