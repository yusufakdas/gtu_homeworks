module zero_extend_imm(_32bit_output, _16bit_input);
	output [31:0] _32bit_output;
	input  [15:0] _16bit_input;
	
	buf _0bit(_32bit_output[0], _16bit_input[0]),
		 _1bit(_32bit_output[1], _16bit_input[1]),
		 _2bit(_32bit_output[2], _16bit_input[2]),
		 _3bit(_32bit_output[3], _16bit_input[3]),
		 _4bit(_32bit_output[4], _16bit_input[4]),
		 _5bit(_32bit_output[5], _16bit_input[5]),
		 _6bit(_32bit_output[6], _16bit_input[6]),
		 _7bit(_32bit_output[7], _16bit_input[7]),
		 _8bit(_32bit_output[8], _16bit_input[8]),
		 _9bit(_32bit_output[9], _16bit_input[9]),
		 _10bit(_32bit_output[10], _16bit_input[10]),
		 _11bit(_32bit_output[11], _16bit_input[11]),
		 _12bit(_32bit_output[12], _16bit_input[12]),
		 _13bit(_32bit_output[13], _16bit_input[13]),
		 _14bit(_32bit_output[14], _16bit_input[14]),
		 _15bit(_32bit_output[15], _16bit_input[15]),
		 _16bit(_32bit_output[16], 1'b0),
		 _17bit(_32bit_output[17], 1'b0),
		 _18bit(_32bit_output[18], 1'b0),
		 _19bit(_32bit_output[19], 1'b0),
		 _20bit(_32bit_output[20], 1'b0),
		 _21bit(_32bit_output[21], 1'b0),
		 _22bit(_32bit_output[22], 1'b0),
		 _23bit(_32bit_output[23], 1'b0),
		 _24bit(_32bit_output[24], 1'b0),
		 _25bit(_32bit_output[25], 1'b0),
		 _26bit(_32bit_output[26], 1'b0),
		 _27bit(_32bit_output[27], 1'b0),
		 _28bit(_32bit_output[28], 1'b0),
		 _29bit(_32bit_output[29], 1'b0),
		 _30bit(_32bit_output[30], 1'b0),
		 _31bit(_32bit_output[31], 1'b0);
	
endmodule
