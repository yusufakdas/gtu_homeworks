`timescale 1ps/1ps
`define CLOCK_TIME 50

module MIPS32_testbench;
	reg clk;
	
	initial clk = 0;
	always #`CLOCK_TIME clk = ~clk;
	
	mips32_single_cycle x(clk);

	always @(posedge clk) 
	begin
		if (x.instruction === 32'hffffffff) begin
			$display("*** PC: %d, HALT! ***\n", x.next_address);
			$stop();
		end
		if (x.opcode === 6'b0) begin
			if (x.funct === 6'b100000) 
				$display("PC: %d, ADD => %32b = %32b + %32b", x.next_address, x.write_data, x.shift_RS_mux_res, x.ALU_down_input);
			else if (x.funct === 6'b100001)
				$display("PC: %d, ADDU => %32b = %32b + %32b", x.next_address, x.write_data, x.shift_RS_mux_res, x.ALU_down_input);
			else if (x.funct === 6'b100100) 
				$display("PC: %d, AND => %32b = %32b & %32b", x.next_address, x.write_data, x.shift_RS_mux_res, x.ALU_down_input);
			else if (x.funct === 6'b100111) 
				$display("PC: %d, NOR => %32b = %32b NOR %32b", x.next_address, x.write_data, x.shift_RS_mux_res, x.ALU_down_input);
			else if (x.funct === 6'b100101) 
				$display("PC: %d, OR => %32b = %32b | %32b", x.next_address, x.write_data, x.shift_RS_mux_res, x.ALU_down_input);
			else if (x.funct === 6'b101011) 
				$display("PC: %d, SLTU => %32b = %32b SLTU %32b", x.next_address, x.write_data, x.shift_RS_mux_res, x.ALU_down_input);
			else if (x.funct === 6'b000000)
				$display("PC: %d, SLL => %32b = %32b << %32b", x.next_address, x.write_data, x.shift_RS_mux_res, x.ALU_down_input);
			else if (x.funct === 6'b000010) 
				$display("PC: %d, SRL => %32b = %32b >>> %32b", x.next_address, x.write_data, x.shift_RS_mux_res, x.ALU_down_input);
			else if (x.funct === 6'b100010) 
				$display("PC: %d, SUB => %32b = %32b - %32b", x.next_address, x.write_data, x.shift_RS_mux_res, x.ALU_down_input);
			else if (x.funct === 6'b100011)
				$display("PC: %d, SUBU => %32b = %32b - %32b", x.next_address, x.write_data, x.shift_RS_mux_res, x.ALU_down_input);
			$display("opcode: %6b, rs: %5b, rt: %5b, rd: %5b, shamt: %5b, funct: %6b", x.opcode, x.rs, x.rt, x.rd, x.shamt, x.funct);
			$display("-----------------------------------------------------------------------------\n");
		end
			
		if (x.opcode !== 6'b0) begin 
			if (x.opcode === 6'b001100) begin
				$display("PC: %d, ANDI => %32b = %32b ANDI %32b (Extension of Immediate_field)", x.next_address, x.write_data, x.shift_RS_mux_res, x.ALU_down_input);
				$display("opcode: %6b, rs: %5b, rt: %5b, imm: %16b", x.opcode, x.rs, x.rt, x.immediate_field);
			end
			else if (x.opcode === 6'b001101) begin
				$display("PC: %d, ORI => %32b = %32b ORI %32b (Extension of Immediate_field)", x.next_address, x.write_data, x.shift_RS_mux_res, x.ALU_down_input);
				$display("opcode: %6b, rs: %5b, rt: %5b, imm: %16b", x.opcode, x.rs, x.rt, x.immediate_field);
			end
			else if (x.opcode === 6'b001001) begin
				$display("PC: %d, ADDIU => %32b = %32b ADDI %32b (Extension of Immediate_field)", x.next_address, x.write_data, x.shift_RS_mux_res, x.ALU_down_input);
				$display("opcode: %6b, rs: %5b, rt: %5b, imm: %16b", x.opcode, x.rs, x.rt, x.immediate_field);
			end
			else if (x.opcode === 6'b100011) begin
				$display("PC: %d, LW => READ %8h from mem[%8h]", x.next_address, x.res, x.alu_result);
				$display("opcode: %6b, rs: %5b, rt: %5b, imm: %16b", x.opcode, x.rs, x.rt, x.immediate_field);
			end
			else if (x.opcode === 6'b101011) begin
				$display("PC: %d, SW => WRITE %8h TO mem[%8h]", x.next_address, x.shift_RT_mux_res, x.alu_result);
				$display("opcode: %6b, rs: %5b, rt: %5b, imm: %16b", x.opcode, x.rs, x.rt, x.immediate_field);
			end
			else if (x.opcode === 6'b000100) begin
				$display("PC: %d, BEQ => if (%d == %d) then branch %d", x.next_address, x.shift_RS_mux_res, x.ALU_down_input, x.next_address + x.value + 1);
				$display("opcode: %6b, rs: %5b, rt: %5b, imm: %16b", x.opcode, x.rs, x.rt, x.immediate_field);
			end
			else if (x.opcode === 6'b000010) begin
				$display("PC: %d, J => jump to $d", x.next_address, x.value);
				$display("opcode: %6b, address: %26b", x.opcode, x.jump_address);
			end
			$display("-----------------------------------------------------------------------------\n");
		end
	end
endmodule
