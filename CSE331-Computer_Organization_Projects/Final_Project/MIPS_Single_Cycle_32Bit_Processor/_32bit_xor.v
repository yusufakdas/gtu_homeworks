module _32bit_xor(result, A, B);
	input  [31:0] A, B;
	output [31:0] result;
	
	xor R0 (result[0],  A[0],  B[0]),
		 R1 (result[1],  A[1],  B[1]),
	 	 R2 (result[2],  A[2],  B[2]),
		 R3 (result[3],  A[3],  B[3]),
		 R4 (result[4],  A[4],  B[4]),
		 R5 (result[5],  A[5],  B[5]),
		 R6 (result[6],  A[6],  B[6]),
		 R7 (result[7],  A[7],  B[7]),
		 R8 (result[8],  A[8],  B[8]),
		 R9 (result[9],  A[9],  B[9]),
		 R10(result[10], A[10], B[10]),
		 R11(result[11], A[11], B[11]),
		 R12(result[12], A[12], B[12]),
		 R13(result[13], A[13], B[13]),
		 R14(result[14], A[14], B[14]),
		 R15(result[15], A[15], B[15]),
		 R16(result[16], A[16], B[16]),
		 R17(result[17], A[17], B[17]),
		 R18(result[18], A[18], B[18]),
		 R19(result[19], A[19], B[19]),
		 R20(result[20], A[20], B[20]),
		 R21(result[21], A[21], B[21]),
		 R22(result[22], A[22], B[22]),
	 	 R23(result[23], A[23], B[23]),
		 R24(result[24], A[24], B[24]),
		 R25(result[25], A[25], B[25]),
		 R26(result[26], A[26], B[26]),
		 R27(result[27], A[27], B[27]),
		 R28(result[28], A[28], B[28]),
		 R29(result[29], A[29], B[29]),
		 R30(result[30], A[30], B[30]),
		 R31(result[31], A[31], B[31]);
		 
endmodule
