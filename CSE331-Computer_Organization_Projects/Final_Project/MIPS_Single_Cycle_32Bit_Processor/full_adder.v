module full_adder(sum, cout, A, B, cin);
   input  A, B, cin;
	output sum, cout;
	wire   first_ha_sum, first_ha_cout;
	wire	 second_ha_cout;
	
	half_adder first_sum(first_ha_sum, first_ha_cout, A, B);
	half_adder second_sum(sum, second_ha_cout, first_ha_sum, cin);
	or	real_cout(cout, first_ha_cout, second_ha_cout);

endmodule
