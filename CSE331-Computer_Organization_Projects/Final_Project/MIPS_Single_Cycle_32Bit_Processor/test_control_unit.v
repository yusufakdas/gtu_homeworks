`define WAIT 50

module test_control_unit;
	wire jump;
	wire reg_dst;
	wire branch;
	wire mem_read;
	wire mem_to_reg;
	wire [2:0] ALU_op;
	wire mem_write;
	wire ALU_src;
	wire reg_write;
	reg [5:0] opcode;
	
	control_unit test_me(jump, reg_dst, branch, mem_read, mem_to_reg, ALU_op, mem_write, ALU_src, reg_write, opcode);
	
	initial begin
		opcode = 6'b100011; // LW 
		#`WAIT;
			
		opcode = 6'b101011; // SW 
		#`WAIT;

		opcode = 6'b000100; // BEQ 
		#`WAIT;
	
		opcode = 6'b001100; // ANDI 
		#`WAIT;
	
		opcode = 6'b001101; // ORI 
		#`WAIT;	

		opcode = 6'b001001; // ADDIU 
		#`WAIT;

		opcode = 6'b000010; // JUMP 
		#`WAIT;	
	end
	
	initial begin
		$monitor("Opcode: %6b, Regdst: %1b, ALUSRC: %1b, mem_to_reg: %1b, reg_write: %1b, mem_read: %1b, mem_write: %1b, branch: %1b, jump: %1b, ALU_op: %3b", 
					opcode, reg_dst, ALU_src, mem_to_reg, reg_write, mem_read, mem_write, branch, jump, ALU_op);
	end
	
	
endmodule
