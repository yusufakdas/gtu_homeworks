module _32bit_input_8x1_mux(R, S2, S1, S0, I7, I6, I5, I4, I3, I2, I1, I0);
	input  S2, S1, S0;
	input  [31:0] I7, I6, I5, I4, I3, I2, I1, I0;
	output [31:0] R;
	wire 	 [31:0] first_mux_wires, second_mux_wires;
	
	_32bit_input_4x1_mux first_4x1_mux(first_mux_wires, S1, S0, I3, I2, I1, I0);
	_32bit_input_4x1_mux second_4x1_mux(second_mux_wires, S1, S0, I7, I6, I5, I4);
	_32bit_input_2x1_mux result_2x1_mux(R, S2, second_mux_wires, first_mux_wires);
		
endmodule
