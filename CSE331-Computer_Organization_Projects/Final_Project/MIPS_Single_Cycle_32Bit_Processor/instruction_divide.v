module instruction_divide(opcode, rs, rt, rd, shamt, funct, imm_field, jump_address, instruction);
	input  [31:0] instruction;
	output [5:0] opcode, funct;
	output [4:0] rs, rt, rd, shamt;
	output [15:0] imm_field;
	output [25:0] jump_address;

	// Get the opcode field.
	buf opcode_bit_5(opcode[5], instruction[31]),
		 opcode_bit_4(opcode[4], instruction[30]),
		 opcode_bit_3(opcode[3], instruction[29]),
		 opcode_bit_2(opcode[2], instruction[28]),
		 opcode_bit_1(opcode[1], instruction[27]),
		 opcode_bit_0(opcode[0], instruction[26]);
		 
	// Get the $rs field.
	buf rs_bit_4(rs[4], instruction[25]),
		 rs_bit_3(rs[3], instruction[24]),
		 rs_bit_2(rs[2], instruction[23]),
		 rs_bit_1(rs[1], instruction[22]),
		 rs_bit_0(rs[0], instruction[21]);
		
	// Get the $rt field.
	buf rt_bit_4(rt[4], instruction[20]),
		 rt_bit_3(rt[3], instruction[19]),
		 rt_bit_2(rt[2], instruction[18]),
		 rt_bit_1(rt[1], instruction[17]),
		 rt_bit_0(rt[0], instruction[16]);
	 
	// Get the $rd field.
	buf rd_bit_4(rd[4], instruction[15]),
		 rd_bit_3(rd[3], instruction[14]),
		 rd_bit_2(rd[2], instruction[13]),
		 rd_bit_1(rd[1], instruction[12]),
		 rd_bit_0(rd[0], instruction[11]);
		 

	// Get the shamt field.
	buf shamt_bit_4(shamt[4], instruction[10]),
		 shamt_bit_3(shamt[3], instruction[9]),
		 shamt_bit_2(shamt[2], instruction[8]),
		 shamt_bit_1(shamt[1], instruction[7]),
		 shamt_bit_0(shamt[0], instruction[6]);
		 
	// Get the funct field.
	buf funct_bit_5(funct[5], instruction[5]),
		 funct_bit_4(funct[4], instruction[4]),
		 funct_bit_3(funct[3], instruction[3]),
		 funct_bit_2(funct[2], instruction[2]),
		 funct_bit_1(funct[1], instruction[1]),
		 funct_bit_0(funct[0], instruction[0]);
		
	// Get the immediate field.
	buf imm_field_bit_15(imm_field[15], instruction[15]),
		 imm_field_bit_14(imm_field[14], instruction[14]),
		 imm_field_bit_13(imm_field[13], instruction[13]),
		 imm_field_bit_12(imm_field[12], instruction[12]),
		 imm_field_bit_11(imm_field[11], instruction[11]),
		 imm_field_bit_10(imm_field[10], instruction[10]),
		 imm_field_bit_9(imm_field[9], instruction[9]),
		 imm_field_bit_8(imm_field[8], instruction[8]),
		 imm_field_bit_7(imm_field[7], instruction[7]),
		 imm_field_bit_6(imm_field[6], instruction[6]),
		 imm_field_bit_5(imm_field[5], instruction[5]),
		 imm_field_bit_4(imm_field[4], instruction[4]),
		 imm_field_bit_3(imm_field[3], instruction[3]),
		 imm_field_bit_2(imm_field[2], instruction[2]),
		 imm_field_bit_1(imm_field[1], instruction[1]),
		 imm_field_bit_0(imm_field[0], instruction[0]);
		 
	// Get the jump address.
	buf jump_bit_25(jump_address[25], instruction[25]),
	    jump_bit_24(jump_address[24], instruction[24]),
	    jump_bit_23(jump_address[23], instruction[23]),
	    jump_bit_22(jump_address[22], instruction[22]),
	    jump_bit_21(jump_address[21], instruction[21]),
	    jump_bit_20(jump_address[20], instruction[20]),
	    jump_bit_19(jump_address[19], instruction[19]),
	    jump_bit_18(jump_address[18], instruction[18]),
	    jump_bit_17(jump_address[17], instruction[17]),
	    jump_bit_16(jump_address[16], instruction[16]),
	    jump_bit_15(jump_address[15], instruction[15]),
	    jump_bit_14(jump_address[14], instruction[14]),
	    jump_bit_13(jump_address[13], instruction[13]),
	    jump_bit_12(jump_address[12], instruction[12]),
	    jump_bit_11(jump_address[11], instruction[11]),
	    jump_bit_10(jump_address[10], instruction[10]),
	    jump_bit_9(jump_address[9], instruction[9]),
	    jump_bit_8(jump_address[8], instruction[8]),
	    jump_bit_7(jump_address[7], instruction[7]),
	    jump_bit_6(jump_address[6], instruction[6]),
	    jump_bit_5(jump_address[5], instruction[5]),
	    jump_bit_4(jump_address[4], instruction[4]),
	    jump_bit_3(jump_address[3], instruction[3]),
	    jump_bit_2(jump_address[2], instruction[2]),
	    jump_bit_1(jump_address[1], instruction[1]),
	    jump_bit_0(jump_address[0], instruction[0]);
		 
endmodule 
