package Q2;

public class Main {
    public static void main(String[] args) {
        System.out.println("###################################################");
        BTree<Integer> tree = new BTree<>(8);
        System.out.println("##### FIRST B-TREE WITH ORDER OF 8 ELEMENTS #####");
        System.out.println("~~~~~ ADDING ELEMENTS TO B-TREE ~~~~~");
        tree.add(8);
        tree.add(2);
        tree.add(9);
        tree.add(4);
        tree.add(5);
        tree.add(16);
        tree.add(12);
        tree.add(8);
        tree.add(7);

        System.out.println(tree);
        System.out.println("---------------------------------------------------");
        System.out.println("~~~~~ ADDING ELEMENTS AGAIN ~~~~~");
        tree.add(30);
        tree.add(6);
        tree.add(3);
        tree.add(75);
        tree.add(11);
        tree.add(10);
        tree.add(186);
        tree.add(31);
        tree.add(24);
        tree.add(69);
        tree.add(14);

        System.out.println(tree);
        System.out.println("---------------------------------------------------");
        System.out.println("~~~~~ ADDING LAST ELEMENTS ~~~~~");
        tree.add(54);
        tree.add(34);
        tree.add(-96);
        tree.add(35);
        tree.add(6);
        tree.add(-963);



        System.out.println(tree);
        System.out.println("---------------------------------------------------");
        System.out.println("###################################################");

        BTree<Character> tree2 = new BTree<>(5);
        System.out.println("##### SECOND B-TREE WITH ORDER OF 5 ELEMENTS #####");
        tree2.add('y');
        tree2.add('h');
        tree2.add('v');
        tree2.add('a');
        tree2.add('c');
        tree2.add('b');
        tree2.add('t');
        tree2.add('5');
        tree2.add('7');
        tree2.add('q');

        System.out.println(tree2);
        System.out.println("---------------------------------------------------");
        System.out.println("~~~~~ ADDING ELEMENTS AGAIN ~~~~~");
        tree2.add('r');
        tree2.add('s');
        tree2.add('l');
        tree2.add('u');
        tree2.add('p');
        tree2.add('g');
        tree2.add('k');
        tree2.add('i');
        tree2.add('m');
        tree2.add('n');
        tree2.add('z');
        tree2.add('x');
        tree2.add('w');
        tree2.add('o');

        System.out.println(tree2);
        System.out.println("---------------------------------------------------");
        System.out.println("###################################################");

        System.out.println("\n");
        System.out.println("##### TRYING TO ADD AN ELEMENT THAT IS PRESENT IN THE TREE #####");
        System.out.println("~~~~~ Adding to 186 to the first tree ~~~~~");
        System.out.println(tree.add(186));
        System.out.println("~~~~~ Adding to 24 to the first tree ~~~~~");
        System.out.println(tree.add(24));
        System.out.println("~~~~~ Adding to 75 to the first tree ~~~~~");
        System.out.println(tree.add(75));
        System.out.println("~~~~~ Adding to 16 to the first tree ~~~~~");
        System.out.println(tree.add(16));
    }
}
