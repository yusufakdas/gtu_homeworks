package Q1;

public class Main {
    private static final int HEIGHT = 6;

    public static void main(String[] args) {
        RedBlackTree<Integer> firstTree = new RedBlackTree<>();
        RedBlackTree<Integer> secondTree = new RedBlackTree<>();
        int i;

        i = 0;
        while (firstTree.getHeightOfTheTree() != HEIGHT) {
            ++i;
            firstTree.add(i);
        }
        System.out.println(firstTree);
        System.out.println("HEIGHT OF THE FIRST TREE: "
                + firstTree.getHeightOfTheTree());
        System.out.println("NUMBER OF NODES OF THE FIRST TREE: " + i);
        System.out.println("----------------------------------------------------");
        System.out.println();

        i = 150;
        while (secondTree.getHeightOfTheTree() != HEIGHT) {
            --i;
            secondTree.add(i);
        }
        System.out.println(secondTree);
        System.out.println("HEIGHT OF THE SECOND TREE: "
                + secondTree.getHeightOfTheTree());
        System.out.println("NUMBER OF NODES OF THE SECOND TREE: " + (150 - i));
    }
}
