package Q3;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Self-balancing binary search tree using the algorithm defined
 * by Adelson-Velskii and Landis.
 * @author Koffman and Wolfgang
 */
public class AVLTree<E extends Comparable<E>>
         extends BinarySearchTreeWithRotate<E> {

    /** Flag to indicate that height of tree has increased. */
    private boolean increase;

    /** Flag to indicate that height of tree has decreased. */
    private boolean decrease;

    /**
     * No parameter constructor of class AVL Tree
     */
    public AVLTree() {
        super();
    }

////////////////////////////////////////////////////////////////////////////////
    /**
     * Constructor that checks whether the tree is AVL or not.
     * @param tree Binary Tree
     * @throws NoAVLTreeException If it is not AVL Tree, throws exception
     */
    public AVLTree(BinaryTree<E> tree) throws NoAVLTreeException {
        if (!isAVL(tree.root)) {
            throw new NoAVLTreeException("The tree does not have " +
                    "AVL Tree properties!");
        }
        if (tree.root != null) {
            List<E> l = levelOrderTraverse(tree);
            for (E e : l)
                add(e);
        } else {
            root = null;
        }
    }

    private List<E> levelOrderTraverse(BinaryTree<E> tree) {
        Queue<Node<E>> q = new LinkedList<>();
        List<E> ll = new LinkedList<E>();
        q.offer(tree.root);

        while (!q.isEmpty()) {
            Node<E> node = q.poll();
            ll.add(node.data);
            if (node.left != null)
                q.offer(node.left);
            if (node.right != null)
                q.offer(node.right);
        }
        return (ll);
    }

    /**
     * Check whether the tree has AVL properties or not
     * @param node Root of the tree
     * @return If it is AVL Tree, return true, otherwise false
     */
    private boolean isAVL(Node<E> node) {
        // For balance values of the subtrees
        List<Integer> balanceList = new ArrayList<>();

        // Depth of the left tree
        int left = calculateBalance(node.left, balanceList);

        // Depth of the right tree
        int right = calculateBalance(node.right, balanceList);

        // Control the balance of the tree
        if (Math.abs(left-right) > 1)
            return (false);

        // Control the balance of the subtrees
        for (Integer i : balanceList)
            if (Math.abs(i) > 1)
                return (false);
        return (true);
    }

    public boolean printAVLProperty() {
        if (isAVL(root)) {
            System.out.println("---------------------> AVL!");
            return (true);
        }
        System.out.println("---------------------> NOT AVL!!!");
        return (false);
    }
    /**
     * Calculates the depth of the tree and balance values
     * @param localRoot For left and right of the tree
     * @param balance Balance values of subtrees
     * @return Depth of the tree
     */
    private int calculateBalance(Node<E> localRoot, List<Integer> balance) {
        if (localRoot == null) return (0);
        int left = 1 + calculateBalance(localRoot.left, balance);
        int right = 1 + calculateBalance(localRoot.right, balance);
        balance.add(right - left);
        return ((left > right) ? left : right);
    }
////////////////////////////////////////////////////////////////////////////////

    /**
     * add starter method.
     * pre the item to insert implements the Comparable interface.
     * @param item The item being inserted.
     * @return true if the object is inserted; false
     *         if the object already exists in the tree
     * @throws ClassCastException if item is not Comparable
     */
    @Override
    public boolean add(E item) {
        increase = false;
        root = add((AVLNode<E>) root, item);
        return addReturn;
    }

// Insert solution to programming project 5, chapter -1 here

    // Methods

    /**
     * Recursive add method. Inserts the given object into the tree.
     * post addReturn is set true if the item is inserted,
     *       false if the item is already in the tree.
     * @param localRoot The local root of the subtree
     * @param item The object to be inserted
     * @return The new local root of the subtree with the item
     *         inserted
     */
    private AVLNode<E> add(AVLNode<E> localRoot, E item) {
        if (localRoot == null) {
            addReturn = true;
            increase = true;
            return new AVLNode<E>(item);
        }

        if (item.compareTo(localRoot.data) == 0) {
            // Item is already in the tree.
            increase = false;
            addReturn = false;
            return localRoot;
        } else if (item.compareTo(localRoot.data) < 0) {
            // item < data
            localRoot.left = add((AVLNode<E>) localRoot.left, item);

            if (increase) {
                decrementBalance(localRoot);
                if (localRoot.balance < AVLNode.LEFT_HEAVY) {
                    increase = false;
                    return rebalanceLeft(localRoot);
                }
            }
        } else { // item > data
			localRoot.right = add((AVLNode<E>) localRoot.right, item);
			if (increase) {
				incrementBalance(localRoot);
				if (localRoot.balance > AVLNode.RIGHT_HEAVY) {
					increase = false;
					return rebalanceRight(localRoot);
				}
			}
        }
		return localRoot; // Rebalance not needed.
    }

    /**
     * Method to rebalance left.
     * pre localRoot is the root of an AVL subtree that is
     *      critically left-heavy.
     * post Balance is restored.
     * @param localRoot Root of the AVL subtree
     *        that needs rebalancing
     * @return a new localRoot
     */
    private AVLNode<E> rebalanceLeft(AVLNode<E> localRoot) {
        AVLNode<E> leftChild = (AVLNode<E>) localRoot.left;

        if (localRoot.balance > AVLNode.BALANCED) {
            AVLNode<E> rightChild = (AVLNode<E>) localRoot.right;
            if (leftChild == null) {
                rightChild.balance = AVLNode.BALANCED;
                localRoot.balance = AVLNode.BALANCED;
            } else if (rightChild.balance < AVLNode.BALANCED) {
                AVLNode<E> rightLeftChild = (AVLNode<E>) rightChild.left;
                rightLeftChild.balance = AVLNode.BALANCED;
                localRoot.balance = AVLNode.BALANCED;
                leftChild.balance = AVLNode.BALANCED;
                localRoot.right = rotateLeft(rightChild);
            } else {
                leftChild.balance = AVLNode.BALANCED;
                localRoot.balance = AVLNode.BALANCED;
            }
            return ((AVLNode<E>) rotateLeft(localRoot));
        } else {
            if (leftChild.balance > AVLNode.BALANCED) { // Left-Right Case
                AVLNode<E> leftRightChild = (AVLNode<E>) leftChild.right;
                if (leftRightChild.balance < AVLNode.BALANCED) {
                    leftChild.balance = AVLNode.LEFT_HEAVY;
                    leftRightChild.balance = AVLNode.BALANCED;
                    localRoot.balance = AVLNode.BALANCED;
                } else if (leftRightChild.balance > AVLNode.BALANCED) {
                    leftChild.balance = AVLNode.BALANCED;
                    leftRightChild.balance = AVLNode.BALANCED;
                    localRoot.balance = AVLNode.RIGHT_HEAVY;
                } else {
                    leftChild.balance = AVLNode.BALANCED;
                    leftRightChild.balance = AVLNode.BALANCED;
                    localRoot.balance = AVLNode.BALANCED;
                }
                // Perform left rotation.
                localRoot.left = rotateLeft(leftChild);
            } else { //Left-Left case
                leftChild.balance = AVLNode.BALANCED;
                localRoot.balance = AVLNode.BALANCED;
            }
        }
        return ((AVLNode<E>) rotateRight(localRoot));
    }

    /**
     * Method to rebalance right.
     * pre localRoot is the root of an AVL subtree that is
     *      critically left-heavy.
     * post Balance is restored.
     * @param localRoot Root of the AVL subtree
     *        that needs rebalancing
     * @return a new localRoot
     */
	private AVLNode<E> rebalanceRight(AVLNode<E> localRoot) {
		AVLNode<E> rightChild = (AVLNode<E>) localRoot.right;
		if (localRoot.balance < AVLNode.BALANCED) {
            AVLNode<E> leftChild = (AVLNode<E>) localRoot.left;
            if (rightChild == null) {
                leftChild.balance = AVLNode.BALANCED;
                localRoot.balance = AVLNode.BALANCED;
            } else if (leftChild.balance > AVLNode.BALANCED) {
                AVLNode<E> leftRightChild = (AVLNode<E>) leftChild.right;
                leftRightChild.balance = AVLNode.BALANCED;
                localRoot.balance = AVLNode.BALANCED;
                leftChild.balance = AVLNode.BALANCED;
                localRoot.left = rotateLeft(leftChild);
            } else {
                leftChild.balance = AVLNode.BALANCED;
                localRoot.balance = AVLNode.BALANCED;
            }
            return ((AVLNode<E>) rotateRight(localRoot));
        } else {
		    if (rightChild.balance < AVLNode.BALANCED) {
                AVLNode<E> rightLeftChild = (AVLNode<E>) rightChild.left;
                if (rightLeftChild.balance < AVLNode.BALANCED) {
                    rightChild.balance = AVLNode.RIGHT_HEAVY;
                    rightLeftChild.balance = AVLNode.BALANCED;
                    localRoot.balance = AVLNode.BALANCED;
                } else if (rightLeftChild.balance > AVLNode.BALANCED) {
                    rightChild.balance = AVLNode.BALANCED;
                    rightLeftChild.balance = AVLNode.BALANCED;
                    localRoot.balance = AVLNode.LEFT_HEAVY;
                } else {
                    rightChild.balance = AVLNode.BALANCED;
                    rightLeftChild.balance = AVLNode.BALANCED;
                    localRoot.balance = AVLNode.BALANCED;
                }
                localRoot.right = rotateRight(rightChild);
            } else {
                rightChild.balance = AVLNode.BALANCED;
                localRoot.balance = AVLNode.BALANCED;
            }
        }
		return ((AVLNode<E>) rotateLeft(localRoot));
	}

    private void decrementBalance(AVLNode<E> node) {
        --node.balance;
        if (node.balance == AVLNode.BALANCED) {
            increase = false;
            decrease = true;
        } else if (node.balance < AVLNode.LEFT_HEAVY) {
            increase = true;
            decrease = false;
        }
    }

	private void incrementBalance(AVLNode<E> node) {
		++node.balance;
		if (node.balance == AVLNode.BALANCED) {
            increase = false;
            decrease = true;
        } else if (node.balance > AVLNode.RIGHT_HEAVY) {
		    increase = true;
		    decrease = false;
        }
	}

    /**
     * Delete an item from the tree
     * @param item item to be deleted
     * @return new root
     */
	@Override
    public E delete(E item) {
        decrease = false;
        root = delete((AVLNode<E>) root, item);
        return (deleteReturn);
    }

    /**
     * Delete an item from the tree
     * @param localRoot Node used in recursive calls
     * @param item item to be deleted
     * @return new root
     */
    private AVLNode<E> delete(AVLNode<E> localRoot, E item) {
        if (localRoot == null) {
            deleteReturn = null;
            decrease = false;
            return (null);
        }

        int compare = item.compareTo(localRoot.data);
        if (compare < 0) {
            localRoot.left = delete((AVLNode<E>) localRoot.left, item);
            if (decrease) {
                incrementBalance(localRoot);
                if (localRoot.balance > AVLNode.RIGHT_HEAVY) {
                    decrease = false;
                    return (rebalanceLeft(localRoot));
                }
            }
        } else if (compare > 0) {
            localRoot.right = delete((AVLNode<E>) localRoot.right, item);
            if (decrease) {
                decrementBalance(localRoot);
                if (localRoot.balance < AVLNode.LEFT_HEAVY) {
                    decrease = false;
                    return (rebalanceRight(localRoot));
                }
            }
        } else {
            deleteReturn = localRoot.data;
            if (localRoot.right == null) {
                decrease = true;
                return ((AVLNode<E>) localRoot.left);
            } else if (localRoot.left == null) {
                decrease = true;
                return ((AVLNode<E>) localRoot.right);
            } else {
                if (localRoot.left.right == null) {
                    localRoot.data = localRoot.left.data;
                    localRoot.left = localRoot.left.left;
                    incrementBalance(localRoot);
                    if (localRoot.balance > AVLNode.RIGHT_HEAVY)
                        return (rebalanceRight(localRoot));
                    else if (localRoot.balance < AVLNode.LEFT_HEAVY)
                        return (rebalanceLeft(localRoot));
                } else {
                    localRoot.data = findLargestChild(localRoot.left);
                    AVLNode<E> leftNode = (AVLNode<E>) localRoot.left;
                    if (leftNode.balance < AVLNode.LEFT_HEAVY)
                        localRoot.left = rebalanceLeft(leftNode);
                    if (decrease)
                        incrementBalance(localRoot);
                }

            }
        }
        return (localRoot);
    }

    /** Find the node that is the
     inorder predecessor and replace it
     with its left child (if any).
     post: The inorder predecessor is removed from the tree.
     @param parent The parent of possible inorder
     predecessor (ip)
     @return The data in the ip
     */
    private E findLargestChild(Node < E > parent) {
        if (parent.right.right == null) {
            E returnValue = parent.right.data;
            parent.right = parent.right.left;
            decrementBalance((AVLNode<E>)parent);
            return returnValue;
        }
        E data = findLargestChild(parent.right);
        AVLNode<E> avlNode = (AVLNode<E>) parent.right;
        if (avlNode.balance < AVLNode.LEFT_HEAVY)
            parent.right = rebalanceLeft(avlNode);
        if (decrease)
            decrementBalance((AVLNode<E>)parent);
        return (data);
    }

    /** Class to represent an AVL Node. It extends the
     * BinaryTree.Node by adding the balance field.
     */
    protected static class AVLNode<E> extends Node<E> {

        /** Constant to indicate left-heavy */
        public static final int LEFT_HEAVY = -1;
        /** Constant to indicate balanced */
        public static final int BALANCED = 0;
        /** Constant to indicate right-heavy */
        public static final int RIGHT_HEAVY = 1;
        /** balance is right subtree height - left subtree height */
        private int balance;

        // Methods
        /**
         * Construct a node with the given item as the data field.
         * @param item The data field
         */
        public AVLNode(E item) {
            super(item);
            balance = BALANCED;
        }

        /**
         * Return a string representation of this object.
         * The balance value is appended to the contents.
         * @return String representation of this object
         */
        @Override
        public String toString() {
            return balance + ": " + super.toString();
        }
    }

}