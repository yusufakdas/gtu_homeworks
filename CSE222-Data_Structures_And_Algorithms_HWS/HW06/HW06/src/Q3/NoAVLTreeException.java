package Q3;

public class NoAVLTreeException extends Exception {

    /**
     * No parameter constructor
     */
    public NoAVLTreeException() {
        super();
    }

    /**
     * Constructor that takes message as string
     * @param msg message
     */
    public NoAVLTreeException(String msg) {
        super(msg);
    }

}
