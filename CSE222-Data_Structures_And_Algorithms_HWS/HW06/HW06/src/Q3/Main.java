package Q3;

public class Main {
	public static void main(String[] args) {
        BinarySearchTree<Integer> binarySearchTree = new BinarySearchTree<>();
        AVLTree<Integer> integerAVL;
        Integer[] data = {18, 25, 12, 8, 7, 24, 16, 28, 30, 35};

        for (Integer d : data)
            binarySearchTree.add(d);
        try {
            integerAVL = new AVLTree<>(binarySearchTree);
            System.out.println(integerAVL);
        } catch (Exception e) {
            System.out.println(binarySearchTree);
            System.out.println(e.getMessage());
        }

        System.out.println("\n--------------------------------------------------");
        binarySearchTree = new BinarySearchTree<>();
        data = new Integer[]{24, 1, 80, 0, 9, 55, 82};

        for (Integer d : data)
            binarySearchTree.add(d);
        try {
            integerAVL = new AVLTree<>(binarySearchTree);
            System.out.println(integerAVL);
            System.out.println("The tree passed as argument has AVL Tree properties!");
        } catch (Exception e) {
            integerAVL = new AVLTree<>();
            System.out.println(binarySearchTree);
            System.out.println(e.getMessage());
            System.exit(1);
        }
        System.out.println("--------------------------------------------------\n");

        System.out.println("#### Deleting some items from the tree ####\n");
        System.out.println("Deleting 12");
        integerAVL.delete(12);
        System.out.println();
        System.out.println(integerAVL);

        System.out.println("Deleting root of the tree: 36");
        integerAVL.delete(36);
        System.out.println();
        System.out.println(integerAVL);

        System.out.println("Deleting left child of the root: 5");
        integerAVL.delete(5);
        System.out.println();
        System.out.println(integerAVL);

        System.out.println("Adding elements to the tree: 16, 87, 49, 66, 32, 88, 100, 10");
        integerAVL.add(16);
        integerAVL.add(87);
        integerAVL.add(49);
        integerAVL.add(66);
        integerAVL.add(32);
        integerAVL.add(88);
        integerAVL.add(100);
        integerAVL.add(10);
        System.out.println();
        System.out.println(integerAVL);

        System.out.println("Deleting 55");
        integerAVL.delete(55);
        System.out.println();
        System.out.println(integerAVL);

        System.out.println("Deleting right child of the root: 80");
        integerAVL.delete(80);
        System.out.println();
        System.out.println(integerAVL);

        System.out.println("Deleting 87");
        integerAVL.delete(87);
        System.out.println();
        System.out.println(integerAVL);

        System.out.println("Deleting 32");
        integerAVL.delete(32);
        System.out.println();
        System.out.println(integerAVL);
    }

}
