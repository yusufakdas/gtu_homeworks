package Q3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AVLTreeTest {

    @Test
    void delete() {
        AVLTree<Integer> tree = new AVLTree<>();
        assertEquals(true, tree.add(45));
        assertEquals(true, tree.add(88));
        assertEquals(true, tree.add(42));
        assertEquals(true, tree.add(41));
        assertEquals(false, tree.add(88));
        assertEquals(true, tree.add(212));
        assertEquals(true, tree.add(12));
        assertEquals(false, tree.add(45));
        assertEquals(true, tree.add(24));
        assertEquals(true, tree.add(36));
        assertEquals(false, tree.add(212));
        assertEquals(false, tree.add(41));
        assertEquals(true, tree.add(89));
        assertEquals(false, tree.add(36));
        assertEquals(false, tree.add(24));

        System.out.println("PASSED!");
    }
}