package Q2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BTreeTest {

    @Test
    void binarySearch() {
        BTree<Integer> intTree = new BTree<>(8);
        System.out.println(intTree);

        System.out.println("Adding 8");
        assertEquals(true, intTree.add(8));
        System.out.println(intTree);

        System.out.println("Adding 28");
        assertEquals(true, intTree.add(28));
        System.out.println(intTree);

        System.out.println("Adding 28 again");
        assertEquals(false, intTree.add(28));
        System.out.println(intTree);

        System.out.println("Adding 14");
        assertEquals(true, intTree.add(14));
        System.out.println(intTree);

        System.out.println("Adding 99");
        assertEquals(true, intTree.add(99));
        System.out.println(intTree);

        System.out.println("Adding 99 again");
        assertEquals(false, intTree.add(99));
        System.out.println(intTree);

        System.out.println("Adding 8 again");
        assertEquals(false, intTree.add(8));
        System.out.println(intTree);

        System.out.println("PASSED!");
    }
}