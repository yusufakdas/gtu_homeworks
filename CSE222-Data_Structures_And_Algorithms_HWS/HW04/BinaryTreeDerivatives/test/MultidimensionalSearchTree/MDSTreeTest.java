package MultidimensionalSearchTree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MDSTreeTest {

    @Test
    void add() {
        MDSTree<Integer> tree = new MDSTree<>();
        boolean actual;

        System.out.println("----------------------------------------------------------");
        System.out.println("Unit Test of add");
        actual = tree.add(new Integer[]{4, 8, 2});
        assertEquals(true, actual);

        actual = tree.add(new Integer[]{6, 2, 1});
        assertEquals(true, actual);

        actual = tree.add(new Integer[]{12, 8, 5});
        assertEquals(true, actual);

        actual = tree.add(new Integer[]{4, 8, 2});
        assertEquals(false, actual);

        actual = tree.add(new Integer[]{99, 25, 4});
        assertEquals(true, actual);

        actual = tree.add(new Integer[]{123, 321, 654});
        assertEquals(true, actual);

        actual = tree.add(new Integer[]{10, 9, 8});
        assertEquals(true, actual);

        actual = tree.add(new Integer[]{12, 8, 5});
        assertEquals(false, actual);

        actual = tree.add(new Integer[]{6, 2, 1});
        assertEquals(false, actual);

        actual = tree.add(new Integer[]{123, 321, 654});
        assertEquals(false, actual);

        System.out.println(tree);
        System.out.println("----------------------------------------------------------");

    }

    @Test
    void contains() {
        MDSTree<Integer> tree = new MDSTree<>(4);
        boolean actual;

        tree.add(new Integer[]{4, 8, 2, 5});
        tree.add(new Integer[]{6, 2, 1, 7});
        tree.add(new Integer[]{12, 8, 5, 88});
        tree.add(new Integer[]{1, 2, 3, 22});
        tree.add(new Integer[]{99, 25, 4, 543});
        tree.add(new Integer[]{23, 321, 654, 12});
        tree.add(new Integer[]{10, 9, 8, 43});


        System.out.println("----------------------------------------------------------");
        System.out.println("Unit Test of contains");
        System.out.println(tree);

        actual = tree.contains(new Integer[]{1, 2, 3, 22});
        assertEquals(true, actual);

        actual = tree.contains(new Integer[]{6, 2, 1, 7});
        assertEquals(true, actual);

        actual = tree.contains(new Integer[]{7, 3, 1, 7});
        assertEquals(false, actual);

        actual = tree.contains(new Integer[]{15, 53, 1, 2});
        assertEquals(false, actual);
        System.out.println("----------------------------------------------------------");

    }

    @Test
    void remove() {
        MDSTree<Integer> tree = new MDSTree<>(5);
        boolean actual;
        tree.add(new Integer[]{4, 8, 2, 5, 4});
        tree.add(new Integer[]{6, 2, 1, 7, 7});
        tree.add(new Integer[]{12, 8, 5, 88, 8});
        tree.add(new Integer[]{1, 2, 3, 22, 12});
        tree.add(new Integer[]{99, 25, 4, 543, 43});
        tree.add(new Integer[]{23, 321, 654, 12, 65});
        tree.add(new Integer[]{10, 9, 8, 43, 23});

        System.out.println("----------------------------------------------------------");
        System.out.println("Unit Test of remove");
        System.out.println("Before deletion");
        System.out.println(tree);

        actual = tree.remove(new Integer[]{4, 8, 2, 5, 4});
        assertEquals(true, actual);

        actual = tree.remove(new Integer[]{6, 2, 1, 7, 7});
        assertEquals(true, actual);

        actual = tree.remove(new Integer[]{1, 2, 3, 22, 12});
        assertEquals(true, actual);

        actual = tree.remove(new Integer[]{125, 53, 1, 2, 43});
        assertEquals(false, actual);

        actual = tree.remove(new Integer[]{1, 3, 1, 42, 34});
        assertEquals(false, actual);

        System.out.println("After deletion");
        System.out.println(tree);
        System.out.println("----------------------------------------------------------");
    }
}