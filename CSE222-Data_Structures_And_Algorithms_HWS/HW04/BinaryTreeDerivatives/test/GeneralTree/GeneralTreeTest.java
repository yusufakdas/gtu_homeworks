package GeneralTree;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GeneralTreeTest {

    /**
     * Testing the add method
     */
    @Test
    void add() {
        System.out.println("\n--------------------------------------------");
        System.out.println("Creating a general tree to test add method.");
        GeneralTree<Integer> intTree = new GeneralTree<>();
        boolean actual;

        actual = intTree.add(12, 21);
        assertEquals(false, actual);

        actual = intTree.add(null, 48);
        assertEquals(true, actual);

        actual = intTree.add(48, 12);
        assertEquals(true, actual);

        actual = intTree.add(4, 3);
        assertEquals(false, actual);

        actual = intTree.add(15, null);
        assertEquals(false, actual);

        actual = intTree.add(12, 20);
        assertEquals(true, actual);

        actual = intTree.add( null, 88);
        assertEquals(true, actual);

        actual = intTree.add(20, 123);
        assertEquals(true, actual);

        actual = intTree.add(48, 44);
        assertEquals(true, actual);

        actual = intTree.add(48, 24);
        assertEquals(true, actual);

        actual = intTree.add(123, 35);
        assertEquals(true, actual);

        actual = intTree.add(89, 1225);
        assertEquals(false, actual);

        System.out.println("The last status this tree");
        System.out.println(intTree);
        System.out.println("--------------------------------------------\n\n");
    }

    /**
     * Testing levelOrderSearch method.
     */
    @Test
    void levelOrderSearch() {
        System.out.println("\n--------------------------------------------");
        System.out.println("Creating a general tree to test levelOrderSearch method.");
        GeneralTree<Double> doubleTree = new GeneralTree<>();

        System.out.println("Adding some items to doubleTree...");
        doubleTree.add(null, 12.87);
        doubleTree.add(12.87, 14.2);
        doubleTree.add(12.87, 1.87);
        doubleTree.add(12.87, 9.78);
        doubleTree.add(9.78, 6.51);
        doubleTree.add(9.78, 2.98);
        doubleTree.add(1.87, 111.1);
        doubleTree.add(null, 66.6);
        doubleTree.add(66.6, 79.96);
        doubleTree.add(null, 77.7);
        doubleTree.add(77.7, 324.66);
        System.out.println("The last status of the tree");
        System.out.println(doubleTree);

        BinaryTree.Node<Double> actual;

        System.out.println("Searching for '77.7'");
        actual = doubleTree.levelOrderSearch(77.7);
        if (actual != null)
            System.out.println("Passing...");
        else assert (false);

        System.out.println("\nSearching for '324.6'");
        actual = doubleTree.levelOrderSearch(324.66);
        if (actual != null)
            System.out.println("Passing...");
        else assert (false);

        System.out.println("\nSearching for '34.51'");
        actual = doubleTree.levelOrderSearch(34.51);
        if (actual == null)
            System.out.println("Cannot found...");
        else assert (false);

        System.out.println("\nSearching for '12.87'");
        actual = doubleTree.levelOrderSearch(12.87);
        if (actual != null)
            System.out.println("Passing...");
        else assert (false);

        System.out.println("\nSearching for '6.51'");
        actual = doubleTree.levelOrderSearch(6.51);
        if (actual != null)
            System.out.println("Passing...");
        else assert (false);

        System.out.println("\nSearching for '23.41'");
        actual = doubleTree.levelOrderSearch(23.41);
        if (actual == null)
            System.out.println("Cannot found...");
        else assert (false);

        System.out.println("\nSearching for '43.98'");
        actual = doubleTree.levelOrderSearch(43.98);
        if (actual == null)
            System.out.println("Cannot found...");
        else assert (false);

        System.out.println("--------------------------------------------\n\n");
    }

    /**
     * Testing postOrderSearch method.
     */
    @Test
    void postOrderSearch() {
        System.out.println("\n--------------------------------------------");
        System.out.println("Creating a general tree to test postOrderSearch method.");
        GeneralTree<Character> charTree = new GeneralTree<>();

        System.out.println("Adding some items to charTree...");
        charTree.add(null, 'a');
        charTree.add('a', 't');
        charTree.add(null, 'y');
        charTree.add('y', 'p');
        charTree.add('p', 's');
        charTree.add('p', 'l');
        charTree.add('s', 'r');
        charTree.add('q', 'b');
        charTree.add('y', '[');
        charTree.add('a', 'w');
        charTree.add('t', 'u');
        System.out.println("The last status of the tree");
        System.out.println(charTree);

        BinaryTree.Node<Character> actual;

        System.out.println("Searching for 'y");
        actual = charTree.postOrderSearch('y');
        if (actual != null)
            System.out.println("Passing...");
        else assert (false);

        System.out.println("\nSearching for 'm'");
        actual = charTree.postOrderSearch('m');
        if (actual == null)
            System.out.println("Cannot found...");
        else assert (false);

        System.out.println("\nSearching for 'w'");
        actual = charTree.postOrderSearch('w');
        if (actual != null)
            System.out.println("Passing...");
        else assert (false);

        System.out.println("\nSearching for 'a'");
        actual = charTree.postOrderSearch('a');
        if (actual != null)
            System.out.println("Passing...");
        else assert (false);

        System.out.println("\nSearching for 'r'");
        actual = charTree.postOrderSearch('r');
        if (actual != null)
            System.out.println("Passing...");
        else assert (false);

        System.out.println("\nSearching for 'j'");
        actual = charTree.postOrderSearch('j');
        if (actual == null)
            System.out.println("Cannot found...");
        else assert (false);

        System.out.println("--------------------------------------------\n\n");
    }
}