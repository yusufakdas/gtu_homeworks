package GeneralTree;

public class MainTest {

    public static void main(String[] args) {
        GeneralTree<Integer> numTree = new GeneralTree<>();

        System.out.println("----------------------- Integer Tree ------------------------");
        System.out.println("===== Current State of the Tree");
        System.out.println(numTree);
        System.out.println("_____\n");

        System.out.println("===== Adding an item to root");
        numTree.add(null, 9);
        System.out.println(numTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of " + 9);
        numTree.add(9, 12);
        System.out.println(numTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of " + 9);
        numTree.add(9, 11);
        System.out.println(numTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of " + 9);
        numTree.add(9, 10);
        System.out.println(numTree);
        System.out.println("_____");

        System.out.println("===== Adding a new item as root value");
        numTree.add(null, 27);
        System.out.println(numTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of " + 10);
        numTree.add(10, 8);
        System.out.println(numTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of " + 10);
        numTree.add(10, 5);
        System.out.println(numTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of " + 9);
        numTree.add(9, 17);
        System.out.println(numTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of " + 12);
        numTree.add(12, 6);
        System.out.println(numTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of " + 12);
        numTree.add(12, 24);
        System.out.println(numTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of " + 24);
        numTree.add(24, 25);
        System.out.println(numTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of " + 25);
        numTree.add(25, 278);
        System.out.println(numTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a root value");
        numTree.add(null, 48);
        System.out.println(numTree);
        System.out.println("_____");

        System.out.println("===== Trying to add an item that its parent is not present in the tree");
        numTree.add(98, 13);
        System.out.println(numTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of " + 278);
        numTree.add(278, 55);
        System.out.println(numTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of " + 278);
        numTree.add(278, 19);
        System.out.println(numTree);
        System.out.println("_____");

        System.out.println("\n");
        System.out.println("----------------------- String Tree ------------------------");
        GeneralTree<String> strTree = new GeneralTree<>();

        System.out.println("===== Trying to add an item to empty tree with its parent");
        strTree.add("Osman", "Orhan");
        System.out.println(strTree);
        System.out.println("_____");

        System.out.println("===== Adding an item to root");
        strTree.add(null, "Robert");
        System.out.println(strTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of Robert");
        strTree.add("Robert", "William");
        System.out.println(strTree);
        System.out.println("_____");

        System.out.println("===== Adding a new item as root value");
        strTree.add(null, "William I");
        System.out.println(strTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of William I");
        strTree.add("William I", "William II");
        System.out.println(strTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of William I");
        strTree.add("William I", "Adela");
        System.out.println(strTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of William I");
        strTree.add("William I", "Henry I");
        System.out.println(strTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of Henry I");
        strTree.add("Henry I", "William");
        System.out.println(strTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of Henry I");
        strTree.add("Henry I", "Matilda");
        System.out.println(strTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of Adela");
        strTree.add("Adela", "Stephen");
        System.out.println(strTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of Matilda");
        strTree.add("Matilda", "Henry II");
        System.out.println(strTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of Henry II");
        strTree.add("Henry II", "Henry");
        System.out.println(strTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of Henry II");
        strTree.add("Henry II", "Richard I");
        System.out.println(strTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of Henry II");
        strTree.add("Henry II", "Geoffrey");
        System.out.println(strTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of Henry II");
        strTree.add("Henry II", "John");
        System.out.println(strTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of John");
        strTree.add("John", "Henry III");
        System.out.println(strTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a child of John");
        strTree.add("John", "Richard");
        System.out.println(strTree);
        System.out.println("_____");

        System.out.println("===== Adding an item as a new root");
        strTree.add(null, "Adam");
        System.out.println(strTree);
        System.out.println("_____");

    }
}
