package GeneralTree;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class GeneralTree<E> extends BinaryTree<E> implements Serializable {
    /**
     * Add an item to general tree with respect to its parent.
     * If parent item is null, then adds to root.
     * @param parentItem The parent item of child item.
     * @param childItem The child item to be added
     * @return If addition is ended in success, then return true, false otherwise.
     */
    public boolean add(E parentItem, E childItem) {
        if ((root == null && parentItem != null) || (childItem == null))
            return (false);

        // This indicates that childItem will be new root node
        if (parentItem == null) {
            Node<E> newNode = new Node<>(childItem);
            newNode.left = root;
            root = newNode;
            return (true);
        }

        //Node<E> searchedItem = levelOrderSearch(parentItem);
        Node<E> searchedItem = postOrderSearch(parentItem);
        if (searchedItem != null) {
            Node<E> newNode = new Node<>(childItem);
            if (searchedItem.left != null) {
                searchedItem = searchedItem.left;
                while (searchedItem.right != null)
                    searchedItem = searchedItem.right;
                searchedItem.right = newNode;
            } else {
                searchedItem.left = newNode;
            }
            return (true);
        }
        return (false);
    }

    /**
     * Search for item by traversing level order
     * @param item The item to be searched
     * @return If item is found, then returns its reference, null otherwise.
     */
    protected Node<E> levelOrderSearch(E item) {
        if (root.left == null)
            return (root);

        Queue<Node<E>> nodeQueue = new LinkedList<>();
        Node<E> current = root;
        boolean toggle = true;
        while (current != null) {
            if (item.equals(current.data))
                return (current);
            if (current.left != null && toggle) {
                current = current.left;
                if (current.right != null) {
                    nodeQueue.offer(current);
                    toggle = false;
                }
            } else if (current.right != null) {
                if (item.equals(current.data))
                    return (current);
                current = current.right;
                if (current.left != null) {
                    nodeQueue.offer(current);
                }
            } else {
                current = nodeQueue.poll();
                toggle = true;
            }
        }
        return (null);
    }

    /**
     * Search for item by traversing post order
     * @param item The item to be searched
     * @return If item is found, then returns its reference, null otherwise.
     */
    protected Node<E> postOrderSearch(E item) {
        if (root.left == null)
            return (root);

        Stack<Node<E>> nodeStack = new Stack<>();
        Node<E> current = root;
        nodeStack.add(current);
        boolean toggle = true;
        while (true) {
            if (current.left != null && toggle) {
                current = current.left;
                nodeStack.push(current);
            } else if (current.right != null) {
                current = current.right;
                if (current.left != null) {
                    nodeStack.push(current);
                    toggle = true;
                } else {
                    if (item.equals(current.data))
                        return (current);
                }
            } else {
                current = nodeStack.pop();
                toggle = false;
                if (item.equals(current.data))
                    return (current);
            }
            if (nodeStack.isEmpty())
                break;
        }

        return (null);
    }

    /**
     * Traverse the tree to obtain string representation
     * @param node Node used in recursive calls
     * @param depth The current depth of the tree
     * @param sb String builder reference
     */
    @Override
    protected void preOrderTraverse(Node<E> node, int depth, StringBuilder sb) {
        for (int i = 1; i < depth; ++i)
            sb.append("-");
        sb.append(">");
        if (node == null) {
            sb.append("***\n");
        } else {
            sb.append(node.data);
            sb.append("\n");
            preOrderTraverse(node.left, depth + 1, sb);
            preOrderTraverse(node.right, depth + 1, sb);
        }
    }
}