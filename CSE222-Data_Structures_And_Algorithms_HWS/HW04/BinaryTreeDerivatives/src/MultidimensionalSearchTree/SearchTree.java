package MultidimensionalSearchTree;

public interface SearchTree<E extends Comparable<E>> {

    /**
     * Add the given item tree
     * @param item Item to be added
     * @return if item is added then returns true, false otherwise.
     */
    boolean add(E[] item);

    /**
     * Search for given item in the tree
     * @param item Item to be looked for
     * @return if item is in the tree, returns true, otherwise false.
     */
    boolean contains(E[] item);

    /**
     * Search for given item in the tree
     * @param target The target item to be searched for
     * @return If item is in the tree, returns its reference, otherwise null.
     */
    E[] find(E[] target);

    /**
     * Delete an item from the tree
     * @param target The item to be deleted
     * @return If item is deleted, then return the deleted item's reference, null otherwise
     */
    E[] delete(E[] target);

    /**
     * Remove an item from the tree
     * @param target The item to be removed
     * @return If item is removed, then returns true, false otherwise.
     */
    boolean remove(E[] target);
}
