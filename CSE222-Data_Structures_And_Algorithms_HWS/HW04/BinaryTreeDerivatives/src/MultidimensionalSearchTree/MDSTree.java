package MultidimensionalSearchTree;

import java.io.Serializable;
import java.security.InvalidParameterException;

public class MDSTree<E extends Comparable<E>> extends BinaryTree<E[]>
        implements SearchTree<E>, Serializable {

    /**
     * The dimension of items
     */
    private int dimension;

    /**
     * Boolean value returned by add method.
     */
    private boolean addReturn;

    /**
     * The generic array returned by delete method
     */
    private E[] deleteReturn;

    /**
     * No parameter constructor that set default dimension value to 3
     */
    public MDSTree() {
        dimension = 3;
    }

    /**
     * Constructor that takes dimension as argument
     * @param dimension The dimension
     */
    public MDSTree(int dimension) {
        this.dimension = dimension;
    }

    /**
     * Calculates the index with respect to current level of tree
     * @param depth The current depth of tree
     * @return Calculated index value
     */
    private int calculateTheIndex(int depth) {
        int index = (depth % dimension) - 1;
        // Fix index value if it is less than zero
        if (index < 0)
            index += dimension;
        return (index);
    }

    /**
     * Wrapper add method
     * Adds the given item MDSTree
     * @param item The item to be added
     * @throws InvalidParameterException If the dimension of item is not equals of tree's items' dimension
     * @return if item is added then returns true, false otherwise.
     */
    @Override
    public boolean add(E[] item) {
        if (item.length != dimension)
            throw new InvalidParameterException("The item must be " + dimension + " dimension!");
        root = add(root, 1, item);
        return (addReturn);
    }

    /**
     * Checks out given items to determine whether they are the same or not
     * @param first The first item
     * @param second The second item
     * @return If they are the same, returns true, false otherwise
     */
    private boolean isSameData(E[] first, E[] second) {
        for (int i = 0; i < dimension; ++i)
            if (!first[i].equals(second[i]))
                return (false);
        return (true);
    }

    /**
     * Recursive add method
     * Adds the given item MDSTree
     * @param node the node parameter to be used in recursive calls
     * @param depth Current depth of tree
     * @param item The item to be added
     * @return If item is added then return its reference, null otherwise
     */
    private Node<E[]> add(Node<E[]> node, int depth, E[] item) {

        // If tree(subtree) is empty add item as its root node
        if (node == null) {
            addReturn = true;
            return (new Node<>(item));
        }

        // Specify the index of item with respect to current depth
        int theIndex = calculateTheIndex(depth);

        // Comparison of item to be added and current node's item
        int compare = item[theIndex].compareTo(node.data[theIndex]);

        // Item's indexth. value is equal to current node's data indexth. value
        if (compare == 0) {
            if (isSameData(item, node.data)) { // Make sure whether they are the same items
                addReturn = false;
                return (node);
            }

            // If they are not the same, then call add method using current node's left node recursively.
            node.left = add(node.left, depth + 1, item);
            return node;
        } else if (compare < 0) { // Item is less
            node.left = add(node.left, depth + 1, item);
            return node;
        } else { // Item is greater
            node.right = add(node.right, depth + 1, item);
            return node;
        }
    }

    /**
     * Search for given item in the tree using find method
     * @param item Item to be looked for
     * @return if item is in the tree, returns true, otherwise false.
     */
    @Override
    public boolean contains(E[] item) {
        return (find(item) != null);
    }

    /**
     * Wrapper find method
     * Search for given item in the tree
     * @param target The target item to be searched for
     * @throws InvalidParameterException If the dimension of item is not equals of tree's items' dimension
     * @return If item is in the tree, returns its reference, otherwise null.
     */
    @Override
    public E[] find(E[] target) {
        if (target.length != dimension)
            throw new InvalidParameterException("The item must be " + dimension + " dimension!");
        return (find(root, 1 , target));
    }

    /**
     * Recursive find method
     * Search for given item in the tree
     * @param node the node parameter to be used in recursive calls
     * @param depth The current depth of the tree
     * @param target The target item to be searched for
     * @return If item is in the tree, returns its reference, otherwise null.
     */
    private E[] find(Node<E[]> node, int depth, E[] target) {

        // If the tree(subtree)'s root node is null
        if (node == null)
            return (null);

        // Specify the index of item with respect to current depth
        int theIndex = calculateTheIndex(depth);

        // Comparison of item to be added and current node's item
        int compare = target[theIndex].compareTo(node.data[theIndex]);

        if (compare == 0) {
            if (isSameData(target, node.data))
                return (node.data);
            else
                return (find(node.left, depth + 1, target));
        } else if (compare < 0) {
            return (find(node.left, depth + 1, target));
        } else {
            return (find(node.right, depth + 1, target));
        }
    }

    /**
     * Wrapper delete method
     * Delete an item from the tree
     * @param target The item to be deleted
     * @return If item is deleted, then return the deleted item's reference, null otherwise
     */
    @Override
    public E[] delete(E[] target) {
        if (target.length != dimension)
            throw new InvalidParameterException("The item must be " + dimension + " dimension!");
        root = delete(root, 1, target);
        return (deleteReturn);
    }

    /**
     * Recursive delete method
     * Delete an item from the tree
     * @param node Node used in recursive calls
     * @param depth The current depth of the tree
     * @param target The item to be deleted from tree
     * @return If item is deleted successfully, then return the deleted node reference, null otherwise.
     */
    private Node<E[]> delete(Node<E[]> node, int depth, E[] target) {
        if (node == null) {
            deleteReturn = null;
            return (null);
        }

        int theIndex = calculateTheIndex(depth);
        int compare = target[theIndex].compareTo(node.data[theIndex]);
        if (compare < 0) {
            node.left = delete(node.left, depth + 1, target);
            return (node);
        } else if (compare > 0) {
            node.right = delete(node.right, depth + 1, target);
            return (node);
        } else {
            if (isSameData(target, node.data)) {
                deleteReturn = node.data;
                if (node.left == null) {
                    return (node.right);
                } else if (node.right == null) {
                    return (node.left);
                } else {
                    if (node.left.right == null) {
                        node.data = node.left.data;
                        node.left = node.left.left;
                        return (node);
                    } else { // CONTROL
                        Node<E[]> tmpNode = node.left;
                        while (tmpNode.right.right != null)
                            tmpNode = tmpNode.right;
                        deleteReturn = tmpNode.right.data;
                        node.data = tmpNode.right.data;
                        tmpNode.right = tmpNode.right.left;
                        return (node);
                    }
                }
            } else {
                node.left = delete(node.left, depth + 1, target);
                return (node);
            }
        }
    }

    /**
     * Remove an item from the tree
     * @param target The item to be removed
     * @return If item is removed, then returns true, false otherwise.
     */
    @Override
    public boolean remove(E[] target) {
        return (delete(target) != null);
    }

    /**
     * Add current node's data to a string using pre-order traverse
     * @param node Node to be used in recursive calls
     * @param depth Current depth of the tree
     * @param sb String builder parameter
     */
    protected void preOrderTraverse(Node<E[]> node, int depth, StringBuilder sb) {
        for (int i = 1; i < depth; ++i)
            sb.append(">");
        sb.append(">");
        if (node == null) {
            sb.append("_______\n");
        } else {
            sb.append("(");
            for (int i = 0; i < dimension - 1; ++i) {
                sb.append(node.data[i]);
                sb.append(", ");
            }
            sb.append(node.data[dimension - 1]);
            sb.append(")\n");
            preOrderTraverse(node.left, depth + 1, sb);
            preOrderTraverse(node.right, depth + 1, sb);
        }
    }

}
