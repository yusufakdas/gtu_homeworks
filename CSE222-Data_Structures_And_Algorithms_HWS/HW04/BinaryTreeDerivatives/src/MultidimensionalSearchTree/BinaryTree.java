package MultidimensionalSearchTree;

import java.io.Serializable;

public class BinaryTree<E> implements Serializable {

    /**
     * The root of the binary tree
     */
    protected Node<E> root;

    /**
     * The default constructor
     */
    public BinaryTree() {
        root = null;
    }

    /**
     * The constructor that takes a node as its argument
     * @param root The node
     */
    protected BinaryTree(Node<E> root) {
        this.root = root;
    }

    /**
     * Constructs a new binary tree with data in its root,leftTree
     * as its left subtree and rightTree as its right subtree.
     * @param data data
     * @param leftTree Left tree
     * @param rightTree Right tree
     */
    protected BinaryTree(E data, BinaryTree<E> leftTree, BinaryTree<E> rightTree) {
        root = new Node<>(data);
        if (leftTree != null)
            root.left = leftTree.root;
        else
            root.left = null;

        if (rightTree != null)
            root.right = rightTree.root;
        else
            root.right = null;
    }
    /**
     * Return the left subtree.
     * @return The left subtree or null if either the root or the left subtree is null
     */
    public BinaryTree<E> getLeftSubtree() {
        if (root != null && root.left != null)
            return (new BinaryTree<>(root.left));
        return (null);
    }

    /**
     * Return the right sub-tree
     * @return the right sub-tree or null if either the root or the right subtree is null.
     */
    public BinaryTree<E> getRightSubtree() {
        if (root != null && root.right != null)
            return (new BinaryTree<>(root.right));
        return (null);
    }

    /**
     * Return the data field of the root
     * @return the data field of the root or null if the root is null
     */
    public E getData() {
        if (root != null)
            return (root.data);
        return (null);
    }

    /**
     * Determine whether this tree is a leaf.
     * @return true if the root has no children
     */
    public boolean isLeaf() {
        return (root != null && root.left == null && root.right == null);
    }

    /** Perform a pre order traversal.
     @param node The local root
     @param depth The depth
     @param sb The string buffer to save the output
     */
    protected void preOrderTraverse(Node<E> node, int depth, StringBuilder sb) {
        for (int i = 1; i < depth; ++i)
            sb.append(" ");
        if (node == null) {
            sb.append("null\n");
        } else {
            sb.append(node.toString());
            sb.append("\n");
            preOrderTraverse(node.left, depth + 1, sb);
            preOrderTraverse(node.right, depth + 1, sb);
        }
    }

    /**
     * Override toString method
     * @return The string representation of binary tree by traversing pre order
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        preOrderTraverse(root, 1, sb);
        return (sb.toString());
    }

    /**
     * Class to encapsulate a tree node.
     */
    protected static class Node<E> implements Serializable {

        /**
         * The information stored in this node.
         */
        protected E data;

        /**
         * Reference to the left child.
         */
        protected Node<E> left;

        /**
         * Reference to the right child.
         */
        protected Node<E> right;

        /**
         * Construct a node with given data and no children.
         * @param data The data to store in this node
         */
        public Node(E data) {
            this.data = data;
            left = null;
            right = null;
        }

        /**
         * Return a string representation of the node.
         * @return A string representation of the data fields
         */
        @Override
        public String toString() {
            return (data.toString());
        }

    }
}
