package MultidimensionalSearchTree;

public class MainTest {

    public static void main(String[] args) {
        System.out.println("Testing a two dimension integer search tree...");
        MDSTree<Integer> mds = new MDSTree<>(2);

        System.out.println("Adding elements to the tree");
        System.out.println("Add {40, 45}");
        mds.add(new Integer[]{40, 45});
        System.out.println(mds);
        System.out.println();

        System.out.println("Add {15, 70}");
        mds.add(new Integer[]{15, 70});
        System.out.println(mds);
        System.out.println();

        System.out.println("Add {70, 10}");
        mds.add(new Integer[]{70, 10});
        System.out.println(mds);
        System.out.println();

        System.out.println("Add {69, 50}");
        mds.add(new Integer[]{69, 50});
        System.out.println(mds);
        System.out.println();

        System.out.println("Add {66, 85}");
        mds.add(new Integer[]{66, 85});
        System.out.println(mds);
        System.out.println();

        System.out.println("Add {85, 90}");
        mds.add(new Integer[]{85, 90});
        System.out.println(mds);
        System.out.println();

        System.out.println("Add {20, 20}");
        mds.add(new Integer[]{20, 20});
        System.out.println(mds);
        System.out.println();

        System.out.println("Add {48, 40}");
        mds.add(new Integer[]{48, 40});
        System.out.println(mds);
        System.out.println();

        System.out.println("Add {15, 89}");
        mds.add(new Integer[]{15, 89});
        System.out.println(mds);
        System.out.println();

        System.out.println();
        System.out.println("Deleting elements from the tree");
        System.out.println("Delete {15, 89}");
        mds.delete(new Integer[]{85, 90});
        System.out.println(mds);
        System.out.println();

        System.out.println("Delete {48, 40}");
        mds.delete(new Integer[]{48, 40});
        System.out.println(mds);
        System.out.println();

        System.out.println("Delete {69, 50}");
        mds.delete(new Integer[]{69, 50});
        System.out.println(mds);
        System.out.println();

        System.out.println("Delete {70, 10}");
        mds.delete(new Integer[]{70, 10});
        System.out.println(mds);
        System.out.println();

        System.out.println("Delete {72, 10}");
        mds.delete(new Integer[]{72, 10});
        System.out.println(mds);
        System.out.println();
    }

}