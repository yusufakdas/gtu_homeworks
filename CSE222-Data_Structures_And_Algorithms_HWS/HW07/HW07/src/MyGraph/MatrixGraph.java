package MyGraph;

import java.io.IOException;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;

public class MatrixGraph extends AbstractGraph {
    private double[][] edges;

    public MatrixGraph(int numV, boolean isDirected) {
        super(numV, isDirected);
        edges = new double[numV][numV];
        for (int row = 0; row < numV; ++row)
            for (int col = 0; col < numV; ++col)
                edges[row][col] = Double.POSITIVE_INFINITY;
    }

    @Override
    public void insert(Edge edge) {
        edges[edge.getSource()][edge.getDest()] = edge.getWeight();
        if (!isDirected())
            edges[edge.getDest()][edge.getSource()] = edge.getWeight();
    }

    @Override
    public boolean isEdge(int source, int dest) {
        return (edges[source][dest] != Double.POSITIVE_INFINITY);
    }

    @Override
    public Edge getEdge(int source, int dest) {
        return (edges[source][dest] == Double.POSITIVE_INFINITY ? null :
                new Edge(source, dest, edges[source][dest]));
    }

    public void randomizeAllEdgeWeight() {
        Random rand = new Random();
        for (int i = 0; i < getNumV(); ++i) {
            for (int j = 0; j < getNumV(); ++j) {
                if (edges[i][j] != Double.POSITIVE_INFINITY)
                    edges[i][j] = rand.nextInt(150);
            }
        }
    }

    @Override
    public Iterator<Edge> edgeIterator(int source) {
        return (new Iter(source));
    }

    private class Iter
            implements Iterator < Edge > {
        // Data fields
        /** The source vertix for this iterator */
        private int source;

        /** The current index for this iterator */
        private int index;

        // Constructor
        /** Construct an EdgeIterator for a given vertix
         *  @param source - The source vertix
         */
        public Iter(int source) {
            this.source = source;
            index = -1;
            advanceIndex();
        }

        /** Return true if there are more edges
         *  @return true if there are more edges
         */
        public boolean hasNext() {
            return index != getNumV();
        }

        /** Return the next edge if there is one
         *  more edges
         *  @return the next Edge in the iteration
         */
        public Edge next() {
            if (index == getNumV()) {
                throw new java.util.NoSuchElementException();
            }
            Edge returnValue = new Edge(source, index,
                    edges[source][index]);
            advanceIndex();
            return returnValue;
        }

        /** Remove is not implememted
         */
        public void remove() {
            throw new UnsupportedOperationException();
        }

        /** Advance the index to the next edge */
        private void advanceIndex() {
            do {
                index++;
            }
            while (index != getNumV()
                    && Double.POSITIVE_INFINITY == edges[source][index]);
        }
    }
}
