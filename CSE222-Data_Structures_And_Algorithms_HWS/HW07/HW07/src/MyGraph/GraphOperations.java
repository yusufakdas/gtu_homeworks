package MyGraph;

import java.security.InvalidParameterException;
import java.util.*;

public class GraphOperations {

    private GraphOperations() {}

    /**
     * Determine if there is any path between vertex v1 and vertex ​ v2 in graph ​g.
     * If v1​ or v2​ are not in g then throw an error.
     * @param graph a graph object
     * @param v1 a vertex label in ​graph
     * @param v2 a vertex label in ​graph
     * @return ​true if there is a path from v1​ to​ v2​ in g, false if not.
     */
    public static boolean is_connected(Graph graph, int v1, int v2) {
        if (v2 >= graph.getNumV())
            throw new InvalidParameterException("**** Invalid Edge Label ****");
        int[] parentArray = BreadthFirstSearch.breadthFirstSearch(graph, v1);
        int[] values = new int[graph.getNumV()];
        int currentIndex = parentArray[v2], previousIndex = v2;
        if (parentArray[v2] == -1)
            return (false);
        for (int i = 0; i < values.length; ++i)
            values[i] = i;
        while (parentArray[currentIndex] != -1) {
            previousIndex = currentIndex;
            currentIndex = parentArray[currentIndex];
        }
        return (values[parentArray[previousIndex]] == v1);
    }

    /**
     * Shortest path between two edges
     * @param v1 a vertex label in g
     * @param v2 a vertex label in g
     * @return a vector of the names of vertices that make up the shortest path,
     * in order If there is no path between the vertices then return an empty
     * vector; ​distance​, total weight of path.
     */
    public static Vector<Integer> shortest_path(Graph graph, int v1, int v2) {
        Vector<Integer> vector = new Vector<>();
        if (is_connected(graph, v1, v2)) {
            int numV = graph.getNumV();
            if (v1 >= numV || v2 >= numV || v1 < 0 || v2 < 0)
                return (vector);
            Stack<Integer> reversePath = new Stack<>();
            int current = v2;
            int[] pred = new int[numV];
            double[] destination = new double[numV];
            DijkstraShortestPath.dijkstraAlgorithm(graph, v1, pred, destination);

            while (pred[current] != -1) {
                reversePath.push(current);
                current = pred[current];
            }
            reversePath.push(current);
            while (!reversePath.empty())
                vector.add(reversePath.pop());
        }
        return (vector);
    }

    /**
     * Check if the graph object is undirected, this is true if all directed edges
     * have a complementary directed edge with the same weight in the opposite
     * direction.
     * @return ​TRUE​ if g ​is undirected, ​FALSE​ if not
     */
    public static boolean is_undirected(Graph graph) {
        boolean found = false;
        int vertexNumbers = graph.getNumV();
        for (int i = 0; i < vertexNumbers; ++i) {
            Iterator<Edge> outerIter = graph.edgeIterator(i);
            while (outerIter.hasNext()) {
                Edge outerEdge = outerIter.next();
                Iterator<Edge> innerIter = graph.edgeIterator(outerEdge.getDest());
                while (innerIter.hasNext() && !found) {
                    Edge innerEdge = innerIter.next();
                    if (compareTwoEdgesForDirected(innerEdge, outerEdge))
                        found = true;
                }
                if (!found)
                    return (false);
                found = false;
            }
        }
        return (true);
    }

    /**
     *
     * CAUTION: This method is designed for undirected graph, so it may not work
     * properly for directed graphs
     * @param graph a graph object.
     * @return ​ TRUE​ if ​g ​is acyclic, ​FALSE​ if not
     */
    public static boolean is_acyclic_graph(Graph graph) {
        Set<Integer> discoverySet = new HashSet<>();
        Stack<Integer> stack = new Stack<>();
        boolean[] isVisited = new boolean[graph.getNumV()];
        int i = 0;
        Iterator<Edge> edgeIterator = graph.edgeIterator(i);
        while (!edgeIterator.hasNext()) {
            edgeIterator = graph.edgeIterator(i);
            ++i;
        }
        stack.push(edgeIterator.next().getSource());
        while (!stack.empty()) {
            int element = stack.pop();
            if (discoverySet.contains(element))
                return (false);
            discoverySet.add(element);
            isVisited[element] = true;
            edgeIterator = graph.edgeIterator(element);
            while (edgeIterator.hasNext()) {
                int value = edgeIterator.next().getDest();
                if (!isVisited[value])
                    stack.push(value);
            }
        }
        return (true);
    }

    /**
     * Plot showing all vertices (labeled) and edges.
     * @param graph a graph object
     */
    public static void plot_graph(Graph graph) {
        int numberOfVertices = graph.getNumV();
        for (int vertex = 0; vertex < numberOfVertices; ++vertex) {
            Iterator<Edge> iterator = graph.edgeIterator(vertex);
            System.out.print(vertex + " |");
            Edge edge;
            while (iterator.hasNext()) {
                edge = iterator.next();
                if (iterator.hasNext()) {
                    System.out.print("{" + edge.getSource() + "->"
                            + edge.getDest() + ", [" + edge.getWeight() + "]}"
                            + " ---> ");
                } else {
                    System.out.print("{" + edge.getSource() + "->"
                            + edge.getDest() + ", [" + edge.getWeight() + "]}");
                }
            }
            System.out.println();
        }
    }

    /**
     * prints vector on the screen
     * @param vec vector to be printed
     */
    public static void printVector(Vector<Integer> vec) {
        for (Integer i : vec)
            System.out.print(i + " ");
        System.out.println();
    }

    private static boolean compareTwoEdgesForDirected(Edge e1, Edge e2) {
        return (e1.getSource() == e2.getDest()
                && e1.getDest() == e2.getSource()
                && e1.getWeight() == e2.getWeight());
    }
}
