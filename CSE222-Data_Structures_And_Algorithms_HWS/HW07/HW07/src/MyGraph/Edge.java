package MyGraph;

import java.util.Objects;

public class Edge {
    private int dest;
    private int source;
    private double weight;

    public Edge(int source, int dest) {
        this(source, dest, 1.0);
    }

    public Edge(int source, int dest, double weight) {
        this.source = source;
        this.dest = dest;
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return (false);
        if (!(o instanceof Edge)) return (false);
        if (o == this) return (true);

        Edge temp = (Edge) o;
        return (temp.source == source && temp.dest == dest);
    }

    public int getDest() {
        return (dest);
    }

    public int getSource() {
        return (source);
    }

    public double getWeight() {
        return (weight);
    }

    public void setWeight(double w) {
        weight = w;
    }

    @Override
    public String toString() {
        return ("(" + getSource() + ", " + getDest()
                + ", " + getWeight() + ")");
    }

    @Override
    public int hashCode() {
        return Objects.hash(dest, source, weight);
    }
}