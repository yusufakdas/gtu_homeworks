package Q2;

import MyGraph.AbstractGraph;
import MyGraph.Graph;
import MyGraph.GraphOperations;

import java.io.File;
import java.util.Scanner;

public class Q2Test {

    public static void main(String[] args) {
        try {
            Scanner scan = new Scanner(new File("./src/Q2/Q2Input"));
            Graph graph = AbstractGraph.createGraph(scan, false, "list");
            GraphOperations.plot_graph(graph);
            System.out.println();

            boolean result = GraphOperations.is_acyclic_graph(graph);
            System.out.println("Is acyclic graph?: " + result);

            result = GraphOperations.is_undirected(graph);
            System.out.println("Is undirected graph? :" + result);
            System.out.println();

            result = GraphOperations.is_connected(graph, 6, 14);
            System.out.println("Is connected 6, 14 ?: " + result);

            result = GraphOperations.is_connected(graph, 3, 5);
            System.out.println("Is connected 3, 5 ?: " + result);

            result = GraphOperations.is_connected(graph, 12, 7);
            System.out.println("Is connected 12, 7 ?: " + result);

            result = GraphOperations.is_connected(graph, 10, 13);
            System.out.println("Is connected 10, 13 ?: " + result);

            result = GraphOperations.is_connected(graph, 11, 6);
            System.out.println("Is connected 11, 6 ?: " + result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
