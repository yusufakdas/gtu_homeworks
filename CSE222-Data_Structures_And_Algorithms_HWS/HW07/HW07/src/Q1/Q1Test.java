package Q1;

import MyGraph.*;

import java.io.File;
import java.util.Scanner;
import java.util.Vector;

public class Q1Test {

    public static void main(String[] args) {
        try {
            Scanner scan = new Scanner(new File("./src/Q1/Q1Input"));
            Graph graph = AbstractGraph.createGraph(scan, true, "matrix");
            ((MatrixGraph)graph).randomizeAllEdgeWeight();
            GraphOperations.plot_graph(graph);
            System.out.println();

            boolean result = GraphOperations.is_acyclic_graph(graph);
            System.out.println("Is acyclic graph?: " + result);

            result = GraphOperations.is_undirected(graph);
            System.out.println("Is undirected graph? :" + result);
            Vector<Integer> path;

            System.out.println("Shortest path from 0 to 4");
            path = GraphOperations.shortest_path(graph, 0, 4);
            GraphOperations.printVector(path);
            System.out.println();

            System.out.println("Shortest path from 6 to 3");
            path = GraphOperations.shortest_path(graph, 6, 3);
            GraphOperations.printVector(path);
            System.out.println();

            System.out.println("Shortest path from 1 to 3");
            path = GraphOperations.shortest_path(graph, 1, 3);
            GraphOperations.printVector(path);
            System.out.println();

            System.out.println("Shortest path from 0 to 5");
            path = GraphOperations.shortest_path(graph, 0, 5);
            GraphOperations.printVector(path);
            System.out.println();

            System.out.println("Shortest path from 1 to 5");
            path = GraphOperations.shortest_path(graph, 1, 5);
            GraphOperations.printVector(path);
            System.out.println();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}