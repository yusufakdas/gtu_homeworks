package Q3;

import MyGraph.*;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Q3Test {

    public static void main(String[] args) {
        try {
            Scanner scan = new Scanner(new File("./src/Q3/Q3Input"));
            Graph graph = AbstractGraph.createGraph(scan, false, "list");
            GraphOperations.plot_graph(graph);
            System.out.println();

            boolean result = GraphOperations.is_acyclic_graph(graph);
            System.out.println("Is acyclic graph? : " + result);

            result = GraphOperations.is_undirected(graph);
            System.out.println("Is undirected graph? :" + result);
            System.out.println();

            int vertexCount = graph.getNumV();
            int[] parentArray;

            System.out.println("BREADTH-FIRST SEARCH ALL SPANNING TREES");
            for (int i = 0; i < vertexCount; ++i) {
                parentArray = BreadthFirstSearch.breadthFirstSearch(graph, i);
                printSpanningTree(parentArray);
                System.out.println();
            }
            System.out.println("----------------------------------------------");

            System.out.println("DEPTH-FIRST SEARCH A SPANNING TREE");
            DepthFirstSearch depthFirstSearch = new DepthFirstSearch(graph);
            parentArray = depthFirstSearch.getParent();
            printSpanningTree(parentArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static int[] getDepthArray(int[] parentArray) {
        int[] depths = new int[parentArray.length];
        for (int i = 0; i < parentArray.length; ++i) {
            int count = 0;
            int current = i;
            while (parentArray[current] != -1) {
                ++count;
                current = parentArray[current];
            }
            depths[i] = count + 1;
        }
        return (depths);
    }

    private static void printSpanningTree(int[] parentArray) {
        int[] depth = getDepthArray(parentArray);
        Map<Integer, Integer> depthMap = new HashMap<>();
        for (int i = 0; i < depth.length; ++i)
            depthMap.put(i, depth[i]);
        depthMap.entrySet().stream()
                .sorted((k1, k2) -> k1.getValue().compareTo(k2.getValue()))
                .forEach(k -> {
                    for (int i = 0; i < k.getValue(); ++i)
                        System.out.print("-");
                    System.out.println(">" + (k.getKey()));
                });
    }
}
