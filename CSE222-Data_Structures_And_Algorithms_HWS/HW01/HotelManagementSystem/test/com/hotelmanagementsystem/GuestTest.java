package com.hotelmanagementsystem;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GuestTest {

    @Test
    void cancelReservation() {
        System.out.println("~~~~~~ Guest cancelReservation method test! ~~~~~~");

        HotelManagementSystem sys = new HotelManagementSystem();

        Guest g1 = new Guest("Michael", "Jackson", "123");
        Guest g2 = new Guest("Vinnie", "Paz", "456");
        Guest g3 = new Guest("Yusuf", "A", "789");

        sys.addGuest(g1);
        sys.addGuest(g2);

        boolean actual;

        try {
            g1.bookARoom(sys.getAndControlRoom(12));
            g1.bookARoom(sys.getAndControlRoom(88));
            g2.bookARoom(sys.getAndControlRoom(15));
            g1.bookARoom(sys.getAndControlRoom(59));
            g2.bookARoom(sys.getAndControlRoom(7));
            g1.bookARoom(sys.getAndControlRoom(24));
            g2.bookARoom(sys.getAndControlRoom(75));

            System.out.println("\n------ Test starts from here ------");
            actual = g3.cancelReservation(sys.getAndControlRoom(78));
            assertEquals(false, actual);

            actual = g1.cancelReservation(sys.getAndControlRoom(59));
            assertEquals(true, actual);

            actual = g2.cancelReservation(sys.getAndControlRoom(7));
            assertEquals(true, actual);

            actual = g2.cancelReservation(sys.getAndControlRoom(45));
            assertEquals(false, actual);

            actual = g1.cancelReservation(sys.getAndControlRoom(75));
            assertEquals(false, actual);

            actual = g1.cancelReservation(sys.getAndControlRoom(12));
            assertEquals(true, actual);

            actual = g1.cancelReservation(sys.getAndControlRoom(12));
            assertEquals(false, actual);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            sys.deleteFiles();
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
        }
    }

    @Test
    void bookARoom() {
        System.out.println("~~~~~~ Guest bookARoom method test! ~~~~~~");

        HotelManagementSystem sys = new HotelManagementSystem();

        Guest g1 = new Guest("Michael", "Jackson", "123");
        Guest g2 = new Guest("Vinnie", "Paz", "456");
        Guest g3 = new Guest("Yusuf", "A", "789");

        sys.addGuest(g1);
        sys.addGuest(g2);

        boolean actual;

        try {
            System.out.println("\n------ Test starts from here ------");
            actual = g3.bookARoom(sys.getAndControlRoom(48));
            assertEquals(false, actual);

            actual = g1.bookARoom(sys.getAndControlRoom(55));
            assertEquals(true, actual);

            actual = g2.bookARoom(sys.getAndControlRoom(55));
            assertEquals(false, actual);

            actual = g1.bookARoom(sys.getAndControlRoom(13));
            assertEquals(true, actual);

            actual = g2.bookARoom(sys.getAndControlRoom(75));
            assertEquals(true, actual);

            actual = g2.bookARoom(sys.getAndControlRoom(75));
            assertEquals(false, actual);

            actual = g1.bookARoom(sys.getAndControlRoom(546));
            assertEquals(false, actual);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            sys.deleteFiles();
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
        }
    }

}