package com.hotelmanagementsystem;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReceptionistTest {

    @Test
    void bookARoom() {
        System.out.println("~~~~~~ Receptionist bookARoom method test! ~~~~~~");

        HotelManagementSystem sys = new HotelManagementSystem();

        Guest g1 = new Guest("Michael", "Jackson", "123");
        Guest g2 = new Guest("Vinnie", "Paz", "456");
        Guest g3 = new Guest("Samet", "S", "239");
        Guest g4 = new Guest("Ayse", "H", "197" );
        Receptionist r1 = new Receptionist("Yusuf", "A", "66");
        Receptionist r2 = new Receptionist("Muhammad", "Ali", "159");

        sys.addGuest(g1);
        sys.addGuest(g2);
        sys.addGuest(g3);
        sys.addReceptionist(r1);
        sys.addReceptionist(r2);

        boolean actual;

        try {
            System.out.println("\n------ Test starts from here ------");
            actual = r1.bookARoom(g3, sys.getAndControlRoom(1));
            assertEquals(true, actual);

            actual = r2.bookARoom(g2, sys.getAndControlRoom(1));
            assertEquals(false, actual);

            actual = r1.bookARoom(g4, sys.getAndControlRoom(89));
            assertEquals(false, actual);

            actual = r2.bookARoom(r1, sys.getAndControlRoom(57));
            assertEquals(false, actual);

            actual = r2.bookARoom(g1, sys.getAndControlRoom(8));
            assertEquals(true, actual);

            actual = r1.bookARoom(g2, sys.getAndControlRoom(78));
            assertEquals(true, actual);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            sys.deleteFiles();
            System.out.println("+++++++++++++++++++++++++++++++++++++++++\n\n");
        }
    }

    @Test
    void cancelReservation() {
        System.out.println("~~~~~~ Receptionist cancelReservation method test! ~~~~~~");

        HotelManagementSystem sys = new HotelManagementSystem();

        Guest g1 = new Guest("Michael", "Jackson", "123");
        Guest g2 = new Guest("Vinnie", "Paz", "456");
        Guest g3 = new Guest("Samet", "S", "239");
        Guest g4 = new Guest("Ayse", "H", "197" );
        Receptionist r1 = new Receptionist("Yusuf", "A", "66");
        Receptionist r2 = new Receptionist("Muhammad", "Ali", "159");

        sys.addGuest(g1);
        sys.addGuest(g2);
        sys.addGuest(g3);
        sys.addReceptionist(r1);
        sys.addReceptionist(r2);

        boolean actual;

        try {
            System.out.println();
            r1.bookARoom(g3, sys.getAndControlRoom(1));
            r2.bookARoom(g2, sys.getAndControlRoom(1));
            r1.bookARoom(g4, sys.getAndControlRoom(97));
            r2.bookARoom(r1, sys.getAndControlRoom(54));
            r2.bookARoom(g1, sys.getAndControlRoom(8));
            r1.bookARoom(g2, sys.getAndControlRoom(12));
            g1.bookARoom(sys.getAndControlRoom(38));

            System.out.println("\n------ Test starts from here ------");
            actual = r1.cancelReservation(sys.getAndControlRoom(8));
            assertEquals(true, actual);

            actual = r2.cancelReservation(sys.getAndControlRoom(97));
            assertEquals(false, actual);

            actual = r1.cancelReservation(sys.getAndControlRoom(54));
            assertEquals(false, actual);

            actual = r1.cancelReservation(sys.getAndControlRoom(8));
            assertEquals(false, actual);

            actual = r2.cancelReservation(sys.getAndControlRoom(12));
            assertEquals(true, actual);

            actual = r1.cancelReservation(sys.getAndControlRoom(38));
            assertEquals(true, actual);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            sys.deleteFiles();
            System.out.println("+++++++++++++++++++++++++++++++++++++++++\n\n");
        }
    }

    @Test
    void checkIn() {
        System.out.println("~~~~~~ Receptionist checkIn method test! ~~~~~~");

        HotelManagementSystem sys = new HotelManagementSystem();

        Guest g1 = new Guest("Michael", "Jackson", "123");
        Guest g2 = new Guest("Vinnie", "Paz", "456");
        Guest g3 = new Guest("Samet", "S", "239");
        Guest g4 = new Guest("Ayse", "H", "197" );
        Receptionist r1 = new Receptionist("Yusuf", "A", "66");
        Receptionist r2 = new Receptionist("Muhammad", "Ali", "159");

        sys.addGuest(g1);
        sys.addGuest(g2);
        sys.addGuest(g3);
        sys.addReceptionist(r1);
        sys.addReceptionist(r2);

        boolean actual;

        try {
            System.out.println();
            g3.bookARoom(sys.getAndControlRoom(1));
            g2.bookARoom(sys.getAndControlRoom(1));
            r1.bookARoom(g4, sys.getAndControlRoom(97));
            r2.bookARoom(r1, sys.getAndControlRoom(45));
            g2.bookARoom(sys.getAndControlRoom(88));
            g1.bookARoom(sys.getAndControlRoom(8));
            r1.bookARoom(g2, sys.getAndControlRoom(74));
            g1.bookARoom(sys.getAndControlRoom(38));

            System.out.println("\n------ Test starts from here ------");
            DateInterface start = new Date(1, 1, 2010);
            DateInterface end = new Date(1, 2, 2010);
            actual = r1.checkIn(sys.getAndControlRoom(8), start, end);
            assertEquals(true, actual);

            start = new Date(12, 10, 2008);
            end = new Date(10, 10, 2007);
            actual = r1.checkIn(sys.getAndControlRoom(1), start, end);
            assertEquals(false, actual);

            start = new Date(18, 3, 2012);
            end = new Date(20, 3, 2012);
            actual = r2.checkIn(sys.getAndControlRoom(1), start, end);
            assertEquals(true, actual);

            actual = r1.checkIn(sys.getAndControlRoom(97), start, end);
            assertEquals(false, actual);

            actual = r2.checkIn(sys.getAndControlRoom(41), start, end);
            assertEquals(false, actual);

            actual = r1.checkIn(sys.getAndControlRoom(13), start, end);
            assertEquals(false, actual);

            start = new Date(1, 3, 2001);
            end = new Date(1, 2, 2002);
            actual = r2.checkIn(sys.getAndControlRoom(1), start, end);
            assertEquals(false, actual);

            actual = r2.checkIn(sys.getAndControlRoom(88), start, end);
            assertEquals(true, actual);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            sys.deleteFiles();
            System.out.println("+++++++++++++++++++++++++++++++++++++++++\n\n");
        }
    }

    @Test
    void checkOut() {
        System.out.println("~~~~~~ Receptionist checkOut method test! ~~~~~~");

        HotelManagementSystem sys = new HotelManagementSystem();

        Guest g1 = new Guest("Michael", "Jackson", "123");
        Guest g2 = new Guest("Vinnie", "Paz", "456");
        Guest g3 = new Guest("Samet", "S", "239");
        Guest g4 = new Guest("Ayse", "H", "197" );
        Receptionist r1 = new Receptionist("Yusuf", "A", "66");
        Receptionist r2 = new Receptionist("Muhammad", "Ali", "159");

        sys.addGuest(g1);
        sys.addGuest(g2);
        sys.addGuest(g3);
        sys.addReceptionist(r1);
        sys.addReceptionist(r2);

        boolean actual;

        try {
            System.out.println();
            g3.bookARoom(sys.getAndControlRoom(1));
            g2.bookARoom(sys.getAndControlRoom(1));
            r1.bookARoom(g4, sys.getAndControlRoom(96));
            r2.bookARoom(r1, sys.getAndControlRoom(45));
            g2.bookARoom(sys.getAndControlRoom(88));
            g1.bookARoom(sys.getAndControlRoom(8));
            r1.bookARoom(g2, sys.getAndControlRoom(12));
            g1.bookARoom(sys.getAndControlRoom(38));

            System.out.println();
            DateInterface start = new Date(1, 1, 2010);
            DateInterface end = new Date(1, 2, 2010);
            r1.checkIn(sys.getAndControlRoom(8), start, end);

            start = new Date(18, 3, 2012);
            end = new Date(20, 4, 2012);
            r2.checkIn(sys.getAndControlRoom(1), start, end);

            start = new Date(24, 7, 2006);
            end = new Date(29, 9, 2006);
            r2.checkIn(sys.getAndControlRoom(88), start, end);

            start = new Date(15, 9, 2003);
            end = new Date(20, 9, 2003);
            r1.checkIn(sys.getAndControlRoom(74), start, end);

            System.out.println("\n------ Test starts from here ------");
            actual = r2.checkOut(sys.getAndControlRoom(74));
            assertEquals(false, actual);

            actual = r1.checkOut(sys.getAndControlRoom(68));
            assertEquals(false, actual);

            actual = r2.checkOut(sys.getAndControlRoom(8));
            assertEquals(true, actual);

            actual = r1.checkOut(sys.getAndControlRoom(1));
            assertEquals(true, actual);

            actual = r1.checkOut(sys.getAndControlRoom(88));
            assertEquals(true, actual);

            actual = r1.checkOut(sys.getAndControlRoom(8));
            assertEquals(false, actual);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            sys.deleteFiles();
            System.out.println("+++++++++++++++++++++++++++++++++++++++++\n\n");
        }
    }
}