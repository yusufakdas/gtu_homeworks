/*
 * author: @yusufakdas
 */

package com.hotelmanagementsystem;

/**
 * Interface for a person
 */
public interface Person {

    /**
     * Accessor method for person name
     */
    String getName();

    /**
     * Mutator method for person name
     */
    void setName(String name);

    /**
     * Accessor method for person surname
     */
    String getSurname();

    /**
     * Mutator method for person surname
     */
    void setSurname(String surname);

    /**
     * Accessor method for person ID
     */
    String getID();

    /**
     * Mutator method for person ID
     */
    void setID(String id);

}
