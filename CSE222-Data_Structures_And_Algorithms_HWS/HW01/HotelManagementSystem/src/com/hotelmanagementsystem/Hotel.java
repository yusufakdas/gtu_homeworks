/*
 * author: @yusufakdas
 */

package com.hotelmanagementsystem;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Hotel class
 */
public class Hotel {

    /**
     * Rooms of Hotel
     */
    private List<Room> mRooms;

    /**
     * Number of Room in the Hotel
     */
    private final int NUMBER_OF_ROOM = 100;

    /**
     * Price for standard room ($)
     */
    private final int PRICE_STANDARD_ROOMS = 50;

    /**
     * Price for exclusive room ($)
     */
    private final int PRICE_EXCLUSIVE_ROOMS = 250;

    /**
     * No parameter constructor
     */
    public Hotel() {
        mRooms = new ArrayList<>();
        initRooms();
        initStatusOfRooms();
    }

    /**
     * Add rooms to ArrayList. First 50 rooms (0 - 49) standard room,
     * Second 50 rooms (50 - 99) exclusive rooms
     */
    private void initRooms() {
        for (int i = 0; i < NUMBER_OF_ROOM / 2; ++i)
            mRooms.add(new StandardRoom(String.valueOf(i), PRICE_STANDARD_ROOMS));
        for (int i = NUMBER_OF_ROOM / 2; i < NUMBER_OF_ROOM; ++i)
            mRooms.add(new ExclusiveRoom(String.valueOf(i), PRICE_EXCLUSIVE_ROOMS));
    }

    /**
     * Initializes room according to theirs status from file if file exists.
     */
    private void initStatusOfRooms() {
        if (checkFileExistence(Constants.RECORDS_FILENAME)) {
            List<String> lst = readFromFile(Constants.RECORDS_FILENAME);
            for (String str : lst) {
                String[] data = str.split(Constants.COMMA);
                int roomNumber = Integer.parseInt(data[0]);
                Room room = mRooms.get(roomNumber);
                room.setGuest(new Guest(data[3], data[4], data[2]));
                if (data[1].equals(Constants.BOOKED))
                    room.changeStatus(RoomStatus.BOOKED);
                else if (data[1].equals(Constants.CHECK_IN)) {
                    room.changeStatus(RoomStatus.CHECK_IN);
                    String[] d = data[5].split("/");
                    room.setStartDate(new Date(Integer.parseInt(d[0]),
                            Integer.parseInt(d[1]), Integer.parseInt(d[2])));
                    d = data[6].split("/");
                    room.setEndDate(new Date(Integer.parseInt(d[0]),
                            Integer.parseInt(d[1]), Integer.parseInt(d[2])));
                }
            }
        }
    }

    /**
     * Read data from file and return the content of file as list
     *
     * @param fileName File to be read
     * @return Content of file as list
     */
    private List<String> readFromFile(String fileName) {
        File file = new File(fileName);
        List<String> lst = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String oneLine;
            while ((oneLine = reader.readLine()) != null)
                lst.add(oneLine);
            reader.close();
        } catch (IOException e) {
            System.out.println("#### File could not opened! Initialization "
                    + "failed ####");
        }

        return (lst);
    }

    /**
     * Check whether specified file is present or not
     *
     * @param  fileName File name to be checked
     * @return If it exists return true, false otherwise.
     */
    private boolean checkFileExistence(String fileName) {
        File file = new File(fileName);
        return (file.exists());
    }

    /**
     * Getter method for a room
     *
     * @throws ArrayIndexOutOfBoundsException If room number is greater than
     *         100 throws exception
     * @see    ArrayIndexOutOfBoundsException
     * @return If room is valid returns room, throws Exception otherwise.
     */
    public Room getRoom(int roomNumber) throws ArrayIndexOutOfBoundsException {
        if (roomNumber >= NUMBER_OF_ROOM)
            throw new ArrayIndexOutOfBoundsException
                    ("#### Invalid room number [" + roomNumber +"] ####");
        return (mRooms.get(roomNumber));
    }

}