/*
 * author: @yusufakdas
 */

package com.hotelmanagementsystem;

/**
 * Room class
 */
public abstract class Room {

    /**
     * Number of room
     */
    private final String ROOM_NUMBER;

    /**
     * Check in date
     */
    private DateInterface mStartDate;

    /**
     * Check out date
     */
    private DateInterface mEndDate;

    /**
     * Status of room
     */
    private RoomStatus mStatus;

    /**
     * Guest of room
     */
    private Guest mGuest;

    /**
     * Constructor that takes room number and room's per night price as parameter
     *
     * @param number Room number
     */
    public Room(String number) {
        ROOM_NUMBER = number;
        mStatus = RoomStatus.AVAILABLE;
        mStartDate = null;
        mEndDate = null;
        mGuest = null;
    }

    /**
     * Check status of room
     *
     * @return If room is available, returns true, false otherwise.
     */
    public boolean isAvailable() {
        return (mStatus == RoomStatus.AVAILABLE);
    }

    /**
     * Change status of room
     */
    public void changeStatus(RoomStatus status) {
        mStatus = status;
    }

    /**
     * Getter method for room number.
     *
     * @return The room number
     */
    public String getRoomNumber() {
        return (ROOM_NUMBER);
    }

    /**
     * Getter method for guest.
     *
     * @return Guest of room
     */
    public Guest getGuest() {
        return (mGuest);
    }

    /**
     * Setter method for guest.
     */
    public void setGuest(Guest g) {
        mGuest = g;
    }

    /**
     * Getter method for room status
     *
     * @return Status of room
     */
    public RoomStatus getStatus() {
        return (mStatus);
    }

    /**
     * Getter method for check in date
     *
     * @return Start date
     */
    public DateInterface getStartDate() {
        return (mStartDate);
    }

    /**
     * Getter method for check out date
     *
     * @return End date
     */
    public DateInterface getEndDate() {
        return (mEndDate);
    }

    /**
     * Setter method for check in date
     *
     * @param start Incoming date argument
     */
    public void setStartDate(DateInterface start) {
        mStartDate = start;
    }

    /**
     * Setter method for check out date
     *
     * @param end Incoming date argument
     */
    public void setEndDate(DateInterface end) {
        mEndDate = end;
    }

    /**
     * Calculate invoice of room according to room types
     * (For showing polymorphism)
     * @return Total invoice
     */
    protected abstract int calculateInvoice();

    /**
     * Additional information for invoice
     * For showing polymorphism
     * @return Additional information
     */
    protected abstract String additionalInfo();

    /**
     * Some information about room
     * (For showing polymorphism)
     * @return Information about room
     */
    public abstract String toString();
}