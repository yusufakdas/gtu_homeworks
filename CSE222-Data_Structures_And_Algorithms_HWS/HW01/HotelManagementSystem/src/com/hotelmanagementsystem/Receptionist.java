/*
 * author: @yusufakdas
 */

package com.hotelmanagementsystem;

import java.util.ArrayList;
import java.util.List;

/**
 * Receptionist class
 */
public class Receptionist extends User {

    /**
     * Constructor that takes name, surname, ID of user
     *
     * @param name    User's name
     * @param surname User's surname
     * @param id      User's id
     */
    public Receptionist(String name, String surname, String id) {
        super(name, surname, id);
    }

    /**
     * Book a room and keep necessary information in records.csv file
     *
     * @param  p Guest to be booked a room for
     * @param  room Room to be booked
     * @return If room is booked successfully returns true, false otherwise.
     */
    public boolean bookARoom(Person p, Room room) {
        if (!(p instanceof Guest)) {
            System.out.println("---> You should send a Guest object");
            return (false);
        }
        if (!checkRoomStatus(room)) {
            System.out.println("---> Room " + room.getRoomNumber()
                    + " is not available.");
            return (false);
        }
        boolean value = false;
        if (((Guest) p).checkUser()) {
            String str = room.getRoomNumber() + Constants.COMMA
                    + Constants.BOOKED + Constants.COMMA + p.getID()
                    + Constants.COMMA + p.getName() + Constants.COMMA
                    + p.getSurname() + Constants.NEWLINE;
            value = writeToFile(Constants.RECORDS_FILENAME, str);
            room.changeStatus(RoomStatus.BOOKED);
            System.out.println("Room " + room.getRoomNumber()
                    + " has been booked by " + Constants.RECEPTIONIST
                    + " for " + p.getName() + " " + p.getSurname());
            room.setGuest((Guest) p);
        } else {
            System.out.println("---> " + p.getName() + " " + p.getSurname()
                    + " have not registered to system yet.");
        }
        return (value);
    }

    /**
     * Cancel a reservation for a guest
     *
     * @param  room Guest's reserved room
     * @return If reservation is canceled successfully returns true, false otherwise.
     */
    @Override
    public boolean cancelReservation(Room room) {
        Guest p = room.getGuest();
        boolean value = false;
        if (checkRoomStatus(room)) {
            System.out.println("---> Room " + room.getRoomNumber()
                    + " is already available");
            return (false);
        }
        if (checkFileExistence(Constants.RECORDS_FILENAME)) {
            List<String> lst = readFromFile(Constants.RECORDS_FILENAME);
            List<String> revision = new ArrayList<>();
            for (String str : lst) {
                String[] data = str.split(Constants.COMMA);
                if (data[0].equals(room.getRoomNumber())
                        && data[2].equals(p.getID())) {
                    value = true;
                } else {
                    revision.add(str + Constants.NEWLINE);
                }
            }
            if (value) {
                writeToFile(Constants.RECORDS_FILENAME, revision);
                room.changeStatus(RoomStatus.AVAILABLE);
                room.setGuest(null);
                System.out.println("Reservation of room " + room.getRoomNumber()
                        + " has been canceled by " + Constants.RECEPTIONIST);
            }
        }
        return (value);
    }

    /**
     * Check in person by getting check in date and check out date and
     * changes its room's status as check in.
     *
     * @param  room Room to be checked in.
     * @param  start Check in date
     * @param  end Check out date
     * @return If check-in is done successfully returns true, false otherwise.
     */
    public boolean checkIn(Room room, DateInterface start, DateInterface end) {
        Guest p = room.getGuest();
        if (p == null){
            System.out.println("---> Room " + room.getRoomNumber() +  " has not"
                               + " be booked yet.");
            return (false);
        }
        if (start.isGreater(end)) {
            System.out.println("---> Check-in date must be greater "
                                + "than check-out date");
            return (false);
        }
        if (!p.checkUser()) {
            System.out.println("---> " + p.getName() + " " + p.getSurname()
                    + " have not registered to system yet.");
            return (false);
        }
        boolean value = false;
        if (checkFileExistence(Constants.RECORDS_FILENAME)) {
            if (checkReservation(room, p, Constants.BOOKED)) {
                List<String> lst = readFromFile(Constants.RECORDS_FILENAME);
                List<String> revision = new ArrayList<>();
                for (String str : lst) {
                    String[] data = str.split(Constants.COMMA);
                    if (data[0].equals(room.getRoomNumber())){
                        revision.add(room.getRoomNumber() + Constants.COMMA
                            + Constants.CHECK_IN + Constants.COMMA
                            + p.getID() + Constants.COMMA + p.getName()
                            + Constants.COMMA + p.getSurname() + Constants.COMMA
                            + start.toString() + Constants.COMMA + end.toString()
                            + Constants.NEWLINE);
                    } else {
                        revision.add(str + Constants.NEWLINE);
                    }
                }
                room.changeStatus(RoomStatus.CHECK_IN);
                room.setStartDate(start);
                room.setEndDate(end);
                System.out.println("Room " + room.getRoomNumber()
                        + " has been checked-in by " + Constants.RECEPTIONIST
                        + " for " + p.getName() + " " + p.getSurname());
                value = writeToFile(Constants.RECORDS_FILENAME, revision);
            } else {
                System.out.println("---> Check-in operation already done");
            }
        }
        return (value);
    }

    /**
     * Check out and show total invoice
     *
     * @param  room Room to be checked in.
     * @return If check-in is done successfully returns true, false otherwise.
     */
    public boolean checkOut(Room room) {
        Guest p = room.getGuest();
        if (p == null){
            System.out.println("---> This room has not be booked yet.");
            return (false);
        }
        if (!p.checkUser()) {
            System.out.println("---> " + p.getName() + " " + p.getSurname()
                                + " have not registered to system yet.");
            return (false);
        }
        int invoice;
        boolean value = false;
        if (checkFileExistence(Constants.RECORDS_FILENAME)) {
            if (checkReservation(room, p, Constants.CHECK_IN)) {
                List<String> lst = readFromFile(Constants.RECORDS_FILENAME);
                List<String> revision = new ArrayList<>();
                for (String str : lst) {
                    String[] data = str.split(Constants.COMMA);
                    if (!data[0].equals(String.valueOf(room.getRoomNumber()))
                            || !data[2].equals(p.getID()))
                        revision.add(str + Constants.NEWLINE);
                }
                value = writeToFile(Constants.RECORDS_FILENAME, revision);
                invoice = room.calculateInvoice();  // Polymorphic call was here!
                System.out.println("Room " + room.getRoomNumber()
                        + " has been checked-out for " + p.getName()
                        + " " + p.getSurname() + ". Total Invoice: $"
                        + invoice + ". (" + room.additionalInfo() + ")");
                room.changeStatus(RoomStatus.AVAILABLE);
                room.setStartDate(null);
                room.setEndDate(null);
                room.setGuest(null);
            } else {
                System.out.println("---> First guest must be checked-in "
                                    + "by receptionist.");
            }
        }
        return (value);
    }

}