/*
 * author: @yusufakdas
 */

package com.hotelmanagementsystem;

/**
 * Date interface
 */
public interface DateInterface {

    /**
     * Accessor method for day
     *
     * @return Day of date
     */
    int getDay();

    /**
     * Accessor method for month
     *
     * @return Month of date
     */
    int getMonth();

    /**
     * Accessor method for year
     *
     * @return Year of date
     */
    int getYear();

    /**
     * Calculates difference between two dates
     *
     * @param date  Other date.
     * @return      Difference
     */
    int differenceBetweenDates(DateInterface date);

    /**
     * Compares to dates.
     *
     * @param date  Other date.
     * @return      If incoming object is greated than this object returns false, true otherwise.
     */
    boolean isGreater(DateInterface date);

}
