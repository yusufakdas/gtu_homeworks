package com.hotelmanagementsystem;

import java.security.InvalidParameterException;

/**
 * Date class
 */
class Date implements DateInterface {

    /**
     * Day of month
     */
    private int mDay;

    /**
     * Month as number
     */
    private int mMonth;

    /**
     * Year of date
     */
    private int mYear;

    /**
     * Constructor that takes day, month and year as argument
     *
     * @param day    Day of Date
     * @param month  Month of Date
     * @param year   Year of Date
     */
    Date(int day, int month, int year) {
        setDay(day);
        setMonth(month);
        setYear(year);
    }

    /**
     * No parameter constructor of Date class
     *
     * ### CAUTION: Date is set to the first day of January of 2000! ###
     */
    Date() {
        this(1, 1, 2000);
    }

    /**
     * Getter method for day
     *
     * @return Day of month
     */
    @Override
    public int getDay() {
        return (mDay);
    }

    /**
     * Setter method for day
     *
     * @exception InvalidParameterException If day is invalid, throws exception.
     * @see InvalidParameterException
     * @param day New day value
     */
    public void setDay(int day) throws InvalidParameterException {
        if (day < 1 || day > 30)
            throw new InvalidParameterException("#### " + day
                      + " cannot be day of a month! ####");
        mDay = day;
    }

    /**
     * Getter method for month
     *
     * @return Month of year
     */
    @Override
    public int getMonth() {
        return (mMonth);
    }

    /**
     * Setter method for month
     *
     * @exception InvalidParameterException If month is invalid, throws exception.
     * @see InvalidParameterException
     * @param month New month value
     */
    public void setMonth(int month) throws InvalidParameterException {
        if (month < 1 || month > 12)
            throw new InvalidParameterException("#### " + month
                      + " cannot be month of a year! ####");
        mMonth = month;
    }

    /**
     * Getter method for year
     *
     * @return Day of year
     */
    @Override
    public int getYear() {
        return (mYear);
    }

    /**
     * Setter method for year
     *
     * @exception InvalidParameterException If year is invalid, throws exception.
     * @see InvalidParameterException
     * @param year New year value
     */
    public void setYear(int year) throws InvalidParameterException {
        if (year < 1980 || year > 3000)
            throw new InvalidParameterException("#### " + year
                      + " is invalid! ####");
        mYear = year;
    }

    /**
     * Calculate difference between two dates
     *
     * @param date  Other date object.
     * @return      Difference
     */
    @Override
    public int differenceBetweenDates(DateInterface date) {
        int other = date.getYear() * Constants.ONE_YEAR + date.getMonth()
                    * Constants.ONE_MONTH + date.getDay();
        int today = getYear() * Constants.ONE_YEAR + getMonth()
                    * Constants.ONE_MONTH + getDay();
        return (today - other);
    }

    /**
     * Override toString method
     *
     * @return String representation of date
     */
    @Override
    public String toString() {
        String zeroOfDay, zeroOfMonth;

        // Control if day and month is less than 10.
        zeroOfDay = (mDay < 10 ? "0" : "");
        zeroOfMonth = (mMonth < 10 ? "0" : "");

        return (zeroOfDay + mDay + "/" + zeroOfMonth + mMonth + "/" + mYear);
    }

    /**
     * Override equals method
     *
     * @param obj  Other object
     * @return     True if dates are equal to each other.
     */
    @Override
    public boolean equals(Object obj) {
        // Control whether incoming object is null or not
        if (obj == null)
            return (false);

        // Control whether incoming object's is instance of this class
        if (obj.getClass() != this.getClass())
            return (false);

        // Control equality of references
        if (obj == this)
            return (true);

        // Downcast to Date class
        Date date = (Date) obj;

        return (date.toString().equals(toString()));
    }

    /**
     * Compares to dates.
     *
     * @param  date Other date.
     * @return If incoming object is greated than this object
     *         returns false, true otherwise.
     */
    @Override
    public boolean isGreater(DateInterface date) {
        if (mYear > date.getYear())
            return (true);
        else if (mYear < date.getYear())
            return (false);

        if (mMonth > date.getMonth())
            return (true);
        else if (mMonth < date.getMonth())
            return (false);

        return (mDay > date.getDay());
    }

}