/*
 * author: @yusufakdas
 */

package com.hotelmanagementsystem;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * User class
 */
public abstract class User implements Person {

    /**
     * Name of user
     */
    private String mName;

    /**
     * Surname of user
     */
    private String mSurname;

    /**
     * ID of user
     */
    private String mID;

    /**
     * Constructor that takes name, surname, ID of user
     *
     * @param name     User's name
     * @param surname  User's surname
     * @param id       User's id
     */
    public User(String name, String surname, String id) {
        mName = name;
        mSurname = surname;
        mID = id;
    }

    /**
     * Getter method of name
     *
     * @return Name of user
     */
    @Override
    public String getName() {
        return (mName);
    }

    /**
     * Setter method of name
     *
     * @param name The value that is intended to change.
     */
    @Override
    public void setName(String name) {
        mName = name;
    }

    /**
     * Getter method of surname
     *
     * @return Surname of user
     */
    @Override
    public String getSurname() {
        return (mSurname);
    }

    /**
     * Setter method of surname
     *
     * @param surname The value that is intended to change.
     */
    @Override
    public void setSurname(String surname) {
        mSurname = surname;
    }

    /**
     * Getter method of ID
     *
     * @return ID of user
     */
    @Override
    public String getID() {
        return (mID);
    }

    /**
     * Setter method of ID
     *
     * @param id The value that is intended to change.
     */
    @Override
    public void setID(String id) {
        mID = id;
    }

    /**
     * Override toString method inherited from Object class
     *
     * @return Representation of object as a string
     */
    @Override
    public String toString() {
        return ("Name: " + mName + ", Surname: " + mSurname
                + ", ID Number: " + mID);
    }

    /**
     * Override equals method to check two person's equality
     *
     * @param  obj Incoming object
     * @return True or false with respect to equality of objects
     */
    @Override
    public boolean equals(Object obj) {
        // Control whether incoming object is null or not
        if (obj == null)
            return (false);

        // Control whether incoming object's is instance of this class
        if (obj.getClass() != this.getClass())
            return (false);

        // Control equality of references
        if (obj == this)
            return (true);

        // Downcast to User class
        Person person = (User) obj;

        return (person.getName().equals(mName)
                && person.getSurname().equals(mSurname)
                && person.getID().equals(mID));
    }

    /**
     * Check user to specify whether it is in users.csv file or not.
     *
     * @return If it is in returns true, false otherwise.
     */
    protected boolean checkUser() {
        if (checkFileExistence(Constants.USERS_FILENAME)) {
            List<String> lst = readFromFile(Constants.USERS_FILENAME);
            for (String str : lst) {
                String[] parsed = str.split(Constants.COMMA);
                if (parsed[2].equals(getID()))
                    return (true);
            }
        }
        return (false);
    }

    /**
     * Checks records.csv file to determine whether room status is booked or
     * checked-in or not
     *
     * @param  room Room object
     * @param  p Person to be checked
     * @param  status Room status.
     * @return If it is in status that is checked returns true, false otherwise.
     */
    protected boolean checkReservation(Room room, Person p, String status) {
        if (checkFileExistence(Constants.RECORDS_FILENAME)) {
            List<String> lst = readFromFile(Constants.RECORDS_FILENAME);
            for (String str : lst) {
                String[] parsed = str.split(Constants.COMMA);
                if (parsed[0].equals(room.getRoomNumber())
                        && parsed[1].equals(status)
                        && parsed[2].equals(p.getID())) {
                    return (true);
                }
            }
        }
        return (false);
    }

    /**
     * Checks status of room (available, booked, checked-in) for receptionist and guest.
     *
     * @param  room Room to be checked
     * @return If room is available return true, false otherwise.
     */
    protected boolean checkRoomStatus(Room room) {
        if (room == null) {
            System.out.println("There is no such a room that has that number "
                                + "in the hotel");
            return (false);
        }
        return (room.isAvailable());
    }

    /**
     * Read data from file and return the content of file as list
     *
     * @param  fileName File to be read
     * @return Content of file as list
     */
    protected List<String> readFromFile(String fileName) {
        File file = new File(fileName);
        List<String> lst = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String oneLine;
            while ((oneLine = reader.readLine()) != null)
                lst.add(oneLine);
            reader.close();
        } catch (IOException e) {
            System.out.println("#### An error occurred ####");
        }
        return (lst);
    }

    /**
     * Write one string data to file in append mode.
     *
     * @param  fileName Name of file
     * @param  data Data to be written
     * @return If file is written successfully returns true, otherwise false.
     */
    protected boolean writeToFile(String fileName, String data) {
        boolean retVal;
        File file = new File(fileName);
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));
            writer.write(data);
            retVal = true;
            writer.close();
        } catch (IOException e) {
            retVal = false;
            System.out.println("#### An error occurred! ####");
        }
        return (retVal);
    }

    /**
     * Write a list to file.
     *
     * @param  fileName Name of file
     * @param  data Data to be written
     * @return If file is written successfully returns true, otherwise false.
     */
    protected boolean writeToFile(String fileName, List<String> data) {
        boolean retVal;
        File file = new File(fileName);
        try {
            retVal = true;
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            for (String str : data)
                writer.write(str);
            writer.close();
        } catch (IOException e) {
            retVal = false;
            System.out.println("#### An error occurred! ####");
        }
        return (retVal);
    }

    /**
     * Check whether specified file is present or not
     *
     * @param  fileName File name to be checked
     * @return If it exists return true, false otherwise.
     */
    protected boolean checkFileExistence(String fileName) {
        File file = new File(fileName);
        return (file.exists());
    }

    /**
     * Abstract method to cancel reservation of room for a guest.
     *
     * @param   room Room of guest
     * @return  If reservation is canceled successfully returns true,
     *          false otherwise.
     */
    public abstract boolean cancelReservation(Room room);

}