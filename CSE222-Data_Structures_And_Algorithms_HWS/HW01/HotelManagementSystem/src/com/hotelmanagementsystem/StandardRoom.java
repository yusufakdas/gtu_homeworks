/*
 * author: @yusufakdas
 */

package com.hotelmanagementsystem;

/**
 * StandardRoom class
 */
public class StandardRoom extends Room {

    /**
     * Price for one night
     */
    private final int PRICE_PER_NIGHT;

    /**
     * Constructor that takes room number and room's per night price as parameter
     *
     * @param number Room number
     */
    public StandardRoom(String number, int price) {
        super(number);
        PRICE_PER_NIGHT = price;
    }

    /**
     * Calculate invoice of room according to room types
     * (For showing polymorphism)
     * @return Total invoice
     */
    @Override
    protected int calculateInvoice() {
        return (PRICE_PER_NIGHT * getEndDate().differenceBetweenDates(getStartDate()));
    }

    /**
     * Additional information for invoice
     * (For showing polymorphism)
     * @return Additional information
     */
    protected String additionalInfo() {
        return ("No additional services");
    }

    /**
     * Some information about room
     * (For showing polymorphism)
     * @return Information about room
     */
    @Override
    public String toString() {
        String str = Constants.STANDARD + " Room - No: " + getRoomNumber();
        if (getStatus() == RoomStatus.AVAILABLE)
            str += (" - " + Constants.AVAILABLE);
        else if (getStatus() == RoomStatus.BOOKED)
            str += (" - " + Constants.BOOKED);
        else if (getStatus() == RoomStatus.CHECK_IN)
            str += (" - " + Constants.CHECK_IN);
        return (str);
    }
}
