/*
 * author: @yusufakdas
 */

package com.hotelmanagementsystem;

/**
 * ExclusiveRoom class
 */
public class ExclusiveRoom extends Room {

    /**
     * Price for one night
     */
    private final int PRICE_PER_NIGHT;

    /**
     * Price for additional services (
     */
    private final int ADDITIONAL_SERVICES = 250;

    /**
     * Constructor that takes room number and room's per night price as parameter
     *
     * @param number Room number
     * @param price Price for per night
     */
    public ExclusiveRoom(String number, int price) {
        super(number);
        PRICE_PER_NIGHT = price;
    }

    /**
     * Calculate invoice of room according to room types
     * (For showing polymorphism)
     * @return Total invoice (Additional $250 for other services)
     */
    @Override
    protected int calculateInvoice() {
        return (PRICE_PER_NIGHT * getEndDate().differenceBetweenDates(getStartDate())
                + ADDITIONAL_SERVICES);
    }

    /**
     * Additional information for invoice
     * (For showing polymorphism)
     * @return Additional information
     */
    protected String additionalInfo() {
        return ("$" + ADDITIONAL_SERVICES + " is added to invoice for "
                + "additional services");
    }

    /**
     * Override toString method
     * (For showing polymorphism)
     * @return Information about room
     */
    @Override
    public String toString() {
        String str = Constants.EXCLUSIVE + " Room - No: " + getRoomNumber();
        if (getStatus() == RoomStatus.AVAILABLE)
            str += (" - " + Constants.AVAILABLE);
        else if (getStatus() == RoomStatus.BOOKED)
            str += (" - " + Constants.BOOKED);
        else if (getStatus() == RoomStatus.CHECK_IN)
            str += (" - " + Constants.CHECK_IN);
        return (str);
    }

}
