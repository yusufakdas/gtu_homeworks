/*
 * author: @yusufakdas
 */

package com.hotelmanagementsystem;

import java.io.*;

/**
 * HotelManagementSystem class
 */
public class HotelManagementSystem {

    /**
     * The hotel to be managed.
     */
    private Hotel mHotel;

    /**
     * No parameter constructor of the class
     */
    public HotelManagementSystem() {
        mHotel = new Hotel();
    }

    /**
     * Add a receptionist to system.
     *
     * @param p Receptionist to be added to system.
     */
    public void addReceptionist(Person p) {
        if (!(p instanceof Receptionist)) {
            System.out.println("---> You should send a Receptionist object");
            return;
        }
        if (checkUser(p)) {
            if (writeToUserFile(p, Constants.RECEPTIONIST))
                System.out.println(p.getName() + " " + p.getSurname()
                        + " has been employed as a receptionist!");
            else
                System.out.println("---> Could not be employed!");
        } else {
            System.out.println("---> There is same receptionist on the system!");
        }
    }

    /**
     * Add a guest to database.
     *
     * @param p Guest to be added to database.
     */
    public void addGuest(Person p) {
        if (!(p instanceof Guest)) {
            System.out.println("---> You should send a Guest object");
            return;
        }
        if (checkUser(p)) {
            if (writeToUserFile(p, Constants.GUEST))
                System.out.println(p.getName() + " " + p.getSurname()
                                + " has been added to system as a guest!");
            else
                System.out.println("---> Could not be added to system!");
        } else {
            System.out.println("---> There is same person on the system!");
        }
    }

    /**
     * Controls whether there is same user or not.
     *
     * @param p Person to be checked
     * @return If there is no same user returns true, false otherwise.
     */
    private boolean checkUser(Person p) {
        File file = new File(Constants.USERS_FILENAME);
        boolean val = true;
        try {
            if (file.exists()) {
                FileReader fileReader = new FileReader(file);
                BufferedReader reader = new BufferedReader(fileReader);
                String oneLine;
                while ((oneLine = reader.readLine()) != null && val) {
                    String[] parsed = oneLine.split(Constants.COMMA);
                    if (parsed[2].equals(p.getID()))
                        val = false;
                }
                reader.close();
            }
        } catch (IOException e) {
            val = true;
            System.out.println("#### An error occurred! ####");
        }
        return (val);
    }

    /**
     * Check given person according to its role and if conditions are
     * satisfied then writes it to file.
     *
     * @param p Person to be written to file
     * @param role Receptionist or Guest
     * @return If information was written to file returns true, false otherwise
     */
    private boolean writeToUserFile(Person p, String role) {
        File file = new File(Constants.USERS_FILENAME);
        boolean val;
        try {
            FileWriter fileWriter = new FileWriter(file, true);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            writer.write(p.getName() + Constants.COMMA + p.getSurname()
                    + Constants.COMMA + p.getID() + Constants.COMMA
                    + role + Constants.NEWLINE);
            writer.close();
            val = true;
        } catch (IOException e) {
            val = false;
            System.out.println("#### An error occurred! ####");
        }
        return (val);
    }

    /**
     * Get a particular room of hotel
     *
     * @param roomNumber Room that guest wants
     * @throws ArrayIndexOutOfBoundsException If room number is greater than
     *         100 throws exception
     * @see    ArrayIndexOutOfBoundsException
     * @return Specified room of hotel
     */
    public Room getAndControlRoom(int roomNumber) throws ArrayIndexOutOfBoundsException{
        return (mHotel.getRoom(roomNumber));
    }

    /**
     * Delete files created
     */
    void deleteFiles() {
        File file1 = new File(Constants.RECORDS_FILENAME);
        File file2 = new File(Constants.USERS_FILENAME);

        if (file1.exists())
            file1.delete();
        if (file2.exists())
            file2.delete();
    }

}