/*
 * author: @yusufakdas
 */

package com.hotelmanagementsystem;

/**
 * This class includes some useful constants to make operations much easier.
 */
public final class Constants {

    /**
     * The object of this class cannot be instantiated.
     */
    private Constants () { }

    /**
     * Comma character constant
     */
    public static final String COMMA = ",";

    /**
     * Newline character constant
     */
    public static final String NEWLINE = "\n";

    /**
     * Receptionist as string constant
     */
    public static final String RECEPTIONIST = "Receptionist";

    /**
     * Guest as string constant
     */
    public static final String GUEST = "Guest";

    /**
     * Book status as string constant
     */
    public static final String BOOKED = "Booked";

    /**
     * Available status as string constant
     */
    public static final String AVAILABLE = "Available";

    /**
     * Check-In status as string constant
     */
    public static final String CHECK_IN = "Checked-In";

    /**
     * Exclusive room
     */
    public static final String EXCLUSIVE = "Exclusive";

    /**
     * Standard room
     */
    public static final String STANDARD = "Standard";

    /**
     * Filename that records are kept
     */
    public static final String RECORDS_FILENAME = "records.csv";

    /**
     * Filename that users are kept
     */
    public static final String USERS_FILENAME = "users.csv";

    /**
     * Month constant. It is 30 for convenience.
     */
    public static final int ONE_MONTH = 30;

    /**
     * Year constant. It is 365 for convenience.
     */
    public static final int ONE_YEAR = 365;

}
