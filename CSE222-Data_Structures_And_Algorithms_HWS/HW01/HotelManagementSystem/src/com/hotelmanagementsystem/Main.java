/*
 * author: @yusufakdas
 */

package com.hotelmanagementsystem;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Main test class
 */
public class Main {

    private static List<User> u = new ArrayList<>();
    private static HotelManagementSystem sys = new HotelManagementSystem();
    private static BufferedReader fileReader;

    public static void main(String[] args) {

        String oneLine;
        try {
            System.out.println("Hotel Management System is starting...\n");

            fileReader = new BufferedReader(new FileReader("testFile"));
            while ((oneLine = fileReader.readLine()) != null) {
                if (oneLine.equals("NEWLINE"))
                    System.out.println();
                else if (oneLine.equals("ADDING_TO_SYS"))
                    System.out.println("~~~~~USERS ARE ADDING TO SYSTEM~~~~~");
                else if (oneLine.equals("BOOKING_ROOMS"))
                    System.out.println("~~~~~BOOKING ROOMS~~~~~");
                else if (oneLine.equals("CANCELLING_RES"))
                    System.out.println("~~~~~CANCELLING RESERVATIONS~~~~~");
                else if (oneLine.equals("ADDING_SOME_TOO"))
                    System.out.println("~~~~~ADDING SOME USER TO~~~~~");
                else if (oneLine.equals("CHECKIN"))
                    System.out.println("~~~~~CHECK IN~~~~~");
                else if (oneLine.equals("CHECKOUT"))
                    System.out.println("~~~~~CHECK OUT~~~~~");
                else if (!oneLine.equals(""))
                   choose(oneLine);
            }
            fileReader.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
    }

    private static void choose(String choice) {
        int index;
        String[] str;
        try {
            str = fileReader.readLine().split(",");
            if (choice.equals("CREATE_GUEST")) {
                u.add(new Guest(str[0], str[1], str[2]));
            } else if (choice.equals("CREATE_RECEPTIONIST")) {
                u.add(new Receptionist(str[0], str[1], str[2]));
            } else if (choice.equals("ADD_GUEST")) {
                index = find(str);
                sys.addGuest(u.get(index));
            } else if (choice.equals("ADD_RECEPTIONIST")) {
                index = find(str);
                sys.addReceptionist(u.get(index));
            } else if (choice.equals("CHOOSE_GUEST")) {
                index = find(str);
                makeOperation(index);
            } else if (choice.equals("CHOOSE_RECEPTIONIST")) {
                index = find(str);
                makeOperation(index);
            } else {
                System.out.println("#### INVALID ####");
                System.exit(1);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
    }

    private static int find(String[] str) {
        for (int i = 0; i < u.size(); ++i) {
            if (u.get(i).getName().equals(str[0])
                && u.get(i).getSurname().equals(str[1])
                && u.get(i).getID().equals(str[2]))
                return (i);
        }
        return (-1);
    }

    private static void makeOperation(int index) {
        User user = u.get(index);
        String s;
        String data[];
        int roomNum, i;
        try {
            s = fileReader.readLine();
            if (user instanceof Guest) {
                if (s.equals("GUEST_BOOK")) {
                    roomNum = Integer.parseInt(fileReader.readLine());
                    try {
                        ((Guest) user).bookARoom(sys.getAndControlRoom(roomNum));
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                        System.exit(1);
                    }
                } else if (s.equals("GUEST_CANCEL")) {
                    roomNum = Integer.parseInt(fileReader.readLine());
                    try {
                        user.cancelReservation(sys.getAndControlRoom(roomNum));
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                        System.exit(1);
                    }
                } else {
                    System.out.println("#### INVALID ####");
                    System.exit(1);
                }
            } else {
                if (s.equals("RECEPTIONIST_BOOK")) {
                    data = fileReader.readLine().split(" - ");
                    i = find(data[0].split(","));
                    roomNum = Integer.parseInt(data[1]);
                    try {
                        ((Receptionist) user).bookARoom(u.get(i),
                                sys.getAndControlRoom(roomNum));
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                        System.exit(1);
                    }
                } else if (s.equals("RECEPTIONIST_CANCEL")) {
                    roomNum = Integer.parseInt(fileReader.readLine());
                    try {
                        user.cancelReservation(sys.getAndControlRoom(roomNum));
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                        System.exit(1);
                    }
                } else if (s.equals("RECEPTIONIST_CHECKIN")) {
                    data = fileReader.readLine().split(" - ");
                    roomNum = Integer.parseInt(data[0]);
                    String[] d1 = data[1].split("/");
                    String[] d2 = data[2].split("/");
                    DateInterface start = new Date(Integer.parseInt(d1[0]),
                            Integer.parseInt(d1[1]), Integer.parseInt(d1[2]));
                    DateInterface end = new Date(Integer.parseInt(d2[0]),
                            Integer.parseInt(d2[1]), Integer.parseInt(d2[2]));
                    try {
                        ((Receptionist)user).checkIn(sys.getAndControlRoom(roomNum),
                                start, end);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                        System.exit(1);
                    }
                } else if (s.equals("RECEPTIONIST_CHECKOUT")) {
                    roomNum = Integer.parseInt(fileReader.readLine());
                    try {
                        ((Receptionist) user).checkOut(sys.getAndControlRoom(roomNum));
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                        System.exit(1);
                    }
                } else {
                    System.out.println("#### INVALID ####");
                    System.exit(1);
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
    }

}
