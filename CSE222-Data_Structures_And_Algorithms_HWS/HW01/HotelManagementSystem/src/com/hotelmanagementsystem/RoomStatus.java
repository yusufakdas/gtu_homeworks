/*
 * author: @yusufakdas
 */

package com.hotelmanagementsystem;

/**
 * Room status enum
 */
public enum RoomStatus {
    AVAILABLE, BOOKED, CHECK_IN
}
