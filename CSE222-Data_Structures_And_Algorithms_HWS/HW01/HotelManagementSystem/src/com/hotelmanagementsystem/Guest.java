/*
 * author: @yusufakdas
 */

package com.hotelmanagementsystem;

import java.util.ArrayList;
import java.util.List;

/**
 * Guest class
 */
public class Guest extends User {

    /**
     * Constructor that takes name, surname, ID of user
     *
     * @param name     User's name
     * @param surname  User's surname
     * @param id       User's id
     */
    public Guest(String name, String surname, String id) {
        super(name, surname, id);
    }

    /**
     * Book a room for a guest if the guest register to system
     *
     * @param   room Room to be booked
     * @return  If room is booked successfully returns true, false otherwise.
     */
    public boolean bookARoom(Room room) {
        if (!checkRoomStatus(room)) {
            System.out.println("---> Room " + room.getRoomNumber()
                                + " is not available.");
            return (false);
        }
        boolean value = false;
        if (checkUser()) {
            String str = room.getRoomNumber() + Constants.COMMA
                         + Constants.BOOKED + Constants.COMMA + getID()
                         + Constants.COMMA + getName() + Constants.COMMA
                         + getSurname() + Constants.NEWLINE;
            value = writeToFile(Constants.RECORDS_FILENAME, str);
            System.out.println("Room " + room.getRoomNumber() +
                    " has been booked by " + getName() + " " + getSurname());
            room.changeStatus(RoomStatus.BOOKED);
            room.setGuest(this);
        } else {
            System.out.println("---> " + getName() + " " + getSurname()
                                + " have not registered to system yet.");
        }
        return (value);
    }

    /**
     * Cancel reservation of room if room is reserved by registered guest.
     *
     * @param   room Room of guest
     * @return  If reservation is canceled successfully returns true,
     *          false otherwise.
     */
    @Override
    public boolean cancelReservation(Room room) {
        if (checkRoomStatus(room)) {
            System.out.println("---> Room " + room.getRoomNumber()
                                + " has already available!");
            return (false);
        }
        boolean found = false;
        if (checkFileExistence(Constants.RECORDS_FILENAME)) {
            List<String> lst = readFromFile(Constants.RECORDS_FILENAME);
            List<String> revision = new ArrayList<>();
            for (String str : lst) {
                String[] data = str.split(Constants.COMMA);
                if (data[0].equals(room.getRoomNumber()) && data[2].equals(getID()))
                    found = true;
                else
                    revision.add(str + Constants.NEWLINE);
            }
            if (found) {
                writeToFile(Constants.RECORDS_FILENAME, revision);
                System.out.println("Reservation of room " + room.getRoomNumber()
                    + " has been canceled by " + getName() + " " + getSurname());
                room.changeStatus(RoomStatus.AVAILABLE);
            } else {
                System.out.println("---> Cannot found reservation for "
                                + getName() + " " + getSurname() + ".");
            }
        }
        return (found);
    }

}