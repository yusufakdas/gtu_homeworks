package com.GTUCourseStructure.part1;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        GTUCourseStructure structure = new GTUCourseStructure();

        System.out.println("-------------------------------------------------");
        System.out.println(">>>>>>>> Trying method getByCode <<<<<<<<");
        print(structure.getByCode("CSE 222"));
        print(structure.getByCode("CSE 241"));
        print(structure.getByCode("CSE 102"));
        print(structure.getByCode("CSE 101"));
        print(structure.getByCode("CSE 234"));
        print(structure.getByCode("TUR 101"));
        print(structure.getByCode("MATH 102"));
        System.out.println();

        System.out.println(">>> The courses that have the same code!");
        print(structure.getByCode("XXX XXX"));
        System.out.println("-------------------------------------------------");
        System.out.println();

        System.out.println(">>>>>>>> Trying method listSemesterCourses <<<<<<<<");
        System.out.println(">>> Semester 4");
        print(structure.listSemesterCourses(4));
        System.out.println();

        System.out.println(">>> Semester 5");
        print(structure.listSemesterCourses(5));
        System.out.println();

        System.out.println(">>> Semester 2");
        print(structure.listSemesterCourses(2));
        System.out.println();

        System.out.println(">>> Semester 8");
        print(structure.listSemesterCourses(8));
        System.out.println("-------------------------------------------------");
        System.out.println();

        System.out.println(">>>>>>>> Trying method getByRange <<<<<<<<");
        int start = 0, end = 10;
        System.out.printf(">>> From %d to %d\n", start, end);
        print(structure.getByRange(start, end));
        System.out.println();

        start = 22;
        end = 25;
        System.out.printf(">>> From %d to %d\n", start, end);
        print(structure.getByRange(start, end));
        System.out.println();

        start = 35;
        end = 50;
        System.out.printf(">>> From %d to %d\n", start, end);
        print(structure.getByRange(start, end));
        System.out.println();

        start = 12;
        end = 18;
        System.out.printf(">>> From %d to %d\n", start, end);
        print(structure.getByRange(start, end));
        System.out.println("-------------------------------------------------");
        System.out.println();

        System.out.println("~~~~~~~~~~~~~~ EXCEPTION TEST ~~~~~~~~~~~~~~~");
        try {
            System.out.println(">>>>>>>> Exception of getByCode 1 <<<<<<<<");
            structure.getByCode("CES 123");
        } catch (Exception e) {
            System.out.println(e);
        }

        System.out.println();

        try {
            System.out.println(">>>>>>>> Exception of getByCode 2 <<<<<<<<");
            structure.getByCode("ASDSD");
        } catch (Exception e) {
            System.out.println(e);
        }

        System.out.println();

        try {
            System.out.println(">>>>>>>> Exception of listSemesterCourses 1 <<<<<<<<");
            structure.listSemesterCourses(15);
        } catch (Exception e) {
            System.out.println(e);
        }

        System.out.println();


        try {
            System.out.println(">>>>>>>> Exception of listSemesterCourses 2 <<<<<<<<");
            structure.listSemesterCourses(-8);
        } catch (Exception e) {
            System.out.println(e);
        }

        System.out.println();

        try {
            System.out.println(">>>>>>>> Exception of getByRange 1 <<<<<<<<");
            structure.getByRange(-5, 4531);
        } catch (Exception e) {
            System.out.println(e);
        }

        System.out.println();

        try {
            System.out.println(">>>>>>>> Exception of getByRange 2 <<<<<<<<");
            structure.getByRange(15, 5);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private static void print(List<Course> code) {
        for (Course e : code)
            System.out.println(e);
    }

}
