package com.GTUCourseStructure.part1;

import java.util.List;

/**
 * Interface for Course Structure
 */
public interface CourseStructure {

    /**
     * Prints course list on the screen.
     */
    void printList();

    /**
     * Returns all courses which have given course code as a list because there
     * might be more than one courses that have the same course code.
     *
     * @param code Code of course
     * @return List of courses according to course code
     */
    List<Course> getByCode(String code);

    /**
     * Returns all courses on given semester.
     *
     * @param semester Semester of courses
     * @return List of courses that are in the same semester.
     */
    List<Course> listSemesterCourses(int semester);

    /**
     * Returns all courses from given start index to last index.
     *
     * @param startIndex start index of range
     * @param lastIndex  last index of range
     * @return List of courses for given range
     */
    List<Course> getByRange(int startIndex, int lastIndex);

}
