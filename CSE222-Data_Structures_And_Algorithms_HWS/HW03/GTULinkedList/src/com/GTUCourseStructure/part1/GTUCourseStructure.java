package com.GTUCourseStructure.part1;

import java.io.*;
import java.security.InvalidParameterException;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import static com.GTUCourseStructure.part1.CourseReader.readDataFromFileToList;

/**
 * GTU course structure class
 */
public class GTUCourseStructure implements CourseStructure {

    /**
     * LinkedList that keeps course data
     */
    private List<Course> mCourseList;

    /**
     * No parameter constructor of GTUCourseStructure
     */
    public GTUCourseStructure() {
        mCourseList = new LinkedList<>();
        try {
            mCourseList = readDataFromFileToList();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Print course list on the screen.
     */
    @Override
    public void printList() {
        for (Course course : mCourseList)
            System.out.println(course);
    }

    /**
     * Returns all courses which have given course code as a list because there
     * might be more than one courses that have the same course code.
     *
     * @param code Code of course
     * @throws NoSuchElementException if given course of code cannot be found
     * @return List of courses according to course code
     */
    @Override
    public List<Course> getByCode(String code) {
        List<Course> lst = new LinkedList<>();

        for (Course course : mCourseList) {
            if (course.getCode().equals(code))
                lst.add(course);
        }

        if (lst.size() == 0)
            throw new NoSuchElementException("#### There is no such a course! ####");
        return (lst);
    }

    /**
     * Returns all courses on given semester.
     *
     * @param semester Semester of courses
     * @throws InvalidParameterException If semester value is invalid
     * @return List of courses that are in the same semester.
     */
    @Override
    public LinkedList<Course> listSemesterCourses(int semester) {
        if (semester < 1 || semester > 8)
            throw new InvalidParameterException("#### Semester value must be " +
                                                "between 1 and 8! ####");
        LinkedList<Course> semesterList = new LinkedList<>();
        for (Course course : mCourseList)
            if (course.getSemester() == semester)
                semesterList.add(course);

        return (semesterList);
    }

    /**
     * Returns all courses from given start index to last index.
     *
     * @param startIndex start index of range
     * @param lastIndex  last index of range
     * @throws InvalidParameterException If given range is invalid
     * @return List of courses for given range
     */
    @Override
    public LinkedList<Course> getByRange(int startIndex, int lastIndex) {
        int size = mCourseList.size();
        if ((startIndex > lastIndex) || (startIndex < 0) || (lastIndex < 0 )
                || (startIndex >= size) || (lastIndex >= size))
            throw new InvalidParameterException("#### Invalid index values! ####");

        LinkedList<Course> rangeList = new LinkedList<>();
        for (int i = startIndex; i < lastIndex; ++i)
            rangeList.add(mCourseList.get(i));

        return (rangeList);
    }

}