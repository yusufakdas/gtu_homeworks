package com.GTUCourseStructure.part1;

/**
 * This class is for keeping record of one course information.
 */
public class Course {

    /**
     * The semester information of course
     */
    private int mSemester;

    /**
     * The code of the course
     */
    private String mCourseCode;

    /**
     * The title of the course
     */
    private String mCourseTitle;

    /**
     * The ECTS Credits information of the course
     */
    private String mECTSCredits;

    /**
     * The GTU Credits information of the course
     */
    private String mGTUCredits;

    /**
     * The H+T+L information of the course
     */
    private String mHTLInformation;

    /**
     * The constructor that takes all course information as parameters
     *
     * @param semester   Semester of the course
     * @param code       Code of the course
     * @param title      Name of the course
     * @param ECTSCredit ECTS credit of the course
     * @param GTUCredit  GTU credit of the course
     * @param HTLInfo    HTL information of the course
     */
    public Course(int semester, String code, String title, String ECTSCredit,
           String GTUCredit, String HTLInfo) {
        mSemester = semester;
        mCourseCode = code;
        mCourseTitle = title;
        mECTSCredits = ECTSCredit;
        mGTUCredits = GTUCredit;
        mHTLInformation = HTLInfo;
    }

    /**
     * Copy Constructor for Course class
     *
     * @param copy The Course object to be copied
     */
    public Course(Course copy) {
        this(copy.getSemester(), copy.getCode(), copy.getTitle(),
             copy.getECTSCredits(), copy.getGTUCredits(), copy.getHTL());
    }

    /**
     * Getter method for semester information
     *
     * @return Semester of the course
     */
    public int getSemester() {
        return (mSemester);
    }

    /**
     * Getter method for code information
     *
     * @return The code of the course
     */
    public String getCode() {
        return (mCourseCode);
    }

    /**
     * Getter method for title information
     *
     * @return The title of the course
     */
    public String getTitle() {
        return (mCourseTitle);
    }

    /**
     * Getter method for ECTS credits information
     *
     * @return The ECTS credits of the course
     */
    public String getECTSCredits() {
        return (mECTSCredits);
    }

    /**
     * Getter method for GTU credits information
     *
     * @return The GTU credits of the course
     */
    public String getGTUCredits() {
        return (mGTUCredits);
    }

    /**
     * Getter method for HTL Information
     *
     * @return The HTL information of the course
     */
    public String getHTL() {
        return (mHTLInformation);
    }

    /**
     * Setter method for semester information
     *
     * @param semester New semester value
     */
    public void setSemester(int semester) {
        mSemester = semester;
    }

    /**
     * Setter method for code information
     *
     * @param code New course code value
     */
    public void setCode(String code) {
        mCourseCode = code;
    }

    /**
     * Setter method for title information
     *
     * @param title New course title value
     */
    public void setTitle(String title) {
        mCourseTitle = title;
    }

    /**
     * Setter method for ECTS credits information
     *
     * @param credit New ECTS credits value
     */
    public void setECTSCredits(String credit) {
        mECTSCredits = credit;
    }

    /**
     * Setter method for GTU credits information
     *
     * @param credit New GTU credits value
     */
    public void setGTUCredits(String credit) {
        mGTUCredits = credit;
    }

    /**
     * Setter method for HTL Information
     *
     * @param htlInfo New HTL value
     */
    public void setHTL(String htlInfo) {
        mHTLInformation = htlInfo;
    }

    /**
     * Override toString method for Course class
     *
     * @return The representation of this class' object as a string
     */
    @Override
    public String toString() {
        return (mSemester + ", " + mCourseCode + ", " + mCourseTitle + ", "
                + mECTSCredits + ", " + mGTUCredits + ", " + mHTLInformation);
    }

    /**
     * Override equals method for Course class
     *
     * @param obj The object to be compared
     * @return If this object is equal to incoming object returns true,
     *         otherwise false
     */
    @Override
    public boolean equals(Object obj) {
        // Check obj reference to determine whether null or not
        if (obj == null)
            return (false);

        // Check class types
        if (obj.getClass() != getClass())
            return (false);

        // Compare reference of this with obj
        if (obj == this)
            return (true);

        // Downcast from Object to Course
        Course course = (Course) obj;

        return (mSemester == course.getSemester()
                && mCourseCode.equals(course.getCode())
                && mCourseTitle.equals(course.getTitle())
                && mECTSCredits.equals(course.getECTSCredits())
                && mGTUCredits.equals(course.getGTUCredits())
                && mHTLInformation.equals(course.mHTLInformation));
    }

}