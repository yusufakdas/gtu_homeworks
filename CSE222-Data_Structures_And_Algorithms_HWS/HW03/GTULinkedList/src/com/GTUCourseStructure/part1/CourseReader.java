package com.GTUCourseStructure.part1;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class CourseReader {

    /**
     * File that keeps course information
     */
    private static final String COURSES_FILENAME = "Courses.csv";

    /**
     * Constant for splitting information from string
     */
    private static final String SEMICOLON = ";";

    /**
     * The object of this class cannot be instantianed.
     */
    private CourseReader() { }

    /**
     * Read course information from file
     *
     * @throws FileNotFoundException If file is not present, throws exception
     */
    public static LinkedList<Course> readDataFromFileToList() throws FileNotFoundException {
        LinkedList<Course> tmpList = new LinkedList<>();

        File file = new File(COURSES_FILENAME);
        if (!file.exists())
            throw new FileNotFoundException("#### The file could not be found! ####");
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String oneLine;
        try {
            reader.readLine(); // Pass header line
            while ((oneLine = reader.readLine()) != null)
                addCourseToList(oneLine.split(SEMICOLON), tmpList);
            reader.close();
        } catch (IOException e) {
            System.out.println("#### Could not be read file! ####");
        }
        return (tmpList);
    }

    /**
     * Add one course to arrayList.
     *
     * @param data String array of course to be added.
     */
    private static void addCourseToList(String[] data, List<Course> lst) {
        int semester = Integer.parseInt(data[0]);
        Course course = new Course(semester, data[1], data[2], data[3],
                data[4], data[5]);
        lst.add(course);
    }

}
