package com.GTUCourseStructure.part3;

import com.GTUCourseStructure.part1.Course;

import java.util.Iterator;

public class Main {
    private static CourseLinkedList list = new CourseLinkedList();

    public static void main(String[] args) {
        print();
        System.out.println(">>> The size of the List is " + list.size());
        System.out.println("-------------------------------------------");
        System.out.println();

        list.add(new Course(1, "CSE 102", "A Dersi", "10", "8", "4+4+4"));
        list.add(new Course(2, "CSE 244", "K Dersi", "14", "8", "4+4+4"));
        list.add(new Course(3, "CSE 303", "D Dersi", "12", "7", "4+4+4"));
        list.add(new Course(1, "CSE 121", "B Dersi", "8", "9", "4+4+4"));
        list.add(new Course(1, "CSE 118", "T Dersi", "12", "12", "4+4+4"));
        list.add(new Course(3, "CSE 399", "C Dersi", "12", "10", "4+4+4"));
        list.add(new Course(4, "CSE 329", "L Dersi", "12", "10", "4+4+4"));
        System.out.println(">>> Some courses is added to Course LinkedList");
        print();
        System.out.println();

        System.out.println(">>> The size of the List is " + list.size());
        System.out.println("-------------------------------------------");
        System.out.println();

        System.out.println(">>> Some courses is added to Course LinkedList");
        list.add(new Course(6, "CSE 618", "Q Dersi", "12", "12", "4+4+4"));
        list.add(new Course(2, "CSE 232", "D Dersi", "7", "5", "4+4+4"));
        list.add(new Course(3, "CSE 333", "S Dersi", "6", "6", "4+4+4"));
        list.add(new Course(4, "CSE 425", "K Dersi", "7", "5", "4+4+4"));
        print();
        System.out.println();

        System.out.println(">>> The size of the List is " + list.size());
        System.out.println("-------------------------------------------");
        System.out.println();


        System.out.println(">>> Usage of next method of iterator");
        Iterator<Course> iter = list.iterator();
        System.out.println(iter.next());
        System.out.println(iter.next());
        System.out.println(iter.next());
        System.out.println(iter.next());
        System.out.println(iter.next());
        System.out.println(iter.next());
        System.out.println("-------------------------------------------");
        System.out.println();

        System.out.println(">>> Usage of nextInSemester method of CourseIterator");
        CourseIterator iter2 = list.courseIterator();
        System.out.println(iter2.nextInSemester());
        System.out.println(iter2.nextInSemester());
        System.out.println(iter2.nextInSemester());
        System.out.println(iter2.nextInSemester());
        System.out.println(iter2.nextInSemester());
        System.out.println();

        iter2 = list.courseIterator(5);
        System.out.println(iter2.nextInSemester());
        System.out.println(iter2.nextInSemester());
        System.out.println(iter2.nextInSemester());
        System.out.println(iter2.nextInSemester());
        System.out.println(iter2.nextInSemester());
        System.out.println();

        iter2 = list.courseIterator(6);
        System.out.println(iter2.nextInSemester());
        System.out.println(iter2.nextInSemester());
        System.out.println(iter2.nextInSemester());
        System.out.println(iter2.nextInSemester());
        System.out.println(iter2.nextInSemester());
        System.out.println("-------------------------------------------");

        System.out.println();

        System.out.println(">>> Before removing");
        print();
        System.out.println();

        System.out.println(">>> After removing some element from the Course LinkedList");
        list.remove(5);
        list.remove(0);
        list.remove(7);
        list.remove(4);
        list.remove(3);
        print();
        System.out.println();

        System.out.println(">>> Use nextInSemester with next");
        iter2 = list.courseIterator();
        System.out.println("NextElement: " + iter2.next());
        System.out.println("Using nextInSemester: " + iter2.nextInSemester());
        System.out.println("Using nextInSemester: " + iter2.nextInSemester());
        System.out.println("Using nextInSemester: " + iter2.nextInSemester());
        System.out.println("Using nextInSemester: " + iter2.nextInSemester());
        System.out.println();
        System.out.println("NextElement: " + iter2.next());
        System.out.println("Using nextInSemester: " + iter2.nextInSemester());
        System.out.println("Using nextInSemester: " + iter2.nextInSemester());
        System.out.println("Using nextInSemester: " + iter2.nextInSemester());
        System.out.println("Using nextInSemester: " + iter2.nextInSemester());
        System.out.println();
        System.out.println("NextElement: " + iter2.next());
        System.out.println("NextElement: " + iter2.next());
        System.out.println("NextElement: " + iter2.next());
        System.out.println("Using nextInSemester: " + iter2.nextInSemester());
        System.out.println("Using nextInSemester: " + iter2.nextInSemester());
        System.out.println("Using nextInSemester: " + iter2.nextInSemester());
        System.out.println("Using nextInSemester: " + iter2.nextInSemester());
        System.out.println("-------------------------------------------");
        System.out.println();

        System.out.println(">>> Print the list using iterator");
        iter = list.iterator();
        while (iter.hasNext()) {
            System.out.println("---" + iter.next());
        }
    }

    private static void print() {
        System.out.println("-----------------LIST----------------");
        for (Course c : list)
            System.out.println(c);
        System.out.println("-------------------------------------");
    }

}
