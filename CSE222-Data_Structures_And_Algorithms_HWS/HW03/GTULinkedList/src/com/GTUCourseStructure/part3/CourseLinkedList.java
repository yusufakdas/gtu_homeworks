package com.GTUCourseStructure.part3;

import com.GTUCourseStructure.part1.Course;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class CourseLinkedList implements Iterable<Course> {

    /**
     * Head of list
     */
    private Node<Course> mHead = null;

    /**
     * The size of the list
     */
    private int mSize = 0;

    /**
     * Getter method for LinkedList size
     * @return the size of the list
     */
    public int size() {
        return (mSize);
    }

    /**
     * Getter method for a course that is in given index.
     * @param index The index of the course
     * @return The course wanted
     */
    public Course get(int index) {
        if (index < 0 || index > mSize)
            throw new IndexOutOfBoundsException("Invalid index " + index);
        return (new Iter(index).next());
    }

    /**
     * The iterator to use foreach loop
     * @return Iterator to head
     */
    @Override
    public Iterator<Course> iterator() {
        return (new Iter(0));
    }

    /**
     * Course iterator that includes nextInSemester and other iterator methods
     * @return Course iterator to head
     */
    public CourseIterator courseIterator(int index) {
        return (new Iter(index));
    }

    /**
     * Course iterator that includes nextInSemester and other iterator methods
     * @return Course iterator to head
     */
    public CourseIterator courseIterator() {
        return (new Iter(0));
    }

    /**
     * Add a course object to end of the list
     * @param obj The course object to be added
     */
    public void add(Course obj) {
        new Iter(mSize).add(obj);
    }

    /**
     * Remove course with respect to given index
     * @param index The index of the course to be removed
     */
    public void remove(int index) {
        if (index < 0 || index > mSize)
            throw new IndexOutOfBoundsException("Invalid index " + index);
        new Iter(index).remove();
    }

    /**
     * Iterator class of class CourseLinkedList
     */
    private class Iter implements CourseIterator {

        /**
         * The index of iterator
         */
        private int mIndex = 0;

        /**
         * Next item
         */
        private Node<Course> nextItem;

        /**
         * The item returned recently by next
         */
        private Node<Course> lastItemNext = null;

        /**
         * Next in Semester
         */
        private Node<Course> nextInSemesterIter;

        /**
         * The item returned recently by next in semester
         */
        private Node<Course> lastItemSemester = null;

        /**
         * Constructor that is used for add, remove and get method of list
         * @param i index value for iterator
         */
        private Iter(int i) {
            if (i < 0 || i > mSize)
                throw new IndexOutOfBoundsException("Invalid index " + i);
            nextItem = mHead;
            for (int mIndex = 0; mIndex < i; ++mIndex)
                next();
            nextInSemesterIter = nextItem;
        }

        /**
         * Checks whether there is next item or not.
         * @return If there is next item, then return true, false otherwise
         */
        @Override
        public boolean hasNext() {
            return (nextItem != null);
        }

        /**
         * Advance iterator to next node
         * @return The last returned course
         */
        @Override
        public Course next() {
            if (!hasNext())
                throw new NoSuchElementException("*** There is no such an " +
                        "element! ***");
            lastItemNext = nextItem;
            nextInSemesterIter = nextItem;
            nextItem = nextItem.next;
            ++mIndex;
            return (lastItemNext.mData);
        }

        /**
         * Advanced to iterator in next semester
         * @return The course over which passed
         */
        @Override
        public Course nextInSemester() {
            lastItemSemester = nextInSemesterIter;
            nextInSemesterIter = nextInSemesterIter.nextInSemester;
            return (lastItemSemester.mData);
        }

        /**
         * Add a course to the list
         * @param obj The course object to be added
         */
        private void add(Course obj) {
            Node<Course> course = new Node<>(obj);
            if (mHead == null) {                // Add to empty list
                mHead = course;
            } else if (lastItemNext == null) {  // Add to beginning
                course.next = mHead;
                mHead = course;
            } else if (nextItem != null) {      // Add to middle of the list
                course.next = nextItem;
                lastItemNext.next = course;
            } else {                            // Add to end
                lastItemNext.next = course;
            }
            linkToSemester(course);
            lastItemNext = null;
            ++mIndex;
            ++mSize;
        }

        /**
         * Link the same semester nodes in a circular way
         * @param c The same semester node
         */
        private void linkToSemester(Node<Course> c) {
            Node<Course> traverse = mHead;
            int theIndex;
            for (theIndex = 0; theIndex < mSize; ++theIndex) {
                if (c.mData.getSemester() == traverse.mData.getSemester())
                    if (c != traverse)
                        break;
                traverse = traverse.next;
            }
            if (mIndex == theIndex) { // Link to empty circular list
                c.nextInSemester = c;
            } else if (mIndex < theIndex) { // link to head of list
                Node<Course> oldHead = traverse;
                c.nextInSemester = oldHead;
                while (traverse.nextInSemester != oldHead)
                    traverse = traverse.nextInSemester;
                traverse.nextInSemester = c;
            } else { // Link to middle and end
                Node<Course> headOfSemester = traverse;
                Node<Course> before = traverse;
                for (int i = theIndex; i <= mSize; ++i) {
                    if (i == mIndex) {
                        if (before.nextInSemester == before) {
                            c.nextInSemester = headOfSemester;
                            before.nextInSemester = c;
                        } else {
                            c.nextInSemester = before.nextInSemester;
                            before.nextInSemester = c;
                        }
                    }
                    if (traverse.mData.getSemester() == c.mData.getSemester())
                        before = traverse;
                    traverse = traverse.next;
                }

            }
            lastItemSemester = null;
        }

        /**
         * Remove the next node.
         * NOTE THAT: this method differs from original Java's iterator
         * remove method. It removes THE NEXT ITERATOR NODE!
         */
        public void remove() {
            if (mIndex == 0) { // Remove from head
                Node<Course> lastNode = mHead;
                while (lastNode.nextInSemester != mHead)
                    lastNode = lastNode.nextInSemester;
                lastNode.nextInSemester = lastNode.nextInSemester.nextInSemester;
                mHead = mHead.next;
            } else { // Remove others
                Node<Course> semester = getTheFirstSemester(nextItem);
                semester.nextInSemester = semester.nextInSemester.nextInSemester;
                lastItemNext.next = nextItem.next;
            }
        }

        /**
         * Look for the the first occurrence of a given semester
         * @param c A course node
         * @return The first occurrence.
         */
        private Node<Course> getTheFirstSemester(Node<Course> c) {
            Node<Course> traverse = mHead;
            for (int i = 0; i < mSize; ++i) {
                if (c.mData.getSemester() == traverse.mData.getSemester()) {
                    if (c != traverse) {
                        return (traverse);
                    }
                }
                traverse = traverse.next;
            }
            return (c);
        }
    }

    /**
     * The building blocks of the linked list
     * @param <E> Data's class type
     */
    private static class Node<E> {

        /**
         * Node data
         */
        private E mData;

        /**
         * Reference to next node
         */
        private Node<E> next = null;

        /**
         * Reference to next semester
         */
        private Node<E> nextInSemester = null;

        /**
         * The constructor of class Node that takes an item as parameter
         */
        private Node(E item) {
            mData = item;
        }

    }
}
