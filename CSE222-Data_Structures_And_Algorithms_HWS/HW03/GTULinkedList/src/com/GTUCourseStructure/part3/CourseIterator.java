package com.GTUCourseStructure.part3;

import com.GTUCourseStructure.part1.Course;

import java.util.Iterator;

/**
 * Iterator of Course
 */
public interface CourseIterator extends Iterator<Course> {
    /**
     * Advance to iterator in next semester
     * @return The course over which passed
     */
    Course nextInSemester();

}