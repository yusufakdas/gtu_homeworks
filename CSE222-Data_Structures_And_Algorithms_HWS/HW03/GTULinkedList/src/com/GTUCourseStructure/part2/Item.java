package com.GTUCourseStructure.part2;

public class Item<E> {
    private E data;
    private int index;
    private int addOrder;
    private boolean status = true;

    public Item(E d, int i, int a) {
        data = d;
        index = i;
        addOrder = a;
    }

    public int getIndex() {
        return (index);
    }

    public int getAddOrder() {
        return (addOrder);
    }

    public E getData() {
        return (data);
    }

    public boolean getStatus() {
        return (status);
    }

    public void setStatus(boolean value) {
        status = value;
    }

    @Override
    public String toString() {
        return (data.toString());
    }

}
