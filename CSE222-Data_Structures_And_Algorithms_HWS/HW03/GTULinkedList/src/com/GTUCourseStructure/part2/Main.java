package com.GTUCourseStructure.part2;

public class Main {
    private static ExtendedList<String> theList = new ExtendedList<>();

    public static void main(String[] args) {
        theList.add("Ali");
        theList.add("Tamer");
        theList.add("Kemal");
        theList.add("Sati");
        theList.add("Saka");
        theList.add("Isma");

        print();
        System.out.println();

        theList.disable("Isma");
        theList.disable("Sati");
        theList.disable("Ali");
        theList.showDisabled();
        System.out.println();

        print();
        System.out.println();

        theList.disable("Tamer");

        print();
        System.out.println();

        theList.showDisabled();
        System.out.println();

        theList.enable("Ali");
        print();
        System.out.println();
        theList.enable("Tamer");
        print();
        System.out.println();
        theList.enable("Sati");
        print();


    }

    private static void print() {
        for (String i : theList)
            System.out.println(i);
    }
}
