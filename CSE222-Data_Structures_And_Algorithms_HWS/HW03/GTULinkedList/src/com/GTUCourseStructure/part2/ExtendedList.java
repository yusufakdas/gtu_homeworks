package com.GTUCourseStructure.part2;

import java.util.LinkedList;

public class ExtendedList<E> extends LinkedList<E> {
    private LinkedList<Item<E>> dataList = new LinkedList<>();

    public void disable(E obj) {
        int indexOfObj = indexOf(obj);
        int theIndex, sizeOfTheList = dataList.size(), otherIndex;
        for (theIndex = 0; theIndex < sizeOfTheList; ++theIndex) {
            otherIndex = indexOf(dataList.get(theIndex).getData());
            if (otherIndex > indexOfObj)
                break;
        }
        dataList.add(theIndex, new Item<>(obj, indexOfObj, sizeOfTheList));
        remove(indexOfObj);
    }

    public void enable(E obj) {
        int index, sizeOfTheList = dataList.size();
        Item<E> data = dataList.get(0);
        for (index = 0; index < sizeOfTheList; ++index) {
            data = dataList.get(index);
            if (data.getData().equals(obj)) {
                break;
            }
        }

        int ind = data.getIndex();
        if (data.getIndex() >= size())
            ind = size() - 1;
        add(data.getIndex(), data.getData());
    }

    public void showDisabled() {
        for (Item<E> traverse : dataList)
            System.out.println("-> "+ traverse + " - " + traverse.getIndex());
    }

}
