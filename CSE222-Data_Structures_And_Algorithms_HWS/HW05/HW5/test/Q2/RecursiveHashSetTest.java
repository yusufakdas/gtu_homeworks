package Q2;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RecursiveHashSetTest {

    @Test
    void size() {
        Set<Integer> set = new RecursiveHashSet<>();
        int actual = set.size();

        System.out.println("----- Size Method -----");

        assertEquals(0, actual);
        set.add(8);
        set.add(25);
        set.add(1);
        set.add(9);
        set.add(28);
        set.add(14);
        set.add(78);

        actual = set.size();
        assertEquals(7, actual);

        set.remove(25);
        set.remove(78);
        set.remove(9);
        set.remove(1);

        actual = set.size();
        assertEquals(3, actual);

        System.out.println("PASSED!");
        System.out.println("-----------------------\n");
    }

    @Test
    void isEmpty() {
        Set<Integer> set = new RecursiveHashSet<>();
        boolean actual = set.isEmpty();

        System.out.println("----- isEmpty Method -----");

        assertEquals(true, actual);
        set.add(125);
        set.add(36);
        set.add(15);
        set.add(5);
        set.add(1);
        set.add(897);
        set.add(12335);
        set.add(745);

        actual = set.isEmpty();
        assertEquals(false, actual);

        set.remove(1);
        set.remove(125);
        set.remove(12335);
        set.remove(745);

        actual = set.isEmpty();
        assertEquals(false, actual);

        set.remove(897);
        set.remove(36);
        set.remove(15);
        set.remove(5);

        actual = set.isEmpty();
        assertEquals(true, actual);

        System.out.println("PASSED!");
        System.out.println("--------------------------\n");
    }

    @Test
    void add() {
        Set<Integer> set = new RecursiveHashSet<>();
        boolean actual;

        System.out.println("----- Add Method -----");

        actual = set.add(45);
        assertEquals(true, actual);

        actual = set.add(17);
        assertEquals(true, actual);

        actual = set.add(5);
        assertEquals(true, actual);

        actual = set.add(4);
        assertEquals(true, actual);

        actual = set.add(458);
        assertEquals(true, actual);

        actual = set.add(1234536);
        assertEquals(true, actual);

        actual = set.add(458);
        assertEquals(false, actual);

        actual = set.add(41589);
        assertEquals(true, actual);

        actual = set.add(4);
        assertEquals(false, actual);

        actual = set.add(17);
        assertEquals(false, actual);

        actual = set.add(45);
        assertEquals(false, actual);

        System.out.println("PASSED!");
        System.out.println("--------------------------\n");
    }

    @Test
    void remove() {
        Set<String> set = new RecursiveHashSet<>();
        boolean actual;

        System.out.println("----- Remove Method -----");

        actual = set.remove("GTU");
        assertEquals(false, actual);

        set.add("GTU");
        set.add("Gebze");
        set.add("Teknik");
        set.add("Universite");
        set.add("Kocaeli");
        set.add("Veri");
        set.add("Yapilari");
        set.add("Istanbul");
        set.add("Algoritma");
        set.add("Random");

        actual = set.remove("Universite");
        assertEquals(true, actual);

        actual = set.remove("Kocaeli");
        assertEquals(true, actual);

        actual = set.remove("Algoritma");
        assertEquals(true, actual);

        actual = set.remove("Random");
        assertEquals(true, actual);

        actual = set.remove("Olursun");
        assertEquals(false, actual);

        actual = set.remove("Seker");
        assertEquals(false, actual);

        System.out.println("PASSED!");
        System.out.println("--------------------------\n");
    }

    @Test
    void contains() {
        Set<Double> set = new RecursiveHashSet<>();
        boolean actual;

        System.out.println("----- Contains Method -----");

        set.add(2.98);
        set.add(3.988);
        set.add(98.21);
        set.add(91.001);
        set.add(12.45);
        set.add(145.54);
        set.add(798.35);

        actual = set.contains(2.98);
        assertEquals(true, actual);

        actual = set.contains(145.54);
        assertEquals(true, actual);

        actual = set.contains(987.23);
        assertEquals(false, actual);

        actual = set.contains(798.35);
        assertEquals(true, actual);

        actual = set.contains(3.988);
        assertEquals(true, actual);

        actual = set.contains(1.258);
        assertEquals(false, actual);

        actual = set.contains(36.87);
        assertEquals(false, actual);

        set.remove(145.54);
        set.remove(798.35);
        set.remove(2.98);

        actual = set.contains(145.54);
        assertEquals(false, actual);

        actual = set.contains(798.35);
        assertEquals(false, actual);

        actual = set.contains(2.98);
        assertEquals(false, actual);

        System.out.println("PASSED!");
        System.out.println("--------------------------\n");
    }

}