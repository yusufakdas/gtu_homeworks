package Q3;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MergeSortDoubleLinkedListTest {

    /**
     * Unit test of Merge Sort
     */
    @Test
    void sort() {
        Integer[] integerArray = {-8, -9, 45, 123, 74, 86, 4, 96, 20, 37, 79, 9
                                  -94, 45, 34, 15, 897, 19, 36, 20, 27, 57, 51};
        LinkedList<Integer> list = new LinkedList<>(Arrays.asList(integerArray));
        boolean actual;

        System.out.println("---> Data inside of integer array <---");
        printArray(integerArray);
        System.out.println();

        System.out.println("---> Data inside of double linked list <---");
        MergeSortDoubleLinkedList.printLinkedList(list);
        System.out.println();

        System.out.println("Sort double linked list and array");
        MergeSortDoubleLinkedList.sort(list);
        Arrays.sort(integerArray);

        System.out.println("Compare double linked list and array.");
        actual = compare(integerArray, list);
        assertEquals(actual, true);
        System.out.println("PASSED!\n");
    }

    private<E> boolean compare(E[] a, List<E> l) {
        int i = 0;
        for (E element : l)
            if (!(a[i++].equals(element)))
                return (false);
        return (true);
    }

    private<E> void printArray(E[] a) {
        for (E element : a)
            System.out.print(element + " ");
        System.out.println();
    }

}