package Q1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OpenDoubleHashMapTest {

    @Test
    void get() {
        MyMap<Integer, String> map = new OpenDoubleHashMap<>();
        System.out.println("---- Test of get method ----");

        System.out.println("Adding some elements to the map...");

        map.put(5, "Apple");
        map.put(6, "Samsung");
        map.put(4, "Nokia");
        map.put(9, "Microsoft");
        map.put(3, "Google");
        map.put(5, "Apple");
        map.put(8, "Facebook");
        map.put(7, "Twitter");

        String actual;

        System.out.println("Getting the value of 4...");
        actual = map.get(4);
        assertEquals("Nokia", actual);

        System.out.println("Getting the value of 8...");
        actual = map.get(8);
        assertEquals("Facebook", actual);

        System.out.println("Getting the value of 3...");
        actual = map.get(3);
        assertEquals("Google", actual);

        System.out.println("Trying to get an element that is not present in the map...");
        actual = map.get(1);
        assertEquals(null, actual);

        System.out.println("PASSED!\n");
    }

    @Test
    void put() {
        MyMap<Integer, String> map = new OpenDoubleHashMap<>();
        System.out.println("---- Test of put method ----");

        System.out.println("Adding some elements to the map...");
        String actual;

        actual = map.put(5, "Apple");
        assertEquals(null, actual);

        actual = map.put(6, "Samsung");
        assertEquals(null, actual);

        actual = map.put(4, "Nokia");
        assertEquals(null, actual);

        actual = map.put(9, "Microsoft");
        assertEquals(null, actual);

        actual = map.put(3, "Google");
        assertEquals(null, actual);

        actual = map.put(8, "Facebook");
        assertEquals(null, actual);

        actual = map.put(7, "Twitter");
        assertEquals(null, actual);

        System.out.println("Updating the elements added...");
        actual = map.put(9, "Linux");
        assertEquals("Microsoft", actual);

        actual = map.put(3, "DuckDuckGo");
        assertEquals("Google", actual);

        actual = map.put(8, "-");
        assertEquals("Facebook", actual);

        actual = map.put(5, "Motorola");
        assertEquals("Apple", actual);

        System.out.println("PASSED!\n");
    }

    @Test
    void remove() {
        MyMap<String, Double> map = new OpenDoubleHashMap<>();
        System.out.println("---- Test of remove method ----");

        System.out.println("Adding some elements to the map...");

        map.put("Ali", 92.5);
        map.put("Veli", 80.30);
        map.put("Ayse", 75.72);
        map.put("Fatma", 92.12);
        map.put("Berk", 84.5);
        map.put("Esra", 90.123);
        map.put("Kemal", 65.8121);

        Double actual;

        System.out.println("Removing elements from the map...");
        actual = map.remove("Ali");
        assertEquals(new Double(92.5), actual);

        actual = map.remove("Ayse");
        assertEquals(new Double(75.72), actual);

        actual = map.remove("Kemal");
        assertEquals(new Double(65.8121), actual);

        actual = map.remove("Berk");
        assertEquals(new Double(84.5), actual);

        actual = map.remove("Fatma");
        assertEquals(new Double(92.12), actual);

        System.out.println("Trying to remove an element that is not present in the map...");
        actual = map.remove("Yusuf");
        assertEquals(null, actual);

        System.out.println("PASS!\n");
    }

    @Test
    void size() {
        MyMap<Integer, Integer> map = new OpenDoubleHashMap<>();
        System.out.println("---- Test of size method ----");

        int actual = map.size();
        System.out.println("Before adding an element...");
        assertEquals(0, actual);

        System.out.println("After adding an element...");
        map.put(5, 7);
        map.put(8, 6);
        map.put(15, 4);
        map.put(9, 65);
        map.put(74, 3);
        map.put(2, 1);
        map.put(7, 36);
        map.put(12, 123);
        actual = map.size();
        assertEquals(8, actual);

        System.out.println("After deleting some elements...");
        map.remove(5);
        map.remove(8);
        map.remove(15);
        map.remove(9);
        map.remove(7);
        map.remove(74);
        actual = map.size();
        assertEquals(2, actual);

        System.out.println("PASSED!\n");
    }

    @Test
    void isEmpty() {
        MyMap<Integer, Integer> map = new OpenDoubleHashMap<>();
        System.out.println("---- Test of isEmpty method ----");

        boolean actual = map.isEmpty();
        System.out.println("Before adding an element...");
        assertEquals(true, actual);

        System.out.println("After adding an element...");
        map.put(5, 7);
        actual = map.isEmpty();
        assertEquals(false, actual);

        System.out.println("After deleting the element added recently...");
        map.remove(5);
        actual = map.isEmpty();
        assertEquals(true, actual);

        System.out.println("PASSED!\n");
    }

}