package Q2;

import java.util.Set;

public class MainTest {

    public static void main(String[] args) {
        Set<Integer> set = new RecursiveHashSet<>(11);

        System.out.println("*-*-*-*-* Testing of Recursive Hash Set *-*-*-*-*");
        System.out.println("Adding some elements to the set...");
        set.add(8);
        set.add(12);
        set.add(34);
        set.add(14);
    }

}
