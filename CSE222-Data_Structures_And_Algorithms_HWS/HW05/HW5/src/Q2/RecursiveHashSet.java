package Q2;

import java.io.Serializable;
import java.security.InvalidParameterException;
import java.util.*;

public class RecursiveHashSet<E> extends AbstractSet<E>
                            implements Set<E>, Serializable {

    /**
     * Element table that holds items not collided.
     */
    private E[] table;

    /**
     * Hash tables for collisions
     */
    private RecursiveHashSet<E>[] collisionTable;

    /**
     * Initial capacity of tables
     */
    private final int INITIAL_CAPACITY;

    /**
     * Number of elements in a set
     */
    private int countElements;

    /**
     * Total elements including recursive tables
     */
    private int totalElements;

    /**
     * The list for iterator.
     */
    private List<E> listForIterator;

    public RecursiveHashSet() {
        this(13);
    }

    /**
     * Constructor that takes initial size as its argument
     * @param initialSize Initial Size
     */
    @SuppressWarnings("unchecked")
    RecursiveHashSet(int initialSize) {
        if (initialSize < 3)
            throw new InvalidParameterException();
        INITIAL_CAPACITY = initialSize;
        table = (E[]) (new Object[INITIAL_CAPACITY]);
        collisionTable = new RecursiveHashSet[INITIAL_CAPACITY];
        listForIterator = new LinkedList<>();
    }

    /**
     * Size of the all elements added
     * @return size
     */
    @Override
    public int size() {
        return (totalElements);
    }

    /**
     * Checks whether the table is empty or not
     * @return If table is empty, return true, otherwise false.
     */
    @Override
    public boolean isEmpty() {
        return (totalElements == 0);
    }

    /**
     * Adds an item to the table
     * @param data Item to be added
     * @return If item is added successfully, return true, otherwise false.
     */
    @Override
    public boolean add(E data) {
        return (add(data, this));
    }

    /**
     * Recursive method for add
     * @param data The data to be added
     * @param ref Table reference
     * @return If item is added successfully, return true, otherwise false.
     */
    private boolean add(E data, RecursiveHashSet<E> ref) {
        int index = data.hashCode() % ref.table.length;
        index += (index < 0 ? ref.table.length : 0);

        if (ref.table[index] == null) {
            ref.table[index] = data;
            listForIterator.add(data);

            ++ref.countElements;
            ++totalElements;

            return (true);
        }

        if ((ref.table[index] != null) && (data.equals(ref.table[index])))
            return (false);

        if (ref.collisionTable[index] == null) {
            int newLength = getTableLength(data, ref.table);
            ref.collisionTable[index] = new RecursiveHashSet<>(newLength);
        }

        return (add(data, ref.collisionTable[index]));
    }

    /**
     * Removes an item from the table
     * @param data Item to be deleted
     * @return If item is removed successfully, returns true, otherwise false.
     */
    @Override
    public boolean remove(Object data) {
        return (remove(data, this));
    }

    /**
     * Recursive method for remove
     * @param data Item to be deleted
     * @param ref Table reference
     * @return If item is added successfully, return true, otherwise false.
     */
    @SuppressWarnings("unchecked")
    private boolean remove(Object data, RecursiveHashSet<E> ref) {
        int index = data.hashCode() % ref.table.length;
        index += (index < 0 ? ref.table.length : 0);

        if (ref.table[index] == null && ref.collisionTable[index] == null)
            return (false);

        if (data.equals(ref.table[index])) {
            --ref.countElements;
            --totalElements;
            ref.table[index] = null;
            listForIterator.remove((E)(data));

            return (true);
        }

        return (ref.collisionTable[index] != null
                && (remove(data, ref.collisionTable[index])));
    }

    /**
     * Calculates new table size
     * @param data data to be used its index
     * @param theTable table
     * @return New size
     */
    private int getTableLength(E data, E[] theTable) {
        int[] primeNumbers = {3, 5, 7, 11, 13, 17, 23, 29};
        int len = table.length;
        Random rand = new Random();

        int index = data.hashCode() % len;
        while (index < 0)
            index += table.length;
        while (len >= 3 && theTable[index] != null) {
            len -= 2;
            index = data.hashCode() % len;
            if (index < 0)
                index += len;
        }

        if (len < 3) {
            len = primeNumbers[rand.nextInt(6)];
            while (len > table.length)
                len = primeNumbers[rand.nextInt(6)];
        }
        return (len);

    }

    /**
     * Contains method of the set
     * @param o The object to be searched
     * @return If object is in the table, then returns true, otherwise false.
     */
    @Override
    public boolean contains(Object o) {
        for (E data : listForIterator)
            if (o.equals(data))
                return (true);
        return (false);
    }

    /**
     * Returns set iterator using inner class
     * @return set iterator
     */
    @Override
    public Iterator<E> iterator() {
        return (new SetIterator());
    }

    /**
     * Calculates the load factor for rehashing
     * @return load factor
     */
    private double getLoadFactor() {
        return ((double) (countElements) / table.length);
    }

    /**
     * Inner iterator class
     */
    private class SetIterator implements Iterator<E> {
        Iterator<E> iterator = listForIterator.iterator();

        @Override
        public boolean hasNext() {
            return (iterator.hasNext());
        }

        @Override
        public E next() {
            return (iterator.next());
        }

    }

}
