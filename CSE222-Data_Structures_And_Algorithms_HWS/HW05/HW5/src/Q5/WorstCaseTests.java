package Q5;

import java.util.*;

import Q4.SortAlgorithms;
import Q3.MergeSortDoubleLinkedList;

public class WorstCaseTests {
    private final static int TEST_SIZE_100 = 100;
    private final static int TEST_SIZE_1000 = 1000;
    private final static int TEST_SIZE_5000 = 5000;
    private final static int TEST_SIZE_10000 = 10000;

    private enum Order {ASCENDING, DESCENDING, NO_CHANGE}
    private enum Algorithm {INSERTION, MERGE, MERGE_WITH_DLL, QUICK, HEAP}


    public static void main(String[] args) {
        testInsertionSort();
        System.out.println("\n\n");
        testQuickSort();
        System.out.println("\n\n");
        testMergeSort();
        System.out.println("\n\n");
        testMergeSortWithDLL();
        System.out.println("\n\n");
        testHeapSort();
    }

    private static long sortAndShowResult(Integer[] data, Algorithm algorithm) {
        long startTime = 0, finishTime = 0;

        if (algorithm == Algorithm.INSERTION) {
            startTime = System.nanoTime();
            SortAlgorithms.insertionSort(data);
            finishTime = System.nanoTime();
        } else if (algorithm == Algorithm.MERGE) {
            startTime = System.nanoTime();
            SortAlgorithms.mergeSort(data);
            finishTime = System.nanoTime();
        } else if (algorithm == Algorithm.MERGE_WITH_DLL) {
            LinkedList<Integer> list = new LinkedList<>(Arrays.asList(data));
            startTime = System.nanoTime();
            MergeSortDoubleLinkedList.sort(list);
            finishTime = System.nanoTime();
        } else if (algorithm == Algorithm.QUICK) {
            startTime = System.nanoTime();
            SortAlgorithms.quickSort(data);
            finishTime = System.nanoTime();
        } else if (algorithm == Algorithm.HEAP) {
            startTime = System.nanoTime();
            SortAlgorithms.heapSort(data);
            finishTime = System.nanoTime();
        } else {
            System.exit(1);
        }

        return (finishTime - startTime);
    }

    //*****************************INSERTION SORT******************************/
    private static void testInsertionSort() {
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~ TEST OF INSERTION SORT ~~~~~~~~~~~~~~~~~~~~~~~~~~");

        double sum = .0;
        for (int i = 0; i < 10; ++i) {
            Integer[] sortData100 = fillArray(TEST_SIZE_100, Order.DESCENDING);
//          SortAlgorithms.printArray(sortData100);
            sum += sortAndShowResult(sortData100, Algorithm.INSERTION);
        }
        System.out.println("Size 100 Worst Case Mean of Insertion Sort:" + sum / 10 + " ns");
        System.out.println();

        sum = .0;
        for (int i = 0; i < 10; ++i) {
            Integer[] sortData1000 = fillArray(TEST_SIZE_1000, Order.DESCENDING);
//          SortAlgorithms.printArray(sortData1000);
            sum += sortAndShowResult(sortData1000, Algorithm.INSERTION);
        }
        System.out.println("Size 1000 Worst Case Mean of Insertion Sort: " + sum / 10 + " ns");
        System.out.println();

        sum = .0;
        for (int i = 0; i < 10; ++i) {
            Integer[] sortData5000 = fillArray(TEST_SIZE_5000, Order.DESCENDING);
//          SortAlgorithms.printArray(sortData5000);
            sum += sortAndShowResult(sortData5000, Algorithm.INSERTION);
        }
        System.out.println("Size 5000 Worst Case Mean of Insertion Sort: " + sum / 10 + " ns");
        System.out.println();

        sum = .0;
        for (int i = 0; i < 10; ++i) {
            Integer[] sortData10000 = fillArray(TEST_SIZE_10000, Order.DESCENDING);
//          SortAlgorithms.printArray(sortData10000);
            sum += sortAndShowResult(sortData10000, Algorithm.INSERTION);
        }
        System.out.println("Size 10000 Worst Case Mean of Insertion Sort: " + sum / 10 + " ns");
        System.out.println();

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    }
    //*************************************************************************/



    //*******************************QUICK SORT********************************/

    private static void testQuickSort() {
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~ TEST OF QUICK SORT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        double sum = .0;
        for (int i = 0; i < 10; ++i) {
            Integer[] sortData100 = fillArray(TEST_SIZE_100, Order.ASCENDING);
//          SortAlgorithms.printArray(sortData100);
            sum += sortAndShowResult(sortData100, Algorithm.QUICK);
        }
        System.out.println("Size 100 Worst Case Mean of Quick Sort:" + sum / 10 + " ns");
        System.out.println();

        sum = .0;
        for (int i = 0; i < 10; ++i) {
            Integer[] sortData1000 = fillArray(TEST_SIZE_1000, Order.ASCENDING);
//          SortAlgorithms.printArray(sortData1000);
            sum += sortAndShowResult(sortData1000, Algorithm.QUICK);
        }
        System.out.println("Size 1000 Worst Case Mean of Quick Sort: " + sum / 10 + " ns");
        System.out.println();

        sum = .0;
        for (int i = 0; i < 10; ++i) {
            Integer[] sortData5000 = fillArray(TEST_SIZE_5000, Order.ASCENDING);
//          SortAlgorithms.printArray(sortData5000);
            sum += sortAndShowResult(sortData5000, Algorithm.QUICK);
        }
        System.out.println("Size 5000 Worst Case Mean of Quick Sort: " + sum / 10 + " ns");
        System.out.println();

        sum = .0;
        for (int i = 0; i < 10; ++i) {
            Integer[] sortData10000 = fillArray(TEST_SIZE_10000, Order.ASCENDING);
//          SortAlgorithms.printArray(sortData10000);
            sum += sortAndShowResult(sortData10000, Algorithm.QUICK);
        }
        System.out.println("Size 10000 Worst Case Mean of Quick Sort: " + sum / 10 + " ns");
        System.out.println();

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    }
    //*************************************************************************/



    //*******************************MERGE SORT********************************/
    private static void testMergeSort() {
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~ TEST OF MERGE SORT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        double sum = .0;
        for (int i = 0; i < 10; ++i) {
            Integer[] sortData100 = fillArray(TEST_SIZE_100, Order.NO_CHANGE);
//          SortAlgorithms.printArray(sortData100);
            sum += sortAndShowResult(sortData100, Algorithm.MERGE);
        }
        System.out.println("Size 100 Worst Case Mean of Merge Sort:" + sum / 10 + " ns");
        System.out.println();

        sum = .0;
        for (int i = 0; i < 10; ++i) {
            Integer[] sortData1000 = fillArray(TEST_SIZE_1000, Order.NO_CHANGE);
//          SortAlgorithms.printArray(sortData1000);
            sum += sortAndShowResult(sortData1000, Algorithm.MERGE);
        }
        System.out.println("Size 1000 Worst Case Mean of Merge Sort: " + sum / 10 + " ns");
        System.out.println();

        sum = .0;
        for (int i = 0; i < 10; ++i) {
            Integer[] sortData5000 = fillArray(TEST_SIZE_5000, Order.NO_CHANGE);
//          SortAlgorithms.printArray(sortData5000);
            sum += sortAndShowResult(sortData5000, Algorithm.MERGE);
        }
        System.out.println("Size 5000 Worst Case Mean of Merge Sort: " + sum / 10 + " ns");
        System.out.println();

        sum = .0;
        for (int i = 0; i < 10; ++i) {
            Integer[] sortData10000 = fillArray(TEST_SIZE_10000, Order.NO_CHANGE);
//          SortAlgorithms.printArray(sortData10000);
            sum += sortAndShowResult(sortData10000, Algorithm.MERGE);
        }
        System.out.println("Size 10000 Worst Case Mean of Merge Sort: " + sum / 10 + " ns");
        System.out.println();

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    }
    //*************************************************************************/

    private static void testMergeSortWithDLL() {
        System.out.println("~~~~~~~~~~~~~~~~~TEST OF MERGE SORT WITH DOUBLE LINKED LIST~~~~~~~~~~~~~~~~~");

        double sum = .0;
        for (int i = 0; i < 10; ++i) {
            Integer[] sortData100 = fillArray(TEST_SIZE_100, Order.NO_CHANGE);
//          SortAlgorithms.printArray(sortData100);
            sum += sortAndShowResult(sortData100, Algorithm.MERGE_WITH_DLL);
        }
        System.out.println("Size 100 Worst Case Mean of Merge Sort with Double Linked List:" + sum / 10 + " ns");
        System.out.println();

        sum = .0;
        for (int i = 0; i < 10; ++i) {
            Integer[] sortData1000 = fillArray(TEST_SIZE_1000, Order.NO_CHANGE);
//          SortAlgorithms.printArray(sortData1000);
            sum += sortAndShowResult(sortData1000, Algorithm.MERGE_WITH_DLL);
        }
        System.out.println("Size 1000 Worst Case Mean of Merge Sort with Double Linked List: " + sum / 10 + " ns");
        System.out.println();

        sum = .0;
        for (int i = 0; i < 10; ++i) {
            Integer[] sortData5000 = fillArray(TEST_SIZE_5000, Order.NO_CHANGE);
//          SortAlgorithms.printArray(sortData5000);
            sum += sortAndShowResult(sortData5000, Algorithm.MERGE_WITH_DLL);
        }
        System.out.println("Size 5000 Worst Case Mean of Merge Sort with Double Linked List: " + sum / 10 + " ns");
        System.out.println();

        sum = .0;
        for (int i = 0; i < 10; ++i) {
            Integer[] sortData10000 = fillArray(TEST_SIZE_10000, Order.NO_CHANGE);
//          SortAlgorithms.printArray(sortData10000);
            sum += sortAndShowResult(sortData10000, Algorithm.MERGE_WITH_DLL);
        }
        System.out.println("Size 10000 Worst Case Mean of Merge Sort with Double Linked List: " + sum / 10 + " ns");
        System.out.println();

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    }

    //******************************HEAP SORT**********************************/
    private static void testHeapSort() {
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~ TEST OF HEAP SORT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        double sum = .0;
        for (int i = 0; i < 10; ++i) {
            Integer[] sortData100 = fillArray(TEST_SIZE_100, Order.NO_CHANGE);
//            SortAlgorithms.printArray(sortData100);
            sum += sortAndShowResult(sortData100, Algorithm.HEAP);
        }
        System.out.println("Size 100 Worst Case Mean of Heap Sort:" + sum / 10 + " ns");
        System.out.println();

        sum = .0;
        for (int i = 0; i < 10; ++i) {
            Integer[] sortData1000 = fillArray(TEST_SIZE_1000, Order.NO_CHANGE);
            //SortAlgorithms.printArray(sortData1000);
            sum += sortAndShowResult(sortData1000, Algorithm.HEAP);
        }
        System.out.println("Size 1000 Worst Case Mean of Heap Sort: " + sum / 10 + " ns");
        System.out.println();

        sum = .0;
        for (int i = 0; i < 10; ++i) {
            Integer[] sortData5000 = fillArray(TEST_SIZE_5000, Order.NO_CHANGE);
          //SortAlgorithms.printArray(sortData5000);
            sum += sortAndShowResult(sortData5000, Algorithm.HEAP);
        }
        System.out.println("Size 5000 Worst Case Mean of Heap Sort: " + sum / 10 + " ns");
        System.out.println();

        sum = .0;
        for (int i = 0; i < 10; ++i) {
            Integer[] sortData10000 = fillArray(TEST_SIZE_10000, Order.NO_CHANGE);
          //SortAlgorithms.printArray(sortData10000);
            sum += sortAndShowResult(sortData10000, Algorithm.HEAP);
        }
        System.out.println("Size 10000 Worst Case Mean of Heap Sort: " + sum / 10 + " ns");
        System.out.println();

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    }
    //*************************************************************************/

    //****************************UTILITY METHODS******************************/
    private static Integer[] fillArray(int size, Order order) {
        Random rand = new Random();
        Integer[] sortData = new Integer[size];

        for (int i = 0; i < size; ++i)
            sortData[i] = rand.nextInt(TEST_SIZE_10000 * 2);

        if (order == Order.DESCENDING)
            Arrays.sort(sortData, Collections.reverseOrder());
        else if (order == Order.ASCENDING)
            Arrays.sort(sortData);

        return (sortData);
    }

}