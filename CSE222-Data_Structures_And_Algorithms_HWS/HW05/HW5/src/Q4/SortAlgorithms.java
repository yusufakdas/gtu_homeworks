package Q4;

public class SortAlgorithms {

    /**
     * The object of this class is not instantiated
     */
    private SortAlgorithms() { }

    public static <T> void printArray(T[] array) {
        for (T element : array)
            System.out.print(element + " ");
        System.out.println("\n");
    }

    ////////////////////////////////////////////////////////////////////////////
    /**
     * Insertion Sort algorithm
     * @param data Array to be sorted
     * @param <T> Comparable generic type
     */
    public static <T extends Comparable<T>> void insertionSort(T[] data) {
        for (int i = 1; i < data.length; ++i) {
            T value = data[i];
            int index = i;
            while (index > 0 && value.compareTo(data[index - 1]) < 0) {
                data[index] = data[index - 1];
                --index;
            }
            data[index] = value;
        }
    }
    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    ////////////////////////////////////////////////////////////////////////////
    /**
     * Merge Sort algorithm
     * @param data Array to be sorted
     * @param <T> Comparable generic type
     */
    @SuppressWarnings("unchecked")
    public static <T extends Comparable<T>> void mergeSort(T[] data) {
        int len = data.length;

        if (len > 1) {
            int middle = len / 2;

            T[] leftSide = (T[]) new Comparable[middle];
            T[] rightSide = (T[]) new Comparable[len - middle];

            System.arraycopy(data, 0, leftSide, 0, middle);
            System.arraycopy(data, middle, rightSide, 0, len - middle);

            mergeSort(leftSide);
            mergeSort(rightSide);
            merge(leftSide, rightSide, data);
        }
    }

    /**
     * Merge operation of merge sort
     * @param left Left side of the array
     * @param right Right side of the array
     * @param output Output array
     * @param <T> Comparable generic type
     */
    private static <T extends Comparable<T>> void merge(T[] left, T[] right,
                                                        T[] output) {
        int leftCounter = 0, rightCounter = 0, i = 0;
        while (leftCounter < left.length && rightCounter < right.length) {
            if (left[leftCounter].compareTo(right[rightCounter]) < 0)
                output[i++] = left[leftCounter++];
            else
                output[i++] = right[rightCounter++];
        }
        while (leftCounter < left.length)
            output[i++] = left[leftCounter++];
        while (rightCounter < right.length)
            output[i++] = right[rightCounter++];
    }
    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    ////////////////////////////////////////////////////////////////////////////

    /**
     * Builds heap with given array, and sorts it using Heap Sort algorithm
     * @param arr Array to be sorted
     * @param <T> Comparable generic type
     */
    public static <T extends Comparable<T>> void heapSort(T[] arr) {
        buildHeap(arr);
        shrinkHeap(arr);
    }

    /**
     * Builds max heap using given array
     * @param arr Array to be sorted
     * @param <T> Comparable generic type
     */
    private static <T extends Comparable<T>> void buildHeap(T[] arr) {
        int i = 1;

        while (i < arr.length) {
            ++i;
            int child = i - 1;
            int parent = (child - 1) / 2;
            while (parent >= 0 && arr[parent].compareTo(arr[child]) < 0) {
                swap(arr, parent, child);
                child = parent;
                parent = (child - 1) / 2;
            }
        }
    }

    /**
     * Puts greater elements to the end of array and heapifies.
     * @param arr Array to be sorted
     * @param <T> Comparable generic type
     */
    private static <T extends Comparable<T>> void shrinkHeap(T[] arr) {
        int n = arr.length;

        while (n > 0) {
            n--;
            swap(arr, 0, n);
            int parent = 0;
            while (true) {
                int leftChild = 2 * parent + 1;
                int rightChild = leftChild + 1;

                if (leftChild >= n) break;

                int maxChild = leftChild;
                if (rightChild < n && arr[leftChild].compareTo(arr[rightChild]) < 0)
                    maxChild = rightChild;
                if (arr[parent].compareTo(arr[maxChild]) < 0) {
                    swap(arr, parent, maxChild);
                    parent = maxChild;
                } else {
                    break;
                }
            }
        }
    }

    /**
     * Swaps elements with each other
     * @param arr Array to be swapped its elements
     * @param i First index
     * @param j Second index
     * @param <T> Comparable generic type
     */
    private static <T> void swap(T[] arr, int i, int j) {
        T data = arr[i];
        arr[i] = arr[j];
        arr[j] = data;
    }
    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    ////////////////////////////////////////////////////////////////////////////
    /**
     * Wrapper method for QuickSort algorithm
     * @param arr Array to be sorted
     * @param <T> Comparable generic type
     */
    public static <T extends Comparable<T>> void quickSort(T[] arr) {
        quickSortRecursive(arr, 0, arr.length - 1);
    }

    /**
     * QuickSort algorithm
     * @param arr Array to be sorted
     * @param first First index
     * @param last Last index
     * @param <T> Comparable generic type
     */
    private static <T extends Comparable<T>> void quickSortRecursive(T[] arr,
                                                                     int first,
                                                                     int last) {
        if (first < last) {
            int pivotIndex = partition(arr, first, last);
            quickSortRecursive(arr, first, pivotIndex - 1);
            quickSortRecursive(arr, pivotIndex + 1, last);
        }
    }

    /**
     * Partitions the given array
     * @param arr Array to be sorted
     * @param first First index
     * @param last Last index
     * @param <T> Comparable generic type
     * @return Pivot index
     */
    private static <T extends Comparable<T>> int partition(T[] arr,
                                                           int first,
                                                           int last) {
        T pivot = arr[first];
        int up = first;
        int down = last;

        do {
            while ((up < last) && (pivot.compareTo(arr[up]) >= 0))
                ++up;
            while (pivot.compareTo(arr[down]) < 0)
                --down;
            if (up < down)
                swap(arr, up, down);
        } while (up < down);
        swap(arr, first, down);

        return (down);
    }
    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
}