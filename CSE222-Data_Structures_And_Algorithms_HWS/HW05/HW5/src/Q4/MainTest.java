package Q4;

import Q3.MergeSortDoubleLinkedList;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;

public class MainTest {

    private static final int TEST_SIZE = 10;

    public static void main(String[] args) {
        Integer[][] size50 = randomDataForSize(50);
        Integer[][] size100 = randomDataForSize(100);
        Integer[][] size250 = randomDataForSize(250);
        Integer[][] size500 = randomDataForSize(500);
        Integer[][] size1000 = randomDataForSize(1000);
        Integer[][] size2500 = randomDataForSize(2500);
        Integer[][] size5000 = randomDataForSize(5000);
        Integer[][] size10000 = randomDataForSize(10000);
        Integer[][] size15000 = randomDataForSize(15000);
        Integer[][] size25000 = randomDataForSize(25000);

        //*********************************************************************/
        System.out.println("##### TEST FOR SIZE 50 #####");
        System.out.println("DATA TO BE SORTED: ");
        //printArray(size50);

        insertionSortTest(size50);
        mergeSortTest(size50);
        mergeSortDLLTest(size50);
        quickSortTest(size50);
        heapSortTest(size50);
        System.out.println("---------------------------------------------------");

        System.out.println("##### TEST FOR SIZE 100 #####");
        System.out.println("DATA TO BE SORTED: ");
        //printArray(size100);

        insertionSortTest(size100);
        mergeSortTest(size100);
        mergeSortDLLTest(size100);
        quickSortTest(size100);
        heapSortTest(size100);
        System.out.println("---------------------------------------------------");

        System.out.println("##### TEST FOR SIZE 250 #####");
        System.out.println("DATA TO BE SORTED: ");
        //printArray(size100);

        insertionSortTest(size250);
        mergeSortTest(size250);
        mergeSortDLLTest(size250);
        quickSortTest(size250);
        heapSortTest(size250);
        System.out.println("---------------------------------------------------");

        System.out.println("##### TEST FOR SIZE 500 #####");
        System.out.println("DATA TO BE SORTED: ");
        //printArray(size500);

        insertionSortTest(size500);
        mergeSortTest(size500);
        mergeSortDLLTest(size500);
        quickSortTest(size500);
        heapSortTest(size500);
        System.out.println("---------------------------------------------------");

        System.out.println("##### TEST FOR SIZE 1000 #####");
        System.out.println("DATA TO BE SORTED: ");
        //printArray(size1000);

        insertionSortTest(size1000);
        mergeSortTest(size1000);
        mergeSortDLLTest(size1000);
        quickSortTest(size1000);
        heapSortTest(size1000);
        System.out.println("---------------------------------------------------");

        System.out.println("##### TEST FOR SIZE 2500 #####");
        System.out.println("DATA TO BE SORTED: ");
        //printArray(size2500);

        insertionSortTest(size2500);
        mergeSortTest(size2500);
        mergeSortDLLTest(size2500);
        quickSortTest(size2500);
        heapSortTest(size2500);
        System.out.println("---------------------------------------------------");

        System.out.println("##### TEST FOR SIZE 5000 #####");
        System.out.println("DATA TO BE SORTED: ");
        //printArray(size5000);

        insertionSortTest(size5000);
        mergeSortTest(size5000);
        mergeSortDLLTest(size5000);
        quickSortTest(size5000);
        heapSortTest(size5000);
        System.out.println("---------------------------------------------------");

        System.out.println("##### TEST FOR SIZE 10000 #####");
        System.out.println("DATA TO BE SORTED: ");
        //printArray(size10000);

        insertionSortTest(size10000);
        mergeSortTest(size10000);
        mergeSortDLLTest(size10000);
        quickSortTest(size10000);
        heapSortTest(size10000);
        System.out.println("---------------------------------------------------");

        System.out.println("##### TEST FOR SIZE 15000 #####");
        System.out.println("DATA TO BE SORTED: ");
        //printArray(size15000);

        insertionSortTest(size15000);
        mergeSortTest(size15000);
        mergeSortDLLTest(size15000);
        quickSortTest(size15000);
        heapSortTest(size15000);
        System.out.println("---------------------------------------------------");

        System.out.println("##### TEST FOR SIZE 25000 #####");
        System.out.println("DATA TO BE SORTED: ");
        //printArray(size25000);

        insertionSortTest(size25000);
        mergeSortTest(size25000);
        mergeSortDLLTest(size25000);
        quickSortTest(size25000);
        heapSortTest(size25000);

        System.out.println("---------------------------------------------------");
    }

    private static void insertionSortTest(Integer[][] data) {
        long start, finish;
        long[] time = new long[TEST_SIZE];
        Integer[][] copy;
        double elapsedTime;

        copy = copyArray(data);
        System.out.println("-- INSERTION SORT --");
        for (int i = 0; i < TEST_SIZE; ++i) {
            start = System.nanoTime();
            SortAlgorithms.insertionSort(copy[i]);
            finish = System.nanoTime();
            time[i] = finish - start;
        }
        elapsedTime = averageElapsedTime(time);
        System.out.println("AVERAGE TIME FOR INSERTION SORT: " + elapsedTime + "\n");
    }

    private static void mergeSortTest(Integer[][] data) {
        long start, finish;
        long[] time = new long[TEST_SIZE];
        Integer[][] copy;
        double elapsedTime;

        copy = copyArray(data);
        System.out.println("-- MERGE SORT --");
        for (int i = 0; i < TEST_SIZE; ++i) {
            start = System.nanoTime();
            SortAlgorithms.mergeSort(copy[i]);
            finish = System.nanoTime();
            time[i] = finish - start;
        }
        elapsedTime = averageElapsedTime(time);
        System.out.println("AVERAGE TIME FOR MERGER SORT: " + elapsedTime + "\n");
    }

    private static void mergeSortDLLTest(Integer[][] data) {
        long start, finish;
        long[] time = new long[TEST_SIZE];
        Integer[][] copy;
        double elapsedTime;

        copy = copyArray(data);
        System.out.println("-- MERGE SORT WITH DLL --");
        for (int i = 0; i < TEST_SIZE; ++i) {
            LinkedList<Integer> list = new LinkedList<>(Arrays.asList(copy[i]));
            start = System.nanoTime();
            MergeSortDoubleLinkedList.sort(list);
            finish = System.nanoTime();
            time[i] = finish - start;
        }
        elapsedTime = averageElapsedTime(time);
        System.out.println("AVERAGE TIME FOR MERGE SORT WITH DLL: " + elapsedTime + "\n");
    }

    private static void quickSortTest(Integer[][] data) {
        long start, finish;
        long[] time = new long[TEST_SIZE];
        Integer[][] copy;
        double elapsedTime;

        copy = copyArray(data);
        System.out.println("-- QUICK SORT --");
        for (int i = 0; i < TEST_SIZE; ++i) {
            start = System.nanoTime();
            SortAlgorithms.quickSort(copy[i]);
            finish = System.nanoTime();
            time[i] = finish - start;
        }
        elapsedTime = averageElapsedTime(time);
        System.out.println("AVERAGE TIME FOR QUICK SORT: " + elapsedTime + "\n");
    }

    private static void heapSortTest(Integer[][] data) {
        long start, finish;
        long[] time = new long[TEST_SIZE];
        Integer[][] copy;
        double elapsedTime;

        copy = copyArray(data);
        System.out.println("-- HEAP SORT --");
        for (int i = 0; i < TEST_SIZE; ++i) {
            start = System.nanoTime();
            SortAlgorithms.heapSort(copy[i]);
            finish = System.nanoTime();
            time[i] = finish - start;
        }

        elapsedTime = averageElapsedTime(time);
        System.out.println("AVERAGE TIME FOR HEAP SORT: " + elapsedTime + "\n");
    }

    private static Integer[] generateRandom(int size) {
        Random rand = new Random();
        Integer[] data = new Integer[size];

        for (int i = 0; i < size; ++i)
            data[i] = rand.nextInt(size * 2);

        return (data);
    }

    private static Integer[][] randomDataForSize(int size) {
        Integer[][] data = new Integer[TEST_SIZE][size];

        for (int i = 0; i < TEST_SIZE; ++i)
            data[i] = generateRandom(size);

        return (data);
    }

    private static double averageElapsedTime(long[] data) {
        double average = .0;
        for (long element : data)
            average += element;
        return (average / data.length);
    }

    private static void printArray(Integer[][] data) {
        for (int i = 0; i < TEST_SIZE; ++i) {
            for (Integer element : data[i])
                System.out.print(element + " ");
            System.out.println("\n");
        }
        System.out.println();
    }

    private static Integer[][] copyArray(Integer[][] array) {
        Integer[][] cloneData = new Integer[TEST_SIZE][];
        for (int i = 0; i < TEST_SIZE; ++i)
            cloneData[i] = array[i].clone();
        return (cloneData);
    }
}
