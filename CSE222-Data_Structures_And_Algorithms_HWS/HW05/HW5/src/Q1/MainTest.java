package Q1;

public class MainTest {

    public static void main(String[] args) {

        MyMap<String, String> enTrDictionary = new OpenDoubleHashMap<>();

        System.out.println("---- Testing Open Addressing Double Hashing Map ----");

        System.out.println("Adding some elements to the map...\n");
        enTrDictionary.put("apple", "elma");
        enTrDictionary.put("house", "ev");
        enTrDictionary.put("school", "okul");
        enTrDictionary.put("aeroplane", "ucak");
        enTrDictionary.put("country", "ulke");
        enTrDictionary.put("blue", "mavi");
        enTrDictionary.put("red", "kirmizi");

        System.out.println("---- Current status of the map ----");
        System.out.println(enTrDictionary);

        System.out.println("Adding some elements to the map for rehashing...\n");
        enTrDictionary.put("one", "bir");
        enTrDictionary.put("notebook", "defter");
        enTrDictionary.put("book", "kitap");
        enTrDictionary.put("computer", "bilgisayar");
        enTrDictionary.put("phone", "telefon");
        enTrDictionary.put("achievement", "basarim");
        enTrDictionary.put("air", "hava");

        System.out.println("---- Current status of the map ----");
        System.out.println(enTrDictionary);

        System.out.println("---- Removing some elements from map...");
        System.out.println("Size before removing: " + enTrDictionary.size());
        System.out.println("----> " + enTrDictionary.remove("apple"));
        System.out.println("----> " + enTrDictionary.remove("GTU"));
        System.out.println("----> " + enTrDictionary.remove("one"));
        System.out.println("----> " + enTrDictionary.remove("phone"));
        System.out.println("----> " + enTrDictionary.remove("Gebze"));
        System.out.println("----> " + enTrDictionary.remove("blue"));
        System.out.println("----> " + enTrDictionary.remove("red"));
        System.out.println("----> " + enTrDictionary.remove("house"));
        System.out.println("----> " + enTrDictionary.remove("Istanbul"));
        System.out.println("Size after removing: " + enTrDictionary.size() + "\n");

        System.out.println("---- Current status of the map after removing ----");
        System.out.println(enTrDictionary);

        System.out.println("Adding some elements again...\n");
        System.out.println("----> " + enTrDictionary.put("amount", "miktar"));
        System.out.println("----> " + enTrDictionary.put("beginning", "baslangic"));
        System.out.println("----> " + enTrDictionary.put("Turkey", "Turkiye") + "\n");

        System.out.println("---- Current status of the map before rehashing ----");
        System.out.println(enTrDictionary);

        System.out.println("Adding some elements again...\n");
        enTrDictionary.put("bad", "kotu");
        enTrDictionary.put("because", "cunku");
        enTrDictionary.put("good", "iyi");
        enTrDictionary.put("beautiful", "guzel");
        enTrDictionary.put("end", "son");
        enTrDictionary.put("before", "once");
        enTrDictionary.put("after", "sonra");
        enTrDictionary.put("ancient", "antik");
        enTrDictionary.put("appearance", "dis gorunus");

        System.out.println("---- Current status of the map after rehashing ----");
        System.out.println(enTrDictionary);

        System.out.println("---- Getting an element...\n");
        System.out.println("----> " + enTrDictionary.get("amount"));
        System.out.println("----> " + enTrDictionary.get("because"));
        System.out.println("----> " + enTrDictionary.get("write"));
        System.out.println("----> " + enTrDictionary.get("Hi"));
        System.out.println("----> " + enTrDictionary.get("bad"));
        System.out.println("----> " + enTrDictionary.get("take"));
        System.out.println("----> " + enTrDictionary.get("good"));
        System.out.println("----> " + enTrDictionary.get("amount"));
        System.out.println("----> " + enTrDictionary.get("come"));
        System.out.println("----> " + enTrDictionary.get("Hello"));
        System.out.println("----> " + enTrDictionary.get("Turkey") + "\n");

        System.out.println("---- Trying to put an elements that is present in the map...\n");
        System.out.println("----> " + enTrDictionary.put("beautiful", "+"));
        System.out.println("----> " + enTrDictionary.put("because", "-"));
        System.out.println("----> " + enTrDictionary.put("after", "123"));
        System.out.println("----> " + enTrDictionary.put("ancient", "369"));
        System.out.println("----> " + enTrDictionary.put("amount", "157"));
        System.out.println("----> " + enTrDictionary.put("aeroplane", "8979") + "\n");

        System.out.println("---- Current status of the map after changing ----");
        System.out.println(enTrDictionary);
        System.out.println("----------------------------------------------------");
        System.out.println("\n\n\n");

        MyMap<Integer, Character> map1 = new OpenDoubleHashMap<>(7, 5);

        System.out.println("Adding some elements to the map...\n");
        System.out.println(map1.put(1, 'A'));
        System.out.println(map1.put(2, 'B'));
        System.out.println(map1.put(3, 'C'));
        System.out.println(map1.put(4, 'D'));
        System.out.println(map1.put(5, 'E'));

        System.out.println("---- Current status of the map ----");
        System.out.println(map1);

        System.out.println(map1.put(1, '+'));
        System.out.println(map1.put(2, '-'));
        System.out.println(map1.put(4, '*'));
        System.out.println(map1.put(5, '/'));
        System.out.println(map1.put(14, 'M'));
        System.out.println(map1.put(24, 'K'));
        System.out.println(map1.put(12, 'L'));
        System.out.println(map1.put(68, 'Y'));
        System.out.println(map1.put(45, 'S'));
        System.out.println(map1.put(74, 'T'));
        System.out.println(map1.put(19, 'R'));
        System.out.println(map1.put(53, 'R'));

        System.out.println("---- Current status of the map ----");
        System.out.println(map1);

        System.out.println("Size before removing: " + map1.size());
        System.out.println("----> " + map1.remove(68));
        System.out.println("----> " + map1.remove(19));
        System.out.println("----> " + map1.remove(5));
        System.out.println("----> " + map1.remove(53));
        System.out.println("----> " + map1.remove(24));
        System.out.println("----> " + map1.remove(45));
        System.out.println("Size after removing: " + map1.size());
        System.out.println();

        System.out.println("---- Current status of the map ----");
        System.out.println(map1);

        System.out.println("----> " + map1.get(1));
        System.out.println("----> " + map1.get(7));
        System.out.println("----> " + map1.get(45));
        System.out.println("----> " + map1.get(12));
        System.out.println("----> " + map1.get(74));
        System.out.println("----> " + map1.get(14));
        System.out.println("----> " + map1.get(89));
        System.out.println("----> " + map1.get(68));

        System.out.println();
        System.out.println("CURRENT TABLE LENGTH BEFORE REHASHING --> " +
                ((OpenDoubleHashMap<Integer, Character>) map1).getCurrentTableLength());
        System.out.println(map1);

        map1.put(1, 'J');
        map1.put(19, 'O');
        map1.put(24, '1');
        map1.put(48, 'y');
        map1.put(785, 'u');
        map1.put(66, 's');
        map1.put(9, 'u');
        map1.put(158, 'f');
        map1.put(0, 'a');
        map1.put(-96, 'k');
        map1.put(321, 'd');
        map1.put(31231, 'a');
        map1.put(7654, 's');
        map1.put(31, 'p');
        map1.put(7654, 'e');
        map1.put(74, 's');

        System.out.println();
        System.out.println("CURRENT TABLE LENGTH AFTER REHASHING --> " +
                ((OpenDoubleHashMap<Integer, Character>) map1).getCurrentTableLength());
        System.out.println(map1);


    }

}
