package Q1;

import java.io.Serializable;
import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.Objects;

public class OpenDoubleHashMap<K, V> implements MyMap <K, V>, Serializable {

    /**
     * The table that holds key-value pairs
     */
    private Entry<K, V>[] table;

    /**
     * A mark for deleted item's place on the table
     */
    private final Entry<K, V> DELETED = new Entry<>(null, null);

    /**
     * Load factor threshold for rehashing
     */
    private static final double LOAD_FACTOR_THRESHOLD = .5;

    /**
     * Initial capacity of the table
     */
    private final int INITIAL_CAPACITY;

    /**
     * Prime number value for double hashing
     */
    private final int DOUBLE_HASHING_PRIME_CONSTANT;

    /**
     * Number of deleted pairs
     */
    private int countDeleted;

    /**
     * Number of pairs on the table
     */
    private int countEntries;

    /**
     * No parameter constructor that initializes the table
     */
    public OpenDoubleHashMap() {
        this(23, 7);
    }

    /**
     * Constructor that takes initial size for the hash table and a constant
     * for double hashing
     * @param initialSize Initial size of the table
     * @param doubleHashConstant Constant value for double hashing
     */
    @SuppressWarnings("unchecked")
    OpenDoubleHashMap(int initialSize, int doubleHashConstant) {
        if (initialSize < doubleHashConstant)
            throw new InvalidParameterException();
        INITIAL_CAPACITY = initialSize;
        DOUBLE_HASHING_PRIME_CONSTANT = doubleHashConstant;
        table = new Entry[INITIAL_CAPACITY];
    }

    /**
     * Gets the value of specified key.
     * @param key Key of specified value
     * @return If key-value mapping is found, returns value, otherwise null.
     */
    @Override
    public V get(Object key) {
        int index = find(key);
        return ((table[index] == null) ? null : table[index].getValue());
    }

    /**
     * Adds an key-value pair or updates the existing pair's value.
     * @param key Key of the pair
     * @param value Value of the pair
     * @return If key-value mapping is added to table, returns null, otherwise
     * old value.
     */
    @Override
    public V put(K key, V value) {
        int index = find(key);
        if (table[index] == null) {
            table[index] = new Entry<>(key, value);
            ++countEntries;
            if (LOAD_FACTOR_THRESHOLD <= getLoadFactor())
                rehash();
            return (null);
        }
        return (table[index].setValue(value));
    }

    /**
     * Removes a key-value pair from the table.
     * @param key Key of pair to be deleted
     * @return If pair is removed, returns its value, otherwise null.
     */
    @Override
    public V remove(Object key) {
        int index = find(key);

        if (table[index] != null) {
            V value = table[index].getValue();
            table[index] = DELETED;
            --countEntries;
            ++countDeleted;
            return (value);
        }
        return (null);
    }

    /**
     * Size of pairs that are present on the table
     * @return size of the pair numbers except for deleted
     */
    @Override
    public int size() {
        return (countEntries);
    }

    /**
     * Checks if table is empty or not
     * @return If table is empty, returns true, otherwise false.
     */
    @Override
    public boolean isEmpty() {
        return (countEntries == 0);
    }

    /**
     * Gets the current table length to understand rehashing
     * @return Current table length
     */
    int getCurrentTableLength() {
        return (table.length);
    }

    /**
     * Calculates the load factor for rehashing
     * @return load factor
     */
    private double getLoadFactor() {
        return ((double) (countDeleted + countEntries) / table.length);
    }

    /**
     * Searches for a key-value pair on the table
     * @param key key of pair
     * @return If key-value pair is present on the table, return its index,
     * otherwise empty slot's index
     */
    private int find(Object key) {
        int index = key.hashCode() % table.length;
        index += (index < 0) ? table.length : 0;
        if (table[index] == null)
            return (index);

        // Collision occurs. Search for using double hashing
        int doubleHash = getDoubleHashingIndex(key);
        while (table[index] != null && !key.equals(table[index].getKey())) {
            index += doubleHash;
            if (index >= table.length)
                index = 0;
        }
        return (index);
    }

    /**
     * This is a double hashing function used in collision.
     * @param key Key of pair
     * @return Calculated index value.
     */
    private int getDoubleHashingIndex(Object key) {
        int objectHash = Objects.hash(key);
        int index = DOUBLE_HASHING_PRIME_CONSTANT - (objectHash % DOUBLE_HASHING_PRIME_CONSTANT);
        return ((index >= table.length ?
                (index % DOUBLE_HASHING_PRIME_CONSTANT) : index));
    }

    /**
     * If load factor threshold exceeds, rehash the table
     */
    @SuppressWarnings("unchecked")
    private void rehash() {
        Entry<K, V>[] oldTable = table;
        table = new Entry[table.length * 2 + 1];

        countDeleted = countEntries = 0;
        for (Entry<K, V> entry : oldTable)
            if ((entry != null) && (entry != DELETED))
                put(entry.getKey(), entry.getValue());
    }

    /**
     * equals method generated by IDE
     * @param o Object to be compared
     * @return If this and incoming object are equal, return true, otherwise false.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OpenDoubleHashMap<?, ?> that = (OpenDoubleHashMap<?, ?>) o;
        return (countDeleted == that.countDeleted &&
                countEntries == that.countEntries &&
                Arrays.equals(table, that.table) &&
                Objects.equals(DELETED, that.DELETED));
    }

    /**
     * hashCode method generated by IDE
     * @return Hash code of this class
     */
    @Override
    public int hashCode() {
        int result = Objects.hash(DELETED, countDeleted, countEntries);
        result = 31 * result + Arrays.hashCode(table);
        return result;
    }

    /**
     * Overrides toString method for this class
     * @return String representation of the table
     */
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < table.length; ++i) {
            if (table[i] != null && table[i] != DELETED) {
                stringBuilder.append("{");
                stringBuilder.append(table[i].toString());
                stringBuilder.append(", 'index: ");
                stringBuilder.append(i);
                stringBuilder.append("'}\n");
            }
        }
        return (stringBuilder.toString());
    }

    // Inner class for key-value mappings
    private static class Entry<K, V> {
        /**
         * Key
         */
        private final K key;

        /**
         * Value
         */
        private V value;

        /**
         * Constructor that takes key and value as its arguments
         * @param key key
         * @param value value
         */
        private Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        /**
         * Getter method for key
         * @return key
         */
        public K getKey() {
            return (key);
        }

        /**
         * Getter method for value
         * @return value
         */
        public V getValue() {
            return (value);
        }

        /**
         * Setter method for value
         * @param newValue value to be changed with old one
         * @return the old value
         */
        public V setValue(V newValue) {
            V oldValue = value;
            value = newValue;
            return (oldValue);
        }

        /**
         * Overrides toString method for this class
         * @return String representation of this class
         */
        @Override
        public String toString() {
            return (key.toString() + ", " + value.toString());
        }
    }

}