package Q1;

public interface MyMap<K, V> {

    /**
     * Gets the value of specified key.
     * @param key Key of specified value
     * @return If key-value mapping is found, returns value, otherwise null.
     */
    V get(Object key);

    /**
     * Adds an key-value pair or updates the existing pair's value.
     * @param key Key of the pair
     * @param value Value of the pair
     * @return If key-value mapping is added to table, returns null, otherwise
     * old value.
     */
    V put(K key, V value);

    /**
     * Removes a key-value pair from the table.
     * @param key Key of pair to be deleted
     * @return If pair is removed, returns its value, otherwise null.
     */
    V remove(Object key);

    /**
     * Size of pairs that are present on the table
     * @return size of the pair numbers except for deleted
     */
    int size();

    /**
     * Checks if table is empty or not
     * @return If table is empty, returns true, otherwise false.
     */
    boolean isEmpty();

}