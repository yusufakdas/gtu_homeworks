/* author: @yusufakdas */
package Q3;

import java.util.List;
import java.util.LinkedList;
import java.util.ListIterator;

public class MergeSortDoubleLinkedList {

    /**
     * The object of this class is not instantiated
     */
    private MergeSortDoubleLinkedList() { }

	/**
	 * Splits a linked list according to given index range
	 * @param lst The linked list to be split
	 * @param begin Beginning index
	 * @param end End index
	 * @return The new formed linked list
	 */
	private static<E> List<E> splitLinkedList(List<E> lst, int begin, int end) {
		List<E> newList = new LinkedList<>();
		ListIterator<E> iter = lst.listIterator(begin);

		for (int i = begin; i < end; ++i)
			newList.add(iter.next());

		return (newList);
	}

	/**
	 * Prints given linked list on the screen
	 * @param lst Linked list to be printed
     */
	public static<E> void printLinkedList(List<E> lst) {
		for (E e : lst) 
			System.out.print(e + " ");
		System.out.println();
	}

	/**
	 * Merges the two sorted linked lists into one linked list
	 * @param leftSide The left-hand side linked list
	 * @param rightSide The left-hand side linked list
	 * @param result The merged result list
	 */
	private static<E extends Comparable<E>> void mergeOperation(List<E> leftSide, 
			List<E> rightSide, List<E> result) {
		ListIterator<E> leftIterator = leftSide.listIterator();
		ListIterator<E> rightIterator = rightSide.listIterator();
		ListIterator<E> resIterator = result.listIterator();

		// Merge elements using listIterator
		while (leftIterator.hasNext() && rightIterator.hasNext()) {
            resIterator.next();
			E leftSideItem = leftIterator.next();
			E rightSideItem = rightIterator.next();
			if (leftSideItem.compareTo(rightSideItem) < 0) {
                resIterator.set(leftSideItem);
                rightIterator.previous();
			} else {
                resIterator.set(rightSideItem);
                leftIterator.previous();
			}
		} 

		// Merge the remaining items of right side
		while (rightIterator.hasNext()) {
            resIterator.next();
            resIterator.set(rightIterator.next());
		}

		// Merge the remaining items of left side
		while (leftIterator.hasNext()) {
            resIterator.next();
            resIterator.set(leftIterator.next());
		}
	}

	/**
	 * Sorts the linked list using merge sort algorithm
	 * @param theList The double linked list to be sorted
	 * pre: The item type included by theList must implement Comparable interface
	 * post: Sorted list
	 */
	public static<E extends Comparable<E>> void sort(List<E> theList) {
		int len = theList.size();	

		if (len > 1) {
			int middle = len / 2;
			
			// Get the left side of the list
			List<E> leftList = splitLinkedList(theList, 0, middle);

			// Get the right side of the list
			List<E> rightList = splitLinkedList(theList, middle, len);
			
			// Sort the left-hand side of the list recursively
            sort(leftList);

			// Sort the right-hand side of the list recursively
            sort(rightList);
			
			// Merge sorted left and right lists into the original list
			mergeOperation(leftList, rightList, theList);
		}
	}

}
