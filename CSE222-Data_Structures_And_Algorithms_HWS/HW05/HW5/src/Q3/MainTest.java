package Q3;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.LinkedList;

public class MainTest {

    public static void main(String[] args) {
        Random rand = new Random();

        System.out.println("----> Sorts data using Integer type <----");
        {
            List<Integer> list = new LinkedList<>();
            for (int i = 0; i < 36; ++i)
                list.add(rand.nextInt(100));
            sortAndPrint(list);
        }

        System.out.println("--------------------------------------------------");

        System.out.println("----> Sorts data using Double type <----");
        {
            List<Double> list = new LinkedList<>();
            double[] data = {12.85, -5.72, 9.45, 4.24, 3.69, -1.26, 3.11, 45.42,
                             24.365, 20.49, -66.158, 37.45, 26.179, 12.12, 14.5};
            for (double d : data)
                list.add(d);
            sortAndPrint(list);
        }

        System.out.println("--------------------------------------------------");

        System.out.println("----> Sorts data using String type <----");
        {
            String[] data = {"Istanbul", "Kocaeli", "Izmir", "Ankara", "Nigde",
                             "Trabzon", "Mardin", "Erzurum", "Bursa", "Canakkale",
                             "Yalova", "Van", "Hatay", "Adana", "Zonguldak"};
            LinkedList<String> list = new LinkedList<>(Arrays.asList(data));
            sortAndPrint(list);
        }

    }

    /**
     * Sorts and prints given double linked list on the screen.
     * @param list Double linked list to be sorted and to be printed
     * @param <E> Comparable generic type
     */
    private static<E extends Comparable<E>> void sortAndPrint(List<E> list) {
        System.out.println("Before sorting: ");
        MergeSortDoubleLinkedList.printLinkedList(list);

        MergeSortDoubleLinkedList.sort(list);

        System.out.println("After sorting: ");
        MergeSortDoubleLinkedList.printLinkedList(list);

        System.out.println();
    }

}
