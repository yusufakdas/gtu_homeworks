~~~~~~~~~~~~~~~~~~~~~~~~~~ TEST OF INSERTION SORT ~~~~~~~~~~~~~~~~~~~~~~~~~~
Size 100 Worst Case Mean of Insertion Sort:218311.2 ns

Size 1000 Worst Case Mean of Insertion Sort: 5655350.3 ns

Size 5000 Worst Case Mean of Insertion Sort: 3.11268464E7 ns

Size 10000 Worst Case Mean of Insertion Sort: 1.149720157E8 ns

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




~~~~~~~~~~~~~~~~~~~~~~~~~~~~ TEST OF QUICK SORT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Size 100 Worst Case Mean of Quick Sort:290726.1 ns

Size 1000 Worst Case Mean of Quick Sort: 2366499.6 ns

Size 5000 Worst Case Mean of Quick Sort: 1.78284713E7 ns

Size 10000 Worst Case Mean of Quick Sort: 6.24384247E7 ns

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




~~~~~~~~~~~~~~~~~~~~~~~~~~~~ TEST OF MERGE SORT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Size 100 Worst Case Mean of Merge Sort:94340.9 ns

Size 1000 Worst Case Mean of Merge Sort: 235338.4 ns

Size 5000 Worst Case Mean of Merge Sort: 2155931.7 ns

Size 10000 Worst Case Mean of Merge Sort: 5970175.3 ns

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




~~~~~~~~~~~~~~~~~TEST OF MERGE SORT WITH DOUBLE LINKED LIST~~~~~~~~~~~~~~~~~
Size 100 Worst Case Mean of Merge Sort with Double Linked List:486913.8 ns

Size 1000 Worst Case Mean of Merge Sort with Double Linked List: 928711.5 ns

Size 5000 Worst Case Mean of Merge Sort with Double Linked List: 3760515.0 ns

Size 10000 Worst Case Mean of Merge Sort with Double Linked List: 2765630.8 ns

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




~~~~~~~~~~~~~~~~~~~~~~~~~~~~ TEST OF HEAP SORT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Size 100 Worst Case Mean of Heap Sort:57165.5 ns

Size 1000 Worst Case Mean of Heap Sort: 625936.2 ns

Size 5000 Worst Case Mean of Heap Sort: 1473264.7 ns

Size 10000 Worst Case Mean of Heap Sort: 2079092.4 ns

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
