def get_char(ch, count):
    """Simulates add to character in C."""
    return chr(ord(ch) + count)

def check_string_pattern(string, data):
    """Return a pattern with respect to given data."""
    
    if not string or not data: # If one of them is empty, return None.
        return None
    
    # Create a map using information given in data.
    data_map = {item.lower():'' for item in data}
    
    # Get the pattern using recursive helper function.
    return check_string_pattern_helper(string.lower(), data_map, "", "", 0)

def check_string_pattern_helper(string, data_map, curr, the_pattern, count_alpha):
    """The recursive helper function."""
    if not string:  # If string is empty. 
        # If string is empty but curr not empty, return False because
        # there is a character in which is not given data,
        return the_pattern if not curr else False 

    # Ignore space.
    if string[0] != '':
        curr += string[0]
        if curr in data_map:  # If curr is in given data
            # Look at whether it holds letter.
            value = data_map.get(curr)
            if value != "":  # Yes, it holds!
                the_pattern += value
            else: # No, It doesn't hold.
                calc = get_char("A", count_alpha) # Calculate the letter.
                data_map[curr] = calc # Add to data map.
                the_pattern += calc # Add to pattern.
                count_alpha += 1 # Increment alphabet counter by one.
            curr = "" 
            
    # Call this function beginning with string[1:].
    return check_string_pattern_helper(string[1:], data_map, curr, the_pattern,
                                       count_alpha)

if __name__ == "__main__":
    data = ["to", "be", "or", "not"]
    string = "Tobeornottobe"
    pattern = "ABCDAB"
    result = check_string_pattern(string, data)
    print("Result Pattern: {}, Given Pattern: {}, STATE: {}".format(result,
          pattern, pattern == result))

