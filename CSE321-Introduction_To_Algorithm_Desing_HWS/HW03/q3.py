def check_index_and_value(array):
    """Check indexes and the values at those indexes to figure out that there is the same.\
(index == arr[index])"""
    
    if not array:  # Check whether list is empty or not. If it is empty return None.
        return None
    
    # Call helper recursive function.
    return check_index_and_value_helper(array, 0, len(array)-1) 

def check_index_and_value_helper(array, begin, end):
    """Helper function. Returns one of indexes if found, otherwise False."""
    if begin == end: # Indicates that there is one element in the list.
        return begin if array[begin] == begin else False # Control for one element.

    # Calculate the middle.
    mid_value = (begin + end) // 2 
    
    ################## DIVIDE SECTION ##################
    # Control the right side of the list.
    right_side = check_index_and_value_helper(array, 0, mid_value)
    
    # Control the left side of the list.
    left_side = check_index_and_value_helper(array, mid_value+1, end)

    ################## COMBINE SECTION ##################
    # Combine the result of right side and left side.
    # Return one of indexes if found otherwise False.
    return left_side or right_side

if __name__ == "__main__":
    data = [-25, -18, -12, 3, 6, 21, 432]
    print("Input data:", data)
    print("Result:", check_index_and_value(data))
