from collections import deque
import xlrd

class BFS_and_DFS:
    """This class is for Breadth First Search and Depth First Search Algorithms."""
 
    def __init__(self, filename):
        """Initialize BFS_and_DFS instance."""
        self.__read_graph_data(filename)

    def __read_graph_data(self, filename):
        """Read the given file to get the edges and vertices of on the graph."""
        
        # Open the XLS file and get the first sheet.
        sheet = xlrd.open_workbook(filename).sheet_by_index(0)
        
        # Assign the function reference to variable.
        read = sheet.cell_value
        
        # Read the root vertex.
        self._root_vertex = int(read(0, 1))
        
        # For edges on the graph
        self._edges = {} 
       
        # Total vertice count
        self._vertice_count = -1
        
        # Read the edges to '_edges dictionary' and calculate the vertice count. 
        for row in range(3, sheet.nrows):
            fr, to = int(read(row, 0)), int(read(row, 1))
            self._vertice_count = max(fr, to, self._vertice_count) # Calculate max vertice count.
            if self._edges.get(fr) == None: 
                self._edges[fr] = [to]
            else:
                self._edges[fr].append(to) # Append adjacent vertex to current.

    def breadth_first_search(self, begin=False):
        """Implementation of decrease and conquer BFS algorithm"""
        
        if not begin: # Check a particular begin vertex whether is specified or not.
            begin = self._root_vertex
        else: 
            if begin <= 0 or begin > 10: # For invalid input
                raise Exception("***** Invalid vertex for beginning. *****")

        # For visited vertex to not visit once more.
        is_visited = [False] * (self._vertice_count + 1)
       
        # For queue data structure
        q = deque()
        
        # The list keeping records of all ordered visited vertices. 
        visit_order = []
        q.append(begin)
        
        # The beginning vertex is already visited.
        is_visited[begin] = True
        
        while len(q) != 0:
            # Get the current vertex from Q.
            current = q.popleft()

            # Add it to visit order list.
            visit_order.append(current)
            
            # Get the adjacent vertices to current, if there is no, return None.
            adjacent_vertices = self._edges.get(current)
            
            # If there are adjacent vertices to current vertex, visit them.
            if adjacent_vertices != None:
                for vertex in adjacent_vertices:
                    # If the vertex is not visited, visit it and add to queue and visit_order list.
                    if not is_visited[vertex]:
                        q.append(vertex)
                        is_visited[vertex] = True
        return visit_order

    def depth_first_search(self, begin=False):
        """Implementation of decrease and conquer DFS algorithm"""
        
        if not begin: # Check a particular begin vertex whether is specified or not.
            begin = self._root_vertex
        else:
            if begin <= 0 or begin > 10: # For invalid input
                raise Exception("***** Invalid vertex for beginning. *****")

        # For visited vertex to not visit once more.
        is_visited = [False] * (self._vertice_count + 1)
        
        # The list keeping records of all ordered visited vertices. 
        visit_order = []
        
        # The beginning vertex is already visited.
        is_visited[begin] = True
        
        # Add it to visit order list.
        visit_order.append(begin)
        
        # Call the helper recursive function to get the visit order.
        return self.__depth_first_search_helper(begin, is_visited, 
                                                visit_order)

    def __depth_first_search_helper(self, begin, is_visited, visit_order):
        """Recursive helper function for Depth First Search"""
        
         # Get the adjacent vertices to current, if there is no, return None.
        adjacent_vertices = self._edges.get(begin)
        
        # If there are adjacent vertices to current vertex, visit them.
        if adjacent_vertices != None:
            for vertex in adjacent_vertices:
                # If the vertex is not visited, visit it and add to queue and visit_order list.
                if not is_visited[vertex]:
                    visit_order.append(vertex)
                    is_visited[vertex] = True
                    
                    # Add the current vertex to implicit stack, search for adjacent vertex recursively.
                    self.__depth_first_search_helper(vertex, is_visited, visit_order)
        return visit_order

def print_visit_order(visit_order):
    """Print the visit order on the screen."""

    for vertex in visit_order[:-1]:
        print("{} -> ".format(vertex), end="")
    print(visit_order[-1])

if __name__ == "__main__":
    search_algorithms = BFS_and_DFS("Graph_data.XLS")

    print("Breadth First Search visit order: ", end="")
    print_visit_order(search_algorithms.breadth_first_search())

    print("Depth First Search visit order: ", end="")
    print_visit_order(search_algorithms.depth_first_search())

