import random

def determine_the_winner(n, m):
    """Simulate One-Pile NIM game and calculates the winner.\
The first player will win the game with given constraint"""
    
    if n % (m+1) == 0: # Check constraint.
        print("n mod(m+1) cannot equal to 0.")
    elif m > n:
        print("m cannot be greater than n.")
    else:
        print("The chips count: {}, at most: {}".format(n, m))
        print("-" * 54)
        first_player = False
        
        # Iterates till the end of game.
        while n != 0:
            # Change turn.
            first_player = not first_player
            
            # This is the key place for algorithm. 
            # First player must take n % (m + 1) chips to win the game.
            # The last turn will remain n <= m chips and it is taken from
            # first player.
            move = n % (m + 1) if n != m else n
            
            # For second player, move is always 0 and it is generated 
            # randomly
            move = move if move != 0 else random.randint(1, m)
            n -= move
          
            # Print in each iteration on the screen.
            print("{} took {} chips. Remaining chips: {} - {} = {}".format(
                  "First Player" if first_player else "Second Player", move,
                  move+n, move, n))

        print("=============== {} has won! ===============".format(
              "First Player" if first_player else "Second Player"))
        print("*" * 54)


if __name__ == "__main__":
    for i in range(20):
        n, m = random.randint(1, 100), random.randint(1, 28)
        print("Round {}.".format(i))
        determine_the_winner(n, m)
        print()


