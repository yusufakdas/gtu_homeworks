def get_largest_sum_of_contiguous_subset(array):
    """This function finds a contiguous subset having the largest sum within given list."""
    if not array: # Check whether list is empty or not. If it is empty return None.
        return None
    
    # The helper recursive function returns the lower and higher indexes of the subset.
    low, high = get_largest_sum_of_contiguous_subset_helper(array, 0, len(array)-1)
    
    # Slices the list to get subset. 
    return array[low:high+1]

def get_largest_sum_of_contiguous_subset_helper(array, low, high):
    """Helper recursive function. Return the boundary of subset as low and high indexes."""
   
    if low == high: # Check whether the list have one item.
        return (low, high)

    # Calculate the middle.
    mid = (low + high) // 2
    
    ################## DIVIDE PART ##################
    # Get the left largest sum of contiguous subset recursively.
    left_largest = get_largest_sum_of_contiguous_subset_helper(array, low, mid)
    
    # Get the right largest sum of contiguous subset recursively.
    right_largest = get_largest_sum_of_contiguous_subset_helper(array, mid+1, high)

    ################## COMBINE PART ##################
    # Combine left and right subset.
    return combine_left_and_right(array, left_largest, right_largest)

def combine_left_and_right(array, left, right):
    """Merges the left and right subsets."""
    
    # Calculate the sum of left part.
    left_sum = sum(array[left[0]:left[1]+1])
    
    # Calculate the sum of right part.
    right_sum = sum(array[right[0]:right[1]+1])
    
    # If left and right subset are contiguous.
    if (right[0] - left[1]) == 1:
        
        # Calculate the both subset sum.
        left_right_sum = left_sum + right_sum
        
        # If it is greater or equal than right and left subset sum, 
        # return low from left subset, high from right subset.
        if right_sum <= left_right_sum and left_sum <= left_right_sum:
            return (left[0], right[1])
    else: # Not contiguous.
        
        # Calculate the sum of range.
        range_sum = sum(array[left[0]:right[1]+1])
        
        # If range sum is greater than left and right subset sum, include items 
        # between two subset and return low and high indexex.
        if right_sum <= range_sum and left_sum <= range_sum:
            return (left[0], right[1])
    
    # Otherwise, return the subset that has largest sum.
    return right if left_sum < right_sum else left

if __name__ == "__main__":
    data = [5, -6, 6, 7, -6, 7, -4, 3]
    print("Data:", data)
    print("The largest sum of contiguous set is",
           get_largest_sum_of_contiguous_subset(data))

