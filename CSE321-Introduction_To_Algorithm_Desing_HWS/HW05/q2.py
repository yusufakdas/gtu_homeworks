# Question 2 - part 2
# Yusuf AKDAS - 151044043

def get_optimal_plan(NY, SF, moving_cost):
    """Compute the optimal plan for the consulting business.
NY parameter: operating costs of NY.
SF parameter: operating costs of SF.
moving_cost parameter: cost of switching cities."""
    
    # If the month count of NY is not equal to month count of SF...
    if len(NY) != len(SF) or len(NY) == 0:
        return None

    # Get the month.
    month = len(NY)

    # Create tables for bottom-up solution of the problem.
    SF_table = [0] * month
    NY_table = [0] * month

    # The optimal sequence of locations
    sequence = [None] * month
    
    # Base cases for solving problem using Dynamic Programming method
    SF_table[0], NY_table[0] = SF[0], NY[0]
    sequence[0] = "NY" if NY_table[0] < SF_table[0] else "SF"

    # Compute the optimal sequence and cost.
    for i in range(1, month):
        SF_table[i] = SF[i] + min(SF_table[i-1], moving_cost + NY_table[i-1])
        NY_table[i] = NY[i] + min(NY_table[i-1], moving_cost + SF_table[i-1])
        sequence[i] = "NY" if NY_table[i] < SF_table[i] else "SF"

    # Return the optimal sequence and the cost in tuple.
    return (sequence, min(SF_table[month-1], NY_table[month-1]))

def main():
    NY = [1, 3, 20, 30]
    SF = [50, 20, 2, 4]

    optimal = get_optimal_plan(NY, SF, 10)
    print("The optimal cost: {}".format(optimal[1]))
    print("The optimal sequence: {}".format(optimal[0]))

    print()

    NY = [1, 8, 2, 5]
    SF = [4, 1, 3, 2]

    optimal = get_optimal_plan(NY, SF, 10)
    print("The optimal cost: {}".format(optimal[1]))
    print("The optimal sequence: {}".format(optimal[0]))

if __name__ == "__main__":
    main()

