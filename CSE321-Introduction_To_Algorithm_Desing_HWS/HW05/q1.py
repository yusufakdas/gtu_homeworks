# Question 1
# Yusuf AKDAS - 151044043

def make_customers_happy(job_list):
    """Minimize the weighted sum of the completion times to make customers happy.
job_list parameter: The list of tuple that includes time for completion and \
the weight of the job."""

    # Add the order number to each one.
    ordered = [(p[0], p[1], order+1) for order, p in enumerate(job_list)]

    # Sort the list by t / w for getting time in unit weight.
    sorted_by_formula = sorted(ordered, key=lambda x: x[0]/x[1])

    # Initialization of some variables
    weighted_sum, total_time, the_sequence = 0, 0, []

    # Calculate the weighted sum and add the order of processing for minimization.
    for time, weight, order in sorted_by_formula:
        total_time += time
        weighted_sum += weight * total_time
        the_sequence.append(order)

    # Return the order of processing jobs and weighted sum in tuple.
    return (the_sequence, weighted_sum)

def main():

    # The first value of tuple: TIME
    # The second value of tuple: WEIGHT
    first_list = [(3, 2), (1, 10)]
    second_list = [(12, 1), (12, 20), (5, 12), (6, 43), (8, 3), (7, 23)]

    first = make_customers_happy(first_list)
    print("The order of processing: {}".format(first[0]))
    print("The weighted sum: {}".format(first[1]))

    print("*" * 50)

    second = make_customers_happy(second_list)
    print("The order of processing: {}".format(second[0]))
    print("The weighted sum: {}".format(second[1]))
    print("*" * 50)

if __name__ == "__main__":
    main()

