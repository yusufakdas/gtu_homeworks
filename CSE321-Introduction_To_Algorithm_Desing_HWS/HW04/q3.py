# Question 3
# Yusuf AKDAS - 151044043 

def combine_k_lists(_2d_list):
    """Combine k sorted list that has lenght of n."""

    if len(_2d_list) == 0: 
        return None
    if len(_2d_list) == 1: 
        return _2d_list[0]

    mid = len(_2d_list) // 2

    # Combine first half
    first_half_combine = combine_k_lists(_2d_list[:mid])

    # Combine second half
    second_half_combine = combine_k_lists(_2d_list[mid:])

    # Combine first and second halves
    return combine_operation(first_half_combine, second_half_combine)

def combine_operation(first_lst, second_lst):
    """This function resembles Merge Sort's merge function"""

    if second_lst == None: 
        return first_lst

    fp = sp = ptr = 0
    flen, slen = len(first_lst), len(second_lst)

    # Result list
    result_lst = [None] * (flen + slen)

    # Merge until one of them exhausts.
    while fp < flen and sp < slen:
        if first_lst[fp] < second_lst[sp]:
            result_lst[ptr] = first_lst[fp]
            fp += 1
        else:
            result_lst[ptr] = second_lst[sp]
            sp += 1
        ptr += 1

    # Remaining of first half
    while fp < flen:
        result_lst[ptr] = first_lst[fp]
        ptr += 1
        fp += 1

    # Remaining of second half
    while sp < slen:
        result_lst[ptr] = second_lst[sp]
        ptr += 1
        sp += 1

    return result_lst 

if __name__ == "__main__":
    _2d_list =[[1, 2, 8, 12], 
               [3, 5, 10, 11], 
               [4, 6, 7, 16], 
               [9, 13, 14, 15]]

    print(combine_k_lists(_2d_list))
