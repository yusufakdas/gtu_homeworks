# Question 2
# Yusuf AKDAS - 151044043

def check_whether_valid(sentence, dictionary):
    """Check whether the sentence can be reconstituted."""

    # For memoization
    memo = [False] * len(sentence)

    # Getting the base case for the dynamic programming solution
    for i in range(1, len(sentence)):
        if sentence[:i+1] in dictionary:
            memo[i] = True
            break

    # Check all substring and record all word's beginning index to memo.
    for i in range(len(sentence)):
        for j in range(i):
            # If the first half is valid and the substring is in dictionary,
            # mark the ith index of the sentence.
            if memo[j] and sentence[j+1:i+1] in dictionary:
                memo[i] = True

    return memo[len(sentence) - 1]

if __name__ == "__main__":
    sentence = "itwasthebestoftimes"
    dictionary = {'it', 'was', 'the', 'best', 'of', 'times'}

    result = check_whether_valid(sentence, dictionary)
    print("The sentence:", sentence)
    print("The dictionary:", dictionary)
    print("The result:", result)

    print('-' * 30)

    dictionary = {'the', 'power', 'of', 'imagination', 'makes', 'us', 'infinite'}
    sentence = "thepowerofimaginationmakesusinfinite"

    result = check_whether_valid(sentence, dictionary)
    print("The sentence:", sentence)
    print("The dictionary:", dictionary)
    print("The result:", result)

    print('-' * 30)
