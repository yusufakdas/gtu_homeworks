# Question 1
# Yusuf AKDAS - 151044043

def get_optimal_sequence(distances):
    """Calculates the optimal path by minimizing the penalty using DP."""

    # For memoization
    memo = [None] * len(distances)

    # The optimal path
    optimal_seq = [None] * len(distances)

    # Function for calculating penalty with respect to x and y values
    calc_penalty = lambda x, y: (200 - (x - y)) ** 2

    # Base case for dynamic programming solution of this problem
    memo[0] = 0

    # Take into account every possibility and choose the optimal one.
    for i in range(len(distances)):
        for j in range(i):
            res = memo[j] + calc_penalty(distances[i], distances[j])
            # Choose the minimum penalty.
            if not memo[i] or res < memo[i]:
                memo[i] = res               # Record the minimum penalty.
                optimal_seq[i] = j + 1      # Record the hotel number.

    # Return the optimal after arranging it.
    return (arrange_the_path(optimal_seq, 0, []), memo[len(distances) - 1])

def arrange_the_path(path, index, curr):
    """Edit the calculated path and add last stop the path"""

    # If path length zero, this means that we are already at the last stop.
    if index >= len(path):
        curr.append(len(path))  # Append the last stop.
        return curr

    # Add the distinct hotel numbers.
    if path[index] and path[index-1] != path[index]:
        curr.append(path[index])

    return arrange_the_path(path, index + 1, curr)

if __name__ == "__main__":
    A = [190, 220, 410, 580, 640, 770, 950, 1100, 1350]
    result = get_optimal_sequence(A)
    print("The path:", result[0])
    print("The penalty:", result[1])
