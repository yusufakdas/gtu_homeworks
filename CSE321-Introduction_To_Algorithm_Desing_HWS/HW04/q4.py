# Question 4
# Yusuf AKDAS - 151044043

import copy 

def choose_invitees(people_list, pairs, bound = 5):
    """Choose invitees with respect to constraints.
people parameter: the list of people
pairs parameter: the list of pairs that represents people who know each other.
bound parameter: it is a parameter to test algorithm easily. Its default value
is 5. Preparing the input list for 5 is really time-consuming so I change it
when testing the algorithm. If you not specify bound parameter explicitly,
it will be 5 as default value."""

    # Copy the people list.
    people = people_list.copy()

    # Calculates each person's friends count
    occurences = {person : 0 for person in people}
    for person in people:
        for pair in pairs:
            occurences[person] += person in pair

    state = False
    while not state and len(person) > 0:
        state = True
        for person in people:

            # If the person does not satisfy constraints,
            # remove the it from the pairs and people lists.
            if (occurences[person] < bound
                    or occurences[person] > len(people) - bound):

                # Remove from pairs data and decrease the occurences.
                for item in pairs:
                    if person in item:
                        occurences[item[0]] -= 1
                        occurences[item[1]] -= 1
                        pairs.remove(item)

                # Set the state false
                state = False

                # Remove the person from people list.
                people.remove(person)
    return (people, occurences)

if __name__ == "__main__":
    people = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M']
    pairs = [['A', 'B'], ['A', 'C'], ['A', 'D'], ['A', 'F'], ['A', 'G'],
             ['B', 'D'], ['B', 'G'], ['B', 'H'], ['B', 'I'],
             ['C', 'E'], ['C', 'H'], 
             ['M', 'J'], ['M', 'L'], ['M', 'G'], ['M', 'D']]
    result = choose_invitees(people, pairs, 2)
    print("People list:", people)
    print("Invitees according to 2:", result[0])
    print("Each person's invited friends count in the party:", result[1])

