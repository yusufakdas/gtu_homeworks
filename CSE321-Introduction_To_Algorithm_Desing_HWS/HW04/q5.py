# Question 5
# Yusuf AKDAS - 151044043

def check_constraints(constraints, var_count):
    """Check whether the constraints is satisfied.
constraints parameter: takes list of string which defines constraint.
var_count parameter: takes the counts of distinct varibles"""

    # For holding vertices on the graph
    vertices = set()

    # Edges between vertices
    edges = {}

    # Function for parsing constraint strings.
    func = lambda item, symbol: list(map(str.strip, item.split(symbol)))

    # Connect equality constraints.
    for item in constraints:
        if "!" not in item:
            data = func(item, "=")

            # It must be connected both way for undirected graph.
            connect_an_vertices(edges, data[0], data[1])
            connect_an_vertices(edges, data[1], data[0])

            # Add to vertices set.
            vertices.add(data[0])
            vertices.add(data[1])

    # Check whether variable of inequal constraints are connected. If they are,
    # return False
    for item in constraints:
        if "!" in item:
            data = func(item, "!=") # for inequality
            if (data[0] in vertices and data[1] in vertices
                    and DFS(edges, data[0], data[1])):
                return False
    return True

def connect_an_vertices(edges, first, second):
    """Connect vertices one way"""

    if not edges.get(first):
        edges[first] = [second]
    else:
        edges[first].append(second)

def DFS(edges, begin, control):
    """Iterative implementation of Depth First Search Algorithm.\
    It is used for the check connectivity of vertices."""

    # A dictionary to keep record of visited vertices
    visited = {vertex: False for vertex in edges}

    # For holding unfinished vertices
    stack = []

    stack.append(begin)
    while len(stack):
        top = stack.pop()

        # If it is not visited, then visit.
        if not visited[top]:
            visited[top] = True

        # Visit the adjacent vertices
        for vertex in edges[top]:
            if not visited[vertex]:
                stack.append(vertex)

    # Check whether the inequal vertex is visited.
    return visited[control]

if __name__ == "__main__":
    constraints = ["x1 = x2", "x2 = x3", "x3 = x4", "x1 != x4"]
    print("Constraints:", constraints)
    print("Result:", check_constraints(constraints, len(constraints)))

    print("-" * 30)

    constraints = ["x1 = x2", "x2 != x3", "x3 != x4", "x1 = x4"]
    print("Constraints:", constraints)
    print("Result:", check_constraints(constraints, len(constraints)))

    print("-" * 30)
